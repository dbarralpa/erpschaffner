package com.schaffner.infraestructure.data.repositories;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.schaffner.infraestructure.data.repositories.contracts.IIngenieriaDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Bobina;
import com.schaffner.modelo.ingenieria.Derivacion;
import com.schaffner.modelo.ingenieria.Diseno;
import com.schaffner.modelo.ingenieria.Distribucion;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;
import com.schaffner.modelo.ingenieria.Especificacion;
import com.schaffner.modelo.ingenieria.EstanqueTapa;
import com.schaffner.modelo.ingenieria.ItemCubicacion;
import com.schaffner.modelo.ingenieria.NucleoFerreteria;
import com.schaffner.modelo.ingenieria.Presspan;
import com.schaffner.modelo.ingenieria.Radiador;
import com.schaffner.modelo.ingenieria.Refrigerante;
import com.schaffner.modelo.ingenieria.TipoAislador;
import com.schaffner.modelo.ingenieria.Tolerancia;

public class IngenieriaDAO implements IIngenieriaDAO {
	private SqlSession sqlSession;

	public IngenieriaDAO() {

	}

	public void setSqlSession(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	/* CUBICA */
	public int insertSeco(List<Accesorio> accesorios, Diseno diseno,
			Especificacion especificacion, NucleoFerreteria nucFerr,
			EstanqueTapa estTapa, Bobina bobinabbt, Bobina bobinabat,
			Bobina bobinabte, Presspan presspanBbt, Presspan presspanBat,
			Presspan presspanBte) throws Exception {
		int i = 0;
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertDiseno", diseno);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertEspecificaciones",
				especificacion);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertBobina", bobinabbt);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertBobina", bobinabat);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertBobina", bobinabte);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPresspan",
				presspanBbt);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPresspan",
				presspanBat);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPresspan",
				presspanBte);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertNucFerr", nucFerr);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertEstanqueTapa",
				estTapa);
		if (!accesorios.isEmpty()) {
			for (int a = 0; a < accesorios.size(); a++) {
				i = sqlSession.insert(
						"com.schaffner.persistencia.mappers.insertAccesorio",
						accesorios.get(a));
			}
		}
		return i;

	}

	public int insertAceite(List<Accesorio> accesorios, Diseno diseno,
			Especificacion especificacion, NucleoFerreteria nucFerr,
			EstanqueTapa estTapa, Bobina bobinabbt, Bobina bobinabat,
			Bobina bobinabte, Presspan presspanBbt, Presspan presspanBat,
			Presspan presspanBte, Radiador rad) throws Exception {
		int i = 0;
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertDiseno", diseno);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertEspecificaciones",
				especificacion);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertBobina", bobinabbt);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertBobina", bobinabat);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertBobina", bobinabte);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPresspan",
				presspanBbt);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPresspan",
				presspanBat);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPresspan",
				presspanBte);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertNucFerr", nucFerr);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertEstanqueTapa",
				estTapa);
		i = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertRadiador", rad);
		if (!accesorios.isEmpty()) {
			for (int a = 0; a < accesorios.size(); a++) {
				i = sqlSession.insert(
						"com.schaffner.persistencia.mappers.insertAccesorio",
						accesorios.get(a));
			}
		}
		return i;

	}

	public int insertAccesorio(Accesorio accesorio) throws Exception {
		return sqlSession
				.insert("com.schaffner.persistencia.mappers.insertAccesorio",
						accesorio);

	}

	public int actualizarAccesorios(Accesorio accesorio) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.updateAccesorios",
				accesorio);

	}

	public int updateSeco(Diseno diseno, Especificacion especificacion,
			NucleoFerreteria nucFerr, EstanqueTapa estTapa, Bobina bobinabbt,
			Bobina bobinabat, Bobina bobinabte, Presspan presspanBbt,
			Presspan presspanBat, Presspan presspanBte) throws Exception {
		int i = 0;
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateDiseno", diseno);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateEspecificacion",
				especificacion);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateBobina", bobinabbt);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateBobina", bobinabat);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateBobina", bobinabte);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateEstanqueTapa",
				estTapa);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updatePresspan",
				presspanBbt);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updatePresspan",
				presspanBat);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updatePresspan",
				presspanBte);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateNucFerr", nucFerr);
		return i;
	}

	public int updateAceite(Diseno diseno, Especificacion especificacion,
			NucleoFerreteria nucFerr, EstanqueTapa estTapa, Bobina bobinabbt,
			Bobina bobinabat, Bobina bobinabte, Presspan presspanBbt,
			Presspan presspanBat, Presspan presspanBte, Radiador rad)
			throws Exception {
		int i = 0;
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateDiseno", diseno);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateEspecificacion",
				especificacion);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateBobina", bobinabbt);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateBobina", bobinabat);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateBobina", bobinabte);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateEstanqueTapa",
				estTapa);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updatePresspan",
				presspanBbt);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updatePresspan",
				presspanBat);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updatePresspan",
				presspanBte);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateNucFerr", nucFerr);
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateRadiador", rad);
		return i;
	}

	/*
	 * public int actualizarBobinaXdisenoXbobina(Bobina bob) throws Exception {
	 * return sqlSession
	 * .update("com.schaffner.persistencia.mappers.updateBobinaXdisenoXbobina",
	 * bob);
	 * 
	 * }
	 * 
	 * public int actualizarDisenoXdiseno(Diseno diseno) throws Exception {
	 * return sqlSession.update(
	 * "com.schaffner.persistencia.mappers.updateDisenoXdiseno", diseno);
	 * 
	 * }
	 */
	public int actualizarPlano(CargarArchivo arch) throws Exception {
		return sqlSession.update("com.schaffner.persistencia.mappers.actualizarPlano",arch);

	}
	
	public int deleteAccesorio(Accesorio accesorio) throws Exception {
		return sqlSession.delete(
				"com.schaffner.persistencia.mappers.deleteAccesorios",
				accesorio);

	}

	public Diseno getDiseno(String diseno) throws Exception {
		return (Diseno) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectDiseno", diseno);

	}

	public Bobina getBobina(Auxiliar aux) throws Exception {
		return (Bobina) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectBobinaXdisenoXtipo",
				aux);

	}

	public NucleoFerreteria getNucleoFerreteria(String diseno) throws Exception {
		return (NucleoFerreteria) sqlSession
				.selectOne(
						"com.schaffner.persistencia.mappers.selectNucleoFerreteriaPorDiseno",
						diseno);

	}

	public Especificacion getEspecificacion(String diseno) throws Exception {
		return (Especificacion) sqlSession
				.selectOne(
						"com.schaffner.persistencia.mappers.selectEspecificacionPorDiseno",
						diseno);

	}

	public Presspan getPresspan(Auxiliar aux) throws Exception {
		return (Presspan) sqlSession
				.selectOne(
						"com.schaffner.persistencia.mappers.selectPresspanXDisenoXbobina",
						aux);

	}

	public Tolerancia getTolerancia(int id) throws Exception {
		return (Tolerancia) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectTolerancia", id);

	}

	public ItemCubicacion getItemCubicacion(String item) throws Exception {
		return (ItemCubicacion) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectItemCubicacionXitem",
				item);

	}

	public EstanqueTapa getEstanqueTapa(String diseno) throws Exception {
		return (EstanqueTapa) sqlSession
				.selectOne(
						"com.schaffner.persistencia.mappers.selectEstanqueTapaXDisenoXbobina",
						diseno);

	}

	public Radiador getRadiador(String diseno) throws Exception {
		return (Radiador) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectRadiadorXDiseno",
				diseno);

	}

	public List<Accesorio> getAccesorios(String diseno) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectAccesoriosXdiseno",
				diseno);

	}

	public List<ItemCubicacion> getItemAdicionales(double item)
			throws Exception {
		return (List) sqlSession
				.selectList(
						"com.schaffner.persistencia.mappers.selectItemCubicacionAdicionalXpotencia",
						item);

	}

	public List<Distribucion> getDistribuciones(String tipo) throws Exception {
		return (List) sqlSession
				.selectList(
						"com.schaffner.persistencia.mappers.selectDistribucionXtipoRefrigerante",
						tipo);

	}

	public List<TipoAislador> getTipoAisladores() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTipoAisladorXtipo");

	}

	public List<Refrigerante> getRefrigerantes() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectRefrigeranteXcodigo");

	}

	public List<Derivacion> getDerivaciones() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectDerivacionXid");

	}

	public List<Auxiliar> getMaterialesXclasificacionAceite() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectMaterialXclasificacionAceite");

	}

	public List<Auxiliar> getMaterialesXclasificacionSeco() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectMaterialXclasificacionSeco");

	}

	public List<Auxiliar> getMaterialesPintura() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectMaterialesPintura");

	}

	public List<Auxiliar> getMaterialesPlanchaFierro() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectMaterialPlanchaFierro");

	}

	public List<Auxiliar> getMaterialesPresspan() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectMaterialPresspan");

	}

	public List<Auxiliar> getTiposRadiadores() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTipoRadiador");

	}

	public List<Auxiliar> getMaterialesRadiador(int tipo) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectMaterialesRadiador",
				tipo);

	}

	public List<Auxiliar> getAceroSilicoso() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectAceroSilicoso");

	}

	public List<Auxiliar> getTipoSoporte() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTipoSoporte");

	}

	public List<Auxiliar> getTipoNucleo() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTipoNucleo");

	}

	public List<Auxiliar> getAislaciones() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectAislaciones");

	}

	public List<Auxiliar> getDisenoBusqueda() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectDisenosBusqueda");

	}

	public List<Distribucion> getDistribucionesAll() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectDistribucionAll");

	}

	public List<Auxiliar> getTablaC1() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTablaC1");

	}

	public List<Auxiliar> getTablaC2() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTablaC2");

	}

	public List<Auxiliar> getTablaC3() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTablaC3");

	}

	public List<Auxiliar> getTablaC4() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTablaC4");

	}

	public Integer getMaxCodigoDiseno(String aux) throws Exception {
		return (Integer) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectMaxDiseno", aux);

	}

	public List<Auxiliar> getCodigosPrueba() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectCodigosPrueba");

	}

	/* DOCUMENTOS APLICABLES */
	
	public int eliminarPermiso(Auxiliar aux) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.eliminarPermiso",
				aux);

	}
	
	public int actualizarDocAplicableEnc(DocAplicableEnc enc) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.actualizarDocAplicableEnc",
				enc);

	}
	
	public int actualizarDocAplicableDet(DocAplicableDet det) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.actualizarDocAplicableDet",
				det);

	}
	
	public int actualizarEstadoDocApli(int num) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.actualizarEstadoDocApli",num);

	}
	
	public int actualizarRevisionPlanoRev(Auxiliar aux) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.actualizarRevisionPlanoRev",aux);

	}
	
	public int actualizarRevisionPlanoDet(Auxiliar aux) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.actualizarRevisionPlanoDet",aux);

	}
	
	public int actualizarEstadoDocApliDetRev(DocAplicableDet idDet) throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.actualizarEstadoDocApliDetRev",idDet);

	}
	
	public int insertPermiso(Auxiliar aux) throws Exception {
		return sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertPermiso",
				aux);

	}

	public int insertPlanoDescripcion(Auxiliar aux) throws Exception {
		return sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertDescripcionPlano",
				aux);

	}

	public int insertArchivoDocApli(CargarArchivo car) throws Exception {
		int e = (Integer) sqlSession.selectOne("com.schaffner.persistencia.mappers.selectPlano", car.getName());
		int a = 0;
		if(e == 0){
		a = sqlSession.insert("com.schaffner.persistencia.mappers.insertArchivosDocApli",car);
			if(a > 0){
				 a = sqlSession.insert("com.schaffner.persistencia.mappers.insertPlanosRevision",car);	
			}
		}else{
			a = sqlSession.insert("com.schaffner.persistencia.mappers.insertPlanosRevision",car);
		}
		return a;
	}

	public int insertDocAplicableEnc(DocAplicableEnc enc) throws Exception {
		return sqlSession
				.insert("com.schaffner.persistencia.mappers.insertDocAplicableEnc",
						enc);

	}

	public int insertDocAplicableDet(DocAplicableDet det) throws Exception {
		int a = sqlSession.insert("com.schaffner.persistencia.mappers.insertDocAplicableDet",det);
		if(a > 0){
			a = sqlSession.insert("com.schaffner.persistencia.mappers.insertDocApliDetRev",det);
		}
		return a;
	}
	
	public int insertDocApliDetRev(DocAplicableDet det) throws Exception {
		return sqlSession.insert("com.schaffner.persistencia.mappers.insertDocApliDetRev",det);
	}

	public List<Auxiliar> getPlanoDescripciones() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectDescripcionPlano");
	}

	public Integer maxNumDocAplicable() throws Exception {
		return (Integer) sqlSession
				.selectOne("com.schaffner.persistencia.mappers.selectmaxNumDocAplicable");
	}

	public List<Auxiliar> getIdPlanos(String id) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectIdPlano", id);
	}
	
	public List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectDocAplicableDet", aux);
	}
	
	public DocAplicableEnc getDocAplicableEnc(int num) throws Exception {
		return (DocAplicableEnc) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectDocAplicableEnc", num);
	}
	
	public Integer docAplicableXnum(int num) throws Exception {
		return (Integer) sqlSession
				.selectOne("com.schaffner.persistencia.mappers.selectDocAplicableXnum");
	}
	
	public List<DocAplicableEnc> getDocsAplicablesEnc(Auxiliar aux) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectDocsAplicablesEnc", aux);
	}
	
	public DocAplicableDet getDocApliDet(int num) throws Exception {
		return (DocAplicableDet) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectDocApliDetXid", num);
	}
	
	public int getDocApliDetRevCount(int num) throws Exception {
		return (Integer) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectDocApliDetRevCount", num);
	}
	
	public CargarArchivo docApliDetArchivo(Auxiliar aux) throws Exception {
		return (CargarArchivo) sqlSession.selectOne("com.schaffner.persistencia.mappers.selectDocApliArchivos", aux);
	}
	
	public List<Usuario> geUsuariosXarea(int area) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectUsuarioXarea", area);
	}
	
	public List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectPermisoXareaXusuario", groupPrincipal);
	}
	
	public List<Auxiliar> geSubGroupPrincipal(int subGroupPrincipal) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectSubGroupPrincipalPro",subGroupPrincipal);
	}
	
	public Integer getPlanoRevision(String idPlano) throws Exception {
		return (Integer) sqlSession.selectOne("com.schaffner.persistencia.mappers.selectLastPlanoRev", idPlano);
	}
	
	public Integer getLastPlanoRevision(String idPlano) throws Exception {
		return (Integer) sqlSession.selectOne("com.schaffner.persistencia.mappers.selectLastPlanoRev", idPlano);
	}
	
	public List<Auxiliar> getEquiposVigentes() throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectEquiposVigentes");
	}
	
	public List<Auxiliar> getEquiposModificarRev(String idPlano) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectEquiposModificarRev", idPlano);
	}
	
	public List<Auxiliar> getEquiposVigentesTpmp() throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectEquiposVigentesTpmp");
	}
	
	public Integer getLastDocApliDet() throws Exception {
		return (Integer) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectmaxNumDocAplicableDet");
	}
	
	public int getMaxPlanoRevision(String idPlano) throws Exception {
		return (Integer) sqlSession.selectOne(
				"com.schaffner.persistencia.mappers.selectMaxPlanoRevision", idPlano);
	}
	
	public List<Auxiliar> getDetPlanoActualizar(String idPlano) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectDetPlanoActualizar", idPlano);
	}
	
	public List<Auxiliar> getEnviarMailCambioPlano(int idPlano) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectEnviarMailCambioPlano", idPlano);
	}
	
	public List<Auxiliar> getEnviarMailRevision(int idPlano) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectEnviarMailRevision", idPlano);
	}
}
