package com.schaffner.infraestructure.data.repositories.contracts;

import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.DocAplicableDet;


public interface IProduccionDAO {
	int eliminarPermiso(Auxiliar aux) throws Exception;
	int eliminarPermisoSubArea(Auxiliar aux) throws Exception;
	int insertPermiso(Auxiliar aux) throws Exception;
	int insertPermisoSubArea(Auxiliar aux) throws Exception;
	int insertImpresionUsuario(Auxiliar aux) throws Exception;
	int insertValPerUsuario(Auxiliar aux) throws Exception;
	List<Auxiliar> geSubGroupPrincipal(int subGroupPrincipal) throws Exception;
	List<Usuario> geUsuariosXarea(int area) throws Exception;
	List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception;
	List<Auxiliar> gePermisosSubArea(int groupPrincipal) throws Exception;
	List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception;
	String puedeImprimir(Auxiliar idPlano) throws Exception;
}

