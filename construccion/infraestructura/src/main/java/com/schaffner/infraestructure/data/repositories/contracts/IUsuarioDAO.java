package com.schaffner.infraestructure.data.repositories.contracts;

import com.schaffner.modelo.Usuario;

public interface IUsuarioDAO {
	Usuario getUsuario(String userName) throws Exception;
}