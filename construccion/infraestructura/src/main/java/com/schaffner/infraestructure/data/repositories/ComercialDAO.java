package com.schaffner.infraestructure.data.repositories;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.transaction.annotation.Transactional;

import com.schaffner.infraestructure.data.repositories.contracts.IComercialDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.ProyectoComercialTransformador;
import com.schaffner.modelo.ProyectoComercialVentaDet;
import com.schaffner.modelo.ProyectoComercialVentaEnc;

public class ComercialDAO implements IComercialDAO {
	private SqlSession sqlSession;

	// private Logging logging;

	public ComercialDAO() {

	}

	public void setSqlSession(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	/*
	 * public void setLogging(Logging logging) { this.logging = logging; }
	 */

	public int updateProyectoVenta(ProyectoComercialVentaEnc pro)
			throws Exception {
		return sqlSession.insert(
				"com.schaffner.persistencia.mappers.updateCotizacion", pro);
	}

	public int insertProyectoVenta(ProyectoComercialVentaEnc pro)
			throws Exception {
		return sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertProyectoVenta", pro);
	}

	public int insertProyectoVentaTrafos(ProyectoComercialTransformador pro)
			throws Exception {
		return sqlSession
				.insert("com.schaffner.persistencia.mappers.insertProyectoTransformadores",
						pro);
	}

	public int insertProyectoVentaDet(ProyectoComercialVentaDet pro)
			throws Exception {
		return sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertProyectoVentaDet",
				pro);
	}

	public int insertProgresoProyecto(Auxiliar pro) throws Exception {
		return sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertProgresoProyecto",
				pro);
	}

	public int insertProyectoEncDetTrafo(ProyectoComercialVentaEnc enc,
			ProyectoComercialVentaDet det, ProyectoComercialTransformador trafo)
			throws Exception {
		int num = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertProyectoVenta", enc);
		if (num > 0) {
			num = sqlSession
					.insert("com.schaffner.persistencia.mappers.insertProyectoVentaDet",
							det);
			if (num > 0) {
				num = sqlSession
						.insert("com.schaffner.persistencia.mappers.insertProyectoTransformadores",
								trafo);
			}
		}

		return num;
	}

	public int insertProyectoDetTrafo(ProyectoComercialVentaDet det,
			ProyectoComercialTransformador trafo) throws Exception {

		int num = sqlSession.insert(
				"com.schaffner.persistencia.mappers.insertProyectoVentaDet",
				det);
		if (num > 0) {
			num = sqlSession
					.insert("com.schaffner.persistencia.mappers.insertProyectoTransformadores",
							trafo);
		}

		return num;
	}

	public int updateProyectoVentaDet(ProyectoComercialVentaDet pro)
			throws Exception {
		return sqlSession.update(
				"com.schaffner.persistencia.mappers.updateProyectoVentaDet",
				pro);
	}

	public int updateProyectoVentaEncDatosGenerales(
			ProyectoComercialVentaEnc pro) throws Exception {
		return sqlSession
				.update("com.schaffner.persistencia.mappers.updateProyectoVentaEncDatosGenerales",
						pro);
	}

	public int updateProyectoVentaDetTrafo(ProyectoComercialTransformador tra,
			ProyectoComercialVentaDet det) throws Exception {
		int i = 0;
		i = sqlSession.update(
				"com.schaffner.persistencia.mappers.updateProyectoTrafo", tra);
		if (i > 0) {
			i = sqlSession
					.update("com.schaffner.persistencia.mappers.updateProyectoVentaDetFew",
							det);
		}
		return i;
	}

	public int deleteProyectoVentaDet(int idProyecto) throws Exception {
		return sqlSession.delete(
				"com.schaffner.persistencia.mappers.deleteProyectoVentaDet",
				idProyecto);
	}

	public int deleteProyectoVentaTrafo(int idProyectoDet) throws Exception {
		return sqlSession.delete(
				"com.schaffner.persistencia.mappers.deleteProyectoVentaTrafo",
				idProyectoDet);
	}

	public int deleteProyectoDetTrafo(int idProDet) throws Exception {
		int i = 0;
		i = deleteProyectoVentaTrafo(idProDet);
		if (i > 0) {
			i = deleteProyectoVentaDet(idProDet);
		}
		return i;
	}

	public int insertProyectoVentaArchivos(CargarArchivo arch) throws Exception {
		return sqlSession
				.insert("com.schaffner.persistencia.mappers.insertProyectoVentaArchivos",
						arch);
	}

	public Integer ultimoNumeroProyectoVenta() throws Exception {
		return (Integer) sqlSession
				.selectOne("com.schaffner.persistencia.mappers.selectUltimoNumeroProyecto");
	}

	public Integer ultimoNumeroProyectoVentaDet() throws Exception {
		return (Integer) sqlSession
				.selectOne("com.schaffner.persistencia.mappers.selectUltimoNumeroProyectoDet");
	}

	public List<ProyectoComercialVentaEnc> proyectosVentas() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectProyectosVentas");
	}

	public List<Auxiliar> tipoCotizacion() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTipoCotizacion");
	}

	public List<Auxiliar> tipoMoneda() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTipoMoneda");
	}

	public List<Auxiliar> formaPago() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectFormaPago");
	}

	public List<Auxiliar> estadoCotizacion() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectEstadoCotizacion");
	}

	public List<Auxiliar> parametros() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectParametros");
	}

	public List<Auxiliar> margenes() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectMargenes");
	}

	public List<ProyectoComercialVentaDet> proyectosVentasDet(int id)
			throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectProyectosVentasDet",
				id);
	}

	public List<ProyectoComercialTransformador> proyectosVentasTrafos(int id)
			throws Exception {
		return (List) sqlSession
				.selectList(
						"com.schaffner.persistencia.mappers.selectProyectosVentasTrafos",
						id);
	}

	public List<CargarArchivo> proyectosVentasArchivos(int id) throws Exception {
		return (List) sqlSession
				.selectList(
						"com.schaffner.persistencia.mappers.selectProyectoVentasArchivos",
						id);
	}

	public List<Auxiliar> normas() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectNormas");
	}

	public List<Auxiliar> temPerdidas() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTemPerdidas");
	}

	public List<Auxiliar> ventilacion() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectVentilacion");
	}

	public List<Auxiliar> temperaturas() throws Exception {
		return (List) sqlSession
				.selectList("com.schaffner.persistencia.mappers.selectTemperatura");
	}

	public List<Auxiliar> progresoProyecto(int id) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectProgresoProyectos",
				id);
	}

	public int idProyectoEncExiste(int id) throws Exception {
		return sqlSession
				.selectOne(
						"com.schaffner.persistencia.mappers.selectIgualNumeroProyectoEnc",
						id);
	}
}