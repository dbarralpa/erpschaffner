package com.schaffner.infraestructure.data.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import com.schaffner.infraestructure.data.repositories.contracts.ISchaffnerDAO;
import com.schaffner.modelo.Auxiliar;

public class SchaffnerDAO implements ISchaffnerDAO {
	private DataSource dataSourceJDBC;

	public void setDataSource(DataSource dataSource) {
		this.dataSourceJDBC = dataSource;
	}

	public Auxiliar detalleMaterialesSchaffner(String codigo) {

		String sql = "select codigo,descripcion,um from prueba where codigo = '"+codigo+"' ";

		Connection conn = null;
		Statement st = null;
		Auxiliar material = new Auxiliar(); 
		try {
			conn = dataSourceJDBC.getConnection();
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				material.setAux9(rs.getString("codigo").trim());
				material.setAux10(rs.getString("descripcion").trim());
				material.setAux11(rs.getString("um").trim());
			}
			rs.close();
			st.close();
			return material;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
}
