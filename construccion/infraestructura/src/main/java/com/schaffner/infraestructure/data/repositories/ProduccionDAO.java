package com.schaffner.infraestructure.data.repositories;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.schaffner.infraestructure.data.repositories.contracts.IProduccionDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.DocAplicableDet;

public class ProduccionDAO implements IProduccionDAO {
	private SqlSession sqlSession;

	public ProduccionDAO() {

	}

	public void setSqlSession(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}
	
	public int eliminarPermiso(Auxiliar aux) throws Exception {
		return  (Integer)sqlSession.delete("com.schaffner.persistencia.mappers.eliminarPermisoPro",aux);
	}
	
	public int eliminarPermisoSubArea(Auxiliar aux) throws Exception {
		return  (Integer)sqlSession.delete("com.schaffner.persistencia.mappers.eliminarPermisoSubAreaPro",aux);
	}
	
	public int insertPermiso(Auxiliar aux) throws Exception {
		return  (Integer)sqlSession.insert("com.schaffner.persistencia.mappers.insertPermisoPro",aux);
	}
	
	public int insertPermisoSubArea(Auxiliar aux) throws Exception {
		return  (Integer)sqlSession.insert("com.schaffner.persistencia.mappers.insertPermisoSubAreaPro",aux);
	}
	
	public int insertImpresionUsuario(Auxiliar aux) throws Exception {
		return  (Integer)sqlSession.insert("com.schaffner.persistencia.mappers.insertImpresionUserPro",aux);
	}
	
	public int insertValPerUsuario(Auxiliar aux) throws Exception {
		return  (Integer)sqlSession.insert("com.schaffner.persistencia.mappers.insertValImpreUserPro",aux);
	}
	
	public List<Auxiliar> geSubGroupPrincipal(int subGroupPrincipal) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectSubGroupPrincipalPro",subGroupPrincipal);
	}
	
	public List<Usuario> geUsuariosXarea(int area) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectUsuarioXareaPro",area);
	}
	
	public List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectPermisoXareaXusuarioPro",groupPrincipal);
	}
	
	public List<Auxiliar> gePermisosSubArea(int groupPrincipal) throws Exception {
		return (List) sqlSession.selectList("com.schaffner.persistencia.mappers.selectPermisoSubAreaPro",groupPrincipal);
	}
	
	public List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception {
		return (List) sqlSession.selectList(
				"com.schaffner.persistencia.mappers.selectDocAplicableDetPro", aux);
	}
	
	public String puedeImprimir(Auxiliar idPlano) throws Exception {
		return (String) sqlSession.selectOne("com.schaffner.persistencia.mappers.selectPuedeImprimirPro", idPlano);
	}
}
