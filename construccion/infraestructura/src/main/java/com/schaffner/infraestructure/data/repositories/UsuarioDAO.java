package com.schaffner.infraestructure.data.repositories;

import org.apache.ibatis.session.SqlSession;

import com.schaffner.infraestructure.data.repositories.contracts.IUsuarioDAO;
import com.schaffner.modelo.Usuario;

public class UsuarioDAO implements IUsuarioDAO {
	private SqlSession sqlSession;

	public UsuarioDAO() {

	}

	public void setSqlSession(SqlSession sqlSession) {
		this.sqlSession = sqlSession;
	}

	public Usuario getUsuario(String userName) throws Exception{
		return (Usuario) sqlSession.selectOne(
					"com.schaffner.persistencia.mappers.selectUsuario", userName);
				
	}

}