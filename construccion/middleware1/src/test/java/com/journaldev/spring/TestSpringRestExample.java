package com.journaldev.spring;


import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.client.RestTemplate;



import com.schaffner.modelo.Persona;

public class TestSpringRestExample {

	public static final String SERVER_URI = "http://localhost:18080/middleware1";
	
	public static void main(String args[]){
		testGetAllEmployee("NESTLE");
		System.out.println("*****");
	}
	private static void testGetDummyEmployee(String nombre) {
		RestTemplate restTemplate = new RestTemplate();
		Persona emp = restTemplate.getForObject(SERVER_URI+"/cliente/"+nombre, Persona.class);
		printEmpData(emp);
	}
	
private static void testGetAllEmployee(String nombre) {
	
		RestTemplate restTemplate = new RestTemplate();
		List<LinkedHashMap> emps = restTemplate.getForObject(SERVER_URI+"/clientesPorNombre/"+nombre, List.class);
		System.out.println(emps.size());
		for(LinkedHashMap map : emps){
			System.out.println("Nombre="+map.get("nombre")+",Rut="+map.get("rut"));
		}
	}
	
	public static void printEmpData(Persona emp){
		System.out.println("Mail="+emp.getEmail()+",Nombre="+emp.getNombre());
	}
}
