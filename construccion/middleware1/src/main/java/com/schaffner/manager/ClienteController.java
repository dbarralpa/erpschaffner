package com.schaffner.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import urlMiddleware.CliRestURIConstants;

import com.schaffner.dao.IClienteDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Persona;

/**
 * Handles requests for the Employee service.
 */
@Controller
public class ClienteController {

	ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
	IClienteDAO clienteDAO = (IClienteDAO) context.getBean("clienteDAO");
	

	@RequestMapping(value = CliRestURIConstants.CLIENTE, method = RequestMethod.GET)
	public @ResponseBody Persona getCliente(@PathVariable("nombre") String nombre) {
		Map<Integer, Persona> empData = new HashMap<Integer, Persona>();
		Persona cli = clienteDAO.cliente(nombre);
		empData.put(1, cli);
		return cli;
	}
	
	@RequestMapping(value = CliRestURIConstants.CLIENTE_POR_RUT, method = RequestMethod.GET)
	public @ResponseBody Persona getClientePorRut(@PathVariable("rut") String rut) {
		Map<Integer, Persona> empData = new HashMap<Integer, Persona>();
		Persona cli = clienteDAO.clientePorRut(rut);
		empData.put(1, cli);
		return cli;
	}

	@RequestMapping(value = CliRestURIConstants.CLIENTES_POR_NOMBRE, method = RequestMethod.GET)
	public @ResponseBody List<Persona> getClientesPorNombre(@PathVariable("nombre") String nombre) {
		Map<Integer, Persona> empData = new HashMap<Integer, Persona>();
		ArrayList<Persona> clientes = clienteDAO.clientesPorNombre(nombre);
		for (int i = 0; i < clientes.size(); i++) {
			Persona emp = new Persona();
			emp.setNombre(clientes.get(i).getNombre());
			emp.setRut(clientes.get(i).getRut());
			empData.put(i, emp);
		}
		List<Persona> emps = new ArrayList<Persona>();
		Set<Integer> empIdKeys = empData.keySet();
		for (Integer i : empIdKeys) {
			emps.add(empData.get(i));
		}
		return emps;
	}
	
	@RequestMapping(value = CliRestURIConstants.VENDEDORES, method = RequestMethod.GET)
	public @ResponseBody List<Persona> getVendedores() {
		Map<Integer, Persona> empData = new HashMap<Integer, Persona>();
		ArrayList<Persona> clientes = clienteDAO.vendedores();
		for (int i = 0; i < clientes.size(); i++) { 
			Persona emp = new Persona();
			emp.setNombre(clientes.get(i).getNombre());
			emp.setId(clientes.get(i).getId());
			emp.setEmail(clientes.get(i).getEmail());
			emp.setTelefono(clientes.get(i).getTelefono());
			empData.put(i, emp);
		}
		List<Persona> emps = new ArrayList<Persona>();
		Set<Integer> empIdKeys = empData.keySet();
		for (Integer i : empIdKeys) {
			emps.add(empData.get(i));
		}
		return emps;
	}
	
	@RequestMapping(value = CliRestURIConstants.FAMILIAS, method = RequestMethod.GET)
	public @ResponseBody List<Auxiliar> getFamilias() {
		Map<Integer, Auxiliar> empData = new HashMap<Integer, Auxiliar>();
		ArrayList<Auxiliar> familias = clienteDAO.familias();
		for (int i = 0; i < familias.size(); i++) { 
			Auxiliar emp = new Auxiliar();
			emp.setAux9(familias.get(i).getAux9());
			emp.setAux10(familias.get(i).getAux10());
			empData.put(i, emp);
		}
		List<Auxiliar> emps = new ArrayList<Auxiliar>();
		Set<Integer> empIdKeys = empData.keySet();
		for (Integer i : empIdKeys) {
			emps.add(empData.get(i));
		}
		return emps;
	}
	
	@RequestMapping(value = CliRestURIConstants.CONTACTOS, method = RequestMethod.GET)
	public @ResponseBody Auxiliar getContactos(@PathVariable("rut") String rut) {
		Map<Integer, Auxiliar> empData = new HashMap<Integer, Auxiliar>();
		Auxiliar contactos = clienteDAO.contactosCliente(rut);
		empData.put(1, contactos);
		return contactos;
	}
	
	@RequestMapping(value = CliRestURIConstants.MATERIAL, method = RequestMethod.GET)
	public @ResponseBody List<Auxiliar> getMaterialesPorCodigo(@PathVariable("nombre") String nombre) {
		Map<Integer, Auxiliar> empData = new HashMap<Integer, Auxiliar>();
		List<Auxiliar> materiales = clienteDAO.materialesSchaffner(nombre);
		for (int i = 0; i < materiales.size(); i++) {
			Auxiliar emp = new Auxiliar();
			emp.setAux9(materiales.get(i).getAux9());
			emp.setAux10(materiales.get(i).getAux10());
			empData.put(i, emp);
		}
		List<Auxiliar> emps = new ArrayList<Auxiliar>();
		Set<Integer> empIdKeys = empData.keySet();
		for (Integer i : empIdKeys) {
			emps.add(empData.get(i));
		}
		return emps;
	}
	
	@RequestMapping(value = CliRestURIConstants.DETALLE_MATERIAL, method = RequestMethod.GET)
	public @ResponseBody List<Auxiliar> getDetalleMateriales(@PathVariable("codigo") String codigo) {
		Map<Integer, Auxiliar> empData = new HashMap<Integer, Auxiliar>();
		List<Auxiliar> materiales = clienteDAO.detalleMaterialesSchaffner(codigo);
		for (int i = 0; i < materiales.size(); i++) {
			Auxiliar emp = new Auxiliar();
			emp.setAux9(materiales.get(i).getAux9());
			emp.setAux10(materiales.get(i).getAux10());
			emp.setAux11(materiales.get(i).getAux11());
			empData.put(i, emp);
		}
		List<Auxiliar> emps = new ArrayList<Auxiliar>();
		Set<Integer> empIdKeys = empData.keySet();
		for (Integer i : empIdKeys) {
			emps.add(empData.get(i));
		}
		return emps;
	}
	
	@RequestMapping(value = CliRestURIConstants.EQUIPOSVIGENTES, method = RequestMethod.GET)
	public @ResponseBody List<Auxiliar> getEquiposVigentes() {
		Map<Integer, Auxiliar> empData = new HashMap<Integer, Auxiliar>();
		ArrayList<Auxiliar> equipos = clienteDAO.equiposVigentes();
		for (int i = 0; i < equipos.size(); i++) { 
			Auxiliar emp = new Auxiliar();
			emp.setAux1(equipos.get(i).getAux1());
			emp.setAux2(equipos.get(i).getAux2());
			empData.put(i, emp);
		}
		List<Auxiliar> emps = new ArrayList<Auxiliar>();
		Set<Integer> empIdKeys = empData.keySet();
		for (Integer i : empIdKeys) {
			emps.add(empData.get(i));
		}
		return emps;
	}
}
