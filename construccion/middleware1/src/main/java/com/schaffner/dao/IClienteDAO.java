package com.schaffner.dao;


import java.util.ArrayList;
import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Persona;

public interface IClienteDAO 
{
	public ArrayList<Persona> clientesPorNombre(String nombre);
	public Persona cliente(String nombre);
	public Persona clientePorRut(String rut);
	public ArrayList<Persona> vendedores();
	public ArrayList<Auxiliar> familias();
	public Auxiliar contactosCliente(String rut);
	public ArrayList<Auxiliar> materialesSchaffner(String material);
	public ArrayList<Auxiliar> detalleMaterialesSchaffner(String material);
	public ArrayList<Auxiliar> equiposVigentes();
	
}




