package com.schaffner.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Persona;

public class ClienteDAO implements IClienteDAO {
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public ArrayList<Persona> clientesPorNombre(String nombre) {

		String sql = " select distinct(UPPER(CTA_RAZONSOCIAL)) as CTA_RAZONSOCIAL";
		sql += " from STO_CTACTE ";
		sql += " inner join sys_persona";
		sql += " on(PER_CODIGO = CTA_CODIGO)";
		sql += " where UPPER(CTA_RAZONSOCIAL) like ? and PER_CLIENTE = 'S'";

		Connection conn = null;
		ArrayList<Persona> clientes = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + nombre.toUpperCase() + "%");
			clientes = new ArrayList<Persona>();
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Persona per = new Persona();
				per.setNombre(rs.getString("CTA_RAZONSOCIAL"));
				clientes.add(per);
			}
			rs.close();
			ps.close();
			return clientes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public Persona cliente(String nombre) {

		String sql = "select distinct(rtrim(CTA_RAZONSOCIAL)) as CTA_RAZONSOCIAL,CTA_CONTACTO,cta_email,PER_CODIGO from STO_CTACTE ";
		sql += "inner join sys_persona ";
		sql += "on(PER_CODIGO = CTA_CODIGO) ";
		sql += "where UPPER(CTA_RAZONSOCIAL) like '%" + nombre
				+ "%' and PER_CLIENTE = 'S'";

		Connection conn = null;
		Statement st = null;
		Persona cliente = null;
		try {
			conn = dataSource.getConnection();
			// PreparedStatement ps = conn.prepareStatement(sql);
			st = conn.createStatement();
			// ps.setString(1, "'"+nombre+"'");
			cliente = new Persona();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				cliente.setNombre(rs.getString("CTA_RAZONSOCIAL"));
				cliente.setContacto(rs.getString("CTA_CONTACTO"));
				cliente.setEmail(rs.getString("cta_email"));
				cliente.setRut(rs.getString("PER_CODIGO").trim());
			}
			rs.close();
			st.close();
			return cliente;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public Persona clientePorRut(String rut) {

		String sql = "select distinct(rtrim(CTA_RAZONSOCIAL)) as CTA_RAZONSOCIAL,CTA_CONTACTO,cta_email,PER_CODIGO from STO_CTACTE ";
		sql += "inner join sys_persona ";
		sql += "on(PER_CODIGO = CTA_CODIGO) ";
		sql += "where PER_CODIGO = '" + rut + "'";

		Connection conn = null;
		Statement st = null;
		Persona cliente = null;
		try {
			conn = dataSource.getConnection();
			// PreparedStatement ps = conn.prepareStatement(sql);
			st = conn.createStatement();
			// ps.setString(1, "'"+nombre+"'");
			cliente = new Persona();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				cliente.setNombre(rs.getString("CTA_RAZONSOCIAL"));
				cliente.setContacto(rs.getString("CTA_CONTACTO"));
				cliente.setEmail(rs.getString("cta_email"));
				cliente.setRut(rs.getString("PER_CODIGO").trim());
			}
			rs.close();
			st.close();
			return cliente;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}

	public ArrayList<Persona> vendedores() {

		String sql = " SELECT TAB_Codigo, TAB_Desc, TAB_Texto1, TAB_Texto2 FROM SYS_TABLAS WHERE TAB_CodTabla = 1";

		Connection conn = null;
		Statement st = null;
		ArrayList<Persona> vendedores = null;
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			vendedores = new ArrayList<Persona>();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Persona per = new Persona();
				per.setId(rs.getInt("TAB_Codigo"));
				per.setNombre(rs.getString("TAB_Desc"));
				per.setTelefono(rs.getString("TAB_Texto1"));
				per.setEmail(rs.getString("TAB_Texto2"));
				vendedores.add(per);
			}
			rs.close();
			st.close();
			return vendedores;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public ArrayList<Auxiliar> familias() {

		String sql = " select gen_Codigo,gen_Desc from ENSchaffner.dbo.EE_tabla_generica where gen_Tipo=25";

		Connection conn = null;
		Statement st = null;
		ArrayList<Auxiliar> familias = null;
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			familias = new ArrayList<Auxiliar>();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Auxiliar per = new Auxiliar();
				per.setAux9(rs.getString("gen_Codigo").trim());
				per.setAux10(rs.getString("gen_Desc").trim());
				familias.add(per);
			}
			rs.close();
			st.close();
			return familias;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public Auxiliar contactosCliente(String rut) {

		String sql = " select a1.ACT_Texto as nombre_contacto1,a2.ACT_Texto as email_contacto1"; 
				sql += " ,a3.ACT_Texto as nombre_contacto2,a4.ACT_Texto as email_contacto2"; 
				sql += " ,a5.ACT_Texto as nombre_contacto3,a6.ACT_Texto as email_contacto3"; 
				sql += " ,a7.ACT_Texto as nombre_contacto4,a8.ACT_Texto as email_contacto4"; 
				sql += " ,a9.ACT_Texto as nombre_contacto5,a10.ACT_Texto as email_contacto5"; 
				sql += " from SiaSchaffner.dbo.sto_ctacte"; 
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a1  on a1.ACT_TIPOCTACORRIENTE =1  and a1.ACT_CodAnalisis=3   and a1.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a2  on a2.ACT_TIPOCTACORRIENTE =1  and a2.ACT_CodAnalisis=4   and a2.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a3  on a3.ACT_TIPOCTACORRIENTE =1  and a3.ACT_CodAnalisis=5   and a3.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a4  on a4.ACT_TIPOCTACORRIENTE =1  and a4.ACT_CodAnalisis=6   and a4.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a5  on a5.ACT_TIPOCTACORRIENTE =1  and a5.ACT_CodAnalisis=7   and a5.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a6  on a6.ACT_TIPOCTACORRIENTE =1  and a6.ACT_CodAnalisis=8   and a6.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a7  on a7.ACT_TIPOCTACORRIENTE =1  and a7.ACT_CodAnalisis=9   and a7.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a8  on a8.ACT_TIPOCTACORRIENTE =1  and a8.ACT_CodAnalisis=10  and a8.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a9  on a9.ACT_TIPOCTACORRIENTE =1  and a9.ACT_CodAnalisis=11  and a9.ACT_Codigo=cta_Codigo";
				sql += " Left join SiaSchaffner.dbo.Sto_ctacteadic a10 on a10.ACT_TIPOCTACORRIENTE =1 and a10.ACT_CodAnalisis=12 and a10.ACT_Codigo=cta_Codigo";
				sql += " where cta_tipoctacorriente=1 and cta_Codigo = '"+rut+"'";

		Connection conn = null;
		Statement st = null;
		Auxiliar contactos = null;
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			contactos = new Auxiliar();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				if(rs.getString("nombre_contacto1") != null){
					contactos.setAux9(rs.getString("nombre_contacto1").trim());
				}else{
					contactos.setAux9(rs.getString("nombre_contacto1"));
				}
				
				if(rs.getString("email_contacto1") != null){
					contactos.setAux10(rs.getString("email_contacto1").trim());
				}else{
					contactos.setAux10(rs.getString("email_contacto1"));
				}
				
				if(rs.getString("nombre_contacto2") != null){
					contactos.setAux11(rs.getString("nombre_contacto2").trim());
				}else{
					contactos.setAux11(rs.getString("nombre_contacto2"));
				}
				
				if(rs.getString("email_contacto2") != null){
					contactos.setAux12(rs.getString("email_contacto2").trim());
				}else{
					contactos.setAux12(rs.getString("email_contacto2"));
				}
				
				if(rs.getString("nombre_contacto3") != null){
					contactos.setAux13(rs.getString("nombre_contacto3").trim());
				}else{
					contactos.setAux13(rs.getString("nombre_contacto3"));
				}
				
				if(rs.getString("email_contacto3") != null){
					contactos.setAux14(rs.getString("email_contacto3").trim());
				}else{
					contactos.setAux14(rs.getString("email_contacto3"));
				}
				
				if(rs.getString("nombre_contacto4") != null){
					contactos.setAux15(rs.getString("nombre_contacto4").trim());
				}else{
					contactos.setAux15(rs.getString("nombre_contacto4"));
				}
				
				if(rs.getString("email_contacto4") != null){
					contactos.setAux16(rs.getString("email_contacto4").trim());
				}else{
					contactos.setAux16(rs.getString("email_contacto4"));
				}
				
				if(rs.getString("nombre_contacto5") != null){
					contactos.setAux17(rs.getString("nombre_contacto5").trim());
				}else{
					contactos.setAux17(rs.getString("nombre_contacto5"));
				}
				
				if(rs.getString("email_contacto5") != null){
					contactos.setAux18(rs.getString("email_contacto5").trim());
				}else{
					contactos.setAux18(rs.getString("email_contacto5"));
				}
				
				
				
			}
			rs.close();
			st.close();
			return contactos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	
	public ArrayList<Auxiliar> materialesSchaffner(String material) {

		String sql = "select PRO_CODPROD, REPLACE(PRO_DESC,'\"\',' ')  as DESCR from STO_PRODUCTO "; 
		sql += "where UPPER(PRO_DESC) like '%"+material+"%'";

		Connection conn = null;
		Statement st = null;
		ArrayList<Auxiliar> materiales = new ArrayList<Auxiliar>(); 
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Auxiliar aux = new Auxiliar();
				aux.setAux9(rs.getString("PRO_CODPROD").trim());
				aux.setAux10(rs.getString("DESCR").trim());
				materiales.add(aux);
			}
			rs.close();
			st.close();
			return materiales;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public ArrayList<Auxiliar> detalleMaterialesSchaffner(String material) {

		String sql = "select codigo,descripcion,um from prueba where codigo in ("+material+") ";

		Connection conn = null;
		Statement st = null;
		ArrayList<Auxiliar> materiales = new ArrayList<Auxiliar>(); 
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Auxiliar aux = new Auxiliar();
				aux.setAux9(rs.getString("codigo").trim());
				aux.setAux10(rs.getString("descripcion").trim());
				aux.setAux11(rs.getString("um").trim());
				materiales.add(aux);
			}
			rs.close();
			st.close();
			return materiales;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	
	public ArrayList<Auxiliar> equiposVigentes() {

		String sql = " SELECT Pedido, Linea FROM ENSchaffner.dbo.TPMP WHERE Estado = 0 AND Activa = 0 AND Status <> 'RevisionFinal'";

		Connection conn = null;
		Statement st = null;
		ArrayList<Auxiliar> equipos = null;
		try {
			conn = dataSource.getConnection();
			st = conn.createStatement();
			equipos = new ArrayList<Auxiliar>();
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Auxiliar aux = new Auxiliar();
				aux.setAux1(rs.getInt("Pedido"));
				aux.setAux2(rs.getInt("Linea"));
				equipos.add(aux);
			}
			rs.close();
			st.close();
			return equipos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	
	
}
