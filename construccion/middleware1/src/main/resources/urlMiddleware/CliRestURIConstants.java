package urlMiddleware;

public class CliRestURIConstants {

	public static final String CLIENTES_POR_NOMBRE = "/clientesPorNombre/{nombre}";
	public static final String CLIENTE = "/cliente/{nombre}";
	public static final String CLIENTE_POR_RUT = "/clientePorRut/{rut}";
	public static final String VENDEDORES = "/vendedores/";
	public static final String FAMILIAS = "/familias/";
	public static final String CONTACTOS = "/contactos/{rut}";
	
	public static final String MATERIAL = "/material/{nombre}";
	public static final String DETALLE_MATERIAL = "/detalleMaterial/{codigo}";
	
	public static final String EQUIPOSVIGENTES = "/equiposVigentes/";
}
