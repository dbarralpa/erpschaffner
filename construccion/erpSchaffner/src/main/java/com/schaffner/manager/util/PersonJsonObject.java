package com.schaffner.manager.util;


import java.util.List;

import com.schaffner.modelo.Auxiliar;

public class PersonJsonObject {

    int iTotalRecords;

    int iTotalDisplayRecords;
    
    int iDisplayLength;

    String sEcho;

    String sColumns;

    List<Auxiliar> aaData;

    public int getiTotalRecords() {
	return iTotalRecords;
    }

    public void setiTotalRecords(int iTotalRecords) {
	this.iTotalRecords = iTotalRecords;
    }

    public int getiTotalDisplayRecords() {
	return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
	this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public String getsEcho() {
	return sEcho;
    }

    public void setsEcho(String sEcho) {
	this.sEcho = sEcho;
    }

    public String getsColumns() {
	return sColumns;
    }

    public void setsColumns(String sColumns) {
	this.sColumns = sColumns;
    }

    public List<Auxiliar> getAaData() {
        return aaData;
    }

    public void setAaData(List<Auxiliar> aaData) {
        this.aaData = aaData;
    }

	public int getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

    
}