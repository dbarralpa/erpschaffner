package com.schaffner.manager.util;

import java.util.Comparator;

import com.schaffner.modelo.ingenieria.Costo;

public class Ordenamiento implements Comparator {

    public Ordenamiento() {
    }

    public int compare(Object o1, Object o2) {

        Costo costo1 = (Costo) o1;
        Costo costo2 = (Costo) o2;

        return costo1.getProceso().compareTo(costo2.getProceso());
    }
}
