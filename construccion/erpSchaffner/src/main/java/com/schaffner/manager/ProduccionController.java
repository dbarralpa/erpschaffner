package com.schaffner.manager;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.schaffner.core.servicio.IngenieriaServicio;
import com.schaffner.core.servicio.ProduccionServicio;
import com.schaffner.manager.util.HelperIngenieria;
import com.schaffner.manager.util.ViewGeneral;
import com.schaffner.manager.util.ViewIngenieria;
import com.schaffner.manager.util.ViewProduccion;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;

@Controller
public class ProduccionController {
	private static final Logger logger = Logger.getLogger(ProduccionController.class);
	@RequestMapping(value = "/produccion-administrar", method = RequestMethod.GET)
	public String administrarPro(Model model) {
		try {
			ProduccionServicio isus = new ProduccionServicio();
			List<Auxiliar> permisos = isus.gePermisosXareaXusuario(3);
			List<Usuario> per = isus.geUsuariosXarea(3);
			model.addAttribute("administrar", ViewProduccion.administracionUsuariosProduccion(per,permisos,  isus.geSubGroupPrincipal(3),isus.gePermisosSubArea(3)));
			model.addAttribute("usuariosImpresionPlanos", ViewProduccion.permisosImpresion(per));
			model.addAttribute("url", "produccion/administracion.jsp");
		} catch (Exception e) {
			logger.error("produccion-administrar", e);
		}

		return "home";
	}
	
	@RequestMapping(value = "/produccion-actualizar-permisos", method = RequestMethod.GET)
	public @ResponseBody String actualizarPermisos(Model model,
			@RequestParam String val_valorPermiso,
			@RequestParam String val_nombrePermiso,
			@RequestParam String val_user) {
		String result = "";
		try {
			ProduccionServicio isus = new ProduccionServicio();
			List<Auxiliar> permisos = isus.gePermisosXareaXusuario(3);
			Auxiliar aux = new Auxiliar();
			
			aux.setAux9(val_user);
			if(val_nombrePermiso.trim().equals("PRO_ADMIN")){
				aux.setAux1(7);
			}else if(val_nombrePermiso.trim().equals("PRO_DOC_APLI")){
				aux.setAux1(8);
			}
			if(HelperIngenieria.checked(permisos, val_user.trim(), val_nombrePermiso).equals("checked")){
				int a = isus.eliminarPermiso(aux);
			}else{
				int a = isus.insertPermiso(aux);
			}
			
			permisos = isus.gePermisosXareaXusuario(3);
			List<Usuario> per = isus.geUsuariosXarea(3);
			result = ViewProduccion.administracionUsuariosProduccion(per,permisos, isus.geSubGroupPrincipal(3),isus.gePermisosSubArea(3)); 
			//result += model.addAttribute("usuariosImpresionPlanos", ViewProduccion.permisosImpresion(per));
		} catch (Exception e) {
			logger.error("produccion-actualizar-permisos", e);
			result = ViewGeneral.error("produccion-administrar", true);
		}
		return result;
	}
	                          
	@RequestMapping(value = "/produccion-actualizar-permisos-subarea1", method = RequestMethod.POST)
	public @ResponseBody String actualizarPermisosSubArea(Model model,
			@RequestParam String val_valorPermiso,
			@RequestParam String val_nombrePermiso,
			@RequestParam String val_user) {
		String result = "";
		try {
			ProduccionServicio isus = new ProduccionServicio();
			List<Auxiliar> permisos = isus.gePermisosXareaXusuario(3);
			Auxiliar aux = new Auxiliar();
			
				aux.setAux9(val_user);
				if(val_nombrePermiso.trim().equals("PRO_ADMIN")){
					aux.setAux1(7);
				}else if(val_nombrePermiso.trim().equals("PRO_DOC_APLI")){
					aux.setAux1(8);
				}
				if(HelperIngenieria.checked(permisos, val_user.trim(), val_nombrePermiso).equals("checked")){
					int a = isus.eliminarPermiso(aux);
				}else{
					int a = isus.insertPermiso(aux);
				}
			
			permisos = isus.gePermisosXareaXusuario(3);
			List<Usuario> per = isus.geUsuariosXarea(3);
			result = ViewProduccion.administracionUsuariosProduccion(per,permisos, isus.geSubGroupPrincipal(3),isus.gePermisosSubArea(3)); 
			//result += model.addAttribute("usuariosImpresionPlanos", ViewProduccion.permisosImpresion(per));
		} catch (Exception e) {
			logger.error("produccion-actualizar-permisos-subarea", e);
			result = ViewGeneral.error("produccion-administrar", true);
		}
		return result;
	}
	
	@RequestMapping(value = "/produccion-busqueda-doc-aplicable", method = RequestMethod.GET)
	public String ingresoBuscarDocAplicable(Model model) {
		try {
			model.addAttribute("buscarDocAplicable", ViewProduccion.buscarDocAplicable("", ""));
			model.addAttribute("url", "produccion/buscarDocAplicablePro.jsp");
		} catch (Exception e) {
			logger.error(e.getMessage(), new Exception("produccion-busqueda-doc-aplicable"));
		}
		return "home";
	}
	
	@RequestMapping(value = "/produccion-encontrar-doc-aplicable", method = RequestMethod.POST)
	public String encontrarDocAplicable(Model model,
			@RequestParam String val_cod_costo, @RequestParam int val_np) {
		try {
			IngenieriaServicio isus = new IngenieriaServicio();
			Auxiliar aux = new Auxiliar();
			aux.setAux1(val_np);
			aux.setAux9(val_cod_costo);
			List<DocAplicableEnc> encs = isus.getDocsAplicablesEnc(aux);
			model.addAttribute("disenoBusquedaDataTable",ViewProduccion.detalleDocApliEnc(encs));
			model.addAttribute("buscarDocAplicable",ViewProduccion.buscarDocAplicable("", ""));
			model.addAttribute("url", "produccion/buscarDocAplicablePro.jsp");
		} catch (Exception e) {
			logger.error("produccion-encontrar-doc-aplicable", e);
		}

		return "home";
	}
	
	@RequestMapping(value = "/produccion-documentos-aplicables-actualizar", method = RequestMethod.GET)
	public String modificarDocAplicableEnc(ModelMap model,
			@RequestParam int val_numero,
			@RequestParam String val_codigo_costo,
			@RequestParam String val_diseno_electrico,
			@RequestParam String val_diseno_mecanico, @RequestParam int val_np,
			@RequestParam String val_item,Principal prin) {
		try {
			IngenieriaServicio isus = new IngenieriaServicio();
			ProduccionServicio pro = new ProduccionServicio();
			/*Auxiliar aux = new Auxiliar();
			aux.setAux9(val_id_plano);
			aux.setAux10(prin.getName());*/
			List<Auxiliar> auxs = isus.getPlanoDescripciones();
			DocAplicableEnc enc = new DocAplicableEnc();
			enc.setNumero(val_numero);
			enc.setDisenoTrafo(val_codigo_costo);
			enc.setDisenoElectrico(val_diseno_electrico);
			enc.setDisenoMecanico(val_diseno_mecanico);
			enc.setNp(val_np);
			enc.setItem(val_item);

			model.addAttribute("archivosDocApli", new CargarArchivo());
			model.addAttribute("cabeceraDocAplicableEnc",ViewProduccion.cabeceraDocAplicableEnc(enc));
			model.addAttribute("comboDescripcionPlano", ViewProduccion.comboDescripcionPlano(auxs, "val_descripcion_plano", 0));
			List<DocAplicableDet> dets = pro.getDocAplicableDet(enc.getNumero());
			List<Auxiliar> auxss = isus.getPlanoDescripciones();
			model.addAttribute("detalleDocumentoAplicable",	ViewProduccion.detalleDocApliPlanos(dets, auxss));
			model.addAttribute("url", "produccion/documentosAplicablesPro.jsp");
	 	} catch (Exception e) {
			logger.error("produccion-documentos-aplicables-actualizar", e);
		}
		return "home";
	}
	
	@RequestMapping(value = "/produccion-doc-apli-cargarArchivos", method = RequestMethod.GET)
	public void doDownload(Model model,HttpServletRequest request,
			HttpServletResponse response, 
			@RequestParam String  val_id_plano,
			@RequestParam int val_idDocApliDet,
			@RequestParam int val_revision,
			@RequestParam int val_id_detRev,
			HttpSession session, 
			Principal prin,
			@RequestParam int val_idEnc) throws Exception {

		try {
			CargarArchivo archivo = new CargarArchivo();
			IngenieriaServicio isus = new IngenieriaServicio();
			ProduccionServicio pro = new ProduccionServicio();
			Auxiliar der = new Auxiliar();
			der.setAux9(prin.getName());
			der.setAux1(val_idDocApliDet);
			der.setAux10(val_id_plano);
			if(pro.puedeImprimir(der).equals("SI")){
			Auxiliar aux5  = new Auxiliar();
			aux5.setAux1(val_revision);
			aux5.setAux9(val_id_plano);
			archivo = isus.docApliDetArchivo(aux5);
			Auxiliar aux1 = new Auxiliar();
			aux1.setAux1(val_id_detRev);
			aux1.setAux35(new Timestamp(new java.util.Date().getTime()));
			aux1.setAux9(prin.getName());
			pro.insertImpresionUsuario(aux1);
		    }else{
		    	Auxiliar aux5  = new Auxiliar();
				aux5.setAux1(0);
				aux5.setAux9("xpfpermiso");
				archivo = isus.docApliDetArchivo(aux5);
		    }
			
			response.setContentType("application/pdf");
			response.setContentLength(archivo.getArchivo().length);
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(archivo.getArchivo(), 0, archivo.getArchivo().length);
            ouputStream.flush();
            ouputStream.close();
		} catch (Exception e) {
			logger.error("produccion-doc-apli-cargarArchivos", e);
		}
	}
	
	@RequestMapping(value = "/produccion-validacion-permiso-usuario", method = RequestMethod.GET)
	public @ResponseBody String validarPermisoImpresion(Model model,
			@RequestParam String val_plano_search,
			@RequestParam String val_nombreUsuario,
			Principal prin) {
		String result = "";
		try {
			ProduccionServicio isus = new ProduccionServicio();
			if(!val_plano_search.trim().equals("") && !val_nombreUsuario.trim().equals("")){
			Auxiliar aux = new Auxiliar();
			aux.setAux11(val_plano_search);
			aux.setAux35(new Timestamp(new java.util.Date().getTime()));
			aux.setAux9(val_nombreUsuario);
			aux.setAux10(prin.getName());
			int a = isus.insertValPerUsuario(aux);
			if(a > 0){
				result = ViewGeneral.success("#", false);
			}else{
				result = ViewGeneral.info("#", false, "No se pudo actualizar");
			  }
			}else{
				result = ViewGeneral.info("#", false, "Debe agregar toda la información requerida");
			}
			
			
		} catch (Exception e) {
			logger.error("produccion-actualizar-permisos-subarea", e);
			result = ViewGeneral.error("#", false);
		}
		return result;
	}
}
