package com.schaffner.manager.util;

public class ViewGeneral {
	public static String error(String url, boolean est) {
		String html = "";
		html += "<div id='login-container' class='animation-slideDown'>";
		html += "<div class='alert alert-danger alert-dismissable'>";
		html += "<button type='button' class='close' data-dismiss='alert' ";
		html += "aria-hidden='true'>&times;</button>";
		html += "<h4>";
		html += "<i class='fa fa-times-circle'></i> Error";
		html += "</h4>";
		html += "Los datos no fueron actualizados";
		if (est) {
			html += "<a href='" + url;
			html += "' class='alert-link'> CLIC AQU�</a>!";
		}
		html += "</div>";
		html += "</div>";

		return html;
	}

	public static String success(String url, boolean est) {
		String html = "";
		html += "<div class='alert alert-success alert-dismissable animation-slideDown'>";
		html += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		html += "<h4><i class='fa fa-check-circle'></i> Exito</h4> Datos actualizados";
		if (est) {
			html += "<a href='";
			html += url + "' class='alert-link'> CLIC AQU�</a>!";
		}
		html += "</div>";

		return html;
	}

	public static String info(String url, boolean est, String informacion) {
		String html = "";
		html += "<div class='alert alert-info alert-dismissable animation-slideDown'>";
		html += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		html += "<h4><i class='fa fa-info-circle'></i> Info</h4> " + informacion;
		if (est) {
			html += "<a href='";
			html += url + "' class='alert-link'> CLIC AQU�</a>!";
		}
		html += "</div>";
		return html;
	}

	public static String warning(String url, boolean est) {
		String html = "";
		html += "<div class='alert alert-warning alert-dismissable' animation-slideDown>";
		html += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		html += "<h4><i class='fa fa-exclamation-circle'></i> Warning</h4>Just to be safe";
		if (est) {
			html += "<a href='";
			html += url + "' class='alert-link'> CLIC AQU�</a>!";
		}
		html += "</div>";

		return html;
	}
}
