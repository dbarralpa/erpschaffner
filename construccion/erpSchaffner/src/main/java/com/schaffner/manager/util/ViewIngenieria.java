package com.schaffner.manager.util;

import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Persona;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Derivacion;
import com.schaffner.modelo.ingenieria.Distribucion;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;
import com.schaffner.modelo.ingenieria.EstanqueTapa;
import com.schaffner.modelo.ingenieria.NucleoFerreteria;
import com.schaffner.modelo.ingenieria.Radiador;
import com.schaffner.modelo.ingenieria.Refrigerante;
import com.schaffner.modelo.ingenieria.TipoAislador;

public class ViewIngenieria {

	public static String error(String url, boolean est) {
		String html = "";
		html += "<div id='login-container' class='animation-slideDown'>";
		html += "<div class='alert alert-danger alert-dismissable'>";
		html += "<button type='button' class='close' data-dismiss='alert' ";
		html += "aria-hidden='true'>&times;</button>";
		html += "<h4>";
		html += "<i class='fa fa-times-circle'></i> Error";
		html += "</h4>";
		html += "Los datos no fueron actualizados";
		if (est) {
			html += "<a href='" + url;
			html += "' class='alert-link'> CLIC AQU�</a>!";
		}
		html += "</div>";
		html += "</div>";

		return html;
	}

	public static String success(String url, boolean est) {
		String html = "";
		html += "<div class='alert alert-success alert-dismissable animation-slideDown'>";
		html += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		html += "<h4><i class='fa fa-check-circle'></i> Exito</h4> Datos actualizados";
		if (est) {
			html += "<a href='";
			html += url + "' class='alert-link'> CLIC AQU�</a>!";
		}
		html += "</div>";

		return html;
	}

	public static String bonina(int valorBob, int codCon, String bobina,
			String nombreBobina) {
		String html = "";
		html = "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=''>Bobina " + bobina
				+ "<span class='text-danger'>*</span></label>";
		// html += "<div class='col-md-8'>";
		html += "<label for='example-2'>";
		html += "<input type='text' id='" + nombreBobina + "' name='"
				+ nombreBobina + "' disabled value='"
				+ HelperIngenieria.tipoPobina(codCon)
				+ "' class='form-control' placeholder='C�digo..'>";
		html += "</label>";
		html += "<label for=''>";
		if (valorBob == 1) {
			html += "<input type='checkbox' id='' name='' value='option1' checked>";
		} else {
			html += "<input type='checkbox' id='' name='' value='option1' disabled>";
		}
		html += "</label>";
		// html += "</div>";
		html += "</div>";

		return html;
	}

	public static String selectDistribucion(List<Distribucion> distri,
			int selected) {
		String html = "";
		html += "<select id='val_datosgenerales_distribucion' name='val_datosgenerales_distribucion' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija Distribuci�n</option>";
		for (int i = 0; i < distri.size(); i++) {
			if (selected == distri.get(i).getCodDistribucion()) {
				html += "<option value='" + distri.get(i).getCodDistribucion()
						+ "' selected>" + distri.get(i).getDistribucion()
						+ "</option>";
			} else {
				html += "<option value='" + distri.get(i).getCodDistribucion()
						+ "'>" + distri.get(i).getDistribucion() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectDistribucionAll(List<Distribucion> distri,
			int selected) {
		String html = "";
		html += "<select id='val_distribuciones_all' name='val_distribuciones_all' ";
		html += "class='form-control'>";
		html += "<option value='0'>Elija Clasificaci�n</option>";
		for (int i = 0; i < distri.size(); i++) {
			if (distri.get(i).getCodDistribucion() == selected) {
				html += "<option value='" + distri.get(i).getCodDistribucion()
						+ "' selected>" + distri.get(i).getDistribucion()
						+ "</option>";
			} else {
				html += "<option value='" + distri.get(i).getCodDistribucion()
						+ "'>" + distri.get(i).getDistribucion() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectTipoAisladores(List<TipoAislador> tipAis,
			int selected) {
		String html = "";
		html += "<select id='val_datosgenerales_disposicion' name='val_datosgenerales_disposicion' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija Aislaci�n</option>";
		for (int i = 0; i < tipAis.size(); i++) {
			if (selected == tipAis.get(i).getCodTipoAislador()) {
				html += "<option value='" + tipAis.get(i).getCodTipoAislador()
						+ "' selected>"
						+ tipAis.get(i).getDescripcionAislador() + "</option>";
			} else {
				html += "<option value='" + tipAis.get(i).getCodTipoAislador()
						+ "'>" + tipAis.get(i).getDescripcionAislador()
						+ "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectRefrigeranteSeco(List<Refrigerante> refri) {
		String html = "";
		html += "<select id='val_datosgenerales_refrigerante' name='val_datosgenerales_refrigerante' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija Refrigerante</option>";
		for (int i = 0; i < refri.size(); i++) {
			if (refri.get(i).getCodRefrigerante() == 0) {
				html += "<option value='" + refri.get(i).getCodRefrigerante()
						+ "' selected>" + refri.get(i).getDesRefrigerante()
						+ "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectRefrigeranteAceite(List<Refrigerante> refri,
			int selected) {
		String html = "";
		html += "<select id='val_datosgenerales_refrigerante' name='val_datosgenerales_refrigerante' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija Refrigerante</option>";
		for (int i = 0; i < refri.size(); i++) {
			if (refri.get(i).getCodRefrigerante() > 0) {
				if (refri.get(i).getCodRefrigerante() == selected) {
					html += "<option value='"
							+ refri.get(i).getCodRefrigerante() + "' selected>"
							+ refri.get(i).getDesRefrigerante() + "</option>";
				} else {
					html += "<option value='"
							+ refri.get(i).getCodRefrigerante() + "'>"
							+ refri.get(i).getDesRefrigerante() + "</option>";
				}
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectRefrigerante(List<Refrigerante> refri,
			int selected) {
		String html = "";
		html += "<select id='val_refrigerante' name='val_refrigerante' ";
		html += "class='form-control'>";
		html += "<option value='-1'>Elija Refrigerante</option>";
		for (int i = 0; i < refri.size(); i++) {
			if (refri.get(i).getCodRefrigerante() == selected) {
				html += "<option value='" + refri.get(i).getCodRefrigerante()
						+ "' selected>" + refri.get(i).getDesRefrigerante()
						+ "</option>";
			} else {
				html += "<option value='" + refri.get(i).getCodRefrigerante()
						+ "'>" + refri.get(i).getDesRefrigerante()
						+ "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectConductorBobina(String nombre) {
		String html = "";
		html += "<select id='" + nombre + "' name='" + nombre + "' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija Conductor</option>";

		html += "<option value='COBRE'>COBRE</option>";
		html += "<option value='ALUMINIO'>ALUMINIO</option>";

		html += "</select>";

		return html;
	}

	public static String selectDerivaciones(List<Derivacion> deri, int selected) {
		String html = "";
		html += "<select id='val_especificaciones_derivacion' name='val_especificaciones_derivacion' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija derivaci�n</option>";
		for (int i = 0; i < deri.size(); i++) {
			if (selected == deri.get(i).getId()) {
				html += "<option value='" + deri.get(i).getId() + "' selected>"
						+ deri.get(i).getDescripcion() + "</option>";
			} else {
				html += "<option value='" + deri.get(i).getId() + "'>"
						+ deri.get(i).getDescripcion() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectMaterialesBobinas(List<Auxiliar> mat,
			String selected, String tipoBobina, String nombreSelect) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='bobinaConductores(this," + tipoBobina + ")'";
		html += "class='form-control'>";
		html += "<option value=''>Elija material</option>";
		for (int i = 0; i < mat.size(); i++) {
			if (mat.get(i).getAux9().equals(selected)) {
				html += "<option value='" + mat.get(i).getAux9()
						+ "' selected>" + mat.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + mat.get(i).getAux9() + "'>"
						+ mat.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectMaterialesPintura(List<Auxiliar> aux,
			String selected, String nombreSelect) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String dimensionesBobinas(double bbtDimRadial,
			double bbtDimAxial, double batDimRadial, double batDimAxial,
			double bteDimRadial, double bteDimAxial) {
		String html = "";
		html = "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='' name='' disabled ";
		html += "value='Dimensi�n axial' class='form-control'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_bbtAxial' name='val_bbtAxial' READONLY ";
		html += "value='" + bbtDimAxial + "' class='form-control' ";
		html += "placeholder='Dise�o..'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_batAxial' name='val_batAxial' READONLY ";
		html += "value='" + batDimAxial + "' class='form-control' ";
		html += "placeholder='Dise�o..'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_bteAxial' name='val_bteAxial' READONLY ";
		html += "value='" + bteDimAxial + "' class='form-control' ";
		html += "placeholder='Dise�o..'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='' name='' disabled ";
		html += "value='Dimensi�n radial' class='form-control'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_bbtRadial' name='val_bbtRadial' READONLY ";
		html += "value='" + bbtDimRadial + "' class='form-control' ";
		html += "placeholder='Dise�o..'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_batRadial' name='val_batRadial' READONLY ";
		html += "value='" + batDimRadial + "' class='form-control' ";
		html += "placeholder='Dise�o..'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_bteRadial' name='val_bteRadial' READONLY ";
		html += "value='" + bteDimRadial + "' class='form-control' ";
		html += "placeholder='Dise�o..'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		return html;
	}

	public static String selectMaterialesPlanchaFierro(List<Auxiliar> aux,
			String selected, String nombreSelect, int tipoEspesor) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='cambiarMaterialPinturaPlanchaFierro(this,"
				+ tipoEspesor + ")'";
		html += "class='form-control'>";
		html += "<option value=''>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String materialesPinturaPlanchaFierro(double val_esp_fondo,
			String val_cod_fondo, double val_esp_manto, String val_cod_manto,
			double val_esp_tapa, String val_cod_tapa,
			String materialesPinturaEspesorManto,
			String materialesPinturaEspesorTapa,
			String materialesPinturaEspesorFondo) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for=''> <input ";
		html += "type='text' id='' name='' disabled value='Espesor costado' ";
		html += "class='form-control' placeholder='C�digo..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_esp_manto'> <input ";
		html += "type='text' id='val_esp_manto' name='val_esp_manto' ";
		html += "value='" + val_esp_manto + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-1 control-label' for=''> <input ";
		html += "type='text' disabled value='mm' ";
		html += "class='form-control'>";
		html += "</label> <label class='col-md-2 control-label' for='val_cod_manto'> <input ";
		html += "type='text' id='val_cod_manto' name='val_cod_manto' readonly ";
		html += "value='" + val_cod_manto + "' class='form-control ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-3 control-label' for='val_material_pintura_plancha_fierro_manto'>";
		html += materialesPinturaEspesorManto + "</label>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for=''> <input ";
		html += "type='text' id='' name='' disabled value='Espesor tapa' ";
		html += "class='form-control' placeholder='C�digo..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_esp_tapa'> <input ";
		html += "type='text' id='val_esp_tapa' name='val_esp_tapa' ";
		html += "value='" + val_esp_tapa + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-1 control-label' for=''> <input ";
		html += "type='text' disabled value='mm' ";
		html += "class='form-control'>";
		html += "</label> <label class='col-md-2 control-label' for='val_cod_tapa'> <input ";
		html += "type='text' id='val_cod_tapa' name='val_cod_tapa' readonly ";
		html += "value='" + val_cod_tapa + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-3 control-label' for='val_material_pintura_plancha_fierro_tapa'>";
		html += materialesPinturaEspesorTapa + "</label>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for=''> <input ";
		html += "type='text' id='' name='' disabled value='Espesor fondo' ";
		html += "class='form-control'>";
		html += "</label> <label class='col-md-2 control-label' for='val_esp_fondo'> <input ";
		html += "type='text' id='val_esp_fondo' name='val_esp_fondo' ";
		html += "value='" + val_esp_fondo + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-1 control-label' for=''> <input ";
		html += "type='text' disabled value='mm' ";
		html += "class='form-control'>";
		html += "</label> <label class='col-md-2 control-label' for='val_cod_fondo'> <input ";
		html += "type='text' id='val_cod_fondo' name='val_cod_fondo' readonly ";
		html += "value='" + val_cod_fondo + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-3 control-label' for='val_material_pintura_plancha_fierro_fondo'>";
		html += materialesPinturaEspesorFondo + "</label>";
		html += "</div>";
		return html;
	}

	public static String selectMaterialesPresspanEntreCapasBbt(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='cambiarMaterialPresspanEntreCapasBbt(this,"
				+ tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String entreCapasBbt(double val_bbtEspesoresCapa1,
			String val_bbtCodCapa1, String val_bbtDescCapa1,
			double val_bbtEspesoresCapa2, String val_bbtCodCapa2,
			String val_bbtDescCapa2, double val_bbtEspesoresCapa3,
			String val_bbtCodCapa3, String val_bbtDescCapa3,
			String grillaPresspanBbt1, String grillaPresspanBbt2,
			String grillaPresspanBbt3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bbtEspesoresCapa1'> <input ";
		html += "type='text' id='val_bbtEspesoresCapa1' name='val_bbtEspesoresCapa1' ";
		html += "value='" + val_bbtEspesoresCapa1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_bbtCodCapa1'> <input ";
		html += "type='text' id='val_bbtCodCapa1' name='val_bbtCodCapa1' readonly ";
		html += "value='" + val_bbtCodCapa1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_bbtDescCapa1'> <input ";
		html += "type='text' id='val_bbtDescCapa1' name='val_bbtDescCapa1' readonly ";
		html += "value='" + val_bbtDescCapa1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBbt1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class=col-md-2 control-label' for='val_bbtEspesoresCapa2'> <input ";
		html += "type='text' id='val_bbtEspesoresCapa2' name='val_bbtEspesoresCapa2' ";
		html += "value='" + val_bbtEspesoresCapa2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_bbtCodCapa2'> <input ";
		html += "type='text' id='val_bbtCodCapa2' name='val_bbtCodCapa2' readonly ";
		html += "value='" + val_bbtCodCapa2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_bbtDescCapa2'> <input ";
		html += "type='text' id='val_bbtDescCapa2' name='val_bbtDescCapa2' readonly ";
		html += "value='" + val_bbtDescCapa2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBbt2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bbtEspesoresCapa3'> <input ";
		html += "type='text' id='val_bbtEspesoresCapa3' name='val_bbtEspesoresCapa3' ";
		html += "value='" + val_bbtEspesoresCapa3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_bbtCodCapa3'> <input ";
		html += "type='text' id='val_bbtCodCapa3' name='val_bbtCodCapa3' readonly ";
		html += "value='" + val_bbtCodCapa3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_bbtDescCapa3'> <input ";
		html += "type='text' id='val_bbtDescCapa3' name='val_bbtDescCapa3' readonly ";
		html += "value='" + val_bbtDescCapa3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBbt3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanAislacionExternaBbt(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='aislacionExternaBbt(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aislacionExternaBbt(int val_espesoresAtbt1,
			String val_codAtbt1, String val_descAtbt1, int val_espesoresAtbt2,
			String val_codAtbt2, String val_descAtbt2, int val_espesoresAtbt3,
			String val_codAtbt3, String val_descAtbt3,
			String aislacionExternaBbt1, String aislacionExternaBbt2,
			String aislacionExternaBbt3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_espesoresAtbt1'> <input ";
		html += "type='text' id='val_espesoresAtbt1' name='val_espesoresAtbt1' ";
		html += "value='" + val_espesoresAtbt1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_codAtbt1'> <input ";
		html += "type='text' id='val_codAtbt1' name='val_codAtbt1' readonly ";
		html += "value='" + val_codAtbt1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_descAtbt1'> <input ";
		html += "type='text' id='val_descAtbt1' name='val_descAtbt1' readonly ";
		html += "value='" + val_descAtbt1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBbt1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_espesoresAtbt2'> <input ";
		html += "type='text' id='val_espesoresAtbt2' name='val_espesoresAtbt2' ";
		html += "value='" + val_espesoresAtbt2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_codAtbt2'> <input ";
		html += "type='text' id='val_codAtbt2' name='val_codAtbt2' readonly ";
		html += "value='" + val_codAtbt2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_descAtbt2'> <input ";
		html += "type='text' id='val_descAtbt2' name='val_descAtbt2' readonly ";
		html += "value='" + val_descAtbt2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBbt2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_espesoresAtbt3'> <input ";
		html += "type='text' id='val_espesoresAtbt3' name='val_espesoresAtbt3' ";
		html += "value='" + val_espesoresAtbt3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_codAtbt3'> <input ";
		html += "type='text' id='val_codAtbt3' name='val_codAtbt3' readonly ";
		html += "value='" + val_codAtbt3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_descAtbt3'> <input ";
		html += "type='text' id='val_descAtbt3' name='val_descAtbt3' readonly ";
		html += "value='" + val_descAtbt3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBbt3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanAislacionBaseBbt(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='alislacionBaseBbt(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aislacionBasebbt(int val_espesoresBase1,
			String val_codBase1, String val_descBase1, int val_espesoresBase2,
			String val_codBase2, String val_descBase2, int val_espesoresBase3,
			String val_codBase3, String val_descBase3,
			String alislacionBaseBbt1, String alislacionBaseBbt2,
			String alislacionBaseBbt3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_espesoresBase1'> <input ";
		html += "type='text' id='val_espesoresBase1' name='val_espesoresBase1' ";
		html += "value='" + val_espesoresBase1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_codBase1'> <input ";
		html += "type='text' id='val_codBase1' name='val_codBase1' readonly ";
		html += "value='" + val_codBase1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_descBase1'> <input ";
		html += "type='text' id='val_descBase1' name='val_descBase1' readonly ";
		html += "value='" + val_descBase1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBbt1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_espesoresBase2'> <input ";
		html += "type='text' id='val_espesoresBase2' name='val_espesoresBase2' ";
		html += "value='" + val_espesoresBase2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_codBase2'> <input ";
		html += "type='text' id='val_codBase2' name='val_codBase2' readonly ";
		html += "value='" + val_codBase2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_descBase2'> <input ";
		html += "type='text' id='val_descBase2' name='val_descBase2' readonly ";
		html += "value='" + val_descBase2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBbt2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_espesoresBase3'> <input ";
		html += "type='text' id='val_espesoresBase3' name='val_espesoresBase3' ";
		html += "value='" + val_espesoresBase3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_codBase3'> <input ";
		html += "type='text' id='val_codBase3' name='val_codBase3' readonly ";
		html += "value='" + val_codBase3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_descBase3'> <input ";
		html += "type='text' id='val_descBase3' name='val_descBase3' readonly ";
		html += "value='" + val_descBase3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBbt3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanEntreCapasBat(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='cambiarMaterialPresspanEntreCapasBat(this,"
				+ tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String entreCapasBat(double val_batEspesoresCapa1,
			String val_batCodCapa1, String val_batDescCapa1,
			double val_batEspesoresCapa2, String val_batCodCapa2,
			String val_batDescCapa2, double val_batEspesoresCapa3,
			String val_batCodCapa3, String val_batDescCapa3,
			String grillaPresspanBat1, String grillaPresspanBat2,
			String grillaPresspanBat3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batEspesoresCapa1'> <input ";
		html += "type='text' id='val_batEspesoresCapa1' name='val_batEspesoresCapa1' ";
		html += "value='" + val_batEspesoresCapa1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batCodCapa1'> <input ";
		html += "type='text' id='val_batCodCapa1' name='val_batCodCapa1' readonly ";
		html += "value='" + val_batCodCapa1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batDescCapa1'> <input ";
		html += "type='text' id='val_batDescCapa1' name='val_batDescCapa1' readonly ";
		html += "value='" + val_batDescCapa1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBat1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class=col-md-2 control-label' for='val_batEspesoresCapa2'> <input ";
		html += "type='text' id='val_batEspesoresCapa2' name='val_batEspesoresCapa2' ";
		html += "value='" + val_batEspesoresCapa2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batCodCapa2'> <input ";
		html += "type='text' id='val_batCodCapa2' name='val_batCodCapa2' readonly ";
		html += "value='" + val_batCodCapa2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batDescCapa2'> <input ";
		html += "type='text' id='val_batDescCapa2' name='val_batDescCapa2' readonly ";
		html += "value='" + val_batDescCapa2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBat2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batEspesoresCapa3'> <input ";
		html += "type='text' id='val_batEspesoresCapa3' name='val_batEspesoresCapa3' ";
		html += "value='" + val_batEspesoresCapa3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batCodCapa3'> <input ";
		html += "type='text' id='val_batCodCapa3' name='val_batCodCapa3' readonly ";
		html += "value='" + val_batCodCapa3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batDescCapa3'> <input ";
		html += "type='text' id='val_batDescCapa3' name='val_batDescCapa3' readonly ";
		html += "value='" + val_batDescCapa3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBat3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanAislacionExternaBat(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='aislacionExternaBat(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aislacionExternaBat(int val_batespesoresAtbt1,
			String val_batcodAtbt1, String val_batdescAtbt1,
			int val_batespesoresAtbt2, String val_batcodAtbt2,
			String val_batdescAtbt2, int val_batespesoresAtbt3,
			String val_batcodAtbt3, String val_batdescAtbt3,
			String aislacionExternaBat1, String aislacionExternaBat2,
			String aislacionExternaBat3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batespesoresAtbt1'> <input ";
		html += "type='text' id='val_batespesoresAtbt1' name='val_batespesoresAtbt1' ";
		html += "value='" + val_batespesoresAtbt1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batcodAtbt1'> <input ";
		html += "type='text' id='val_batcodAtbt1' name='val_batcodAtbt1' readonly ";
		html += "value='" + val_batcodAtbt1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batdescAtbt1'> <input ";
		html += "type='text' id='val_batdescAtbt1' name='val_batdescAtbt1' readonly ";
		html += "value='" + val_batdescAtbt1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBat1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batespesoresAtbt2'> <input ";
		html += "type='text' id='val_batespesoresAtbt2' name='val_batespesoresAtbt2' ";
		html += "value='" + val_batespesoresAtbt2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batcodAtbt2'> <input ";
		html += "type='text' id='val_batcodAtbt2' name='val_batcodAtbt2' readonly ";
		html += "value='" + val_batcodAtbt2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batdescAtbt2'> <input ";
		html += "type='text' id='val_batdescAtbt2' name='val_batdescAtbt2' readonly ";
		html += "value='" + val_batdescAtbt2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBat2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batespesoresAtbt3'> <input ";
		html += "type='text' id='val_batespesoresAtbt3' name='val_batespesoresAtbt3' ";
		html += "value='" + val_batespesoresAtbt3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batcodAtbt3'> <input ";
		html += "type='text' id='val_batcodAtbt3' name='val_batcodAtbt3' readonly ";
		html += "value='" + val_batcodAtbt3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batdescAtbt3'> <input ";
		html += "type='text' id='val_batdescAtbt3' name='val_batdescAtbt3' readonly ";
		html += "value='" + val_batdescAtbt3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBat3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanAislacionBaseBat(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='alislacionBaseBat(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aislacionBasebat(int val_batespesoresBase1,
			String val_batcodBase1, String val_batdescBase1,
			int val_batespesoresBase2, String val_batcodBase2,
			String val_batdescBase2, int val_batespesoresBase3,
			String val_batcodBase3, String val_batdescBase3,
			String alislacionBaseBat1, String alislacionBaseBat2,
			String alislacionBaseBat3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batespesoresBase1'> <input ";
		html += "type='text' id='val_batespesoresBase1' name='val_batespesoresBase1' ";
		html += "value='" + val_batespesoresBase1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batcodBase1'> <input ";
		html += "type='text' id='val_batcodBase1' name='val_batcodBase1' readonly ";
		html += "value='" + val_batcodBase1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batdescBase1'> <input ";
		html += "type='text' id='val_batdescBase1' name='val_batdescBase1' readonly ";
		html += "value='" + val_batdescBase1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBat1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batespesoresBase2'> <input ";
		html += "type='text' id='val_batespesoresBase2' name='val_batespesoresBase2' ";
		html += "value='" + val_batespesoresBase2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batcodBase2'> <input ";
		html += "type='text' id='val_batcodBase2' name='val_batcodBase2' readonly ";
		html += "value='" + val_batcodBase2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batdescBase2'> <input ";
		html += "type='text' id='val_batdescBase2' name='val_batdescBase2' readonly ";
		html += "value='" + val_batdescBase2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBat2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_batespesoresBase3'> <input ";
		html += "type='text' id='val_batespesoresBase3' name='val_batespesoresBase3' ";
		html += "value='" + val_batespesoresBase3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_batcodBase3'> <input ";
		html += "type='text' id='val_batcodBase3' name='val_batcodBase3' readonly ";
		html += "value='" + val_batcodBase3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_batdescBase3'> <input ";
		html += "type='text' id='val_batdescBase3' name='val_batdescBase3' readonly ";
		html += "value='" + val_batdescBase3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBat3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanEntreCapasBte(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='entreCapasBte(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String entreCapasBte(double val_bteEspesoresCapa1,
			String val_bteCodCapa1, String val_bteDescCapa1,
			double val_bteEspesoresCapa2, String val_bteCodCapa2,
			String val_bteDescCapa2, double val_bteEspesoresCapa3,
			String val_bteCodCapa3, String val_bteDescCapa3,
			String grillaPresspanBte1, String grillaPresspanBte2,
			String grillaPresspanBte3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteEspesoresCapa1'> <input ";
		html += "type='text' id='val_bteEspesoresCapa1' name='val_bteEspesoresCapa1'  ";
		html += "value='" + val_bteEspesoresCapa1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_bteCodCapa1'> <input ";
		html += "type='text' id='val_bteCodCapa1' name='val_bteCodCapa1' readonly ";
		html += "value='" + val_bteCodCapa1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_bteDescCapa1'> <input ";
		html += "type='text' id='val_bteDescCapa1' name='val_bteDescCapa1' readonly ";
		html += "value='" + val_bteDescCapa1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBte1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class=col-md-2 control-label' for='val_bteEspesoresCapa2'> <input ";
		html += "type='text' id='val_bteEspesoresCapa2' name='val_bteEspesoresCapa2'  ";
		html += "value='" + val_bteEspesoresCapa2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_bteCodCapa2'> <input ";
		html += "type='text' id='val_bteCodCapa2' name='val_bteCodCapa2' readonly ";
		html += "value='" + val_bteCodCapa2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_bteDescCapa2'> <input ";
		html += "type='text' id='val_bteDescCapa2' name='val_bteDescCapa2' readonly ";
		html += "value='" + val_bteDescCapa2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBte2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteEspesoresCapa3'> <input ";
		html += "type='text' id='val_bteEspesoresCapa3' name='val_bteEspesoresCapa3' ";
		html += "value='" + val_bteEspesoresCapa3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_bteCodCapa3'> <input ";
		html += "type='text' id='val_bteCodCapa3' name='val_bteCodCapa3' readonly ";
		html += "value='" + val_bteCodCapa3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_bteDescCapa3'> <input ";
		html += "type='text' id='val_bteDescCapa3' name='val_bteDescCapa3' readonly ";
		html += "value='" + val_bteDescCapa3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ grillaPresspanBte3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanAislacionExternaBte(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='aislacionExternaBte(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aislacionExternaBte(int val_bteespesoresAtbt1,
			String val_btecodAtbt1, String val_btedescAtbt1,
			int val_bteespesoresAtbt2, String val_btecodAtbt2,
			String val_btedescAtbt2, int val_bteespesoresAtbt3,
			String val_btecodAtbt3, String val_btedescAtbt3,
			String aislacionExternaBte1, String aislacionExternaBte2,
			String aislacionExternaBte3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteespesoresAtbt1'> <input ";
		html += "type='text' id='val_bteespesoresAtbt1' name='val_bteespesoresAtbt1' ";
		html += "value='" + val_bteespesoresAtbt1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_btecodAtbt1'> <input ";
		html += "type='text' id='val_btecodAtbt1' name='val_btecodAtbt1' readonly ";
		html += "value='" + val_btecodAtbt1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_btedescAtbt1'> <input ";
		html += "type='text' id='val_btedescAtbt1' name='val_btedescAtbt1' readonly ";
		html += "value='" + val_btedescAtbt1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBte1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteespesoresAtbt2'> <input ";
		html += "type='text' id='val_bteespesoresAtbt2' name='val_bteespesoresAtbt2' ";
		html += "value='" + val_bteespesoresAtbt2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_btecodAtbt2'> <input ";
		html += "type='text' id='val_btecodAtbt2' name='val_btecodAtbt2' readonly ";
		html += "value='" + val_btecodAtbt2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_btedescAtbt2'> <input ";
		html += "type='text' id='val_btedescAtbt2' name='val_btedescAtbt2' readonly ";
		html += "value='" + val_btedescAtbt2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBte2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteespesoresAtbt3'> <input ";
		html += "type='text' id='val_bteespesoresAtbt3' name='val_bteespesoresAtbt3' ";
		html += "value='" + val_bteespesoresAtbt3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_btecodAtbt3'> <input ";
		html += "type='text' id='val_btecodAtbt3' name='val_btecodAtbt3' readonly ";
		html += "value='" + val_btecodAtbt3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_btedescAtbt3'> <input ";
		html += "type='text' id='val_btedescAtbt3' name='val_btedescAtbt3' readonly ";
		html += "value='" + val_btedescAtbt3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ aislacionExternaBte3;
		html += "</label></div>";
		return html;
	}

	public static String selectMaterialesPresspanAislacionBaseBte(
			List<Auxiliar> aux, String selected, String nombreSelect,
			int tipoPresspan) {
		String html = "";
		html += "<select id='" + nombreSelect + "' name='" + nombreSelect
				+ "' onchange='alislacionBaseBte(this," + tipoPresspan + ")'";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aislacionBasebte(int val_bteespesoresBase1,
			String val_btecodBase1, String val_btedescBase1,
			int val_bteespesoresBase2, String val_btecodBase2,
			String val_btedescBase2, int val_bteespesoresBase3,
			String val_btecodBase3, String val_btedescBase3,
			String alislacionBaseBte1, String alislacionBaseBte2,
			String alislacionBaseBte3) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteespesoresBase1'> <input ";
		html += "type='text' id='val_bteespesoresBase1' name='val_bteespesoresBase1' ";
		html += "value='" + val_bteespesoresBase1 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_btecodBase1'> <input ";
		html += "type='text' id='val_btecodBase1' name='val_btecodBase1' readonly ";
		html += "value='" + val_btecodBase1 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_btedescBase1'> <input ";
		html += "type='text' id='val_btedescBase1' name='val_btedescBase1' readonly ";
		html += "value='" + val_btedescBase1 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBte1;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteespesoresBase2'> <input ";
		html += "type='text' id='val_bteespesoresBase2' name='val_bteespesoresBase2' ";
		html += "value='" + val_bteespesoresBase2 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_btecodBase2'> <input ";
		html += "type='text' id='val_btecodBase2' name='val_btecodBase2' readonly ";
		html += "value='" + val_btecodBase2 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_btedescBase2'> <input ";
		html += "type='text' id='val_btedescBase2' name='val_btedescBase2' readonly ";
		html += "value='" + val_btedescBase2 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBte2;
		html += "</label></div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-2 control-label' for='val_bteespesoresBase3'> <input ";
		html += "type='text' id='val_bteespesoresBase3' name='val_bteespesoresBase3' ";
		html += "value='" + val_bteespesoresBase3 + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label> <label class='col-md-2 control-label' for='val_btecodBase3'> <input ";
		html += "type='text' id='val_btecodBase3' name='val_btecodBase3' readonly ";
		html += "value='" + val_btecodBase3 + "' class='form-control' ";
		html += "placeholder='C�digo..'>";
		html += "</label> <label class='col-md-4 control-label' for='val_btedescBase3'> <input ";
		html += "type='text' id='val_btedescBase3' name='val_btedescBase3' readonly ";
		html += "value='" + val_btedescBase3 + "' class='form-control' ";
		html += "placeholder='Descripci�n..'>";
		html += "</label><label class='col-md-4 control-label'>"
				+ alislacionBaseBte3;
		html += "</label></div>";
		return html;
	}

	public static String comboTipoRadiador(List<Auxiliar> aux, int selected) {
		String html = "";
		html += "<select id='comboTipoRadiador' name='comboTipoRadiador' onchange='comboTipoRadiador1()' ";
		html += "class='form-control'>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux1() == selected) {
				html += "<option value='" + aux.get(i).getAux1()
						+ "' selected>" + aux.get(i).getAux9() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux1() + "'>"
						+ aux.get(i).getAux9() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	private static String comboMaterialRadiador(List<Auxiliar> aux,
			String selected) {
		String html = "";
		html += "<select id='comboMaterialRadiador' name='comboMaterialRadiador' ";
		html += "class='form-control'>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String radiador(List<Auxiliar> auxTipo, int selTipo,
			List<Auxiliar> auxMaterial, String selMaterial, Radiador rad) {
		String html = "";
		if (rad.getCodTipoRadiador() != 0) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for=''>Material ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += comboMaterialRadiador(auxMaterial, selMaterial);
			html += "</div>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for='val_radiador_alto'>Alto ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += "<input type='text' id='val_radiador_alto' name='val_radiador_alto'  ";
			html += "value='" + rad.getAltoRadiador() + "' ";
			html += "class='form-control' placeholder='Alto..' readonly>";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for='val_radiador_ancho'>Ancho ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += "<input type='text' id='val_radiador_ancho' name='val_radiador_ancho'  ";
			html += "value='" + rad.getAnchoRadiador() + "' ";
			html += "class='form-control' placeholder='Ancho..' readonly>";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for='val_radiador_num'>N� Radiadores ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += "<input type='text' id='val_radiador_num' name='val_radiador_num'  ";
			html += "value='" + rad.getNumRadiadores() + "' ";
			html += "class='form-control' placeholder='Ancho..'>";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for='val_radiador_elementos'>N� Elementos ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += "<input type='text' id='val_radiador_elementos' name='val_radiador_elementos' ";
			html += "value='" + rad.getNumElementos() + "' ";
			html += "class='form-control' placeholder='N�num elemen..'>";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for='val_radiador_peso_elemento'>Peso elemento ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += "<input type='text' id='val_radiador_peso_elemento' name='val_radiador_peso_elemento'  ";
			html += "value='" + rad.getKilo() + "' ";
			html += "class='form-control' placeholder='Peso..' readonly>";
			html += "</div>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-4 control-label' for='val_radiador_cap_elemento'>Cap. elemento ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='col-md-6'>";
			html += "<div class='input-group'>";
			html += "<input type='text' id='val_radiador_cap_elemento' name='val_radiador_cap_elemento' ";
			html += "value='" + rad.getLitros() + "' ";
			html += "class='form-control' placeholder='Cap..' readonly>";
			html += "</div>";
			html += "</div>";
			html += "</div>";
		} else {
			html += "<input type='hidden' id='comboTipoRadiador' name='comboTipoRadiador' value='0'>";
			html += "<input type='hidden' id='comboMaterialRadiador' name='comboMaterialRadiador' value='0'>";
			html += "<input type='hidden' id='val_radiador_num' name='val_radiador_num' value='0'>";
			html += "<input type='hidden' id='val_radiador_elementos' name='val_radiador_elementos' value='0'>";
		}
		return html;
	}

	private static String comboAceroSilicoso(String nombre, List<Auxiliar> aux,
			String selected, int h) {
		String html = "";

		html += "<select id='" + nombre + "' name='" + nombre
				+ "' onchange=\"aceroSilicoso(this,'val_acesilicant" + h
				+ "','val_acesilipesonucleo" + h + "'," + h + ")\" ";
		html += "class='form-control'>";
		html += "<option value='0'>Elija material</option>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(i).getAux9()
						+ "' selected>" + aux.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux9() + "'>"
						+ aux.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String aceroSilicoso(List<Auxiliar> aux,
			NucleoFerreteria nucFerr, int i) {
		String html = "";
		if (i == 1) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for='val_acesilicant1'> ";
			html += comboAceroSilicoso("val_silicoso1", aux,
					nucFerr.getCodigoSilicoso1(), 1);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant1'> <input ";
			html += "type='text' id='val_acesilicant1' name='val_acesilicant1'  ";
			html += "value='" + nucFerr.getCantidad1()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo1'> <input ";
			html += "type='text' id='val_acesilipesonucleo1' name='val_acesilipesonucleo1'  ";
			html += "value='" + nucFerr.getPesoNucleo1()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 2) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso2", aux,
					nucFerr.getCodigoSilicoso2(), 2);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant2'> <input ";
			html += "type='text' id='val_acesilicant2' name='val_acesilicant2'  ";
			html += "value='" + nucFerr.getCantidad2()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo2'> <input ";
			html += "type='text' id='val_acesilipesonucleo2' name='val_acesilipesonucleo2'  ";
			html += "value='" + nucFerr.getPesoNucleo2()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 3) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso3", aux,
					nucFerr.getCodigoSilicoso3(), 3);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant3'> <input ";
			html += "type='text' id='val_acesilicant3' name='val_acesilicant3'  ";
			html += "value='" + nucFerr.getCantidad3()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo3'> <input ";
			html += "type='text' id='val_acesilipesonucleo3' name='val_acesilipesonucleo3'  ";
			html += "value='" + nucFerr.getPesoNucleo3()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 4) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso4", aux,
					nucFerr.getCodigoSilicoso4(), 4);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant4'> <input ";
			html += "type='text' id='val_acesilicant4' name='val_acesilicant4'  ";
			html += "value='" + nucFerr.getCantidad4()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo4'> <input ";
			html += "type='text' id='val_acesilipesonucleo4' name='val_acesilipesonucleo4'  ";
			html += "value='" + nucFerr.getPesoNucleo4()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 5) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso5", aux,
					nucFerr.getCodigoSilicoso5(), 5);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant5'> <input ";
			html += "type='text' id='val_acesilicant5' name='val_acesilicant5'  ";
			html += "value='" + nucFerr.getCantidad5()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo5'> <input ";
			html += "type='text' id='val_acesilipesonucleo5' name='val_acesilipesonucleo5'  ";
			html += "value='" + nucFerr.getPesoNucleo5()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 6) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso6", aux,
					nucFerr.getCodigoSilicoso6(), 6);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant6'> <input ";
			html += "type='text' id='val_acesilicant6' name='val_acesilicant6'  ";
			html += "value='" + nucFerr.getCantidad6()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo6'> <input ";
			html += "type='text' id='val_acesilipesonucleo6' name='val_acesilipesonucleo6'  ";
			html += "value='" + nucFerr.getPesoNucleo6()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 7) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso7", aux,
					nucFerr.getCodigoSilicoso7(), 7);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant7'> <input ";
			html += "type='text' id='val_acesilicant7' name='val_acesilicant7'  ";
			html += "value='" + nucFerr.getCantidad7()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo7'> <input ";
			html += "type='text' id='val_acesilipesonucleo7' name='val_acesilipesonucleo7'  ";
			html += "value='" + nucFerr.getPesoNucleo7()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 8) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso8", aux,
					nucFerr.getCodigoSilicoso8(), 8);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant8'> <input ";
			html += "type='text' id='val_acesilicant8' name='val_acesilicant8'  ";
			html += "value='" + nucFerr.getCantidad8()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo8'> <input ";
			html += "type='text' id='val_acesilipesonucleo8' name='val_acesilipesonucleo8'  ";
			html += "value='" + nucFerr.getPesoNucleo8()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 9) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso9", aux,
					nucFerr.getCodigoSilicoso9(), 9);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant9'> <input ";
			html += "type='text' id='val_acesilicant9' name='val_acesilicant9'  ";
			html += "value='" + nucFerr.getCantidad9()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo9'> <input ";
			html += "type='text' id='val_acesilipesonucleo9' name='val_acesilipesonucleo9'  ";
			html += "value='" + nucFerr.getPesoNucleo9()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		} else if (i == 10) {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for=''> ";
			html += comboAceroSilicoso("val_silicoso10", aux,
					nucFerr.getCodigoSilicoso10(), 10);
			html += "</label> <label class='col-md-3 control-label' for='val_acesilicant10'> <input ";
			html += "type='text' id='val_acesilicant10' name='val_acesilicant10'  ";
			html += "value='" + nucFerr.getCantidad10()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</label> <label class='col-md-3 control-label' for='val_acesilipesonucleo10'> <input ";
			html += "type='text' id='val_acesilipesonucleo10' name='val_acesilipesonucleo10'  ";
			html += "value='" + nucFerr.getPesoNucleo10()
					+ "' class='form-control' ";
			html += "placeholder='Peso n�cleo..'>";
			html += "</label>";
			html += "</div>";
		}
		return html;
	}

	public static String comboSoporte(List<Auxiliar> aux, int selected) {
		String html = "";
		for (int i = 0; i < aux.size(); i++) {
			html += "<div class='radio'>";
			html += "<label for='val_radioSoporte'> <input type='radio' ";
			html += "id='val_radioSoporte" + i + "' name='val_radioSoporte' ";
			if (aux.get(i).getAux1() == selected) {
				html += "value='" + aux.get(i).getAux1() + "' checked ";
			} else {
				html += "value='" + aux.get(i).getAux1() + "' ";
			}
			html += ">";
			html += aux.get(i).getAux9();
			html += "</label>";
			html += "</div>";
		}
		return html;
	}

	public static String comboTipoNucleo(List<Auxiliar> aux, int selected) {
		String html = "";
		html += "<select id='val_tipoNucleo' name='val_tipoNucleo' onchange='tipoNucleo(this)' ";
		html += "class='form-control'>";
		for (int i = 0; i < aux.size(); i++) {
			if (aux.get(i).getAux21() == selected) {
				html += "<option value='" + aux.get(i).getAux21()
						+ "' selected>" + aux.get(i).getAux9() + "</option>";
			} else {
				html += "<option value='" + aux.get(i).getAux21() + "'>"
						+ aux.get(i).getAux9() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String nucleo(List<Auxiliar> aux, NucleoFerreteria nucFerr) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='val_tipoNucleo'>Tipo ";
		html += "<span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='input-group'>";
		html += comboTipoNucleo(aux, nucFerr.getTipo());
		html += "</div>";
		html += "</div>";

		if (nucFerr.getTipo() == '1') {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for='val_nucferr_anchoCabezal'>Ancho (cabezal) ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='input-group'><input ";
			html += "type='text' id='val_nucferr_anchoCabezal' name='val_nucferr_anchoCabezal'  ";
			html += "value='" + nucFerr.getChapaMayorNucleo()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "</div>";
			html += "</div>";

			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for='val_nucferr_largoVentana'>Largo (ventana) ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='input-group'><input ";
			html += "type='text' id='val_nucferr_largoVentana' name='val_nucferr_largoVentana'  ";
			html += "value='" + nucFerr.getApilamientoChapas()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "<input type='hidden' id='val_nucferr_diametro' name='val_nucferr_diametro' value='0'>";
			html += "</div>";
			html += "</div>";
		}
		if (nucFerr.getTipo() == '0') {
			html += "<div class='form-group'>";
			html += "<label class='col-md-6 control-label' for='val_nucferr_diametro'>Di�metro ";
			html += "<span class='text-danger'>*</span>";
			html += "</label>";
			html += "<div class='input-group'><input ";
			html += "type='text' id='val_nucferr_diametro' name='val_nucferr_diametro' ";
			html += "value='" + nucFerr.getDiametroNucleo()
					+ "' class='form-control' ";
			html += "placeholder='Cant..'>";
			html += "<input type='hidden' id='val_nucferr_anchoCabezal' name='val_nucferr_anchoCabezal' value='0'>";
			html += "<input type='hidden' id='val_nucferr_largoVentana' name='val_nucferr_largoVentana' value='0'>";
			html += "</div>";
			html += "</div>";

		}
		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='val_entreCentroNucleo'>Entrecentro n�cleo ";
		html += "<span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='input-group'><input ";
		html += "type='text' id='val_entreCentroNucleo' name='val_entreCentroNucleo' ";
		html += "value='" + nucFerr.getEntreCentroNucleo()
				+ "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='val_ventana_nucleo'>Alto ventana";
		html += "<span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='input-group'><input ";
		html += "type='text' id='val_ventana_nucleo' name='val_ventana_nucleo'  ";
		html += "value='" + nucFerr.getVentanaNucleo()
				+ "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</div>";
		html += "</div>";

		if (nucFerr.getTipo() == '1') {
			html += "<div class='form-group'>";
			html += "<label class='col-md-3 control-label'>Corte</label>";
			html += "<div class='col-md-6'>";
			html += "<label class='radio-inline' for='val_corte45'>";
			html += "<input type='radio' id='val_corte45' name='val_corte' value='0' disabled> 45�";
			html += "</label>";
			html += "<label class='radio-inline' for='val_corte90'>";
			html += "<input type='radio' id='val_corte90' name='val_corte' value='1' checked readonly> 90�";
			html += "</label>";
			html += "</div>";
			html += "</div>";
		}
		if (nucFerr.getTipo() == '0') {
			html += "<div class='form-group'>";
			html += "<label class='col-md-3 control-label'>Corte</label>";
			html += "<div class='col-md-6'>";
			html += "<label class='radio-inline' for='val_corte45'>";
			html += "<input type='radio' id='val_corte45' name='val_corte' value='0' checked readonly> 45�";
			html += "</label>";
			html += "<label class='radio-inline' for='val_corte90'>";
			html += "<input type='radio' id='val_corte90' name='val_corte' value='1' disabled> 90�";
			html += "</label>";
			html += "</div>";
			html += "</div>";
		}
		return html;
	}

	private static String comboPinturaEstanque(List<Auxiliar> aux,
			String selected, int i) {
		String html = "";

		html += "<select id='comboPinturaEstanque" + i
				+ "' name='comboPinturaEstanque" + i
				+ "' onchange='cambiarMaterialPinturaEstanque(this," + i
				+ ")' ";
		html += "class='form-control'>";
		html += "<option value=''>Elija material</option>";
		for (int h = 0; h < aux.size(); h++) {
			if (aux.get(h).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(h).getAux9()
						+ "' selected>" + aux.get(h).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(h).getAux9() + "'>"
						+ aux.get(h).getAux10() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String pinturaEstanque(List<Auxiliar> aux,
			EstanqueTapa estTapa) {
		String html = "";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label'> ";
		html += "Interior";
		html += "</label> <label class='col-md-3 control-label'> &nbsp;";
		html += "</label>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='comboPinturaEstanque1'> ";
		html += comboPinturaEstanque(aux, estTapa.getPin1(), 1);
		html += "</label> <label class='col-md-3 control-label' for='val_codPin1'> <input ";
		html += "type='text' id='val_codPin1' name='val_codPin1' disabled ";
		html += "value='" + estTapa.getPin1() + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='comboPinturaEstanque2'> ";
		html += comboPinturaEstanque(aux, estTapa.getPin2(), 2);
		html += "</label> <label class='col-md-3 control-label' for='val_codPin2'> <input ";
		html += "type='text' id='val_codPin2' name='val_codPin2' disabled ";
		html += "value='" + estTapa.getPin2() + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label'> ";
		html += "Exterior";
		html += "</label> <label class='col-md-3 control-label'> &nbsp;";
		html += "</label>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='comboPinturaEstanque3'> ";
		html += comboPinturaEstanque(aux, estTapa.getPin3(), 3);
		html += "</label> <label class='col-md-3 control-label' for='val_codPin3'> <input ";
		html += "type='text' id='val_codPin3' name='val_codPin3' disabled ";
		html += "value='" + estTapa.getPin3() + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-6 control-label' for='comboPinturaEstanque4'> ";
		html += comboPinturaEstanque(aux, estTapa.getPin4(), 4);
		html += "</label> <label class='col-md-3 control-label' for='val_codPin4'> <input ";
		html += "type='text' id='val_codPin4' name='val_codPin4' disabled ";
		html += "value='" + estTapa.getPin4() + "' class='form-control' ";
		html += "placeholder='Cant..'>";
		html += "</label>";
		html += "</div>";
		return html;
	}

	public static String disenoBusquedaDataTable(List<Auxiliar> disenos) {
		String html = "";
		for (int i = 0; i < disenos.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'><a href='ingenieria-home?val_diseno="
					+ disenos.get(i).getAux9()
					+ "'>"
					+ disenos.get(i).getAux9() + "</a></td> ";
			html += "<td class='text-center'>" + disenos.get(i).getAux10()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux1()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux28()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux29()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux30()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux2()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux11()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux31()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux3()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux12()
					+ "</td>";
			html += "<td class='text-center'>" + disenos.get(i).getAux13()
					+ "</td>";
			html += "</tr>";
		}
		return html;
	}

	public static String theadDiseno(String grillaDistribucion,
			String grillaRefrigerante, String grillaConductoresBbt,
			String grillaConductoresBat, String val_diseno, String val_codigo,
			String val_potencia, String val_kv, String val_voltaje_primario,
			String val_voltaje_secundario, String val_impedancia,
			String val_altura_instalacion) {
		String html = "";
		html += "<form id='form-validation' action='ingenieria-buscar-disenos-datatable' method='post' ";
		html += "class='form-horizontal'>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_diseno>Dise�o ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_diseno' name='val_diseno' ";
		html += "class='form-control' placeholder='Dise�o..' value='"
				+ val_diseno + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_codigo>C�digo ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_codigo' name='val_codigo' ";
		html += "class='form-control' placeholder='C�digo..' value='"
				+ val_codigo + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_distribuciones_all>Clasificaci�n ";
		html += "</label>";
		html += "<div class='col-md-5'>" + grillaDistribucion + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_potencia>Potencia ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_potencia' name='val_potencia' ";
		html += "class='form-control' placeholder='Potencia..' value='"
				+ val_potencia + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_kv>Kv </label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_kv' name='val_kv' class='form-control' placeholder='Kv..' value='"
				+ val_kv + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_refrigerante>Refrigerante";
		html += "</label>";
		html += "<div class='col-md-5'>" + grillaRefrigerante + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_voltaje_primario>Voltaje	primario </label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_voltaje_primario' ";
		html += "name='val_voltaje_primario' class='form-control'  ";
		html += "placeholder='Vol. primario..' value=" + val_voltaje_primario
				+ " autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_voltaje_secundario>Voltaje secundario </label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_voltaje_secundario' ";
		html += "name='val_voltaje_secundario' class='form-control' ";
		html += "placeholder='Vol. secundario..' value='"
				+ val_voltaje_secundario + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_impedancia>Impedancia</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_impedancia' name='val_impedancia' ";
		html += "class='form-control' placeholder='Impedancia..' value="
				+ val_impedancia + " autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_altura_instalacion>Altura instalaci�n </label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_altura_instalacion' ";
		html += "name='val_altura_instalacion' class='form-control' ";
		html += "placeholder='Alt. instalaci�n..' value='"
				+ val_altura_instalacion + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_conductor_bbt>Conductor bobina BT </label>";
		html += "<div class='col-md-5'>" + grillaConductoresBbt + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_conductor_bat>Conductor bobina AT </label>";
		html += "<div class='col-md-5'>" + grillaConductoresBat + "</div>";
		html += "</div>";
		html += "<div class='form-group form-actions'>";
		html += "<div class='col-md-8 col-md-offset-4'>";
		html += "<button type='submit' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Buscar </button>";
		html += "</div>";
		html += "</div>";
		html += "</form>";
		return html;
	}

	public static String accesoriosDataTable(List<Accesorio> accesorios) {
		String html = "";

		html += "<div class='table-responsive'>";
		html += "<table id='accesorio-datatable' ";
		html += "class='table table-vcenter table-condensed table-bordered'>";
		html += "<thead>";
		html += "<tr>";
		html += "<th class='text-center' data-toggle='tooltip' title='C�digo'>C�digo</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Descripci�n'>Descripci�n</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Cantidad'>Cantidad</th>";
		html += "</tr>";
		html += "</thead>";
		html += "<tbody>";
		for (int i = 0; i < accesorios.size(); i++) {
			html += "<tr>";

			html += "<td class='text-center'><a href='#' onclick=\"accesoriosModificar('"
					+ accesorios.get(i).getCodigo()
					+ "','"
					+ accesorios.get(i).getDescripcion()
					+ "', "
					+ accesorios.get(i).getCantidad()
					+ ")\">"
					+ accesorios.get(i).getCodigo();
			html += "</a></td>";
			html += "<td class='text-center'>"
					+ accesorios.get(i).getDescripcion() + "</td>";
			html += "<td class='text-center'>"
					+ accesorios.get(i).getCantidad() + "</td>";
			html += "</tr>";
		}
		html += "</tbody>";
		html += "</table>";
		html += "</div>";

		return html;
	}

	public static String modificarListAccesorios(String codigo,
			String descripcion, String cantidad, String nombreJavaScript,
			String boton, int valueHidden) {
		String html = "";
		html += "<form action='#' method='post' class='form-inline'>";
		html += "<div class='form-group'>";
		html += "<label class='col-sm-6 control-label' for='val_codigo_accesorio'>C�digo</label>";
		html += "<input type='text' id='val_codigo_accesorio' ";
		html += "name='val_codigo_accesorio' class='form-control' value='"
				+ codigo + "' ";
		html += "placeholder='C�digo..'>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-sm-6 control-label' for='val_descri_accesorio'>Descripci�n</label>";
		html += "<input type='text' id='val_descri_accesorio' ";
		html += "name='val_descri_accesorio' class='form-control' value='"
				+ descripcion + "' ";
		html += "placeholder='Descripci�n..'>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-sm-6 control-label' ";
		html += "for='val_cantidad_accesorio'>Cantidad</label> <input ";
		html += "type='number' id='val_cantidad_accesorio' ";
		html += "name='val_cantidad_accesorio' onfocus='obtenerCodigo()' value='"
				+ cantidad + "' ";
		html += "class='form-control' placeholder='Cantidad..'>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<button type='button' class='btn btn-primary' onclick='"
				+ nombreJavaScript + "()'>";
		html += "<i class='fa fa-user'></i> " + boton + " </button>";
		html += "<button type='button' class='btn btn-primary' onclick=\"accesoriosDelete('"
				+ codigo + "')\">";
		html += "<i class='fa fa-user'></i> Delete </button>";
		html += "<button type='button' class='btn btn-primary' onclick='accesoriosLimpiar()'>";
		html += "<i class='fa fa-user'></i> Limpiar </button>";
		html += "</div>";
		html += "<input type='hidden' id='modificarAccesorio' name='modificarAccesorio' value='"
				+ valueHidden + "'>";
		html += "</form>";
		return html;
	}

	public static String comboTablaC(List<Auxiliar> aux, String nombre,
			String tipo, String selected) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<select id='" + nombre + "' name='" + nombre + "' ";
		html += "class='form-control' onchange='codigoTrafoCoti()'>";
		html += "<option value=''>" + tipo + "</option>";
		for (int h = 0; h < aux.size(); h++) {
			if (aux.get(h).getAux9().equals(selected)) {
				html += "<option value='" + aux.get(h).getAux9()
						+ "' selected>" + aux.get(h).getAux10() + "</option>";
			} else {
				html += "<option value='" + aux.get(h).getAux9() + "'>"
						+ aux.get(h).getAux10() + "</option>";
			}
		}
		html += "</select>";
		html += "</div>";

		return html;
	}

	public static String familia(String selected, String campo2, String combo1) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<select id='combo1' name='combo1' class='form-control' onchange='codigoTrafoCoti()'>";
		html += "<option value=''>Familia</option>";
		if (!campo2.equals("2")) {
			if (combo1.equals("1")) {
				html += "<option value='1' selected>Aereo (aceite o silicona); Ventilado (seco)</option>";
			} else {
				html += "<option value='1'>Aereo (aceite o silicona); Ventilado (seco)</option>";
			}
			/*
			 * if (campo2.equals("2")) { html +=
			 * "<option value='2' selected>2 Elementos (ECM)</option>"; } else {
			 * html += "<option value='2'>2 Elementos (ECM)</option>"; } if
			 * (campo2.equals("3")) { html +=
			 * "<option value='3' selected>3 Elementos (ECM)</option>"; } else {
			 * html += "<option value='3'>3 Elementos (ECM)</option>"; }
			 */
			if (combo1.equals("4")) {
				html += "<option value='4' selected>Superficie (Pad Mounted)</option>";
			} else {
				html += "<option value='4'>Superficie (Pad Mounted)</option>";
			}
			if (combo1.equals("5")) {
				html += "<option value='5' selected>Tipo Subestaci�n Unitaria</option>";
			} else {
				html += "<option value='5'>Tipo Subestaci�n Unitaria</option>";
			}
			if (combo1.equals("6")) {
				html += "<option value='6' selected>Sumergible (aceite o silicona); No Ventilado (seco)</option>";
			} else {
				html += "<option value='6'>Sumergible (aceite o silicona); No Ventilado (seco)</option>";
			}
		} else {
			if (combo1.equals("2")) {
				html += "<option value='2' selected>2 o 3 Elementos (ECM)</option>";
			} else {
				html += "<option value='2'>2 o 3 Elementos (ECM)</option>";
			}
		}

		html += "</select>";
		html += "</div>";
		return html;
	}

	public static String codigoTrafoCotiGenerar(String val, String diseno,
			String seAc) {
		String html = "";
		if (seAc.equals("1")) {
			html += "<form id='form-validation' action='ingenieria-insertar-diseno-seco' method='post' ";
		}
		if (seAc.equals("0")) {
			html += "<form id='form-validation' action='ingenieria-insertar-diseno-aceite' method='post' ";
		}
		html += "<div class='form-group'>";
		html += "<input type='text' id='val_codigo_equicotiGene' ";
		html += "name='val_codigo_equicotiGene' value='" + val
				+ "' class='form-control' ";
		html += "placeholder='Dise�o..' readonly>";
		html += "<button type='submit' class='btn btn-primary'>";
		html += "<i class='fa fa-user'></i> Guardar dise�o </button>";
		html += "</div>";
		html += "<input type='hidden' id='diseno2' name='diseno2' value='"
				+ diseno + "'>";
		html += "</form>";
		return html;
	}

	public static String comboDescripcionPlano(List<Auxiliar> aux,
			String nombre, int selected) {
		String html = "";
		html += "<select id='" + nombre + "' name='" + nombre + "' ";
		html += "class='form-control'>";
		html += "<option value='0'>Elija Descripci�n</option>";
		for (int h = 0; h < aux.size(); h++) {
			if (selected == aux.get(h).getAux1()) {
				html += "<option value='" + aux.get(h).getAux1()
						+ "' selected>" + aux.get(h).getAux9() + "</option>";
			} else {
				html += "<option value='" + aux.get(h).getAux1() + "'>"
						+ aux.get(h).getAux9() + "</option>";
			}

		}
		html += "</select>";
		return html;
	}

	private static String descripcionPlano(List<Auxiliar> aux, int selected) {
		String html = "";
		for (int h = 0; h < aux.size(); h++) {
			if (selected == aux.get(h).getAux1()) {
				html += aux.get(h).getAux9();
			} else {
				html += "";
			}
		}

		return html;
	}

	public static String detalleDocApliPlanos(List<DocAplicableDet> dets,
			List<Auxiliar> auxs) {
		String html = "";
		html += "<div class='table-responsive'>";
		html += "<table id='example-datatable' ";
		html += "class='table table-vcenter table-condensed table-bordered'>";
		html += "<thead>";
		html += "<tr>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Id del documento'>Id</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Nombre documento'>Nombre documento</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Nombre del plano'>Nombre del plano</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='N�mero de revisi�n'>N� Rev.</th>";
		html += "</tr>";
		html += "</thead>";
		html += "<tbody>";
		for (int i = 0; i < dets.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'><a href='#' onclick=\"marcarParaActualizarDocApliDet("
					+ dets.get(i).getId() + "," + dets.get(i).getDescPlano()
					+ ",'" + dets.get(i).getIdPlano() + "',"
					+ dets.get(i).getNumero() + ")\">" + dets.get(i).getId()
					+ "</a></td>";
			html += "<td class='text-center'>" + descripcionPlano(auxs, dets.get(i).getDescPlano())
					+ "</td>";
				html += "<td class='text-center'><a href='ingenieria-doc-apli-cargarArchivos?val_id_plano="
						+ dets.get(i).getIdPlano() + "&val_revision="+dets.get(i).getRevision()+"' target='_blank'>"
						+ dets.get(i).getIdPlano() + "</td>";
			html += "<td class='text-center'>" + dets.get(i).getRevision() + "</td>";
			html += "</tr>";
		}
		html += "</tbody>";
		html += "</table>";
		html += "</div>";

		return html;
	}

	public static String buscarDocAplicable(String val_cod_costo, String val_np) {
		String html = "";
		html += "<form id='form-validation' action='ingenieria-encontrar-doc-aplicable' method='post' ";
		html += "class='form-horizontal'>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_cod_costo'>C�digo de costo ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_cod_costo' name='val_cod_costo' ";
		html += "class='form-control' placeholder='C�digo de costo..' value='"
				+ val_cod_costo + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_np'>Np ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_np' name='val_np' ";
		html += "class='form-control' placeholder='Np..' value='" + val_np
				+ "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group form-actions'>";
		html += "<div class='col-md-8 col-md-offset-4'>";
		html += "<button type='submit' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Buscar </button>";
		html += "</div>";
		html += "</div>";
		html += "</form>";
		return html;
	}

	public static String detalleDocApliEnc(List<DocAplicableEnc> encs) {
		String html = "";
		for (int i = 0; i < encs.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'><a href='ingenieria-documentos-aplicables-actualizar?val_numero="
					+ encs.get(i).getNumero();
			html += "&val_codigo_costo=" + encs.get(i).getDisenoTrafo();
			html += "&val_diseno_electrico=" + encs.get(i).getDisenoElectrico();
			html += "&val_diseno_mecanico=" + encs.get(i).getDisenoMecanico();
			html += "&val_np=" + encs.get(i).getNp();
			html += "&val_item=" + encs.get(i).getItem();
			html += "'>" + encs.get(i).getNumero() + "</a></td>";
			html += "<td class='text-center'>" + encs.get(i).getNp() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getItem() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getDisenoElectrico() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getDisenoMecanico() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getDisenoTrafo() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getFechaIngreso() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getUsuario() + "</td>";
			html += "</tr>";
		}
		return html;
	}

	public static String cabeceraDocAplicableEnc(DocAplicableEnc enc) {
		String html = "";
		html += "<div class='row'>";
		html += "<fieldset>";
		html += "<legend>";
		html += "INGRESAR DOCUMENTO APLICABLE &nbsp;&nbsp;&nbsp;&nbsp;";
		html += "<button type='button' class='btn btn-sm btn-primary' ";
		html += "onclick='guardarDocAplicables()'>";
		html += "<i class='fa fa-angle-right'></i> Guardar</button>";
		html += "&nbsp;&nbsp;&nbsp;&nbsp;";
		html += "<a href='ingenieria-busqueda-doc-aplicable'>Buscar documento aplicable</a>";
		html += "</legend>";
		html += "</fieldset>";
		html += "<form id='form-validation' action='#' method='post' ";
		html += "class='form-horizontal form-bordered'>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_diseno' name='val_diseno' ";
		html += "class='form-control' placeholder='C�digo de costo..' value='"
				+ enc.getDisenoTrafo() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_np' name='val_np' ";
		if (enc.getNp() > 0) {
			html += "class='form-control' placeholder='Np ej: 77452..' value='"
					+ enc.getNp() + "'>";
		} else {
			html += "class='form-control' placeholder='Np ej: 77452..'>";
		}
		html += "<input type='text' id='val_item' name='val_item' ";
		html += "class='form-control col-md-1' placeholder='Item ej: 1-2-8..' value='"
				+ enc.getItem() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_diseno_elec' ";
		html += "name='val_diseno_elec' class='form-control' ";
		html += "placeholder='Dise�o el�ctrico..' value='"
				+ enc.getDisenoElectrico() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_diseno_mec' name='val_diseno_mec' ";
		html += "class='form-control' placeholder='Diseno mec�nico..' value='"
				+ enc.getDisenoMecanico() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</form>";
		html += "</div>";
		return html;
	}

	public static String agregarPlanos(String comboDescripcionPlano,
			String planosearch, String javascript, String nomBoton, int id,
			int numero) {
		String html = "";
		html += "<form id='form-validation' action='ingenieria-actualizar-doc-apli-det1' method='post' ";
		html += "class='form-horizontal form-bordered'>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>" + comboDescripcionPlano + "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='planosearch' ";
		html += "name='planosearch' placeholder='buscar plano..' required value='"
				+ planosearch + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		if (nomBoton.equals("Agregar")) {
			html += "<button type='button' class='btn btn-sm btn-primary' ";
			html += "onclick='" + javascript + "'>";
			html += "<i class='fa fa-angle-right'></i> " + nomBoton
					+ " </button>";
		}
		if (nomBoton.equals("Actualizar")) {
			html += "<button type='submit' class='btn btn-sm btn-primary'>";
			html += "<i class='fa fa-angle-right'></i> " + nomBoton
					+ " </button>";
		}
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<input type='hidden' id='idDet' name='idDet' value='" + id
				+ "'>";
		html += "<input type='hidden' id='num' name='num' value='" + numero
				+ "'>";
		html += "</form>";
		return html;
	}

	public static String administracionUsuariosIngenieria(List<Usuario> per,
			List<Auxiliar> auxs) {
		String html = "";
		html += "<div class='tab-pane active' id='search-tab-administrar'>";
		html += "<div class='table-responsive'>";
		html += "<table id='example-datatable' class='table table-vcenter table-condensed table-bordered'>";
		html += "<thead>";
		html += "<tr>";
		html += "<th class='text-center' data-toggle='tooltip'>Usuario</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Administrar el �rea de ingenier�a'>Admin</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Ingresar y modificar documentos aplicables'>Ing doc Apli</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Ingresar planos'>Ing Planos</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Cubicar'>Cubi</th>";
		html += "</tr>";
		html += "</thead>";
		html += "<tbody>";
		for (int i = 0; i < per.size(); i++) {
			html += "<tr>";
			html += "<td>" + per.get(i).getNombre() + "</td>";
			html += "<td class='text-center'><input type='checkbox' onclick=\"actualizarPermisos(this,'ING_ADMIN','"
					+ per.get(i).getUsuario()
					+ "')\" id='ingAdminDocApli"
					+ i
					+ "' name='ingAdminDocApli"
					+ i
					+ "' "
					+ HelperIngenieria.checked(auxs, per.get(i).getUsuario(),
							"ING_ADMIN") + "></td>";
			html += "<td class='text-center'><input type='checkbox' onclick=\"actualizarPermisos(this,'ING_DOC_APLI','"
					+ per.get(i).getUsuario()
					+ "')\" id='ingDocApli"
					+ i
					+ "' name='ingDocApli"
					+ i
					+ "' "
					+ HelperIngenieria.checked(auxs, per.get(i).getUsuario(),
							"ING_DOC_APLI") + "></td>";
			html += "<td class='text-center'><input type='checkbox' onclick=\"actualizarPermisos(this,'ING_PLANOS','"
					+ per.get(i).getUsuario()
					+ "')\" id='ingPlanos"
					+ i
					+ "' name='ingPlanos"
					+ i
					+ "' "
					+ HelperIngenieria.checked(auxs, per.get(i).getUsuario(),
							"ING_PLANOS") + "></td>";
			html += "<td class='text-center'><input type='checkbox' onclick=\"actualizarPermisos(this,'ING_CUBICAR','"
					+ per.get(i).getUsuario()
					+ "')\" id='cubicar"
					+ i
					+ "' name='cubicar"
					+ i
					+ "' "
					+ HelperIngenieria.checked(auxs, per.get(i).getUsuario(),
							"ING_CUBICAR") + "></td>";
			html += "</tr>";
		}
		html += "</tbody>";
		html += "</table>";
		html += "</div>";
		html += "</div>";
		return html;
	}
	
	/*public static String selectSubAreas(List<Auxiliar> subAreasProduccion) {
		String html = "";
		
		html += "<select id='val_sub_areas' name='val_sub_areas' ";
		html += "class='form-control'>";
		html += "<option value='0'>Elija Sub �rea</option>";
		for (int i = 0; i < subAreasProduccion.size(); i++) {
			html += "<option value='" + subAreasProduccion.get(i).getAux1()
						+ "'>" + subAreasProduccion.get(i).getAux9() + "</option>";
		}
		html += "</select>";

		return html;
	}*/
}
