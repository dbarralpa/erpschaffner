package com.schaffner.manager.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.schaffner.modelo.ingenieria.Costo;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class CubicacionDataSource extends JRAbstractBeanDataSourceProvider {

	private List<Costo> costos;
	

	public CubicacionDataSource() {
		super(Costo.class);
	}

	public JRDataSource create(JasperReport jrReport) throws JRException {
		costos = new ArrayList<Costo>();
		return new JRBeanCollectionDataSource(costos);
	}


	public void dispose(JRDataSource jrds) throws JRException {
		costos.clear();
		costos= null;
	}
	
	public JRDataSource llenarLista(List<Costo> costos1){
		Collections.sort(costos1, new Ordenamiento());
		this.costos = costos1;
		/*for(int i=0;i<costos.size();i++){
			
			this.costos.add(new Costo(det.get(i).getTag(), det.get(i).getDescripcion(), det.get(i).getCantidad(), det.get(i).getCostoTotalMoneda()));
		}*/
		return new JRBeanCollectionDataSource(this.costos);
	}

}
