package com.schaffner.manager.util;

import java.util.ArrayList;
import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Persona;
import com.schaffner.modelo.ProyectoComercialTransformador;

public class HelperComercial {
	public static float cantMargen(List<Auxiliar> margenes, String idMargen) {
		float val = 0f;
		for (int i = 0; i < margenes.size(); i++) {
			if (margenes.get(i).getAux9().equals(idMargen)) {
				val = margenes.get(i).getAux1() / (float) 100;
			}
		}
		return val;
	}

	public static int precioUniPeso(List<Auxiliar> parametros) {
		int val = 0;
		for (int i = 0; i < parametros.size(); i++) {
			if (parametros.get(i).getAux2() == 3) {
				val = parametros.get(i).getAux1();
			}
		}
		return val;
	}

	public static int precioUniUf(List<Auxiliar> parametros) {
		int val = 0;
		for (int i = 0; i < parametros.size(); i++) {
			if (parametros.get(i).getAux2() == 5) {
				val = parametros.get(i).getAux1();
			}
		}
		return val;
	}

	public static Persona vendedor(List<Persona> personas, int id) {
		Persona per = new Persona();
		for (int i = 0; i < personas.size(); i++) {
			if (id == personas.get(i).getId()) {
				per = personas.get(i);
			}
		}
		return per;
	}

	public static ProyectoComercialTransformador proyectoTrafo(
			List<ProyectoComercialTransformador> trafos, int idDet) {
		ProyectoComercialTransformador tra = new ProyectoComercialTransformador();
		for (int i = 0; i < trafos.size(); i++) {
			if (idDet == trafos.get(i).getIdProyectoVentaDet()) {
				tra = trafos.get(i);
				break;
			}
		}
		return tra;
	}

	public static List<Auxiliar> contactos(Auxiliar conts) {
		ArrayList<Auxiliar> contactos = new ArrayList<Auxiliar>();
		if (conts.getAux9() != null) {
			Auxiliar aux = new Auxiliar();
			aux.setAux9(conts.getAux9());
			aux.setAux10(conts.getAux10());
			contactos.add(aux);
		}

		if (conts.getAux11() != null) {
			Auxiliar aux1 = new Auxiliar();
			aux1.setAux9(conts.getAux11());
			aux1.setAux10(conts.getAux12());
			contactos.add(aux1);
		}
		if (conts.getAux13() != null) {
			Auxiliar aux2 = new Auxiliar();
			aux2.setAux9(conts.getAux13());
			aux2.setAux10(conts.getAux14());
			contactos.add(aux2);
		}
		if (conts.getAux15() != null) {
			Auxiliar aux3 = new Auxiliar();
			aux3.setAux9(conts.getAux15());
			aux3.setAux10(conts.getAux16());
			contactos.add(aux3);
		}
		if (conts.getAux17() != null) {
			Auxiliar aux4 = new Auxiliar();
			aux4.setAux9(conts.getAux17());
			aux4.setAux10(conts.getAux18());
			contactos.add(aux4);
		}
		return contactos;
	}

}
