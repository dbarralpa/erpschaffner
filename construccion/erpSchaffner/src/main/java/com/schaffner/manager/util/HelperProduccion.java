package com.schaffner.manager.util;



import java.util.List;

import com.schaffner.modelo.Auxiliar;
public class HelperProduccion {
	
	public static String checked(List<Auxiliar> auxs,String user, String permiso){
		String checked = "";
		for(int i=0;i<auxs.size();i++){
			if(auxs.get(i).getAux9().equals(user) && auxs.get(i).getAux10().equals(permiso)){
				checked = "checked";
				break;
			}
		}
		return checked;
	}
	
	public static int permisosSubArea(List<Auxiliar> auxs,String user, int perSubArea){
		int val = 0;
		for(int i=0;i<auxs.size();i++){
			if(auxs.get(i).getAux1() == perSubArea && auxs.get(i).getAux9().equals(user)){
			   val = perSubArea;
			}
		}
		return val;
	}
}
