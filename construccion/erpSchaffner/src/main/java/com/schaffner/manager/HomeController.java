package com.schaffner.manager;

import java.security.Principal;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.schaffner.core.servicio.UsuarioServicio;
import com.schaffner.manager.util.ViewGeneral;
import com.schaffner.modelo.Usuario;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("usuarioLogeado")
public class HomeController {
	private static final Logger logger = Logger
			.getLogger(HomeController.class);
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(HttpSession session, Principal principal) {
		/*
		 * ApplicationContext context = new ClassPathXmlApplicationContext(
		 * "com/schaffner/core/beans/usuario.xml"); IUsuarioServicio isus =
		 * (IUsuarioServicio) context .getBean("usuarioServicio");
		 * 
		 * model.addAttribute("serverTime",
		 * isus.getUsuarioByUsuario("dbarra").getNombre());
		 */
		if (principal != null) {
			/*
			 * Usuario user = new Usuario(); user.setUsuario("Davicito");
			 * model.addAttribute("usuarioLogeado", user);
			 * model.addAttribute("url", "body.jsp");
			 */
			return "home";
		} else {
			return "login";
		}
	}

	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String printWelcome(ModelMap model, Principal principal) {
		try {
			UsuarioServicio ser = new UsuarioServicio();
			Usuario user = ser.getUsuario(principal.getName());
			model.addAttribute("usuarioLogeado", user);
			model.addAttribute("url", "body.jsp");
		} catch (Exception e) {

		}

		return "home";

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model, HttpSession session, Principal principal) {
		return login(session, principal);

	}

	@RequestMapping(value = "/loginError", method = RequestMethod.GET)
	public String loginerror(ModelMap model, HttpSession session,
			Principal principal) {
		try {
		model.addAttribute("error", "true");
		} catch (Exception e) {
			logger.error("loginError", e);
		}
		return login(session, principal);

	}

	@RequestMapping(value = "/common", method = RequestMethod.GET)
	public String common(ModelMap model) {

		return "common_page";

	}

	@RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
	public String accessDenied(ModelMap model) {
		model.addAttribute("url", "body.jsp");
		model.addAttribute("valor", ViewGeneral.info("#", false, "No tiene acceso a este m�dulo"));
		return "home";

	}

}
