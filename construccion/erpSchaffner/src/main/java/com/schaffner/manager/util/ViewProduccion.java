package com.schaffner.manager.util;

import java.util.List;

import com.schaffner.manager.util.HelperProduccion;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;

public class ViewProduccion {

	public static String administracionUsuariosProduccion(
			List<Usuario> usuariosXarea, List<Auxiliar> permisosXarea,
			List<Auxiliar> subAreasProduccion, List<Auxiliar> permisosPorSubArea) {
		String html = "";

		html += "<div class='table-responsive'>";
		html += "<table id='example-datatable' class='table table-vcenter table-condensed table-bordered'>";
		html += "<thead>";
		html += "<tr>";
		html += "<th class='text-center' data-toggle='tooltip'>Usuario</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Administrar el �rea de producci�n'>Admin</th>";
		html += "<th class='text-center' data-toggle='tooltip' title='Documentos aplicables'>Doc apli</th>";
		html += "</tr>";
		html += "</thead>";
		html += "<tbody>";
		for (int i = 0; i < usuariosXarea.size(); i++) {
			html += "<tr>";
			html += "<td>" + usuariosXarea.get(i).getNombre() + "</td>";
			html += "<td class='text-center'><input type='checkbox' onclick=\"actualizarPermisos(this,'PRO_ADMIN','"
					+ usuariosXarea.get(i).getUsuario()
					+ "')\" id='proAdminDocApli"
					+ i
					+ "' name='proAdminDocApli"
					+ i
					+ "' "
					+ HelperProduccion.checked(permisosXarea, usuariosXarea
							.get(i).getUsuario(), "PRO_ADMIN") + "></td>";
			html += "<td class='text-center'><input type='checkbox' onclick=\"actualizarPermisosSubArea1(this,'PRO_DOC_APLI','"
					+ usuariosXarea.get(i).getUsuario()
					+ "','"
					+ i
					+ "')\" id='proDocApli"
					+ i
					+ "' name='proDocApli"
					+ i
					+ "' "
					+ HelperProduccion.checked(permisosXarea, usuariosXarea
							.get(i).getUsuario(), "PRO_DOC_APLI") + "></td>";
			html += "</tr>";
		}
		html += "</tbody>";
		html += "</table>";
		html += "</div>";
		return html;
	}

	public static String selectSubAreas(List<Auxiliar> subAreasProduccion,
			List<Auxiliar> permisosPorSubArea, String user, int a) {
		String html = "";
		html += "<div class='col-md-6'>";
		html += "<select id='val_sub_areas" + a + "' name='val_sub_areas" + a
				+ "' ";
		html += "class='form-control'>";
		html += "<option value='0'>Elija Sub �rea</option>";
		for (int i = 0; i < subAreasProduccion.size(); i++) {
			if (HelperProduccion.permisosSubArea(permisosPorSubArea, user,
					subAreasProduccion.get(i).getAux1()) == subAreasProduccion
					.get(i).getAux1()) {
				html += "<option value='" + subAreasProduccion.get(i).getAux1()
						+ "' selected>" + subAreasProduccion.get(i).getAux9()
						+ "</option>";
			} else {
				html += "<option value='" + subAreasProduccion.get(i).getAux1()
						+ "'>" + subAreasProduccion.get(i).getAux9()
						+ "</option>";
			}
		}
		html += "</select>";
		html += "</div>";
		return html;
	}

	public static String buscarDocAplicable(String val_cod_costo, String val_np) {
		String html = "";
		html += "<form id='form-validation' action='produccion-encontrar-doc-aplicable' method='post' ";
		html += "class='form-horizontal'>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_cod_costo'>C�digo de costo ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_cod_costo' name='val_cod_costo' ";
		html += "class='form-control' placeholder='C�digo de costo..' value='"
				+ val_cod_costo + "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_np'>Np ";
		html += "</label>";
		html += "<div class='col-md-5'>";
		html += "<input type='text' id='val_np' name='val_np' ";
		html += "class='form-control' placeholder='Np..' value='" + val_np
				+ "' autocomplete='off'>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group form-actions'>";
		html += "<div class='col-md-8 col-md-offset-4'>";
		html += "<button type='submit' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Buscar </button>";
		html += "</div>";
		html += "</div>";
		html += "</form>";
		return html;
	}

	public static String detalleDocApliEnc(List<DocAplicableEnc> encs) {
		String html = "";
		for (int i = 0; i < encs.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'><a href='produccion-documentos-aplicables-actualizar?val_numero="
					+ encs.get(i).getNumero();
			html += "&val_codigo_costo=" + encs.get(i).getDisenoTrafo();
			html += "&val_diseno_electrico=" + encs.get(i).getDisenoElectrico();
			html += "&val_diseno_mecanico=" + encs.get(i).getDisenoMecanico();
			html += "&val_np=" + encs.get(i).getNp();
			html += "&val_item=" + encs.get(i).getItem();
			html += "'>" + encs.get(i).getNumero() + "</a></td>";
			html += "<td class='text-center'>" + encs.get(i).getNp() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getItem()
					+ "</td>";
			html += "<td class='text-center'>"
					+ encs.get(i).getDisenoElectrico() + "</td>";
			html += "<td class='text-center'>"
					+ encs.get(i).getDisenoMecanico() + "</td>";
			html += "<td class='text-center'>" + encs.get(i).getDisenoTrafo()
					+ "</td>";
			html += "<td class='text-center'>" + encs.get(i).getFechaIngreso()
					+ "</td>";
			html += "<td class='text-center'>" + encs.get(i).getUsuario()
					+ "</td>";
			html += "</tr>";
		}
		return html;
	}

	public static String cabeceraDocAplicableEnc(DocAplicableEnc enc) {
		String html = "";
		html += "<div class='row'>";
		html += "<fieldset>";
		html += "<legend>";
		html += "DOCUMENTO APLICABLE";
		html += "</legend>";
		html += "</fieldset>";
		html += "<form id='form-validation' action='#' method='post' ";
		html += "class='form-horizontal form-bordered'>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_diseno' name='val_diseno' ";
		html += "class='form-control' placeholder='C�digo de costo..' value='"
				+ enc.getDisenoTrafo() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_np' name='val_np' ";
		if (enc.getNp() > 0) {
			html += "class='form-control' placeholder='Np ej: 77452..' value='"
					+ enc.getNp() + "'>";
		} else {
			html += "class='form-control' placeholder='Np ej: 77452..'>";
		}
		html += "<input type='text' id='val_item' name='val_item' ";
		html += "class='form-control col-md-1' placeholder='Item ej: 1-2-8..' value='"
				+ enc.getItem() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_diseno_elec' ";
		html += "name='val_diseno_elec' class='form-control' ";
		html += "placeholder='Dise�o el�ctrico..' value='"
				+ enc.getDisenoElectrico() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='col-md-3'>";
		html += "<div class='block-section'>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-12'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_diseno_mec' name='val_diseno_mec' ";
		html += "class='form-control' placeholder='Diseno mec�nico..' value='"
				+ enc.getDisenoMecanico() + "'>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</form>";
		html += "</div>";
		return html;
	}

	public static String comboDescripcionPlano(List<Auxiliar> aux,
			String nombre, int selected) {
		String html = "";
		html += "<select id='" + nombre + "' name='" + nombre + "' ";
		html += "class='form-control'>";
		html += "<option value='0'>Elija Descripci�n</option>";
		for (int h = 0; h < aux.size(); h++) {
			if (selected == aux.get(h).getAux1()) {
				html += "<option value='" + aux.get(h).getAux1()
						+ "' selected>" + aux.get(h).getAux9() + "</option>";
			} else {
				html += "<option value='" + aux.get(h).getAux1() + "'>"
						+ aux.get(h).getAux9() + "</option>";
			}

		}
		html += "</select>";
		return html;
	}

	public static String detalleDocApliPlanos(List<DocAplicableDet> dets,
			List<Auxiliar> auxs) {
		String html = "";
		html += "<div class='table-responsive'>";
		html += "<table id='example-datatable' ";
		html += "class='table table-vcenter table-condensed table-bordered'>";
		html += "<thead>";
		html += "<tr>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Id del documento'>Id</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Nombre documento'>Nombre documento</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='Nombre del plano'>Nombre del plano</th>";
		html += "<th class='text-center' data-toggle='tooltip' ";
		html += "title='N�mero de revisi�n'>N� Rev.</th>";
		html += "</tr>";
		html += "</thead>";
		html += "<tbody>";
		for (int i = 0; i < dets.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'>" + dets.get(i).getId() + "</td>";
			html += "<td class='text-center'>"
					+ descripcionPlano(auxs, dets.get(i).getDescPlano())
					+ "</td>";
			html += "<td class='text-center'><a href='produccion-doc-apli-cargarArchivos?val_idDocApliDet="
					+ dets.get(i).getId()
					+ "&val_id_plano="
					+ dets.get(i).getIdPlano()
					+ "&val_id_detRev="
					+ dets.get(i).getIdDetRev()
					+ "&val_revision="
					+ dets.get(i).getRevision()
					+ "&val_idEnc="
					+ dets.get(i).getNumero()
					+ "' id='val"
					+ i
					+ "' target='_blank'>"
					+ dets.get(i).getIdPlano()
					+ "</a></td>";

			html += "<td class='text-center'>" + dets.get(i).getRevision()
					+ "</td>";
			html += "</tr>";
		}
		html += "</tbody>";
		html += "</table>";
		html += "</div>";

		return html;
	}

	private static String descripcionPlano(List<Auxiliar> aux, int selected) {
		String html = "";
		for (int h = 0; h < aux.size(); h++) {
			if (selected == aux.get(h).getAux1()) {
				html += aux.get(h).getAux9();
			} else {
				html += "";
			}
		}

		return html;
	}

	public static String permisosImpresion(List<Usuario> usuariosXarea) {
		String html = "";
		html += "<select id='val_vali_usuario_impresion' name='val_vali_usuario_impresion' ";
		html += "class='form-control col-md-4'>";
		html += "<option value=''>Elija usuario</option>";
		for (int h = 0; h < usuariosXarea.size(); h++) {
			html += "<option value='" + usuariosXarea.get(h).getUsuario()
					+ "'>" + usuariosXarea.get(h).getNombre() + "</option>";
		}
		html += "</select>";

		return html;
	}

}
