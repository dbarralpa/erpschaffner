package com.schaffner.manager.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Persona;
import com.schaffner.modelo.ProyectoComercialTransformador;
import com.schaffner.modelo.ProyectoComercialVentaDet;
import com.schaffner.modelo.ProyectoComercialVentaEnc;

public class ViewComercial {
	private static SimpleDateFormat fo = new SimpleDateFormat("dd/MM/yyyy");

	public ViewComercial() {

	}

	public static String modalVentas(ProyectoComercialVentaEnc pro, int i,
			List<Auxiliar> tipoCotizacion, List<Auxiliar> tipoMoneda,
			List<Auxiliar> formaPago, List<Persona> vendedores) {
		String html = "";
		html += "<div id='modal-ventas" + i
				+ "' class='modal fade' tabindex='-1' ";
		html += "role='dialog' aria-hidden='true'>";
		html += "<div class='modal-dialog'>";
		html += "<div class='modal-content'>";
		html += "<div class='modal-header text-center'>";
		html += "<h2 class='modal-title'>Editar oferta N� "
				+ pro.getIdProyecto() + "</h2>";
		html += "</div>";
		html += "<div class='modal-body'>";
		html += "<form action='comercial-editarCotizacion-update' method='post' id='form-validation' ";
		html += "class='form-horizontal form-bordered'>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_proyecto>Nombre ";
		html += "proyecto <span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_proyecto' name='val_proyecto' value='"
				+ pro.getNombreProyecto() + "' ";
		html += "class='form-control' placeholder='nombre proyecto..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_tipoCotizacion'>Tipo ";
		html += "cotizaci&oacute;n <span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += selectTipoCotizacion(tipoCotizacion, pro.getIdTipoCotizacion(),
				"");
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_fechaCliente>Fecha ";
		html += "cliente <span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_fechaCliente' name='val_fechaCliente' ";
		html += "class='form-control' placeholder='dd/mm/yyyy' value='"
				+ pro.getFechaCliente() + "'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_tipoMoneda'>Tipo ";
		html += "moneda <span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += selectTipoMoneda(tipoMoneda, pro.getIdTipoMoneda(), "");
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_vendedor'>Vendedor ";
		html += "<span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += selectVendedores(vendedores, pro.getIdVendedor(), "");
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_formaPago'>Forma de pago<span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += selectFormaPago(formaPago, pro.getIdFormaPago(), "");
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_fechaIngEntregara>Fecha ";
		html += "ing entregar�";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_fechaIngEntregara' name='val_fechaIngEntregara' ";
		if (pro.getFechaIngEnregara() != null) {
			html += "class='form-control' placeholder='dd/mm/yyyy' value='"
					+ pro.getFechaIngEnregara() + "'> <span ";
		} else {
			html += "class='form-control' placeholder='dd/mm/yyyy' value=''> <span ";
		}
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_fechaIngEntrego>Fecha ";
		html += "ing entreg�";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_fechaIngEntrego' name='val_fechaIngEntrego' ";
		if (pro.getFechaIngEntrego() != null) {
			html += "class='form-control' placeholder='dd/mm/yyyy' value='"
					+ pro.getFechaIngEntrego() + "'> <span ";
		} else {
			html += "class='form-control' placeholder='dd/mm/yyyy' value=''> <span ";
		}
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_fechaEnvioIng>Fecha ";
		html += "envio a ing";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_fechaEnvioIng' name='val_fechaEnvioIng' ";
		if (pro.getFechaEnvioIng() != null) {
			html += "class='form-control' placeholder='dd/mm/yyyy' value='"
					+ pro.getFechaEnvioIng() + "'> <span ";
		} else {
			html += "class='form-control' placeholder='dd/mm/yyyy' value=''> <span ";
		}
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group form-actions'>";
		html += "<div class='col-xs-12 text-right'>";
		html += "<button type='button' class='btn btn-sm btn-default' ";
		html += "data-dismiss='modal'>Close</button>";
		html += "<button type='submit' class='btn btn-sm btn-primary'>Save ";
		html += "Changes</button>";
		html += "</div>";
		html += "</div>";
		html += "<input type='hidden' name='idProyecto' id='idProyecto' value='"
				+ pro.getIdProyecto() + "'>";
		html += "</form>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		return html;
	}

	public static String selectTipoMoneda(List<Auxiliar> tipoMonedas,
			int selected, String disabled) {
		String html = "";
		html += "<select id='val_tipoMoneda' name='val_tipoMoneda' ";
		html += "class='form-control'>";
		html += "<option value=''>Tipo moneda</option>";
		for (int i = 0; i < tipoMonedas.size(); i++) {
			if (selected == tipoMonedas.get(i).getAux1()) {
				html += "<option value='" + tipoMonedas.get(i).getAux1()
						+ "' selected>" + tipoMonedas.get(i).getAux9()
						+ "</option>";
			} else {
				html += "<option value='" + tipoMonedas.get(i).getAux1() + "'>"
						+ tipoMonedas.get(i).getAux9() + "</option>";
			}
		}
		html += "</select>";

		return html;
	}

	public static String selectTipoCotizacion(List<Auxiliar> tipoCotizacion,
			int selected, String disabled) {
		String html = "";
		html += "<select id='val_tipoCotizacion' name='val_tipoCotizacion' ";
		html += "class='form-control'>";
		html += "<option value=''>Tipo cotizaci�n</option>";
		for (int i = 0; i < tipoCotizacion.size(); i++) {
			if (selected == tipoCotizacion.get(i).getAux1()) {
				html += "<option value='" + tipoCotizacion.get(i).getAux1()
						+ "' selected " + disabled + ">"
						+ tipoCotizacion.get(i).getAux9() + "</option>";
			} else {
				html += "<option value='" + tipoCotizacion.get(i).getAux1()
						+ "'>" + tipoCotizacion.get(i).getAux9() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectVendedores(List<Persona> vendedores,
			int selected, String disabled) {
		String html = "";
		html += "<select id='val_vendedor' name='val_vendedor' ";
		html += "class='form-control'>";
		html += "<option value=''>Vendedores</option>";
		for (int i = 0; i < vendedores.size(); i++) {
			if (selected == vendedores.get(i).getId()) {
				html += "<option value='" + vendedores.get(i).getId()
						+ "' selected " + disabled + ">"
						+ vendedores.get(i).getNombre() + "</option>";
			} else {
				html += "<option value='" + vendedores.get(i).getId() + "'>"
						+ vendedores.get(i).getNombre() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectVendedoresString(List<Persona> vendedores,
			int selected) {
		String html = "";
		for (int i = 0; i < vendedores.size(); i++) {
			if (selected == vendedores.get(i).getId()) {
				html = vendedores.get(i).getNombre();
			}
		}
		return html;
	}

	public static String selectTipoCotizacionString(
			List<Auxiliar> tipoCotizacion, int selected) {
		String html = "";
		for (int i = 0; i < tipoCotizacion.size(); i++) {
			if (selected == tipoCotizacion.get(i).getAux1()) {
				html = tipoCotizacion.get(i).getAux9();
			}
		}

		return html;
	}

	public static String selectFormaPago(List<Auxiliar> formaPago,
			int selected, String disabled) {
		String html = "";
		html += "<select id='val_formaPago' name='val_formaPago' ";
		html += "class='form-control'>";
		html += "<option value=''>Forma pago</option>";
		for (int i = 0; i < formaPago.size(); i++) {
			if (selected == formaPago.get(i).getAux1()) {
				html += "<option value='" + formaPago.get(i).getAux1()
						+ "' selected " + disabled + ">"
						+ formaPago.get(i).getAux9() + "</option>";
			} else {
				html += "<option value='" + formaPago.get(i).getAux1() + "'>"
						+ formaPago.get(i).getAux9() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectEstadoCotizacion(
			List<Auxiliar> estadoCotizacion, int selected, String disabled) {
		String html = "";
		html += "<select id='val_estadoCotizacion' name='val_estadoCotizacion' "
				+ disabled + " ";
		html += "class='form-control'>";
		html += "<option value=''>Estado cotizacion</option>";
		for (int i = 0; i < estadoCotizacion.size(); i++) {
			if (selected == estadoCotizacion.get(i).getAux1()) {
				html += "<option value='" + estadoCotizacion.get(i).getAux1()
						+ "' selected " + disabled + ">"
						+ estadoCotizacion.get(i).getAux9() + "</option>";
			} else {
				html += "<option value='" + estadoCotizacion.get(i).getAux1()
						+ "'>" + estadoCotizacion.get(i).getAux9()
						+ "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectMargenes(List<Auxiliar> margenes, int selected,
			String disabled) {
		String html = "";
		html += "<select id='val_margen' name='val_margen' " + disabled + " ";
		html += "class='form-control'>";
		html += "<option value='SIN'>Margen</option>";
		for (int i = 0; i < margenes.size(); i++) {
			if (!margenes.get(i).getAux9().equals("SIN")) {
				if (selected == margenes.get(i).getAux1()) {
					html += "<option value='" + margenes.get(i).getAux9()
							+ "' selected " + disabled + ">"
							+ margenes.get(i).getAux9() + "</option>";
				} else {
					html += "<option value='" + margenes.get(i).getAux9()
							+ "'>" + margenes.get(i).getAux9() + "</option>";
				}
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectMargenes(List<Auxiliar> margenes,
			String selected, String disabled) {
		String html = "";
		html += "<select id='val_margen' name='val_margen' " + disabled + " ";
		html += "class='form-control'>";
		html += "<option value='SIN'>Margen</option>";
		for (int i = 0; i < margenes.size(); i++) {
			if (!margenes.get(i).getAux9().equals("SIN")) {
				if (selected.equals(margenes.get(i).getAux9())) {
					html += "<option value='" + margenes.get(i).getAux9()
							+ "' selected " + disabled + ">"
							+ margenes.get(i).getAux9() + "</option>";
				} else {
					html += "<option value='" + margenes.get(i).getAux9()
							+ "'>" + margenes.get(i).getAux9() + "</option>";
				}
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectEstadoCotizacionString(
			List<Auxiliar> estadoCotizacion, int selected) {
		String html = "";
		for (int i = 0; i < estadoCotizacion.size(); i++) {
			if (selected == estadoCotizacion.get(i).getAux1()) {
				html = estadoCotizacion.get(i).getAux9();
			}
		}

		return html;
	}

	public static String selectContactos(List<Auxiliar> contactos,
			String selected, String disabled) {
		String html = "";
		html += "<select id='val_contacto1' name='val_contacto1' onchange='contacto()' "
				+ disabled + " ";
		html += "class='form-control'>";
		html += "<option value=''>Contacto</option>";
		for (int i = 0; i < contactos.size(); i++) {
			if (selected.equals(contactos.get(i).getAux9())) {
				html += "<option value='" + contactos.get(i).getAux9()
						+ "' selected " + disabled + ">"
						+ contactos.get(i).getAux9() + "</option>";
			} else {
				html += "<option value='" + contactos.get(i).getAux9() + "'>"
						+ contactos.get(i).getAux9() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectContactosEmail(List<Auxiliar> contactos,
			String selected, String disabled) {
		String html = "";
		html += "<select id='val_email1' name='val_email1' ";
		html += "class='form-control'>";
		html += "<option value=''>Email</option>";
		for (int i = 0; i < contactos.size(); i++) {
			if (selected.equals(contactos.get(i).getAux10())) {
				html += "<option value='" + contactos.get(i).getAux10()
						+ "' selected " + disabled + ">"
						+ contactos.get(i).getAux10() + "</option>";
			} else {
				html += "<option value='" + contactos.get(i).getAux10() + "'>"
						+ contactos.get(i).getAux10() + "</option>";
			}
		}
		html += "</select>";
		return html;
	}

	public static String selectContactosVacio() {
		String html = "";
		html += "<select id='val_contacto1' name='val_contacto1'";
		html += "class='form-control'>";
		html += "<option value=''>Contacto</option>";
		html += "</select>";
		return html;
	}

	public static String selectContactosEmailVacio() {
		String html = "";
		html += "<select id='val_email1' name='val_email1'";
		html += "class='form-control'>";
		html += "<option value=''>Email</option>";
		html += "</select>";
		return html;
	}

	public static String editarCotizaciones(
			List<ProyectoComercialVentaEnc> proyectosVentas,
			List<Auxiliar> estadoCotizaciones, List<Auxiliar> tipoCotizaciones,
			List<Persona> vendedores) {
		String html = "";
		html += "<tbody>";
		for (int i = 0; i < proyectosVentas.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='comercial-editarCotizacion-resumenCostos?id="
					+ proyectosVentas.get(i).getId() + "' ";
			html += "data-toggle='tooltip' title='Edit'";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='#modal-ventas" + proyectosVentas.get(i).getId()
					+ "' data-toggle='modal' title='Edit' ";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>"
					+ selectEstadoCotizacionString(estadoCotizaciones,
							proyectosVentas.get(i).getIdEstado()) + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getIdProyecto() + "</td>";
			html += "<td class='text-center'>"
					+ selectTipoCotizacionString(tipoCotizaciones,
							proyectosVentas.get(i).getIdTipoCotizacion())
					+ "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getIdCliente() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getNombreProyecto() + "</td>";
			html += "<td class='text-center'>"
					+ selectVendedoresString(vendedores, proyectosVentas.get(i)
							.getIdVendedor()) + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaEnvioIng() + "</td>";

			Calendar calendar = Calendar.getInstance();
			String fecha[] = proyectosVentas.get(i).getFechaCliente()
					.split("/");
			java.sql.Date date = java.sql.Date.valueOf(fecha[2] + "-"
					+ fecha[1] + "-" + fecha[0]);
			calendar.setTime(date); // Configuramos la fecha que se recibe
			calendar.add(Calendar.DAY_OF_YEAR, -1);

			html += "<td class='text-center'>" + fo.format(calendar.getTime())
					+ "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaCliente() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaIngEnregara() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaIngEntrego() + "</td>";

			if (!proyectosVentas.get(i).getFechaIngEnregara().trim().equals("")
					&& !proyectosVentas.get(i).getFechaIngEntrego().trim()
							.equals("")) {
				String[] fecha1 = proyectosVentas.get(i).getFechaIngEnregara()
						.split("/");
				String[] fecha2 = proyectosVentas.get(i).getFechaIngEntrego()
						.split("/");
				Date fech1 = java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1]
						+ "-" + fecha1[0]);
				Date fech2 = java.sql.Date.valueOf(fecha2[2] + "-" + fecha2[1]
						+ "-" + fecha2[0]);
				long fec = fech2.getTime() - fech1.getTime();
				html += "<td class='text-center'>" + (fec / 86400000) + "</td>";
			} else {
				html += "<td class='text-center'>&nbsp;</td>";
			}

			if (!proyectosVentas.get(i).getFechaEnvioIng().trim().equals("")
					&& !proyectosVentas.get(i).getFechaIngEnregara().trim()
							.equals("")) {
				String[] fecha1 = proyectosVentas.get(i).getFechaIngEnregara()
						.split("/");
				String[] fecha2 = proyectosVentas.get(i).getFechaEnvioIng()
						.split("/");
				Date fech1 = java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1]
						+ "-" + fecha1[0]);
				Date fech2 = java.sql.Date.valueOf(fecha2[2] + "-" + fecha2[1]
						+ "-" + fecha2[0]);
				long fec = fech1.getTime() - fech2.getTime();
				html += "<td class='text-center'>" + (fec / 86400000) + "</td>";
			} else {
				html += "<td class='text-center'>&nbsp;</td>";
			}
			html += "</tr>";
		}
		html += "</tbody>";
		return html;
	}

	public static String progresoOferta(
			List<ProyectoComercialVentaEnc> proyectosVentas,
			List<Auxiliar> estadoCotizaciones, List<Auxiliar> tipoCotizaciones,
			List<Persona> vendedores) {
		String html = "";
		html += "<tbody>";
		for (int i = 0; i < proyectosVentas.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='comercial-vendedores-editarProgresoOferta?id="
					+ proyectosVentas.get(i).getId() + "' ";
			html += "data-toggle='tooltip' title='Edit'";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>"
					+ selectEstadoCotizacionString(estadoCotizaciones,
							proyectosVentas.get(i).getIdEstado()) + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getIdProyecto() + "</td>";
			html += "<td class='text-center'>"
					+ selectTipoCotizacionString(tipoCotizaciones,
							proyectosVentas.get(i).getIdTipoCotizacion())
					+ "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getIdCliente() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getNombreProyecto() + "</td>";
			html += "<td class='text-center'>"
					+ selectVendedoresString(vendedores, proyectosVentas.get(i)
							.getIdVendedor()) + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaEnvioIng() + "</td>";

			Calendar calendar = Calendar.getInstance();
			String fecha[] = proyectosVentas.get(i).getFechaCliente()
					.split("/");
			java.sql.Date date = java.sql.Date.valueOf(fecha[2] + "-"
					+ fecha[1] + "-" + fecha[0]);
			calendar.setTime(date); // Configuramos la fecha que se recibe
			calendar.add(Calendar.DAY_OF_YEAR, -1);

			html += "<td class='text-center'>" + fo.format(calendar.getTime())
					+ "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaCliente() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaIngEnregara() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaIngEntrego() + "</td>";

			if (!proyectosVentas.get(i).getFechaIngEnregara().trim().equals("")
					&& !proyectosVentas.get(i).getFechaIngEntrego().trim()
							.equals("")) {
				String[] fecha1 = proyectosVentas.get(i).getFechaIngEnregara()
						.split("/");
				String[] fecha2 = proyectosVentas.get(i).getFechaIngEntrego()
						.split("/");
				Date fech1 = java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1]
						+ "-" + fecha1[0]);
				Date fech2 = java.sql.Date.valueOf(fecha2[2] + "-" + fecha2[1]
						+ "-" + fecha2[0]);
				long fec = fech2.getTime() - fech1.getTime();
				html += "<td class='text-center'>" + (fec / 86400000) + "</td>";
			} else {
				html += "<td class='text-center'>&nbsp;</td>";
			}

			if (!proyectosVentas.get(i).getFechaEnvioIng().trim().equals("")
					&& !proyectosVentas.get(i).getFechaIngEnregara().trim()
							.equals("")) {
				String[] fecha1 = proyectosVentas.get(i).getFechaIngEnregara()
						.split("/");
				String[] fecha2 = proyectosVentas.get(i).getFechaEnvioIng()
						.split("/");
				Date fech1 = java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1]
						+ "-" + fecha1[0]);
				Date fech2 = java.sql.Date.valueOf(fecha2[2] + "-" + fecha2[1]
						+ "-" + fecha2[0]);
				long fec = fech1.getTime() - fech2.getTime();
				html += "<td class='text-center'>" + (fec / 86400000) + "</td>";
			} else {
				html += "<td class='text-center'>&nbsp;</td>";
			}
			html += "</tr>";
		}
		html += "</tbody>";
		return html;
	}

	public static String resumenCostosCuenta(ProyectoComercialVentaEnc pro,
			List<Auxiliar> parametros, List<Auxiliar> tipoCotizaciones,
			List<Persona> vendedores, Persona cliente) {
		String html = "";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label'>CT N�</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>&nbsp;</label>";
		html += "</div>";
		html += "<label class='col-md-4 control-label'>Tipo</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>"
				+ selectTipoCotizacionString(tipoCotizaciones,
						pro.getIdTipoCotizacion()) + "</label>";
		html += "</div>";
		html += "<label class='col-md-4 control-label'>Cliente</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>" + cliente.getNombre()
				+ "</label>";
		html += "</div>";
		html += "<label class='col-md-4 control-label'>Contacto</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>" + pro.getContacto()
				+ "</label>";
		html += "</div>";
		html += "<label class='col-md-4 control-label'>Email</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>" + pro.getEmail()
				+ "</label>";
		html += "</div>";
		html += "<label class='col-md-4 control-label'>Proyecto</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>"
				+ pro.getNombreProyecto() + "</label>";
		html += "</div>";
		html += "<label class='col-md-4 control-label'>Vendedor</label>";
		html += "<div class='col-md-8'>";
		html += "<label class='col-md-8 control-label'>"
				+ selectVendedoresString(vendedores, pro.getIdVendedor())
				+ "</label>";
		html += "</div>";
		for (int i = 0; i < parametros.size(); i++) {
			html += "<label class='col-md-4 control-label'>"
					+ parametros.get(i).getAux9() + "</label>";
			html += "<div class='col-md-8'>";
			html += "<label class='col-md-8 control-label'>"
					+ parametros.get(i).getAux1() + "</label>";
			html += "</div>";
		}
		html += "</div>";
		return html;
	}

	public static String tablaMargenes(List<Auxiliar> margenes) {
		String html = "";
		for (int i = 0; i < margenes.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'>" + margenes.get(i).getAux9()
					+ "</td>";
			html += "<td class='text-center'>" + margenes.get(i).getAux1()
					+ "</td>";
			html += "<td class='text-center'>" + margenes.get(i).getAux2()
					+ "</td>";
			html += "</tr>";
		}
		return html;
	}

	public static String proyectosVentasArchivos(List<CargarArchivo> archivos) {
		String html = "";
		for (int i = 0; i < archivos.size(); i++) {
			html += "<a href='comercial-editarCotizacion-resumenCostos-cargarArchivos?arch="
					+ i + "' class='list-group-item'>";
			html += "<h4 class='list-group-item-heading remove-margin'>";
			html += "<i class='fa fa-file fa-fw'></i> "
					+ archivos.get(i).getName() + " </h4></a>";
		}
		return html;
	}

	public static String tablaDetalleProyectoVenta(
			List<ProyectoComercialVentaDet> det) {
		String html = "";
		DecimalFormat formateador = new DecimalFormat("###,###.##");
		html += "<tbody>";
		int cantidad = 0;
		int costoUni = 0;
		int costoTotal = 0;
		int precioUniDolar = 0;
		int precioTotalDolar = 0;
		int precioUniPeso = 0;
		int precioTotalPeso = 0;
		int precioUniUf = 0;
		int precioTotalUf = 0;
		for (int i = 0; i < det.size(); i++) {
			cantidad += det.get(i).getCantidad();
			costoUni += det.get(i).getCostoUnitario();
			costoTotal += det.get(i).getCostoTotal();
			precioUniDolar += det.get(i).getPrecioUniDolar();
			precioTotalDolar += det.get(i).getPrecioTotalDolar();
			precioUniPeso += det.get(i).getPrecioUniPeso();
			precioTotalPeso += det.get(i).getPrecioTotalPeso();
			precioUniUf += det.get(i).getPrecioUniUf();
			precioTotalUf += det.get(i).getPrecioTotalUf();
			html += "<tr>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='#modal-ventas" + det.get(i).getId()
					+ "' data-toggle='modal' title='Edit' ";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='#modal-ventas-trafos" + det.get(i).getId()
					+ "' data-toggle='modal' title='Mostrar trafo' ";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>" + det.get(i).getTag() + "</td>";
			html += "<td class='text-center'>" + det.get(i).getDescripcion()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getDiseno()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getCodigoCubica()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getIdMargen()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getCantidad()
					+ "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getCostoUnitario())
					+ "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getCostoTotal()) + "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getPrecioUniDolar())
					+ "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getPrecioTotalDolar())
					+ "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getPrecioUniPeso())
					+ "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getPrecioTotalPeso())
					+ "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getPrecioUniUf()) + "</td>";
			html += "<td class='text-center'>"
					+ formateador.format(det.get(i).getPrecioTotalUf())
					+ "</td>";
			html += "</tr>";
		}
		html += "</tbody>";
		html += "<tfoot>";
		html += "<td class='text-right' colspan='7'>Totales</td>";
		html += "<td class='text-center'>" + formateador.format(cantidad)
				+ "</td>";
		html += "<td class='text-center'>" + formateador.format(costoUni)
				+ "</td>";
		html += "<td class='text-center'>" + formateador.format(costoTotal)
				+ "</td>";
		html += "<td class='text-center'>" + formateador.format(precioUniDolar)
				+ "</td>";
		html += "<td class='text-center'>"
				+ formateador.format(precioTotalDolar) + "</td>";
		html += "<td class='text-center'>" + formateador.format(precioUniPeso)
				+ "</td>";
		html += "<td class='text-center'>"
				+ formateador.format(precioTotalPeso) + "</td>";
		html += "<td class='text-center'>" + formateador.format(precioUniUf)
				+ "</td>";
		html += "<td class='text-center'>" + formateador.format(precioTotalUf)
				+ "</td>";
		html += "</tfoot>";
		return html;
	}

	public static String modalVentasDet(ProyectoComercialVentaDet det, int i,
			List<Auxiliar> margenes) {
		String html = "";
		html += "<div id='modal-ventas" + i
				+ "' class='modal fade' tabindex='-1' ";
		html += "role='dialog' aria-hidden='true'>";
		html += "<div class='modal-dialog'>";
		html += "<div class='modal-content'>";
		html += "<div class='modal-header text-center'>";
		html += "<h2 class='modal-title'>Editar detalle N� "
				+ det.getIdProyecto() + "</h2>";
		html += "</div>";
		html += "<div class='modal-body'>";
		html += "<form action='comercial-editarCotizacion-proyectoVentasDet-update' method='post' id='form-validation' ";
		html += "class='form-horizontal form-bordered'>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=text_tag>Tag ";
		html += "<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='text_tag' name='text_tag' value='"
				+ det.getTag() + "' ";
		html += "class='form-control' placeholder='Tag..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=textarea_descripcion>Descripci�n ";
		html += "<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='textarea_descripcion' name='textarea_descripcion' value='"
				+ det.getDescripcion() + "' ";
		html += "class='form-control' placeholder='Descripci�n..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=text_cubica>C�digo ";
		html += "cubica<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='text_cubica' name='text_cubica' value='"
				+ det.getCodigoCubica() + "' ";
		html += "class='form-control' placeholder='C�digo cubica..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=text_diseno>Dise�o ";
		html += "<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='text_diseno' name='text_diseno' value='"
				+ det.getDiseno() + "' ";
		html += "class='form-control' placeholder='Dise�o..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_margen'>Forma de pago<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += selectMargenes(margenes, det.getIdMargen(), "");
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=text_cant>Cantidad ";
		html += "<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='text_cant' name='text_cant' value='"
				+ det.getCantidad() + "' ";
		html += "class='form-control' placeholder='Cantidad..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=text_cu_usd>Costo unitario en d�lares ";
		html += "<span class='text-danger'></span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='text_cu_usd' name='text_cu_usd' value='"
				+ det.getCostoUnitario() + "' ";
		html += "class='form-control' placeholder='Costo unitario en d�lares..'> <span ";
		html += "class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<div class='col-md-6'>";
		html += "<button type='submit' name='update' id='update' value='delete' class='btn btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Borrar</button>&nbsp;";
		html += "<button type='submit' name='update' id='update' value='update' class='btn btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Actualizar</button>";
		html += "</div>";
		html += "</div>";
		html += "<input type='hidden' name='idProyectoDet' id='idProyectoDet' value='"
				+ det.getIdProyecto() + "'>";
		html += "</form>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		return html;
	}

	public static String datosGenerales(ProyectoComercialVentaEnc enc) {
		String html = "";
		html = "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='text_entrega'>Entrega</label>";
		html += "<div class='col-md-9'>";
		html += "<input type='text' id='text_entrega' name='text_entrega' value='"
				+ enc.getPlazoEntrega() + "' ";
		html += "class='form-control' placeholder='Plazo de entrega..'>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='text_validez'>Validez</label>";
		html += "<div class='col-md-9'>";
		html += "<input type='text' id='text_validez' name='text_validez' value='"
				+ enc.getValidezOferta() + "' ";
		html += "class='form-control' placeholder='Validez de la oferta..'>";
		html += "</div>";
		html += "</div>";
		return html;
	}

	public static String proyectoVentasTransfos(Persona cliente,
			ProyectoComercialVentaEnc proEnc, String tipoCotizacion,
			String tipoMoneda, String formaPago, String vendedores,
			List<Auxiliar> contactos) {

		String html = "<form id='form-validation'";
		html += "action='comercial-solicitudCotizacion-proyectoTrafoGuardar' ";
		html += "method='post' class='form-horizontal form-bordered'>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_cliente>Cliente";
		html += "<span class='text-danger'>*</span>";
		html += "</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_cliente' name='val_cliente' readonly ";
		html += "class='form-control' placeholder='cliente..' ";
		html += "value="
				+ cliente.getNombre()
				+ "> <span class='input-group-addon'><i class='gi gi-user'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_contacto1'>Contacto</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += selectContactos(contactos, proEnc.getContacto(), "");
		html += "<span class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_email1'>Email</label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += selectContactosEmail(contactos, proEnc.getEmail(), "");
		html += " <span class='input-group-addon'>";
		html += "<i class='gi gi-envelope'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_proyecto>Nombre proyecto <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_proyecto' name='val_proyecto' value='"
				+ proEnc.getNombreProyecto() + "' ";
		html += "class='form-control' placeholder='nombre proyecto..'> <span class='input-group-addon'><i class='gi gi-asterisk'></i></span>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_tipoCotizacion'>Tipo cotizaci&oacute;n <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-6'>" + tipoCotizacion + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for=val_fechaCliente>Fecha cliente <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-6'>";
		html += "<div class='input-group'>";
		html += "<input type='text' id='val_fechaCliente' name='val_fechaCliente' class='form-control input-datepicker-close' ";
		html += "value='"
				+ proEnc.getFechaCliente()
				+ "' data-date-format='dd/mm/yyyy' placeholder='dd/mm/yyyy' readonly>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_tipoMoneda'>Tipo moneda <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-6'>" + tipoMoneda + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_formaPago'>Forma de pago<span class='text-danger'>*</span></label>";
		html += "<div class='col-md-6'>" + formaPago + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label' for='val_vendedor'>";
		html += "Vendedor <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-6'>" + vendedores + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-4 control-label'><a href='#modal-cargar-archivos' data-toggle='modal'>Cargar Archivos</a></label>";
		html += "</div>";
		html += "<div class='form-group form-actions'>";
		html += "<div class='col-md-8 col-md-offset-4'>";
		html += "<button type='submit' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Cargar transformadores </button>";
		html += "</div>";
		html += "</div>";
		html += "</form>";

		return html;
	}

	public static String familias(List<Auxiliar> familias, String selected,
			String disabled) {
		String html = "";
		for (int i = 0; i < familias.size(); i++) {
			if (selected.equals(familias.get(i).getAux9())) {
				html += "<label class='radio-inline' for='tipo'> <input type='radio' checked "
						+ disabled
						+ " id='tipo' name='tipo' value='"
						+ familias.get(i).getAux9()
						+ "'> "
						+ familias.get(i).getAux9() + " </label>";
			} else {
				html += "<label class='radio-inline' for='tipo'> <input type='radio' id='tipo' "
						+ disabled
						+ " name='tipo' value='"
						+ familias.get(i).getAux9()
						+ "'> "
						+ familias.get(i).getAux9() + " </label>";
			}
		}
		return html;
	}

	public static String normas(List<Auxiliar> normas, int selected,
			String disabled) {
		String html = "";
		for (int i = 0; i < normas.size(); i++) {
			if (selected == normas.get(i).getAux1()) {
				html += "<label class='radio-inline' for='norma'> <input type='radio' id='norma' name='norma' checked "
						+ disabled
						+ " value='"
						+ normas.get(i).getAux1()
						+ "'>" + normas.get(i).getAux9() + " </label> ";
			} else {
				html += "<label class='radio-inline' for='norma'> <input type='radio' id='norma' name='norma' "
						+ disabled
						+ " value='"
						+ normas.get(i).getAux1()
						+ "'>" + normas.get(i).getAux9() + " </label> ";
			}
		}
		return html;
	}

	public static String auxHtml(List<Auxiliar> auxs, int selected) {
		String html = "";
		for (int i = 0; i < auxs.size(); i++) {
			if (selected == auxs.get(i).getAux1()) {
				html += auxs.get(i).getAux9();
			}
		}
		return html;
	}

	public static String temPerdidas(List<Auxiliar> temPerdidas, int selected,
			String disabled) {
		String html = "";
		for (int i = 0; i < temPerdidas.size(); i++) {
			if (selected == temPerdidas.get(i).getAux1()) {
				html += "<label class='radio-inline' for='temPerdidas'> <input type='radio' id='temPerdidas' name='temPerdidas' checked "
						+ disabled
						+ " value='"
						+ temPerdidas.get(i).getAux1()
						+ "'>" + temPerdidas.get(i).getAux9() + " </label> ";
			} else {
				html += "<label class='radio-inline' for='temPerdidas'> <input type='radio' id='temPerdidas' name='temPerdidas' "
						+ disabled
						+ " value='"
						+ temPerdidas.get(i).getAux1()
						+ "'>" + temPerdidas.get(i).getAux9() + " </label> ";
			}
		}
		return html;
	}

	public static String ventilacion(List<Auxiliar> ventilacion, int selected,
			String disabled) {
		String html = "";
		for (int i = 0; i < ventilacion.size(); i++) {
			if (selected == ventilacion.get(i).getAux1()) {
				html += "<label class='radio-inline' for='ventilacion'> <input type='radio' id='ventilacion' name='ventilacion' checked "
						+ disabled
						+ " value='"
						+ ventilacion.get(i).getAux1()
						+ "'>" + ventilacion.get(i).getAux9() + " </label> ";
			} else {
				html += "<label class='radio-inline' for='ventilacion'> <input type='radio' id='ventilacion' name='ventilacion' "
						+ disabled
						+ " value='"
						+ ventilacion.get(i).getAux1()
						+ "'>" + ventilacion.get(i).getAux9() + " </label> ";
			}
		}
		return html;
	}

	public static String temperaturas(List<Auxiliar> temperaturas,
			int selected, String disabled) {
		String html = "";
		for (int i = 0; i < temperaturas.size(); i++) {
			if (selected == temperaturas.get(i).getAux1()) {
				html += "<label class='radio-inline' for='temperatura'> <input type='radio' id='temperatura' name='temperatura' checked "
						+ disabled
						+ " value='"
						+ temperaturas.get(i).getAux1()
						+ "'>" + temperaturas.get(i).getAux9() + " </label> ";
			} else {
				html += "<label class='radio-inline' for='temperatura'> <input type='radio' id='temperatura' name='temperatura' "
						+ disabled
						+ " value='"
						+ temperaturas.get(i).getAux1()
						+ "'>" + temperaturas.get(i).getAux9() + " </label> ";
			}
		}
		return html;
	}

	public static String modalVentasTrafos(
			ProyectoComercialTransformador trafo,
			ProyectoComercialVentaDet det, List<Auxiliar> familias,
			List<Auxiliar> normas, List<Auxiliar> temPerdidas,
			List<Auxiliar> ventilacion, List<Auxiliar> temperaturas,
			String disabled) {
		String html = "";
		html += "<div id='modal-ventas-trafos" + det.getId()
				+ "' class='modal fade' tabindex='-1' ";
		html += "role='dialog' aria-hidden='true'>";
		html += "<div class='modal-dialog'>";
		html += "<div class='modal-content'>";
		html += "<div class='modal-header text-center'>";
		html += "<h2 class='modal-title'>Solicitud de transformador</h2>";
		html += "</div>";
		html += "<div class='modal-body'>";
		html += "<form action='comercial-solicitudCotizacion-actualizarProyectoTrafos' method='post' id='form-validation' ";
		html += "class='form-horizontal form-bordered'>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='tipo'>Tipo</label>";
		html += "<div class='col-md-9'>"
				+ familias(familias, trafo.getIdFamilia(), disabled) + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Normas</label>";
		html += "<div class='col-md-9'>";
		html += normas(normas, trafo.getIdNorma(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Tem. perdidas</label>";
		html += "<div class='col-md-9'>";
		html += temPerdidas(temPerdidas, trafo.getIdTemPerdidas(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Ventilaci�n</label>";
		html += "<div class='col-md-9'>";
		html += ventilacion(ventilacion, trafo.getIdVentilacion(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Temperatura</label>";
		html += "<div class='col-md-9'>";
		html += temperaturas(temperaturas, trafo.getIdTemperatura(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_potencia'>Potencia";
		html += " (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_potencia' name='val_potencia' value='"
				+ trafo.getPotencia() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_voltaje_primario'>Voltaje";
		html += " Primario (V) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_voltaje_primario' name='val_voltaje_primario' value='"
				+ trafo.getVoltajePrimario() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_voltaje_secundario'>Voltaje";
		html += " secundario (V) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_voltaje_secundario' name='val_voltaje_secundario' value='"
				+ trafo.getVoltajeSecundario() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_impedancia'>Impedancia";
		html += " (%) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_impedancia' name='val_impedancia' value='"
				+ trafo.getImpedancia() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_frecuencia'>Frecuencia";
		html += " (Hz) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_frecuencia' name='val_frecuencia' value='"
				+ trafo.getFrecuencia() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='tag'>Tag </label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='tag' name='tag' value='"
				+ det.getTag() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_cantidad'>Cantidad</label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_cantidad' name='val_cantidad' value='"
				+ det.getCantidad() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='observacion'>Observaciones</label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<textarea rows='9' class='form-control' id='observacion' name='observacion' " + disabled + ">"
				+ trafo.getObservacion() + "</textarea>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		/*html += "<div class='form-group form-actions'>";
		html += "<button type='submit' name='actualizar' value='delete' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Eliminar </button>";
		html += "<button type='submit' name='actualizar' value='actualizar' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Actualizar </button><br/><br/>";
		html += "</div>";
		html += "<input type='hidden' name='idTrafo' id='idTrafo' value='"
						+ trafo.getIdTransformador() + "'>";
		html += "<input type='hidden' name='idProDet' id='idProDet' value='"
				+ det.getIdProyecto() + "'>";*/
		html += "</form>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>\n";

		return html;
	}
	
	public static String modalVentasTrafos2(
			ProyectoComercialTransformador trafo,
			ProyectoComercialVentaDet det, List<Auxiliar> familias,
			List<Auxiliar> normas, List<Auxiliar> temPerdidas,
			List<Auxiliar> ventilacion, List<Auxiliar> temperaturas,
			String disabled) {
		String html = "";
		html += "<div id='modal-ventas-trafos" + det.getId()
				+ "' class='modal fade' tabindex='-1' ";
		html += "role='dialog' aria-hidden='true'>";
		html += "<div class='modal-dialog'>";
		html += "<div class='modal-content'>";
		html += "<div class='modal-header text-center'>";
		html += "<h2 class='modal-title'>Solicitud de transformador</h2>";
		html += "</div>";
		html += "<div class='modal-body'>";
		html += "<form action='comercial-solicitudCotizacion-actualizarProyectoTrafos' method='post' id='form-validation' ";
		html += "class='form-horizontal form-bordered'>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='tipo'>Tipo</label>";
		html += "<div class='col-md-9'>"
				+ familias(familias, trafo.getIdFamilia(), disabled) + "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Normas</label>";
		html += "<div class='col-md-9'>";
		html += normas(normas, trafo.getIdNorma(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Tem. perdidas</label>";
		html += "<div class='col-md-9'>";
		html += temPerdidas(temPerdidas, trafo.getIdTemPerdidas(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Ventilaci�n</label>";
		html += "<div class='col-md-9'>";
		html += ventilacion(ventilacion, trafo.getIdVentilacion(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label'>Temperatura</label>";
		html += "<div class='col-md-9'>";
		html += temperaturas(temperaturas, trafo.getIdTemperatura(), disabled);
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_potencia'>Potencia";
		html += " (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_potencia' name='val_potencia' value='"
				+ trafo.getPotencia() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_voltaje_primario'>Voltaje";
		html += " Primario (V) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_voltaje_primario' name='val_voltaje_primario' value='"
				+ trafo.getVoltajePrimario() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_voltaje_secundario'>Voltaje";
		html += " secundario (V) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_voltaje_secundario' name='val_voltaje_secundario' value='"
				+ trafo.getVoltajeSecundario() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_impedancia'>Impedancia";
		html += " (%) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_impedancia' name='val_impedancia' value='"
				+ trafo.getImpedancia() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_frecuencia'>Frecuencia";
		html += " (Hz) (KVA) <span class='text-danger'>*</span></label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_frecuencia' name='val_frecuencia' value='"
				+ trafo.getFrecuencia() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='tag'>Tag </label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='tag' name='tag' value='"
				+ det.getTag() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='val_cantidad'>Cantidad</label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<input type='text' class='form-control' id='val_cantidad' name='val_cantidad' value='"
				+ det.getCantidad() + "' " + disabled + ">";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group'>";
		html += "<label class='col-md-3 control-label' for='observacion'>Observaciones</label>";
		html += "<div class='col-md-9'>";
		html += "<div class='input-group'>";
		html += "<textarea rows='9' class='form-control' id='observacion' name='observacion' " + disabled + ">"
				+ trafo.getObservacion() + "</textarea>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "<div class='form-group form-actions'>";
		html += "<button type='submit' name='actualizar' value='delete' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Eliminar </button>";
		html += "<button type='submit' name='actualizar' value='actualizar' class='btn btn-sm btn-primary'>";
		html += "<i class='fa fa-arrow-right'></i> Actualizar </button><br/><br/>";
		html += "</div>";
		html += "<input type='hidden' name='idTrafo' id='idTrafo' value='"
						+ trafo.getIdTransformador() + "'>";
		html += "<input type='hidden' name='idProDet' id='idProDet' value='"
				+ det.getIdProyecto() + "'>";
		html += "</form>";
		html += "</div>";
		html += "</div>";
		html += "</div>";
		html += "</div>\n";

		return html;
	}

	public static String tablaDetalleProgresoProyecto(List<Auxiliar> det) {
		String html = "";
		html += "<tbody>";
		for (int i = 0; i < det.size(); i++) {

			html += "<tr>";
			html += "<td class='text-center'>" + det.get(i).getAux1()
					+ "%</td>";
			html += "<td class='text-center'>" + det.get(i).getAux10()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getAux11()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getAux9() + "</td>";
			html += "</tr>";
		}
		html += "</tbody>";
		return html;
	}

	public static String tablaDetalleTrafos(
			List<ProyectoComercialTransformador> tra,List<ProyectoComercialVentaDet> det, List<Auxiliar> normas,
			List<Auxiliar> temPer, List<Auxiliar> vent, List<Auxiliar> temp) {
		String html = "";
		html += "<tbody>";
		for (int i = 0; i < tra.size(); i++) {

			html += "<tr>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='#modal-ventas-trafos" + det.get(i).getId()
					+ "' data-toggle='modal' title='Edit' ";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>" + tra.get(i).getIdFamilia()
					+ "</td>";
			html += "<td class='text-center'>"
					+ auxHtml(normas, tra.get(i).getIdNorma()) + "</td>";
			html += "<td class='text-center'>"
					+ auxHtml(temPer, tra.get(i).getIdTemPerdidas()) + "</td>";
			html += "<td class='text-center'>"
					+ auxHtml(vent, tra.get(i).getIdVentilacion()) + "</td>";
			html += "<td class='text-center'>"
					+ auxHtml(temp, tra.get(i).getIdTemperatura()) + "</td>";
			html += "<td class='text-center'>" + tra.get(i).getPotencia()
					+ "</td>";
			html += "<td class='text-center'>"
					+ tra.get(i).getVoltajePrimario() + "</td>";
			html += "<td class='text-center'>"
					+ tra.get(i).getVoltajeSecundario() + "</td>";
			html += "<td class='text-center'>" + tra.get(i).getImpedancia()
					+ "</td>";
			html += "<td class='text-center'>" + tra.get(i).getFrecuencia()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getTag()
					+ "</td>";
			html += "<td class='text-center'>" + det.get(i).getCantidad()
					+ "</td>";
			html += "<td class='text-center'>" + tra.get(i).getObservacion()
					+ "</td>";
			html += "</tr>";
		}
		html += "</tbody>";
		return html;
	}
	
	public static String editarCotizacionesTrafo(
			List<ProyectoComercialVentaEnc> proyectosVentas,
			List<Auxiliar> estadoCotizaciones, List<Auxiliar> tipoCotizaciones,
			List<Persona> vendedores) {
		String html = "";
		html += "<tbody>";
		for (int i = 0; i < proyectosVentas.size(); i++) {
			html += "<tr>";
			html += "<td class='text-center'>";
			html += "<div class='btn-group'>";
			html += "<a href='comercial-editarCotizacion-trafoEdit?trafoId=" + proyectosVentas.get(i).getId() + "'  title='Edit' ";
			html += "class='btn btn-xs btn-default'><i class='fa fa-pencil'></i></a>";
			html += "</div>";
			html += "</td>";
			html += "<td class='text-center'>"
					+ selectEstadoCotizacionString(estadoCotizaciones,
							proyectosVentas.get(i).getIdEstado()) + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getIdProyecto() + "</td>";
			html += "<td class='text-center'>"
					+ selectTipoCotizacionString(tipoCotizaciones,
							proyectosVentas.get(i).getIdTipoCotizacion())
					+ "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getIdCliente() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getNombreProyecto() + "</td>";
			html += "<td class='text-center'>"
					+ selectVendedoresString(vendedores, proyectosVentas.get(i)
							.getIdVendedor()) + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaEnvioIng() + "</td>";

			Calendar calendar = Calendar.getInstance();
			String fecha[] = proyectosVentas.get(i).getFechaCliente()
					.split("/");
			java.sql.Date date = java.sql.Date.valueOf(fecha[2] + "-"
					+ fecha[1] + "-" + fecha[0]);
			calendar.setTime(date); 
			calendar.add(Calendar.DAY_OF_YEAR, -1);

			html += "<td class='text-center'>" + fo.format(calendar.getTime())
					+ "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaCliente() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaIngEnregara() + "</td>";
			html += "<td class='text-center'>"
					+ proyectosVentas.get(i).getFechaIngEntrego() + "</td>";

			if (!proyectosVentas.get(i).getFechaIngEnregara().trim().equals("")
					&& !proyectosVentas.get(i).getFechaIngEntrego().trim()
							.equals("")) {
				String[] fecha1 = proyectosVentas.get(i).getFechaIngEnregara()
						.split("/");
				String[] fecha2 = proyectosVentas.get(i).getFechaIngEntrego()
						.split("/");
				Date fech1 = java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1]
						+ "-" + fecha1[0]);
				Date fech2 = java.sql.Date.valueOf(fecha2[2] + "-" + fecha2[1]
						+ "-" + fecha2[0]);
				long fec = fech2.getTime() - fech1.getTime();
				html += "<td class='text-center'>" + (fec / 86400000) + "</td>";
			} else {
				html += "<td class='text-center'>&nbsp;</td>";
			}

			if (!proyectosVentas.get(i).getFechaEnvioIng().trim().equals("")
					&& !proyectosVentas.get(i).getFechaIngEnregara().trim()
							.equals("")) {
				String[] fecha1 = proyectosVentas.get(i).getFechaIngEnregara()
						.split("/");
				String[] fecha2 = proyectosVentas.get(i).getFechaEnvioIng()
						.split("/");
				Date fech1 = java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1]
						+ "-" + fecha1[0]);
				Date fech2 = java.sql.Date.valueOf(fecha2[2] + "-" + fecha2[1]
						+ "-" + fecha2[0]);
				long fec = fech1.getTime() - fech2.getTime();
				html += "<td class='text-center'>" + (fec / 86400000) + "</td>";
			} else {
				html += "<td class='text-center'>&nbsp;</td>";
			}
			html += "</tr>";
		}
		html += "</tbody>";
		return html;
	}

}

