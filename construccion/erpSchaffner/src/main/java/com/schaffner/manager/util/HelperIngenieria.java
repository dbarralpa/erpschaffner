package com.schaffner.manager.util;

import java.util.ArrayList;
import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Costo;
import com.schaffner.modelo.ingenieria.Presspan;

public class HelperIngenieria {

	public static String tipoPobina(int val) {
		String tipo = "";
		if (val == 0) {
			tipo = "NO LLEVA";
		}
		if (val == 1) {
			tipo = "RECTANGULAR";
		}
		if (val == 2) {
			tipo = "CIRCULAR";
		}
		if (val == 3) {
			tipo = "DE_DISCO";
		}
		if (val == 4) {
			tipo = "PLETINA";
		}
		if (val == 5) {
			tipo = "LAMINA";
		}
		return tipo;
	}

	public static List<Auxiliar> disenos(String val_diseno, String val_codigo,
			String val_distribuciones_all, double val_potencia, double val_kv,
			String val_refrigerante, int val_voltaje_primario,
			String val_voltaje_secundario, double val_impedancia,
			int val_altura_instalacion, String val_conductor_bbt,
			String val_conductor_bat, List<Auxiliar> disenos) {
		List<Auxiliar> disenos1 = new ArrayList<Auxiliar>();
		if (val_diseno.trim().length() > 0) {
			for (int i = (disenos.size() - 1); i >= 0; i--) {
				if (val_diseno.trim().equals(disenos.get(i).getAux9())) {
					disenos1.add(disenos.get(i));
				}
			}
		}

		if (val_codigo.trim().length() > 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_codigo.trim().equals(disenos.get(i).getAux10())) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (!val_codigo.trim().equals(disenos1.get(i).getAux10())) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (Integer.parseInt(val_distribuciones_all) != 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (Integer.parseInt(val_distribuciones_all) == disenos
							.get(i).getAux1()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (Integer.parseInt(val_distribuciones_all) != disenos1
							.get(i).getAux1()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_potencia != 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_potencia == disenos.get(i).getAux28()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_potencia != disenos1.get(i).getAux28()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_kv != 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_kv == disenos.get(i).getAux29()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_kv != disenos1.get(i).getAux29()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (Double.parseDouble(val_refrigerante) != -1) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (Double.parseDouble(val_refrigerante) == disenos.get(i)
							.getAux30()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (Double.parseDouble(val_refrigerante) != disenos1.get(i)
							.getAux30()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_voltaje_primario != 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_voltaje_primario == disenos.get(i).getAux2()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_voltaje_primario != disenos1.get(i).getAux2()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_voltaje_secundario.trim().length() > 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_voltaje_secundario.trim().equals(
							disenos.get(i).getAux11())) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (!val_voltaje_secundario.trim().equals(
							disenos1.get(i).getAux11())) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_impedancia != 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_impedancia == disenos.get(i).getAux31()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_impedancia != disenos1.get(i).getAux31()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_altura_instalacion != 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_altura_instalacion == disenos.get(i).getAux3()) {
						disenos1.add(disenos.get(i));
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_altura_instalacion != disenos1.get(i).getAux3()) {
						disenos1.remove(i);
						/*
						 * if(disenos1.size() > 1){ i = i+1; }
						 */
					}
				}
			}
		}

		if (val_conductor_bbt.trim().length() > 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_conductor_bbt.equals("ALUMINIO")) {
						if (disenos.get(i).getAux12()
								.indexOf(val_conductor_bbt) > -1) {
							disenos1.add(disenos.get(i));
						}
					}
					if (val_conductor_bbt.equals("COBRE")) {
						if (disenos.get(i).getAux12()
								.indexOf(val_conductor_bbt) > -1
								|| disenos.get(i).getAux12().indexOf("CU") > -1) {
							disenos1.add(disenos.get(i));
						}
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_conductor_bbt.equals("ALUMINIO")) {
						if (disenos1.get(i).getAux12()
								.indexOf(val_conductor_bbt) == -1) {
							disenos1.remove(i);
						}
					}
					if (val_conductor_bbt.equals("COBRE")) {
						if (disenos1.get(i).getAux12()
								.indexOf(val_conductor_bbt) == -1
								&& disenos1.get(i).getAux12().indexOf("CU") == -1) {
							disenos1.remove(i);
						}
					}
				}
			}
		}

		if (val_conductor_bat.trim().length() > 0) {
			if (disenos1.isEmpty()) {
				for (int i = (disenos.size() - 1); i >= 0; i--) {
					if (val_conductor_bat.equals("ALUMINIO")) {
						if (disenos.get(i).getAux13()
								.indexOf(val_conductor_bat) > -1) {
							disenos1.add(disenos.get(i));
						}
					}
					if (val_conductor_bat.equals("COBRE")) {
						if (disenos.get(i).getAux13()
								.indexOf(val_conductor_bat) > -1
								|| disenos.get(i).getAux13().indexOf("CU") > -1) {
							disenos1.add(disenos.get(i));
						}
					}
				}
			} else {
				for (int i = (disenos1.size() - 1); i >= 0; i--) {
					if (val_conductor_bat.equals("ALUMINIO")) {
						if (disenos1.get(i).getAux13()
								.indexOf(val_conductor_bat) == -1) {
							disenos1.remove(i);
						}
					}
					if (val_conductor_bbt.equals("COBRE")) {
						if (disenos1.get(i).getAux13()
								.indexOf(val_conductor_bat) == -1
								&& disenos1.get(i).getAux13().indexOf("CU") == -1) {
							disenos1.remove(i);
						}
					}
				}
			}
		}

		if (disenos1.size() > 100) {
			for (int i = 0; i < disenos1.size(); i++) {
				if (i > 100) {
					disenos1.remove(i);
					i--;
				}
			}
		}

		return disenos1;
	}

	public static String codigos(List<Auxiliar> aux) {
		String detalle = "";
		for (int i = 0; i < aux.size(); i++) {
			detalle += "'" + aux.get(i).getAux9() + "',";
		}
		return detalle;
	}

	public static String codigosAccesorios(List<Accesorio> aux) {
		String detalle = "";
		for (int i = 0; i < aux.size(); i++) {
			detalle += "'" + aux.get(i).getCodigo() + "',";
		}
		return detalle;
	}

	public static String presspanEntreCapas(Presspan press) {
		return "'" + press.getCodCapa1() + "','" + press.getCodCapa2() + "','"
				+ press.getCodCapa3() + "'";
	}

	public static String presspanAislacionExterna(Presspan press) {
		return "'" + press.getCodAtbt1() + "','" + press.getCodAtbt2() + "','"
				+ press.getCodAtbt3() + "'";
	}

	public static String presspanAislacionBase(Presspan press) {
		return "'" + press.getCodBase1() + "','" + press.getCodBase2() + "','"
				+ press.getCodBase3() + "'";
	}

	public static void detallePresspanEntreCapas(Presspan press,
			List<Auxiliar> auxs) {
		for (int i = 0; i < auxs.size(); i++) {
			if (auxs.get(i).getAux9().equals(press.getCodCapa1())) {
				press.setDescCapa1(auxs.get(i).getAux10());
			}
			if (auxs.get(i).getAux9().equals(press.getCodCapa2())) {
				press.setDescCapa2(auxs.get(i).getAux10());
			}
			if (auxs.get(i).getAux9().equals(press.getCodCapa3())) {
				press.setDescCapa3(auxs.get(i).getAux10());
			}
		}
	}

	public static void detallePresspanAislacionExterna(Presspan press,
			List<Auxiliar> auxs) {
		for (int i = 0; i < auxs.size(); i++) {
			if (auxs.get(i).getAux9().equals(press.getCodAtbt1())) {
				press.setDescAtbt1(auxs.get(i).getAux10());
			}
			if (auxs.get(i).getAux9().equals(press.getCodAtbt2())) {
				press.setDescAtbt2(auxs.get(i).getAux10());
			}
			if (auxs.get(i).getAux9().equals(press.getCodAtbt3())) {
				press.setDescAtbt3(auxs.get(i).getAux10());
			}
		}
	}

	public static void detallePresspanAislacionBase(Presspan press,
			List<Auxiliar> auxs) {
		for (int i = 0; i < auxs.size(); i++) {
			if (auxs.get(i).getAux9().equals(press.getCodBase1())) {
				press.setDescBase1(auxs.get(i).getAux10());
			}
			if (auxs.get(i).getAux9().equals(press.getCodBase2())) {
				press.setDescBase2(auxs.get(i).getAux10());
			}
			if (auxs.get(i).getAux9().equals(press.getCodBase3())) {
				press.setDescBase3(auxs.get(i).getAux10());
			}
		}
	}

	public static List<Accesorio> detalleAccesorio(List<Accesorio> acceso,
			List<Auxiliar> auxs) {
		for (int a = 0; a < acceso.size(); a++) {
			for (int i = 0; i < auxs.size(); i++) {
				if (auxs.get(i).getAux9().equals(acceso.get(a).getCodigo())) {
					acceso.get(a).setDescripcion(auxs.get(i).getAux10());
					break;
				}
			}
		}
		return acceso;
	}
	
	public static void disenoAccesorios(List<Accesorio> accesorios, String diseno){
		for(int i=0;i<accesorios.size();i++){
			accesorios.get(i).setDiseno(diseno);
		}
	}
	
	public static double totalPrecio(List<Costo> costos){
		double val = 0d;
		for(int i=0;i<costos.size();i++){
			val += costos.get(i).getCostoTotal();
		}
		return val;
	}
	
	public static String checked(List<Auxiliar> auxs,String user, String permiso){
		String checked = "";
		for(int i=0;i<auxs.size();i++){
			if(auxs.get(i).getAux9().equals(user) && auxs.get(i).getAux10().equals(permiso)){
				checked = "checked";
				break;
			}
		}
		return checked;
	}
	
	public static boolean equipoVigente(List<Auxiliar> equiposVigenteScha, int np, int item){
		boolean est = false;
		for(int a=0;a<equiposVigenteScha.size();a++){
			Auxiliar sch = equiposVigenteScha.get(a);
			if(sch.getAux1() == np && sch.getAux2() == item){
				est = true;
			}
		}
		return est;
	}
}
