package com.schaffner.manager.util;

import java.util.ArrayList;
import java.util.List;

import com.schaffner.modelo.ProyectoComercialVentaDet;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ProyectoVentaDetDataSource extends JRAbstractBeanDataSourceProvider {

	private List<ProyectoComercialVentaDet> det;
	

	public ProyectoVentaDetDataSource() {
		super(ProyectoComercialVentaDet.class);
	}

	public JRDataSource create(JasperReport jrReport) throws JRException {
		det = new ArrayList<ProyectoComercialVentaDet>();
		return new JRBeanCollectionDataSource(det);
	}


	public void dispose(JRDataSource jrds) throws JRException {
		det.clear();
		det= null;
	}
	
	public JRDataSource llenarLista(List<ProyectoComercialVentaDet> det){
		for(int i=0;i<det.size();i++){
			this.det.add(new ProyectoComercialVentaDet(det.get(i).getTag(), det.get(i).getDescripcion(), det.get(i).getCantidad(), det.get(i).getCostoTotalMoneda()));
		}
		return new JRBeanCollectionDataSource(this.det);
	}

}
