package com.schaffner.manager;

import java.io.IOException;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.schaffner.core.servicio.IngenieriaServicio;
import com.schaffner.manager.util.CubicacionDataSource;
import com.schaffner.manager.util.HelperIngenieria;
import com.schaffner.manager.util.Tag;
import com.schaffner.manager.util.ViewGeneral;
import com.schaffner.manager.util.ViewIngenieria;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Bobina;
import com.schaffner.modelo.ingenieria.Costo;
import com.schaffner.modelo.ingenieria.Diseno;
import com.schaffner.modelo.ingenieria.Distribucion;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;
import com.schaffner.modelo.ingenieria.Especificacion;
import com.schaffner.modelo.ingenieria.EstanqueTapa;
import com.schaffner.modelo.ingenieria.ItemCubicacion;
import com.schaffner.modelo.ingenieria.NucleoFerreteria;
import com.schaffner.modelo.ingenieria.Presspan;
import com.schaffner.modelo.ingenieria.Radiador;
import com.schaffner.modelo.ingenieria.Refrigerante;
import com.schaffner.modelo.ingenieria.TipoAislador;

@Controller
@SessionAttributes({ "archivosDocApli" })
public class IngenieriaController {
	final String SERVER_URI = "http://bitnami-wildfly-1208.cloudapp.net/middleware1";
	List<Tag> data = new ArrayList<Tag>();
	private static final Logger logger = Logger
			.getLogger(IngenieriaController.class);
	ApplicationContext context = new ClassPathXmlApplicationContext(
			"classpath:com/schaffner/core/beans/ingenieria.xml");
	@Autowired
	private JavaMailSender mailSender;

	@RequestMapping(value = "/ingenieria-home", method = RequestMethod.GET)
	public String home(Model model, String val_diseno) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			// String diseno = "C229919901";
			// String diseno = "C110310802";
			// String diseno = "C110100508";
			String diseno = val_diseno;
			double bbtDimRadial = 0;
			double batDimRadial = 0;
			double bteDimRadial = 0;
			double bbtDimAxial = 0;
			double batDimAxial = 0;
			double bteDimAxial = 0;
			Diseno dis = isus.getDiseno(diseno);
			Radiador rad = null;
			String url = "";
			List<Refrigerante> ref = isus.getRefrigerantes();
			List<Auxiliar> materialesPresspan = null;
			List<Auxiliar> materialesXclasificacion = null;
			if (dis.getIdRefrigerante() == 1) {
				rad = isus.getRadiador(diseno);
				model.addAttribute(
						"comboTipoRadiador",
						ViewIngenieria.comboTipoRadiador(
								isus.getTiposRadiadores(),
								rad.getCodTipoRadiador()));
				model.addAttribute("radiador", ViewIngenieria.radiador(
						isus.getTiposRadiadores(), rad.getCodTipoRadiador(),
						isus.getMaterialesRadiador(rad.getCodTipoRadiador()),
						rad.getCodigoRadiador(), rad));
				List<TipoAislador> tipoAis = isus.getTipoAisladores();
				model.addAttribute(
						"grillaAisladores",
						ViewIngenieria.selectTipoAisladores(tipoAis,
								dis.getAislador()));
				List<Distribucion> distri = isus.getDistribuciones("F");
				model.addAttribute(
						"grillaDistribucion",
						ViewIngenieria.selectDistribucion(distri,
								dis.getIdDistribucion()));

				model.addAttribute(
						"grillaRefrigerante",
						ViewIngenieria.selectRefrigeranteAceite(ref,
								dis.getIdRefrigerante()));
				materialesPresspan = isus.getMaterialesPresspan();
				String matPress = HelperIngenieria.codigos(materialesPresspan);
				List<Auxiliar> auxsPress = detalleMateriales(matPress
						.substring(0, (matPress.length() - 1)));
				materialesPresspan = auxsPress;
				materialesXclasificacion = isus
						.getMaterialesXclasificacionAceite();
				String codigos = HelperIngenieria
						.codigos(materialesXclasificacion);
				List<Auxiliar> auxs = detalleMateriales(codigos.substring(0,
						(codigos.length() - 1)));
				materialesXclasificacion = auxs;
				url = "cubicarAceite.jsp";
			}
			if (dis.getIdRefrigerante() == 0) {
				List<Distribucion> distri = isus.getDistribuciones("S");
				model.addAttribute(
						"grillaDistribucion",
						ViewIngenieria.selectDistribucion(distri,
								dis.getIdDistribucion()));
				model.addAttribute("grillaRefrigerante",
						ViewIngenieria.selectRefrigeranteSeco(ref));
				materialesPresspan = isus.getAislaciones();
				String matPress = HelperIngenieria.codigos(materialesPresspan);
				List<Auxiliar> auxsPress = detalleMateriales(matPress
						.substring(0, (matPress.length() - 1)));
				materialesPresspan = auxsPress;
				materialesXclasificacion = isus
						.getMaterialesXclasificacionSeco();
				String codigos = HelperIngenieria
						.codigos(materialesXclasificacion);
				List<Auxiliar> auxs = detalleMateriales(codigos.substring(0,
						(codigos.length() - 1)));
				materialesXclasificacion = auxs;
				url = "cubicarSeco.jsp";
			}
			Especificacion espe = isus.getEspecificacion(diseno);
			Auxiliar aux = new Auxiliar();
			aux.setAux1(1);
			aux.setAux9(diseno);
			Bobina bob = isus.getBobina(aux);
			Presspan press = isus.getPresspan(aux);
			model.addAttribute("bbt", bob);
			model.addAttribute("pressbbt", press);

			String pressEntrCapasBbt = HelperIngenieria
					.presspanEntreCapas(press);
			List<Auxiliar> auxsPressBbt = detalleMateriales(pressEntrCapasBbt);
			HelperIngenieria.detallePresspanEntreCapas(press, auxsPressBbt);

			String pressAislacionExternaBbt = HelperIngenieria
					.presspanAislacionExterna(press);
			List<Auxiliar> auxsPress1Bbt = detalleMateriales(pressAislacionExternaBbt);
			HelperIngenieria.detallePresspanAislacionExterna(press,
					auxsPress1Bbt);

			String pressAislacionBaseBbt = HelperIngenieria
					.presspanAislacionBase(press);
			List<Auxiliar> auxsPress2Bbt = detalleMateriales(pressAislacionBaseBbt);
			HelperIngenieria.detallePresspanAislacionBase(press, auxsPress2Bbt);

			String entreCapasBbt1 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBbt(materialesPresspan,
							press.getCodCapa1(), "entreCapasBbt1", 1);
			String entreCapasBbt2 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBbt(materialesPresspan,
							press.getCodCapa2(), "entreCapasBbt2", 2);
			String entreCapasBbt3 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBbt(materialesPresspan,
							press.getCodCapa3(), "entreCapasBbt3", 3);

			model.addAttribute("entreCapasBbt", ViewIngenieria.entreCapasBbt(
					press.getCanEspesoresCapa1(), press.getCodCapa1(),
					press.getDescCapa1(), press.getCanEspesoresCapa2(),
					press.getCodCapa2(), press.getDescCapa2(),
					press.getCanEspesoresCapa3(), press.getCodCapa3(),
					press.getDescCapa3(), entreCapasBbt1, entreCapasBbt2,
					entreCapasBbt3));
			String aislacionExternaBbt1 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBbt(
							materialesPresspan, press.getCodAtbt1(),
							"aislacionExternaBbt1", 1);
			String aislacionExternaBbt2 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBbt(
							materialesPresspan, press.getCodAtbt2(),
							"aislacionExternaBbt2", 2);
			String aislacionExternaBbt3 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBbt(
							materialesPresspan, press.getCodAtbt3(),
							"aislacionExternaBbt3", 3);
			model.addAttribute("aislacionExternaBbt", ViewIngenieria
					.aislacionExternaBbt(press.getCanEspesoresAtbt1(),
							press.getCodAtbt1(), press.getDescAtbt1(),
							press.getCanEspesoresAtbt2(), press.getCodAtbt2(),
							press.getDescAtbt2(), press.getCanEspesoresAtbt3(),
							press.getCodAtbt3(), press.getDescAtbt3(),
							aislacionExternaBbt1, aislacionExternaBbt2,
							aislacionExternaBbt3));
			String alislacionBaseBbt1 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBbt(
							materialesPresspan, press.getCodBase1(),
							"alislacionBaseBbt1", 1);
			String alislacionBaseBbt2 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBbt(
							materialesPresspan, press.getCodBase2(),
							"alislacionBaseBbt2", 2);
			String alislacionBaseBbt3 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBbt(
							materialesPresspan, press.getCodBase3(),
							"alislacionBaseBbt3", 3);
			model.addAttribute("alislacionBaseBbt", ViewIngenieria
					.aislacionBasebbt(press.getCanEspesoresBase1(),
							press.getCodBase1(), press.getDescBase1(),
							press.getCanEspesoresBase2(), press.getCodBase2(),
							press.getDescBase2(), press.getCanEspesoresBase3(),
							press.getCodBase3(), press.getDescBase3(),
							alislacionBaseBbt1, alislacionBaseBbt2,
							alislacionBaseBbt3));

			model.addAttribute("matBbt", ViewIngenieria
					.selectMaterialesBobinas(materialesXclasificacion,
							bob.getCodigoCondCu(), "1",
							"val_bobina_MaterialBbt"));
			bbtDimRadial = bob.getDimensionRadial();
			bbtDimAxial = bob.getDimensionAxial();
			aux.setAux1(2);
			bob = isus.getBobina(aux);
			press = isus.getPresspan(aux);
			model.addAttribute("bat", bob);
			model.addAttribute("pressbat", press);

			String pressEntrCapasBat = HelperIngenieria
					.presspanEntreCapas(press);
			List<Auxiliar> auxsPressBat = detalleMateriales(pressEntrCapasBat);
			HelperIngenieria.detallePresspanEntreCapas(press, auxsPressBat);

			String pressAislacionExternaBat = HelperIngenieria
					.presspanAislacionExterna(press);
			List<Auxiliar> auxsPress1Bat = detalleMateriales(pressAislacionExternaBat);
			HelperIngenieria.detallePresspanAislacionExterna(press,
					auxsPress1Bat);

			String pressAislacionBaseBat = HelperIngenieria
					.presspanAislacionBase(press);
			List<Auxiliar> auxsPress2Bat = detalleMateriales(pressAislacionBaseBat);
			HelperIngenieria.detallePresspanAislacionBase(press, auxsPress2Bat);

			String entreCapasBat1 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBat(materialesPresspan,
							press.getCodCapa1(), "entreCapasBat1", 1);
			String entreCapasBat2 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBat(materialesPresspan,
							press.getCodCapa2(), "entreCapasBat2", 2);
			String entreCapasBat3 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBat(materialesPresspan,
							press.getCodCapa3(), "entreCapasBat3", 3);
			model.addAttribute("entreCapasBat", ViewIngenieria.entreCapasBat(
					press.getCanEspesoresCapa1(), press.getCodCapa1(),
					press.getDescCapa1(), press.getCanEspesoresCapa2(),
					press.getCodCapa2(), press.getDescCapa2(),
					press.getCanEspesoresCapa3(), press.getCodCapa3(),
					press.getDescCapa3(), entreCapasBat1, entreCapasBat2,
					entreCapasBat3));
			String aislacionExternaBat1 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBat(
							materialesPresspan, press.getCodAtbt1(),
							"aislacionExternaBat1", 1);
			String aislacionExternaBat2 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBat(
							materialesPresspan, press.getCodAtbt2(),
							"aislacionExternaBat2", 2);
			String aislacionExternaBat3 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBat(
							materialesPresspan, press.getCodAtbt3(),
							"aislacionExternaBat3", 3);
			model.addAttribute("aislacionExternaBat", ViewIngenieria
					.aislacionExternaBat(press.getCanEspesoresAtbt1(),
							press.getCodAtbt1(), press.getDescAtbt1(),
							press.getCanEspesoresAtbt2(), press.getCodAtbt2(),
							press.getDescAtbt2(), press.getCanEspesoresAtbt3(),
							press.getCodAtbt3(), press.getDescAtbt3(),
							aislacionExternaBat1, aislacionExternaBat2,
							aislacionExternaBat3));
			String alislacionBaseBat1 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBat(
							materialesPresspan, press.getCodBase1(),
							"alislacionBaseBat1", 1);
			String alislacionBaseBat2 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBat(
							materialesPresspan, press.getCodBase2(),
							"alislacionBaseBat2", 2);
			String alislacionBaseBat3 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBat(
							materialesPresspan, press.getCodBase3(),
							"alislacionBaseBat3", 3);
			model.addAttribute("alislacionBaseBat", ViewIngenieria
					.aislacionBasebat(press.getCanEspesoresBase1(),
							press.getCodBase1(), press.getDescBase1(),
							press.getCanEspesoresBase2(), press.getCodBase2(),
							press.getDescBase2(), press.getCanEspesoresBase3(),
							press.getCodBase3(), press.getDescBase3(),
							alislacionBaseBat1, alislacionBaseBat2,
							alislacionBaseBat3));
			model.addAttribute("matBat", ViewIngenieria
					.selectMaterialesBobinas(materialesXclasificacion,
							bob.getCodigoCondCu(), "2",
							"val_bobina_MaterialBat"));
			batDimRadial = bob.getDimensionRadial();
			batDimAxial = bob.getDimensionAxial();
			aux.setAux1(3);
			bob = isus.getBobina(aux);
			press = isus.getPresspan(aux);
			model.addAttribute("bte", bob);
			model.addAttribute("pressbte", press);

			String pressEntrCapasBte = HelperIngenieria
					.presspanEntreCapas(press);
			List<Auxiliar> auxsPressBte = detalleMateriales(pressEntrCapasBte);
			HelperIngenieria.detallePresspanEntreCapas(press, auxsPressBte);

			String pressAislacionExternaBte = HelperIngenieria
					.presspanAislacionExterna(press);
			List<Auxiliar> auxsPress1Bte = detalleMateriales(pressAislacionExternaBte);
			HelperIngenieria.detallePresspanAislacionExterna(press,
					auxsPress1Bte);

			String pressAislacionBaseBte = HelperIngenieria
					.presspanAislacionBase(press);
			List<Auxiliar> auxsPress2Bte = detalleMateriales(pressAislacionBaseBte);
			HelperIngenieria.detallePresspanAislacionBase(press, auxsPress2Bte);

			String entreCapasBte1 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBte(materialesPresspan,
							press.getCodCapa1(), "entreCapasBte1", 1);
			String entreCapasBte2 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBte(materialesPresspan,
							press.getCodCapa2(), "entreCapasBte2", 2);
			String entreCapasBte3 = ViewIngenieria
					.selectMaterialesPresspanEntreCapasBte(materialesPresspan,
							press.getCodCapa3(), "entreCapasBte3", 3);
			model.addAttribute("entreCapasBte", ViewIngenieria.entreCapasBte(
					press.getCanEspesoresCapa1(), press.getCodCapa1(),
					press.getDescCapa1(), press.getCanEspesoresCapa2(),
					press.getCodCapa2(), press.getDescCapa2(),
					press.getCanEspesoresCapa3(), press.getCodCapa3(),
					press.getDescCapa3(), entreCapasBte1, entreCapasBte2,
					entreCapasBte3));
			String aislacionExternaBte1 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBte(
							materialesPresspan, press.getCodAtbt1(),
							"aislacionExternaBte1", 1);
			String aislacionExternaBte2 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBte(
							materialesPresspan, press.getCodAtbt2(),
							"aislacionExternaBte2", 2);
			String aislacionExternaBte3 = ViewIngenieria
					.selectMaterialesPresspanAislacionExternaBte(
							materialesPresspan, press.getCodAtbt3(),
							"aislacionExternaBte3", 3);
			model.addAttribute("aislacionExternaBte", ViewIngenieria
					.aislacionExternaBte(press.getCanEspesoresAtbt1(),
							press.getCodAtbt1(), press.getDescAtbt1(),
							press.getCanEspesoresAtbt2(), press.getCodAtbt2(),
							press.getDescAtbt2(), press.getCanEspesoresAtbt3(),
							press.getCodAtbt3(), press.getDescAtbt3(),
							aislacionExternaBte1, aislacionExternaBte2,
							aislacionExternaBte3));
			String alislacionBaseBte1 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBte(
							materialesPresspan, press.getCodBase1(),
							"alislacionBaseBte1", 1);
			String alislacionBaseBte2 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBte(
							materialesPresspan, press.getCodBase2(),
							"alislacionBaseBte2", 2);
			String alislacionBaseBte3 = ViewIngenieria
					.selectMaterialesPresspanAislacionBaseBte(
							materialesPresspan, press.getCodBase3(),
							"alislacionBaseBte3", 3);
			model.addAttribute("alislacionBaseBte", ViewIngenieria
					.aislacionBasebte(press.getCanEspesoresBase1(),
							press.getCodBase1(), press.getDescBase1(),
							press.getCanEspesoresBase2(), press.getCodBase2(),
							press.getDescBase2(), press.getCanEspesoresBase3(),
							press.getCodBase3(), press.getDescBase3(),
							alislacionBaseBte1, alislacionBaseBte2,
							alislacionBaseBte3));
			model.addAttribute("matBte", ViewIngenieria
					.selectMaterialesBobinas(materialesXclasificacion,
							bob.getCodigoCondCu(), "3",
							"val_bobina_MaterialBte"));
			bteDimRadial = bob.getDimensionRadial();
			bteDimAxial = bob.getDimensionAxial();
			EstanqueTapa est = isus.getEstanqueTapa(diseno);
			model.addAttribute("EstanqueTapa", est);

			List<Auxiliar> materPintura = isus.getMaterialesPintura();
			String codigos = HelperIngenieria.codigos(materPintura);
			List<Auxiliar> auxs = detalleMateriales(codigos.substring(0,
					(codigos.length() - 1)));
			materPintura = auxs;
			model.addAttribute("pinturaEstanque",
					ViewIngenieria.pinturaEstanque(materPintura, est));

			model.addAttribute("Derivaciones", ViewIngenieria
					.selectDerivaciones(isus.getDerivaciones(),
							espe.getDerivacion()));
			model.addAttribute("Especificacion", espe);
			model.addAttribute("Diseno", dis);
			model.addAttribute("bobinaBBT", ViewIngenieria.bonina(
					dis.getValorBbt(), dis.getCodCondBbt(), "BT",
					"val_datosgenerales_bbt"));
			model.addAttribute("bobinaBAT", ViewIngenieria.bonina(
					dis.getValorBat(), dis.getCodCondBat(), "AT",
					"val_datosgenerales_bat"));
			model.addAttribute("bobinaBTE", ViewIngenieria.bonina(
					dis.getValorBte(), dis.getCodCondBte(), "TE",
					"val_datosgenerales_bte"));
			model.addAttribute("dimensionesBobinas", ViewIngenieria
					.dimensionesBobinas(bbtDimRadial, bbtDimAxial,
							batDimRadial, batDimAxial, bteDimRadial,
							bteDimAxial));
			List<Auxiliar> matePlanFierro = isus.getMaterialesPlanchaFierro();
			String codigos1 = HelperIngenieria.codigos(matePlanFierro);
			List<Auxiliar> auxs1 = detalleMateriales(codigos1.substring(0,
					(codigos1.length() - 1)));
			matePlanFierro = auxs1;
			String materialesPinturaEspesorManto = ViewIngenieria
					.selectMaterialesPlanchaFierro(matePlanFierro,
							est.getCodManto(),
							"val_material_pintura_plancha_fierro_manto", 1);
			String materialesPinturaEspesorTapa = ViewIngenieria
					.selectMaterialesPlanchaFierro(matePlanFierro,
							est.getCodTapa(),
							"val_material_pintura_plancha_fierro_tapa", 2);
			String materialesPinturaEspesorFondo = ViewIngenieria
					.selectMaterialesPlanchaFierro(matePlanFierro,
							est.getCodFondo(),
							"val_material_pintura_plancha_fierro_fondo", 3);
			model.addAttribute("materialesPinturaPlanchaFierro", ViewIngenieria
					.materialesPinturaPlanchaFierro(est.getEspFondo(),
							est.getCodFondo(), est.getEspManto(),
							est.getCodManto(), est.getEspTapa(),
							est.getCodTapa(), materialesPinturaEspesorManto,
							materialesPinturaEspesorTapa,
							materialesPinturaEspesorFondo));
			NucleoFerreteria nucFerr = isus.getNucleoFerreteria(diseno);

			List<Auxiliar> auxSili = isus.getAceroSilicoso();
			String matSili = HelperIngenieria.codigos(auxSili);
			List<Auxiliar> auxsSili = detalleMateriales(matSili.substring(0,
					(matSili.length() - 1)));
			auxSili = auxsSili;
			model.addAttribute("aceroSilicoso1",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 1));
			model.addAttribute("aceroSilicoso2",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 2));
			model.addAttribute("aceroSilicoso3",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 3));
			model.addAttribute("aceroSilicoso4",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 4));
			model.addAttribute("aceroSilicoso5",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 5));
			model.addAttribute("aceroSilicoso6",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 6));
			model.addAttribute("aceroSilicoso7",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 7));
			model.addAttribute("aceroSilicoso8",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 8));
			model.addAttribute("aceroSilicoso9",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 9));
			model.addAttribute("aceroSilicoso10",
					ViewIngenieria.aceroSilicoso(auxSili, nucFerr, 10));
			model.addAttribute(
					"soporte",
					ViewIngenieria.comboSoporte(isus.getTipoSoporte(),
							nucFerr.getSoporte()));
			model.addAttribute("nucleo",
					ViewIngenieria.nucleo(isus.getTipoNucleo(), nucFerr));
			List<Accesorio> acceso = isus.getAccesorios(diseno);
			String codigosAcceso = HelperIngenieria.codigosAccesorios(acceso);
			List<Auxiliar> auxsAcceso = detalleMateriales(codigosAcceso
					.substring(0, (codigosAcceso.length() - 1)));
			acceso = HelperIngenieria.detalleAccesorio(acceso, auxsAcceso);
			model.addAttribute("accesoriosDataTable",
					ViewIngenieria.accesoriosDataTable(acceso));
			model.addAttribute("modificarListAccesorios", ViewIngenieria
					.modificarListAccesorios("", "", "",
							"accesoriosDatatables", "Guardar", 0));
			model.addAttribute("tabla_c1", ViewIngenieria.comboTablaC(
					isus.getTablaC1(), "combo3", "Voltaje", ""));
			model.addAttribute("tabla_c2", ViewIngenieria.comboTablaC(
					isus.getTablaC2(), "combo5", "Kva", ""));
			model.addAttribute("familia", ViewIngenieria.familia("", "", ""));
			model.addAttribute("url", "ingenieria/" + url);
		} catch (Exception e) {
			logger.error("ingenieria-home", e);
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-bobina-conductores", method = RequestMethod.GET)
	public @ResponseBody String conductoresBobinas(Model model,
			@RequestParam byte val_bobina, @RequestParam String val_codigo,
			@RequestParam double val_bbtAxial,
			@RequestParam double val_batAxial,
			@RequestParam double val_bteAxial,
			@RequestParam double val_bbtRadial,
			@RequestParam double val_batRadial,
			@RequestParam double val_bteRadial) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			if (val_bobina == 1) {
				ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				result = ViewIngenieria.dimensionesBobinas(item.getDim1(),
						item.getDim2(), val_batRadial, val_batAxial,
						val_bteRadial, val_bteAxial);
			}

			if (val_bobina == 2) {
				ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				result = ViewIngenieria.dimensionesBobinas(val_bbtRadial,
						val_bbtAxial, item.getDim1(), item.getDim2(),
						val_bteRadial, val_bteAxial);
			}

			if (val_bobina == 3) {
				ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				result = ViewIngenieria.dimensionesBobinas(val_bbtRadial,
						val_bbtAxial, val_batRadial, val_batAxial,
						item.getDim1(), item.getDim2());
			}

		} catch (Exception e) {
			logger.error("ingenieria-bobina-conductores", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-pintura-plancha-fierro", method = RequestMethod.GET)
	public @ResponseBody String espesoresPlanchasFierro(Model model,
			@RequestParam byte val_espesor, @RequestParam String val_codigo,
			@RequestParam double val_esp_fondo,
			@RequestParam String val_cod_fondo,
			@RequestParam double val_esp_manto,
			@RequestParam String val_cod_manto,
			@RequestParam double val_esp_tapa, @RequestParam String val_cod_tapa) {
		String result = "";
		String materialesPinturaEspesorManto = "";
		String materialesPinturaEspesorTapa = "";
		String materialesPinturaEspesorFondo = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			ItemCubicacion item = isus.getItemCubicacion(val_codigo);
			List<Auxiliar> matePlanFierro = isus.getMaterialesPlanchaFierro();
			String codigos1 = HelperIngenieria.codigos(matePlanFierro);
			List<Auxiliar> auxs1 = detalleMateriales(codigos1.substring(0,
					(codigos1.length() - 1)));
			matePlanFierro = auxs1;
			if (val_espesor == 1) {

				materialesPinturaEspesorManto = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_codigo,
								"val_material_pintura_plancha_fierro_manto", 1);
				materialesPinturaEspesorTapa = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_cod_tapa,
								"val_material_pintura_plancha_fierro_tapa", 2);
				materialesPinturaEspesorFondo = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_cod_fondo,
								"val_material_pintura_plancha_fierro_fondo", 3);
				result = ViewIngenieria.materialesPinturaPlanchaFierro(
						val_esp_fondo, val_cod_fondo, item.getDim1(),
						val_codigo, val_esp_tapa, val_cod_tapa,
						materialesPinturaEspesorManto,
						materialesPinturaEspesorTapa,
						materialesPinturaEspesorFondo);
			}

			if (val_espesor == 2) {
				materialesPinturaEspesorManto = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_cod_manto,
								"val_material_pintura_plancha_fierro_manto", 1);
				materialesPinturaEspesorTapa = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_codigo,
								"val_material_pintura_plancha_fierro_tapa", 2);
				materialesPinturaEspesorFondo = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_cod_fondo,
								"val_material_pintura_plancha_fierro_fondo", 3);
				result = ViewIngenieria.materialesPinturaPlanchaFierro(
						val_esp_fondo, val_cod_fondo, val_esp_manto,
						val_cod_manto, item.getDim1(), val_codigo,
						materialesPinturaEspesorManto,
						materialesPinturaEspesorTapa,
						materialesPinturaEspesorFondo);
			}

			if (val_espesor == 3) {
				materialesPinturaEspesorManto = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_cod_manto,
								"val_material_pintura_plancha_fierro_manto", 1);
				materialesPinturaEspesorTapa = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_cod_tapa,
								"val_material_pintura_plancha_fierro_tapa", 2);
				materialesPinturaEspesorFondo = ViewIngenieria
						.selectMaterialesPlanchaFierro(matePlanFierro,
								val_codigo,
								"val_material_pintura_plancha_fierro_fondo", 3);
				result = ViewIngenieria.materialesPinturaPlanchaFierro(
						item.getDim1(), val_codigo, val_esp_manto,
						val_cod_manto, val_esp_tapa, val_cod_tapa,
						materialesPinturaEspesorManto,
						materialesPinturaEspesorTapa,
						materialesPinturaEspesorFondo);
			}
		} catch (Exception e) {
			logger.error("ingenieria-material-pintura-plancha-fierro", e);
		}

		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-entrecapas-bbt", method = RequestMethod.GET)
	public @ResponseBody String entreCapasBbt(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam double val_bbtEspesoresCapa1,
			@RequestParam String val_bbtCodCapa1,
			@RequestParam String val_bbtDescCapa1,
			@RequestParam double val_bbtEspesoresCapa2,
			@RequestParam String val_bbtCodCapa2,
			@RequestParam String val_bbtDescCapa2,
			@RequestParam double val_bbtEspesoresCapa3,
			@RequestParam String val_bbtCodCapa3,
			@RequestParam String val_bbtDescCapa3) {
		String result = "";
		String entreCapasBbt1 = "";
		String entreCapasBbt2 = "";
		String entreCapasBbt3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> matsPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(matsPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			matsPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBbt1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_codigo, "entreCapasBbt1", 1);
				entreCapasBbt2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_bbtCodCapa2, "entreCapasBbt2", 2);
				entreCapasBbt3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_bbtCodCapa3, "entreCapasBbt3", 3);
				result = ViewIngenieria.entreCapasBbt(val_bbtEspesoresCapa1,
						val_codigo, descri.get(0).getAux10(),
						val_bbtEspesoresCapa2, val_bbtCodCapa2,
						val_bbtDescCapa2, val_bbtEspesoresCapa3,
						val_bbtCodCapa3, val_bbtDescCapa3, entreCapasBbt1,
						entreCapasBbt2, entreCapasBbt3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBbt1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_bbtCodCapa1, "entreCapasBbt1", 1);
				entreCapasBbt2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_codigo, "entreCapasBbt2", 2);
				entreCapasBbt3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_bbtCodCapa3, "entreCapasBbt3", 3);
				result = ViewIngenieria.entreCapasBbt(val_bbtEspesoresCapa1,
						val_bbtCodCapa1, val_bbtDescCapa1,
						val_bbtEspesoresCapa2, val_codigo, descri.get(0)
								.getAux10(), val_bbtEspesoresCapa3,
						val_bbtCodCapa3, val_bbtDescCapa3, entreCapasBbt1,
						entreCapasBbt2, entreCapasBbt3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBbt1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_bbtCodCapa1, "entreCapasBbt1", 1);
				entreCapasBbt2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_bbtCodCapa2, "entreCapasBbt2", 2);
				entreCapasBbt3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBbt(matsPresspan,
								val_codigo, "entreCapasBbt3", 3);
				result = ViewIngenieria.entreCapasBbt(val_bbtEspesoresCapa1,
						val_bbtCodCapa1, val_bbtDescCapa1,
						val_bbtEspesoresCapa2, val_bbtCodCapa2,
						val_bbtDescCapa2, val_bbtEspesoresCapa3, val_codigo,
						descri.get(0).getAux10(), entreCapasBbt1,
						entreCapasBbt2, entreCapasBbt3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-entrecapas-bbt", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-aislacion-externa-bbt", method = RequestMethod.GET)
	public @ResponseBody String aislacionExternaBbt(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam int val_espesoresAtbt1,
			@RequestParam String val_codAtbt1,
			@RequestParam String val_descAtbt1,
			@RequestParam int val_espesoresAtbt2,
			@RequestParam String val_codAtbt2,
			@RequestParam String val_descAtbt2,
			@RequestParam int val_espesoresAtbt3,
			@RequestParam String val_codAtbt3,
			@RequestParam String val_descAtbt3) {
		String result = "";
		String aislacionExternaBbt1 = "";
		String aislacionExternaBbt2 = "";
		String aislacionExternaBbt3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materialesPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(materialesPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			materialesPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBbt1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codigo,
								"aislacionExternaBbt1", 1);
				aislacionExternaBbt2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codAtbt2,
								"aislacionExternaBbt2", 2);
				aislacionExternaBbt3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codAtbt3,
								"aislacionExternaBbt3", 3);
				result = ViewIngenieria.aislacionExternaBbt(val_espesoresAtbt1,
						val_codigo, descri.get(0).getAux10(),
						val_espesoresAtbt2, val_codAtbt2, val_descAtbt2,
						val_espesoresAtbt3, val_codAtbt3, val_descAtbt3,
						aislacionExternaBbt1, aislacionExternaBbt2,
						aislacionExternaBbt3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBbt1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codAtbt1,
								"aislacionExternaBbt1", 1);
				aislacionExternaBbt2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codigo,
								"aislacionExternaBbt2", 2);
				aislacionExternaBbt3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codAtbt3,
								"aislacionExternaBbt3", 3);
				result = ViewIngenieria.aislacionExternaBbt(val_espesoresAtbt1,
						val_codAtbt1, val_descAtbt1, val_espesoresAtbt2,
						val_codigo, descri.get(0).getAux10(),
						val_espesoresAtbt3, val_codAtbt3, val_descAtbt3,
						aislacionExternaBbt1, aislacionExternaBbt2,
						aislacionExternaBbt3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBbt1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codAtbt1,
								"aislacionExternaBbt1", 1);
				aislacionExternaBbt2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codAtbt2,
								"aislacionExternaBbt2", 2);
				aislacionExternaBbt3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBbt(
								materialesPresspan, val_codigo,
								"aislacionExternaBbt3", 3);
				result = ViewIngenieria.aislacionExternaBbt(val_espesoresAtbt1,
						val_codAtbt1, val_descAtbt1, val_espesoresAtbt2,
						val_codAtbt2, val_descAtbt2, val_espesoresAtbt3,
						val_codigo, descri.get(0).getAux10(),
						aislacionExternaBbt1, aislacionExternaBbt2,
						aislacionExternaBbt3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-aislacion-externa-bbt",
					e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-aislacion-base-bbt", method = RequestMethod.GET)
	public @ResponseBody String aislacionBaseBbt(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam int val_espesoresBase1,
			@RequestParam String val_codBase1,
			@RequestParam String val_descBase1,
			@RequestParam int val_espesoresBase2,
			@RequestParam String val_codBase2,
			@RequestParam String val_descBase2,
			@RequestParam int val_espesoresBase3,
			@RequestParam String val_codBase3,
			@RequestParam String val_descBase3) {
		String result = "";
		String aislacionBaseBbt1 = "";
		String aislacionBaseBbt2 = "";
		String aislacionBaseBbt3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materialesPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(materialesPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			materialesPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBbt1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codigo,
								"aislacionBaseBbt1", 1);
				aislacionBaseBbt2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codBase2,
								"aislacionBaseBbt2", 2);
				aislacionBaseBbt3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codBase3,
								"aislacionBaseBbt3", 3);
				result = ViewIngenieria
						.aislacionBasebbt(val_espesoresBase1, val_codigo,
								descri.get(0).getAux10(), val_espesoresBase2,
								val_codBase2, val_descBase2,
								val_espesoresBase3, val_codBase3,
								val_descBase3, aislacionBaseBbt1,
								aislacionBaseBbt2, aislacionBaseBbt3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBbt1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codBase1,
								"aislacionBaseBbt1", 1);
				aislacionBaseBbt2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codigo,
								"aislacionBaseBbt2", 2);
				aislacionBaseBbt3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codBase3,
								"aislacionBaseBbt3", 3);
				result = ViewIngenieria
						.aislacionBasebbt(val_espesoresBase1, val_codBase1,
								val_descBase1, val_espesoresBase2, val_codigo,
								descri.get(0).getAux10(), val_espesoresBase3,
								val_codBase3, val_descBase3, aislacionBaseBbt1,
								aislacionBaseBbt2, aislacionBaseBbt3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBbt1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codBase1,
								"aislacionBaseBbt1", 1);
				aislacionBaseBbt2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codBase2,
								"aislacionBaseBbt2", 2);
				aislacionBaseBbt3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBbt(
								materialesPresspan, val_codigo,
								"aislacionBaseBbt3", 3);
				result = ViewIngenieria
						.aislacionBasebbt(val_espesoresBase1, val_codBase1,
								val_descBase1, val_espesoresBase2,
								val_codBase2, val_descBase2,
								val_espesoresBase3, val_codigo, descri.get(0)
										.getAux10(), aislacionBaseBbt1,
								aislacionBaseBbt2, aislacionBaseBbt3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-aislacion-base-bbt", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-entrecapas-bat", method = RequestMethod.GET)
	public @ResponseBody String entreCapasBat(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam double val_batEspesoresCapa1,
			@RequestParam String val_batCodCapa1,
			@RequestParam String val_batDescCapa1,
			@RequestParam double val_batEspesoresCapa2,
			@RequestParam String val_batCodCapa2,
			@RequestParam String val_batDescCapa2,
			@RequestParam double val_batEspesoresCapa3,
			@RequestParam String val_batCodCapa3,
			@RequestParam String val_batDescCapa3) {
		String result = "";
		String entreCapasBat1 = "";
		String entreCapasBat2 = "";
		String entreCapasBat3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> matsPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(matsPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			matsPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBat1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_codigo, "entreCapasBat1", 1);
				entreCapasBat2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_batCodCapa2, "entreCapasBat2", 2);
				entreCapasBat3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_batCodCapa3, "entreCapasBat3", 3);
				result = ViewIngenieria.entreCapasBat(val_batEspesoresCapa1,
						val_codigo, descri.get(0).getAux10(),
						val_batEspesoresCapa2, val_batCodCapa2,
						val_batDescCapa2, val_batEspesoresCapa3,
						val_batCodCapa3, val_batDescCapa3, entreCapasBat1,
						entreCapasBat2, entreCapasBat3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBat1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_batCodCapa1, "entreCapasBat1", 1);
				entreCapasBat2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_codigo, "entreCapasBat2", 2);
				entreCapasBat3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_batCodCapa3, "entreCapasBat3", 3);
				result = ViewIngenieria.entreCapasBat(val_batEspesoresCapa1,
						val_batCodCapa1, val_batDescCapa1,
						val_batEspesoresCapa2, val_codigo, descri.get(0)
								.getAux10(), val_batEspesoresCapa3,
						val_batCodCapa3, val_batDescCapa3, entreCapasBat1,
						entreCapasBat2, entreCapasBat3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBat1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_batCodCapa1, "entreCapasBat1", 1);
				entreCapasBat2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_batCodCapa2, "entreCapasBat2", 2);
				entreCapasBat3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBat(matsPresspan,
								val_codigo, "entreCapasBat3", 3);
				result = ViewIngenieria.entreCapasBat(val_batEspesoresCapa1,
						val_batCodCapa1, val_batDescCapa1,
						val_batEspesoresCapa2, val_batCodCapa2,
						val_batDescCapa2, val_batEspesoresCapa3, val_codigo,
						descri.get(0).getAux10(), entreCapasBat1,
						entreCapasBat2, entreCapasBat3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-entrecapas-bat", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-aislacion-externa-bat", method = RequestMethod.GET)
	public @ResponseBody String aislacionExternaBat(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam int val_batespesoresAtbt1,
			@RequestParam String val_batcodAtbt1,
			@RequestParam String val_batdescAtbt1,
			@RequestParam int val_batespesoresAtbt2,
			@RequestParam String val_batcodAtbt2,
			@RequestParam String val_batdescAtbt2,
			@RequestParam int val_batespesoresAtbt3,
			@RequestParam String val_batcodAtbt3,
			@RequestParam String val_batdescAtbt3) {
		String result = "";
		String aislacionExternaBat1 = "";
		String aislacionExternaBat2 = "";
		String aislacionExternaBat3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materialesPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(materialesPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			materialesPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBat1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_codigo,
								"aislacionExternaBat1", 1);
				aislacionExternaBat2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_batcodAtbt2,
								"aislacionExternaBat2", 2);
				aislacionExternaBat3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_batcodAtbt3,
								"aislacionExternaBat3", 3);
				result = ViewIngenieria.aislacionExternaBat(
						val_batespesoresAtbt1, val_codigo, descri.get(0)
								.getAux10(), val_batespesoresAtbt2,
						val_batcodAtbt2, val_batdescAtbt2,
						val_batespesoresAtbt3, val_batcodAtbt3,
						val_batdescAtbt3, aislacionExternaBat1,
						aislacionExternaBat2, aislacionExternaBat3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBat1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_batcodAtbt1,
								"aislacionExternaBat1", 1);
				aislacionExternaBat2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_codigo,
								"aislacionExternaBat2", 2);
				aislacionExternaBat3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_batcodAtbt3,
								"aislacionExternaBat3", 3);
				result = ViewIngenieria.aislacionExternaBat(
						val_batespesoresAtbt1, val_batcodAtbt1,
						val_batdescAtbt1, val_batespesoresAtbt2, val_codigo,
						descri.get(0).getAux10(), val_batespesoresAtbt3,
						val_batcodAtbt3, val_batdescAtbt3,
						aislacionExternaBat1, aislacionExternaBat2,
						aislacionExternaBat3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBat1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_batcodAtbt1,
								"aislacionExternaBat1", 1);
				aislacionExternaBat2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_batcodAtbt2,
								"aislacionExternaBat2", 2);
				aislacionExternaBat3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBat(
								materialesPresspan, val_codigo,
								"aislacionExternaBat3", 3);
				result = ViewIngenieria.aislacionExternaBat(
						val_batespesoresAtbt1, val_batcodAtbt1,
						val_batdescAtbt1, val_batespesoresAtbt2,
						val_batcodAtbt2, val_batdescAtbt2,
						val_batespesoresAtbt3, val_codigo, descri.get(0)
								.getAux10(), aislacionExternaBat1,
						aislacionExternaBat2, aislacionExternaBat3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-aislacion-externa-bat",
					e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-aislacion-base-bat", method = RequestMethod.GET)
	public @ResponseBody String aislacionBaseBat(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam int val_batespesoresBase1,
			@RequestParam String val_batcodBase1,
			@RequestParam String val_batdescBase1,
			@RequestParam int val_batespesoresBase2,
			@RequestParam String val_batcodBase2,
			@RequestParam String val_batdescBase2,
			@RequestParam int val_batespesoresBase3,
			@RequestParam String val_batcodBase3,
			@RequestParam String val_batdescBase3) {
		String result = "";
		String aislacionBaseBat1 = "";
		String aislacionBaseBat2 = "";
		String aislacionBaseBat3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materialesPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(materialesPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			materialesPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBat1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_codigo,
								"aislacionBaseBat1", 1);
				aislacionBaseBat2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_batcodBase2,
								"aislacionBaseBat2", 2);
				aislacionBaseBat3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_batcodBase3,
								"aislacionBaseBat3", 3);
				result = ViewIngenieria.aislacionBasebat(val_batespesoresBase1,
						val_codigo, descri.get(0).getAux10(),
						val_batespesoresBase2, val_batcodBase2,
						val_batdescBase2, val_batespesoresBase3,
						val_batcodBase3, val_batdescBase3, aislacionBaseBat1,
						aislacionBaseBat2, aislacionBaseBat3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBat1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_batcodBase1,
								"aislacionBaseBat1", 1);
				aislacionBaseBat2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_codigo,
								"aislacionBaseBat2", 2);
				aislacionBaseBat3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_batcodBase3,
								"aislacionBaseBat3", 3);
				result = ViewIngenieria.aislacionBasebat(val_batespesoresBase1,
						val_batcodBase1, val_batdescBase1,
						val_batespesoresBase2, val_codigo, descri.get(0)
								.getAux10(), val_batespesoresBase3,
						val_batcodBase3, val_batdescBase3, aislacionBaseBat1,
						aislacionBaseBat2, aislacionBaseBat3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBat1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_batcodBase1,
								"aislacionBaseBat1", 1);
				aislacionBaseBat2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_batcodBase2,
								"aislacionBaseBat2", 2);
				aislacionBaseBat3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBat(
								materialesPresspan, val_codigo,
								"aislacionBaseBat3", 3);
				result = ViewIngenieria.aislacionBasebat(val_batespesoresBase1,
						val_batcodBase1, val_batdescBase1,
						val_batespesoresBase2, val_batcodBase2,
						val_batdescBase2, val_batespesoresBase3, val_codigo,
						descri.get(0).getAux10(), aislacionBaseBat1,
						aislacionBaseBat2, aislacionBaseBat3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-aislacion-base-bat", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-entrecapas-bte", method = RequestMethod.GET)
	public @ResponseBody String entreCapasBte(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam double val_bteEspesoresCapa1,
			@RequestParam String val_bteCodCapa1,
			@RequestParam String val_bteDescCapa1,
			@RequestParam double val_bteEspesoresCapa2,
			@RequestParam String val_bteCodCapa2,
			@RequestParam String val_bteDescCapa2,
			@RequestParam double val_bteEspesoresCapa3,
			@RequestParam String val_bteCodCapa3,
			@RequestParam String val_bteDescCapa3) {
		String result = "";
		String entreCapasBte1 = "";
		String entreCapasBte2 = "";
		String entreCapasBte3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> matsPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(matsPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			matsPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBte1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_codigo, "entreCapasBte1", 1);
				entreCapasBte2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_bteCodCapa2, "entreCapasBte2", 2);
				entreCapasBte3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_bteCodCapa3, "entreCapasBte3", 3);
				result = ViewIngenieria.entreCapasBte(val_bteEspesoresCapa1,
						val_codigo, descri.get(0).getAux10(),
						val_bteEspesoresCapa2, val_bteCodCapa2,
						val_bteDescCapa2, val_bteEspesoresCapa3,
						val_bteCodCapa3, val_bteDescCapa3, entreCapasBte1,
						entreCapasBte2, entreCapasBte3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBte1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_bteCodCapa1, "entreCapasBte1", 1);
				entreCapasBte2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_codigo, "entreCapasBte2", 2);
				entreCapasBte3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_bteCodCapa3, "entreCapasBte3", 3);
				result = ViewIngenieria.entreCapasBte(val_bteEspesoresCapa1,
						val_bteCodCapa1, val_bteDescCapa1,
						val_bteEspesoresCapa2, val_codigo, descri.get(0)
								.getAux10(), val_bteEspesoresCapa3,
						val_bteCodCapa3, val_bteDescCapa3, entreCapasBte1,
						entreCapasBte2, entreCapasBte3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				entreCapasBte1 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_bteCodCapa1, "entreCapasBte1", 1);
				entreCapasBte2 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_bteCodCapa2, "entreCapasBte2", 2);
				entreCapasBte3 = ViewIngenieria
						.selectMaterialesPresspanEntreCapasBte(matsPresspan,
								val_codigo, "entreCapasBte3", 3);
				result = ViewIngenieria.entreCapasBte(val_bteEspesoresCapa1,
						val_bteCodCapa1, val_bteDescCapa1,
						val_bteEspesoresCapa2, val_bteCodCapa2,
						val_bteDescCapa2, val_bteEspesoresCapa3, val_codigo,
						descri.get(0).getAux10(), entreCapasBte1,
						entreCapasBte2, entreCapasBte3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-entrecapas-bte", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-aislacion-externa-bte", method = RequestMethod.GET)
	public @ResponseBody String aislacionExternaBte(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam int val_bteespesoresAtbt1,
			@RequestParam String val_btecodAtbt1,
			@RequestParam String val_btedescAtbt1,
			@RequestParam int val_bteespesoresAtbt2,
			@RequestParam String val_btecodAtbt2,
			@RequestParam String val_btedescAtbt2,
			@RequestParam int val_bteespesoresAtbt3,
			@RequestParam String val_btecodAtbt3,
			@RequestParam String val_btedescAtbt3) {
		String result = "";
		String aislacionExternaBte1 = "";
		String aislacionExternaBte2 = "";
		String aislacionExternaBte3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materialesPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(materialesPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			materialesPresspan = auxsPress;
			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBte1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_codigo,
								"aislacionExternaBte1", 1);
				aislacionExternaBte2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_btecodAtbt2,
								"aislacionExternaBte2", 2);
				aislacionExternaBte3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_btecodAtbt3,
								"aislacionExternaBte3", 3);
				result = ViewIngenieria.aislacionExternaBte(
						val_bteespesoresAtbt1, val_codigo, descri.get(0)
								.getAux10(), val_bteespesoresAtbt2,
						val_btecodAtbt2, val_btedescAtbt2,
						val_bteespesoresAtbt3, val_btecodAtbt3,
						val_btedescAtbt3, aislacionExternaBte1,
						aislacionExternaBte2, aislacionExternaBte3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBte1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_btecodAtbt1,
								"aislacionExternaBte1", 1);
				aislacionExternaBte2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_codigo,
								"aislacionExternaBte2", 2);
				aislacionExternaBte3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_btecodAtbt3,
								"aislacionExternaBte3", 3);
				result = ViewIngenieria.aislacionExternaBte(
						val_bteespesoresAtbt1, val_btecodAtbt1,
						val_btedescAtbt1, val_bteespesoresAtbt2, val_codigo,
						descri.get(0).getAux10(), val_bteespesoresAtbt3,
						val_btecodAtbt3, val_btedescAtbt3,
						aislacionExternaBte1, aislacionExternaBte2,
						aislacionExternaBte3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionExternaBte1 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_btecodAtbt1,
								"aislacionExternaBte1", 1);
				aislacionExternaBte2 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_btecodAtbt2,
								"aislacionExternaBte2", 2);
				aislacionExternaBte3 = ViewIngenieria
						.selectMaterialesPresspanAislacionExternaBte(
								materialesPresspan, val_codigo,
								"aislacionExternaBte3", 3);
				result = ViewIngenieria.aislacionExternaBte(
						val_bteespesoresAtbt1, val_btecodAtbt1,
						val_btedescAtbt1, val_bteespesoresAtbt2,
						val_btecodAtbt2, val_btedescAtbt2,
						val_bteespesoresAtbt3, val_codigo, descri.get(0)
								.getAux10(), aislacionExternaBte1,
						aislacionExternaBte2, aislacionExternaBte3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-aislacion-externa-bte",
					e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-presspan-aislacion-base-bte", method = RequestMethod.GET)
	public @ResponseBody String aislacionBaseBte(Model model,
			@RequestParam byte val_tipo_presspan,
			@RequestParam String val_codigo,
			@RequestParam int val_bteespesoresBase1,
			@RequestParam String val_btecodBase1,
			@RequestParam String val_btedescBase1,
			@RequestParam int val_bteespesoresBase2,
			@RequestParam String val_btecodBase2,
			@RequestParam String val_btedescBase2,
			@RequestParam int val_bteespesoresBase3,
			@RequestParam String val_btecodBase3,
			@RequestParam String val_btedescBase3) {
		String result = "";
		String aislacionBaseBte1 = "";
		String aislacionBaseBte2 = "";
		String aislacionBaseBte3 = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materialesPresspan = isus.getMaterialesPresspan();
			String matPress = HelperIngenieria.codigos(materialesPresspan);
			List<Auxiliar> auxsPress = detalleMateriales(matPress.substring(0,
					(matPress.length() - 1)));
			materialesPresspan = auxsPress;

			List<Auxiliar> descri = detalleMateriales(val_codigo);
			if (val_tipo_presspan == 1) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBte1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_codigo,
								"aislacionBaseBte1", 1);
				aislacionBaseBte2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_btecodBase2,
								"aislacionBaseBte2", 2);
				aislacionBaseBte3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_btecodBase3,
								"aislacionBaseBte3", 3);
				result = ViewIngenieria.aislacionBasebte(val_bteespesoresBase1,
						val_codigo, descri.get(0).getAux10(),
						val_bteespesoresBase2, val_btecodBase2,
						val_btedescBase2, val_bteespesoresBase3,
						val_btecodBase3, val_btedescBase3, aislacionBaseBte1,
						aislacionBaseBte2, aislacionBaseBte3);
			}

			if (val_tipo_presspan == 2) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo);
				aislacionBaseBte1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_btecodBase1,
								"aislacionBaseBte1", 1);
				aislacionBaseBte2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_codigo,
								"aislacionBaseBte2", 2);
				aislacionBaseBte3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_btecodBase3,
								"aislacionBaseBte3", 3);
				result = ViewIngenieria.aislacionBasebte(val_bteespesoresBase1,
						val_btecodBase1, val_btedescBase1,
						val_bteespesoresBase2, val_codigo, descri.get(0)
								.getAux10(), val_bteespesoresBase3,
						val_btecodBase3, val_btedescBase3, aislacionBaseBte1,
						aislacionBaseBte2, aislacionBaseBte3);
			}

			if (val_tipo_presspan == 3) {
				// ItemCubicacion item = isus.getItemCubicacion(val_codigo)
				aislacionBaseBte1 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_btecodBase1,
								"aislacionBaseBte1", 1);
				aislacionBaseBte2 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_btecodBase2,
								"aislacionBaseBte2", 2);
				aislacionBaseBte3 = ViewIngenieria
						.selectMaterialesPresspanAislacionBaseBte(
								materialesPresspan, val_codigo,
								"aislacionBaseBte3", 3);
				result = ViewIngenieria.aislacionBasebte(val_bteespesoresBase1,
						val_btecodBase1, val_btedescBase1,
						val_bteespesoresBase2, val_btecodBase2,
						val_btedescBase2, val_bteespesoresBase3, val_codigo,
						descri.get(0).getAux10(), aislacionBaseBte1,
						aislacionBaseBte2, aislacionBaseBte3);
			}

		} catch (Exception e) {
			logger.error("ingenieria-material-presspan-aislacion-base-bte", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-nuc-ferr-tipo-nucleo", method = RequestMethod.GET)
	public @ResponseBody String tipoNucleo(Model model,
			@RequestParam char tipoNucleo,
			@RequestParam int val_entreCentroNucleo,
			@RequestParam int val_ventana_nucleo) {
		String result = "";

		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			NucleoFerreteria nucFerr = new NucleoFerreteria();
			nucFerr.setTipo(tipoNucleo);
			nucFerr.setEntreCentroNucleo(val_entreCentroNucleo);
			nucFerr.setVentanaNucleo(val_ventana_nucleo);
			result = ViewIngenieria.nucleo(isus.getTipoNucleo(), nucFerr);

		} catch (Exception e) {
			logger.error("ingenieria-nuc-ferr-tipo-nucleo", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-nuc-ferr-tipo-acero-silicoso", method = RequestMethod.GET)
	public @ResponseBody String tipoNucleo(Model model,
			@RequestParam String val_codigo_silicoso,
			@RequestParam int val_acesilicant,
			@RequestParam double val_acesilipesonucleo, @RequestParam int i,
			@RequestParam char val_tipo_nucleo) {
		String result = "";

		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			ItemCubicacion item = isus.getItemCubicacion(val_codigo_silicoso);
			NucleoFerreteria nucFerr = new NucleoFerreteria();
			List<Auxiliar> descri = detalleMateriales(val_codigo_silicoso);
			item.setDescripcion(descri.get(0).getAux10());
			List<Auxiliar> auxSili = isus.getAceroSilicoso();
			String matSili = HelperIngenieria.codigos(auxSili);
			List<Auxiliar> auxsSili = detalleMateriales(matSili.substring(0,
					(matSili.length() - 1)));
			auxSili = auxsSili;
			if (i == 1) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso1(val_codigo_silicoso);
					nucFerr.setCantidad1(val_acesilicant);
					nucFerr.setPesoNucleo1(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso1(val_codigo_silicoso);
					nucFerr.setCantidad1(val_acesilicant);
					nucFerr.setPesoNucleo1(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 2) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso2(val_codigo_silicoso);
					nucFerr.setCantidad2(val_acesilicant);
					nucFerr.setPesoNucleo2(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso2(val_codigo_silicoso);
					nucFerr.setCantidad2(val_acesilicant);
					nucFerr.setPesoNucleo2(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 3) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso3(val_codigo_silicoso);
					nucFerr.setCantidad3(val_acesilicant);
					nucFerr.setPesoNucleo3(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso3(val_codigo_silicoso);
					nucFerr.setCantidad3(val_acesilicant);
					nucFerr.setPesoNucleo3(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 4) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso4(val_codigo_silicoso);
					nucFerr.setCantidad4(val_acesilicant);
					nucFerr.setPesoNucleo4(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso4(val_codigo_silicoso);
					nucFerr.setCantidad4(val_acesilicant);
					nucFerr.setPesoNucleo4(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 5) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso5(val_codigo_silicoso);
					nucFerr.setCantidad5(val_acesilicant);
					nucFerr.setPesoNucleo5(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso5(val_codigo_silicoso);
					nucFerr.setCantidad5(val_acesilicant);
					nucFerr.setPesoNucleo5(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 6) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso6(val_codigo_silicoso);
					nucFerr.setCantidad6(val_acesilicant);
					nucFerr.setPesoNucleo6(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso6(val_codigo_silicoso);
					nucFerr.setCantidad6(val_acesilicant);
					nucFerr.setPesoNucleo6(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 7) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso7(val_codigo_silicoso);
					nucFerr.setCantidad7(val_acesilicant);
					nucFerr.setPesoNucleo7(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso7(val_codigo_silicoso);
					nucFerr.setCantidad7(val_acesilicant);
					nucFerr.setPesoNucleo7(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 8) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso8(val_codigo_silicoso);
					nucFerr.setCantidad8(val_acesilicant);
					nucFerr.setPesoNucleo8(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso8(val_codigo_silicoso);
					nucFerr.setCantidad8(val_acesilicant);
					nucFerr.setPesoNucleo8(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 9) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso9(val_codigo_silicoso);
					nucFerr.setCantidad9(val_acesilicant);
					nucFerr.setPesoNucleo9(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso9(val_codigo_silicoso);
					nucFerr.setCantidad9(val_acesilicant);
					nucFerr.setPesoNucleo9(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			} else if (i == 10) {
				if (item.getDescripcion().toUpperCase().indexOf("NUCLEO") > -1) {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso10(val_codigo_silicoso);
					nucFerr.setCantidad10(val_acesilicant);
					nucFerr.setPesoNucleo10(item.getDim3());
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				} else {
					nucFerr.setTipo(val_tipo_nucleo);
					nucFerr.setCodigoSilicoso10(val_codigo_silicoso);
					nucFerr.setCantidad10(val_acesilicant);
					nucFerr.setPesoNucleo10(val_acesilipesonucleo);
					result = ViewIngenieria.aceroSilicoso(auxSili, nucFerr, i);
				}
			}

		} catch (Exception e) {
			logger.error("ingenieria-nuc-ferr-tipo-acero-silicoso", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-material-pintura-estanque", method = RequestMethod.GET)
	public @ResponseBody String pinturaEstanque(Model model,
			@RequestParam int i, @RequestParam String val_codPin1,
			@RequestParam String val_codPin2, @RequestParam String val_codPin3,
			@RequestParam String val_codPin4, @RequestParam String val_codigo) {
		String result = "";

		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> materPintura = isus.getMaterialesPintura();
			String codigos = HelperIngenieria.codigos(materPintura);
			List<Auxiliar> auxs = detalleMateriales(codigos.substring(0,
					(codigos.length() - 1)));
			materPintura = auxs;
			if (i == 1) {
				EstanqueTapa est = new EstanqueTapa();
				est.setPin1(val_codigo);
				est.setPin2(val_codPin2);
				est.setPin3(val_codPin3);
				est.setPin4(val_codPin4);
				result = ViewIngenieria.pinturaEstanque(materPintura, est);

			}

			if (i == 2) {
				EstanqueTapa est = new EstanqueTapa();
				est.setPin1(val_codPin1);
				est.setPin2(val_codigo);
				est.setPin3(val_codPin3);
				est.setPin4(val_codPin4);
				result = ViewIngenieria.pinturaEstanque(materPintura, est);

			}

			if (i == 3) {
				EstanqueTapa est = new EstanqueTapa();
				est.setPin1(val_codPin1);
				est.setPin2(val_codPin2);
				est.setPin3(val_codigo);
				est.setPin4(val_codPin4);
				result = ViewIngenieria.pinturaEstanque(materPintura, est);

			}

			if (i == 4) {
				EstanqueTapa est = new EstanqueTapa();
				est.setPin1(val_codPin1);
				est.setPin2(val_codPin2);
				est.setPin3(val_codPin3);
				est.setPin4(val_codigo);
				result = ViewIngenieria.pinturaEstanque(materPintura, est);

			}

		} catch (Exception e) {
			logger.error("ingenieria-material-pintura-estanque", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-busqueda", method = RequestMethod.GET)
	public String busqueda(Model model) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			model.addAttribute("theadDiseno", ViewIngenieria.theadDiseno(
					ViewIngenieria.selectDistribucionAll(
							isus.getDistribucionesAll(), 0), ViewIngenieria
							.selectRefrigerante(isus.getRefrigerantes(), -1),
					ViewIngenieria.selectConductorBobina("val_conductor_bbt"),
					ViewIngenieria.selectConductorBobina("val_conductor_bat"),
					"", "", "0", "0", "0", "", "0", "0"));
			model.addAttribute("url", "ingenieria/buscarDiseno.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-busqueda", e);
		}

		return "home";
	}

	@RequestMapping(value = "/ingenieria-buscar-disenos-datatable", method = RequestMethod.POST)
	public String buscarDiseno(Model model, String val_diseno,
			String val_codigo, String val_distribuciones_all,
			double val_potencia, double val_kv, String val_refrigerante,
			int val_voltaje_primario, String val_voltaje_secundario,
			double val_impedancia, int val_altura_instalacion,
			String val_conductor_bbt, String val_conductor_bat)
			throws Exception {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			model.addAttribute("theadDiseno", ViewIngenieria.theadDiseno(
					ViewIngenieria.selectDistribucionAll(
							isus.getDistribucionesAll(),
							Integer.parseInt(val_distribuciones_all)),
					ViewIngenieria.selectRefrigerante(isus.getRefrigerantes(),
							Integer.parseInt(val_refrigerante)), ViewIngenieria
							.selectConductorBobina("val_conductor_bbt"),
					ViewIngenieria.selectConductorBobina("val_conductor_bat"),
					val_diseno, val_codigo, String.valueOf(val_potencia),
					String.valueOf(val_kv), String
							.valueOf(val_voltaje_primario),
					val_voltaje_secundario, String.valueOf(val_impedancia),
					String.valueOf(val_altura_instalacion)));
			List<Auxiliar> disenos = isus.getDisenoBusqueda();
			List<Auxiliar> disenos1 = HelperIngenieria.disenos(val_diseno,
					val_codigo, val_distribuciones_all, val_potencia, val_kv,
					val_refrigerante, val_voltaje_primario,
					val_voltaje_secundario, val_impedancia,
					val_altura_instalacion, val_conductor_bbt,
					val_conductor_bat, disenos);
			model.addAttribute("disenoBusquedaDataTable",
					ViewIngenieria.disenoBusquedaDataTable(disenos1));

		} catch (Exception e) {
			logger.error("ingenieria-buscar-disenos-datatable", e);
		}
		model.addAttribute("url", "ingenieria/buscarDiseno.jsp");
		return "home";
	}

	@RequestMapping(value = "/ingenieria-accesorios-datatable", method = RequestMethod.GET)
	public @ResponseBody String accesoriosDatatables(
			@RequestParam int val_cantidad_accesorio,
			@RequestParam String val_codigo_accesorio,
			@RequestParam String val_datosgenerales_codigo,
			@RequestParam int modificarAccesorio) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Accesorio accesorio = new Accesorio();
			accesorio.setCodigo(val_codigo_accesorio.trim());
			accesorio.setCantidad(val_cantidad_accesorio);
			accesorio.setDiseno(val_datosgenerales_codigo.trim());
			if (modificarAccesorio == 0) {
				isus.insertAccesorio(accesorio);
			}
			if (modificarAccesorio == 1) {
				isus.actualizarAccesorios(accesorio);
			}
			result = ViewIngenieria.accesoriosDataTable(isus
					.getAccesorios(val_datosgenerales_codigo.trim()));
		} catch (Exception e) {
			logger.error("ingenieria-accesorios-datatable", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-accesorios-delete", method = RequestMethod.GET)
	public @ResponseBody String accesoriosDelete(
			@RequestParam String val_codigo_accesorio,
			@RequestParam String val_datosgenerales_codigo) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Accesorio accesorio = new Accesorio();
			accesorio.setCodigo(val_codigo_accesorio.trim());
			accesorio.setDiseno(val_datosgenerales_codigo.trim());
			isus.deleteAccesorio(accesorio);
			result = ViewIngenieria.accesoriosDataTable(isus
					.getAccesorios(val_datosgenerales_codigo.trim()));
		} catch (Exception e) {
			logger.error("ingenieria-accesorios-delete", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-accesorios-modificar", method = RequestMethod.GET)
	public @ResponseBody String accesoriosModificar(
			@RequestParam String val_cantidad_accesorio,
			@RequestParam String val_codigo_accesorio,
			@RequestParam String val_descripcion_accesorio) {
		String result = "";
		try {
			result = ViewIngenieria.modificarListAccesorios(
					val_codigo_accesorio, val_descripcion_accesorio,
					val_cantidad_accesorio, "accesoriosDatatables",
					"Actualizar", 1);
		} catch (Exception e) {
			logger.error("ingenieria-accesorios-modificar", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-accesorios-limpiar", method = RequestMethod.GET)
	public @ResponseBody String accesoriosLimpiar() {
		String result = "";
		try {
			result = ViewIngenieria.modificarListAccesorios("", "", "",
					"accesoriosDatatables", "Guardar", 0);
		} catch (Exception e) {
			logger.error("ingenieria-accesorios-limpiar", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-codigo-trafo-cotizacion", method = RequestMethod.GET)
	public @ResponseBody String codigoTrafoCoti(Model model,
			@RequestParam String val_combo0, @RequestParam String val_combo1,
			@RequestParam String val_combo2, @RequestParam String val_combo3,
			@RequestParam String val_combo4, @RequestParam String val_combo5) {
		String result = "";
		try {

			if (val_combo2.equals("2")) {
				IngenieriaServicio isus = (IngenieriaServicio) context
						.getBean("ingenieriaServicio");
				result += ViewIngenieria.familia(val_combo1, val_combo2,
						val_combo1);
				result += ViewIngenieria.comboTablaC(isus.getTablaC3(),
						"combo3", "Voltaje", val_combo3);
				result += ViewIngenieria.comboTablaC(isus.getTablaC4(),
						"combo5", "Kva", val_combo5);
			} else {
				IngenieriaServicio isus = (IngenieriaServicio) context
						.getBean("ingenieriaServicio");
				result += ViewIngenieria.familia(val_combo1, val_combo2,
						val_combo1);
				result += ViewIngenieria.comboTablaC(isus.getTablaC1(),
						"combo3", "Voltaje", val_combo3);
				result += ViewIngenieria.comboTablaC(isus.getTablaC2(),
						"combo5", "Kva", val_combo5);
			}

		} catch (Exception e) {
			logger.error("ingenieria-codigo-trafo-cotizacion", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-codigo-trafo-cotizacion-generar", method = RequestMethod.GET)
	public @ResponseBody String codigoTrafoCotiGenerar(Model model,
			@RequestParam String val_combo0, @RequestParam String val_combo1,
			@RequestParam String val_combo2, @RequestParam String val_combo3,
			@RequestParam String val_combo4, @RequestParam String val_combo5,
			@RequestParam String diseno2, @RequestParam String seAc) {
		String result = "";
		try {

			if (!val_combo0.trim().equals("")) {
				result += val_combo0;
			}

			if (!val_combo2.trim().equals("")) {
				result += val_combo2;
			}

			if (!val_combo1.trim().equals("")) {
				result += val_combo1;
			}

			if (!val_combo3.trim().equals("")) {
				result += String.valueOf(100 + Integer.parseInt(val_combo3)).substring(1,3);
			}

			if (!val_combo4.trim().equals("")) {
				result += val_combo4;
			}

			if (!val_combo5.trim().equals("")) {
				result += String.valueOf(100 + Integer.parseInt(val_combo5)).substring(1,3);
			}
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");

			int val = isus.getMaxCodigoDiseno(result);
			String val1 = "";
			if (val < 10) {
				val1 += "0" + val;
			} else {
				val1 += val;
			}
			result = ViewIngenieria.codigoTrafoCotiGenerar(result + val1,
					diseno2, seAc);
		} catch (Exception e) {
			logger.error("ingenieria-codigo-trafo-cotizacion-generar", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-insertar-diseno-seco", method = RequestMethod.POST)
	public String insertarDisenoSeco(Model model, Principal principal,
			@RequestParam String diseno2,
			@RequestParam String val_codigo_equicotiGene) {
		SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Diseno dis = isus.getDiseno(diseno2);
			dis.setIdDiseno(val_codigo_equicotiGene);
			dis.setFecha(fecha.format(new Date()));
			dis.setUsuario(principal.getName());
			Especificacion especificacion = isus.getEspecificacion(diseno2);
			especificacion.setDiseno(val_codigo_equicotiGene);
			Auxiliar aux = new Auxiliar();
			aux.setAux9(diseno2);
			aux.setAux1(1);
			Bobina bobbbt = isus.getBobina(aux);
			bobbbt.setDiseno(val_codigo_equicotiGene);
			Presspan prebbt = isus.getPresspan(aux);
			prebbt.setDiseno(val_codigo_equicotiGene);
			aux.setAux1(2);
			Bobina bobbat = isus.getBobina(aux);
			bobbat.setDiseno(val_codigo_equicotiGene);
			Presspan prebat = isus.getPresspan(aux);
			prebat.setDiseno(val_codigo_equicotiGene);
			aux.setAux1(3);
			Bobina bobbte = isus.getBobina(aux);
			bobbte.setDiseno(val_codigo_equicotiGene);
			Presspan prebte = isus.getPresspan(aux);
			prebte.setDiseno(val_codigo_equicotiGene);
			NucleoFerreteria nucFerr = isus.getNucleoFerreteria(diseno2);
			nucFerr.setDiseno(val_codigo_equicotiGene);
			EstanqueTapa estTapa = isus.getEstanqueTapa(diseno2);
			estTapa.setDiseno(val_codigo_equicotiGene);
			List<Accesorio> accesorios = isus.getAccesorios(diseno2);
			HelperIngenieria.disenoAccesorios(accesorios,
					val_codigo_equicotiGene);

			isus.insertSeco(accesorios, dis, especificacion, nucFerr, estTapa,
					bobbbt, bobbat, bobbte, prebbt, prebat, prebte);
			// model.addAttribute("url", "ingenieria/buscarDiseno.jsp");
			model.addAttribute(
					"popup",
					ViewIngenieria.success("ingenieria-home?val_diseno="
							+ val_codigo_equicotiGene, true));

		} catch (Exception e) {
			logger.error("ingenieria-insertar-diseno-seco", e);
			model.addAttribute("popup",
					ViewIngenieria.error("ingenieria-busqueda", true));
		}
		return "information";
	}

	@RequestMapping(value = "/ingenieria-insertar-diseno-aceite", method = RequestMethod.POST)
	public String insertarDisenoAceite(Model model, Principal principal,
			@RequestParam String diseno2,
			@RequestParam String val_codigo_equicotiGene) {
		SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Diseno dis = isus.getDiseno(diseno2);
			dis.setIdDiseno(val_codigo_equicotiGene);
			dis.setFecha(fecha.format(new Date()));
			dis.setUsuario(principal.getName());
			Especificacion especificacion = isus.getEspecificacion(diseno2);
			especificacion.setDiseno(val_codigo_equicotiGene);
			Auxiliar aux = new Auxiliar();
			aux.setAux9(diseno2);
			aux.setAux1(1);
			Bobina bobbbt = isus.getBobina(aux);
			bobbbt.setDiseno(val_codigo_equicotiGene);
			Presspan prebbt = isus.getPresspan(aux);
			prebbt.setDiseno(val_codigo_equicotiGene);
			aux.setAux1(2);
			Bobina bobbat = isus.getBobina(aux);
			bobbat.setDiseno(val_codigo_equicotiGene);
			Presspan prebat = isus.getPresspan(aux);
			prebat.setDiseno(val_codigo_equicotiGene);
			aux.setAux1(3);
			Bobina bobbte = isus.getBobina(aux);
			bobbte.setDiseno(val_codigo_equicotiGene);
			Presspan prebte = isus.getPresspan(aux);
			prebte.setDiseno(val_codigo_equicotiGene);
			NucleoFerreteria nucFerr = isus.getNucleoFerreteria(diseno2);
			nucFerr.setDiseno(val_codigo_equicotiGene);
			EstanqueTapa estTapa = isus.getEstanqueTapa(diseno2);
			estTapa.setDiseno(val_codigo_equicotiGene);
			Radiador rad = isus.getRadiador(diseno2);
			rad.setDiseno(val_codigo_equicotiGene);
			List<Accesorio> accesorios = isus.getAccesorios(diseno2);
			HelperIngenieria.disenoAccesorios(accesorios,
					val_codigo_equicotiGene);

			isus.insertAceite(accesorios, dis, especificacion, nucFerr,
					estTapa, bobbbt, bobbat, bobbte, prebbt, prebat, prebte,
					rad);
			// model.addAttribute("url", "ingenieria/buscarDiseno.jsp");
			model.addAttribute(
					"popup",
					ViewIngenieria.success("ingenieria-home?val_diseno="
							+ val_codigo_equicotiGene, true));

		} catch (Exception e) {
			logger.error("ingenieria-insertar-diseno-aceite", e);
			model.addAttribute("popup",
					ViewIngenieria.error("ingenieria-busqueda", true));
		}
		return "information";
	}

	@RequestMapping(value = "/ingenieria-update-diseno-seco", method = RequestMethod.GET)
	public @ResponseBody String updateDisenoSeco(Model model,
			@RequestParam String val_datosgenerales_diseno,
			@RequestParam String val_datosgenerales_codigo,
			@RequestParam int val_datosgenerales_distribucion,
			@RequestParam double val_datosgenerales_potencia,
			@RequestParam int val_datosgenerales_fases,
			@RequestParam int val_datosgenerales_refrigerante,
			@RequestParam double val_datosgenerales_kv,
			@RequestParam int val_especificaciones_derivacion,
			@RequestParam int val_especificaciones_impulso1,
			@RequestParam int val_especificaciones_impulso2,
			@RequestParam int val_especificaciones_impulso3,
			@RequestParam String val_bobina_MaterialBbt,
			@RequestParam String val_bobina_MaterialBat,
			@RequestParam String val_bobina_MaterialBte,
			@RequestParam double val_bbtAxial,
			@RequestParam double val_batAxial,
			@RequestParam double val_bteAxial,
			@RequestParam double val_bbtRadial,
			@RequestParam double val_batRadial,
			@RequestParam double val_bteRadial,
			@RequestParam double val_bobina_incrementoPapel_bbt,
			@RequestParam double val_bobina_incrementoPapel_bat,
			@RequestParam double val_bobina_incrementoPapel_bte,
			@RequestParam double val_bobina_largoEpiraMedia_bbt,
			@RequestParam double val_bobina_largoEpiraMedia_bat,
			@RequestParam double val_bobina_largoEpiraMedia_bte,
			@RequestParam int val_bobina_vueltasXfase_bbt,
			@RequestParam int val_bobina_vueltasXfase_bat,
			@RequestParam int val_bobina_vueltasXfase_bte,
			@RequestParam int val_bobina_principiosXcapas_bbt,
			@RequestParam int val_bobina_principiosXcapas_bat,
			@RequestParam int val_bobina_principiosXcapas_bte,
			@RequestParam int val_bobina_condParalelo_bbt,
			@RequestParam int val_bobina_condParalelo_bat,
			@RequestParam int val_bobina_condParalelo_bte,
			@RequestParam double val_bobina_largoBobinaCuellos_bbt,
			@RequestParam double val_bobina_largoBobinaCuellos_bat,
			@RequestParam double val_bobina_largoBobinaCuellos_bte,
			@RequestParam double val_bobina_espesorFisico_bbt,
			@RequestParam double val_bobina_espesorFisico_bat,
			@RequestParam double val_bobina_espesorFisico_bte,
			@RequestParam int val_bobina_ductosCompletos_bbt,
			@RequestParam int val_bobina_ductosCompletos_bat,
			@RequestParam int val_bobina_ductosCompletos_bte,
			@RequestParam int val_bobina_ductosCabezal_bbt,
			@RequestParam int val_bobina_ductosCabezal_bat,
			@RequestParam int val_bobina_ductosCabezal_bte,
			@RequestParam int val_bobina_ductosNubTat_bbt,
			@RequestParam int val_bobina_ductosNubTat_bat,
			@RequestParam int val_bobina_ductosNubTat_bte,
			@RequestParam int val_bobina_ductosCabezalAtbt_bbt,
			@RequestParam int val_bobina_ductosCabezalAtbt_bat,
			@RequestParam int val_bobina_ductosCabezalAtbt_bte,
			@RequestParam int val_bobina_condAxiales_bbt,
			@RequestParam int val_bobina_condAxiales_bat,
			@RequestParam int val_bobina_condAxiales_bte,
			@RequestParam double val_bobina_capasDiscos_bbt,
			@RequestParam double val_bobina_capasDiscos_bat,
			@RequestParam double val_bobina_capasDiscos_bte,
			@RequestParam int val_estanquetapa_largo,
			@RequestParam int val_estanquetapa_ancho,
			@RequestParam int val_estanquetapa_alto,
			@RequestParam double val_esp_manto,
			@RequestParam String val_cod_manto,
			@RequestParam String val_material_pintura_plancha_fierro_manto,
			@RequestParam String val_material_pintura_plancha_fierro_tapa,
			@RequestParam String val_material_pintura_plancha_fierro_fondo,
			@RequestParam double val_esp_tapa,
			@RequestParam String val_cod_tapa,
			@RequestParam double val_esp_fondo,
			@RequestParam String val_cod_fondo,
			@RequestParam String val_codPin1, @RequestParam String val_codPin2,
			@RequestParam String val_codPin3, @RequestParam String val_codPin4,
			@RequestParam double val_bbtEspesoresCapa1,
			@RequestParam String val_bbtCodCapa1,
			@RequestParam double val_bbtEspesoresCapa2,
			@RequestParam String val_bbtCodCapa2,
			@RequestParam double val_bbtEspesoresCapa3,
			@RequestParam String val_bbtCodCapa3,
			@RequestParam double val_batEspesoresCapa1,
			@RequestParam String val_batCodCapa1,
			@RequestParam double val_batEspesoresCapa2,
			@RequestParam String val_batCodCapa2,
			@RequestParam double val_batEspesoresCapa3,
			@RequestParam String val_batCodCapa3,
			@RequestParam double val_bteEspesoresCapa1,
			@RequestParam String val_bteCodCapa1,
			@RequestParam double val_bteEspesoresCapa2,
			@RequestParam String val_bteCodCapa2,
			@RequestParam double val_bteEspesoresCapa3,
			@RequestParam String val_bteCodCapa3,
			@RequestParam int val_espesoresAtbt1,
			@RequestParam String val_codAtbt1,
			@RequestParam int val_espesoresAtbt2,
			@RequestParam String val_codAtbt2,
			@RequestParam int val_espesoresAtbt3,
			@RequestParam String val_codAtbt3,
			@RequestParam int val_batespesoresAtbt1,
			@RequestParam String val_batcodAtbt1,
			@RequestParam int val_batespesoresAtbt2,
			@RequestParam String val_batcodAtbt2,
			@RequestParam int val_batespesoresAtbt3,
			@RequestParam String val_batcodAtbt3,
			@RequestParam int val_bteespesoresAtbt1,
			@RequestParam String val_btecodAtbt1,
			@RequestParam int val_bteespesoresAtbt2,
			@RequestParam String val_btecodAtbt2,
			@RequestParam int val_bteespesoresAtbt3,
			@RequestParam String val_btecodAtbt3,
			@RequestParam int val_espesoresBase1,
			@RequestParam String val_codBase1,
			@RequestParam int val_espesoresBase2,
			@RequestParam String val_codBase2,
			@RequestParam int val_espesoresBase3,
			@RequestParam String val_codBase3,
			@RequestParam int val_batespesoresBase1,
			@RequestParam String val_batcodBase1,
			@RequestParam int val_batespesoresBase2,
			@RequestParam String val_batcodBase2,
			@RequestParam int val_batespesoresBase3,
			@RequestParam String val_batcodBase3,
			@RequestParam int val_bteespesoresBase1,
			@RequestParam String val_btecodBase1,
			@RequestParam int val_bteespesoresBase2,
			@RequestParam String val_btecodBase2,
			@RequestParam int val_bteespesoresBase3,
			@RequestParam String val_btecodBase3,
			@RequestParam String val_silicoso1,
			@RequestParam int val_acesilicant1,
			@RequestParam double val_acesilipesonucleo1,
			@RequestParam String val_silicoso2,
			@RequestParam int val_acesilicant2,
			@RequestParam double val_acesilipesonucleo2,

			@RequestParam int val_radioSoporte,
			@RequestParam char val_tipoNucleo,
			@RequestParam int val_nucferr_anchoCabezal,
			@RequestParam double val_nucferr_largoVentana,
			@RequestParam int val_nucferr_diametro,
			@RequestParam int val_entreCentroNucleo,
			@RequestParam int val_ventana_nucleo, @RequestParam int val_corte

	) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Diseno dis = new Diseno();
			dis.setIdDiseno(val_datosgenerales_codigo);
			dis.setCodigo1(val_datosgenerales_diseno);
			dis.setIdDistribucion(val_datosgenerales_distribucion);
			dis.setPotencia(val_datosgenerales_potencia);
			dis.setKv(val_datosgenerales_kv);
			dis.setFase(val_datosgenerales_fases);
			dis.setIdRefrigerante(val_datosgenerales_refrigerante);

			Especificacion especificacion = new Especificacion();
			especificacion.setDiseno(val_datosgenerales_codigo);
			especificacion.setIdDerivacion(val_especificaciones_derivacion);
			especificacion.setNivelImpulso1(val_especificaciones_impulso1);
			especificacion.setNivelImpulso2(val_especificaciones_impulso2);
			especificacion.setNivelImpulso3(val_especificaciones_impulso3);

			Bobina bobbbt = new Bobina();
			bobbbt.setDiseno(val_datosgenerales_codigo);
			bobbbt.setTipoBobina(1);
			bobbbt.setCodigoCondCu(val_bobina_MaterialBbt);
			bobbbt.setDimensionAxial(val_bbtAxial);
			bobbbt.setDimensionRadial(val_bbtRadial);
			bobbbt.setIncrementoPapel(val_bobina_incrementoPapel_bbt);
			bobbbt.setLargoEspiraMedia(val_bobina_largoEpiraMedia_bbt);
			bobbbt.setVueltasXfase(val_bobina_vueltasXfase_bbt);
			bobbbt.setnPrincipiosXcapa(val_bobina_principiosXcapas_bbt);
			bobbbt.setCondParalelo(val_bobina_condParalelo_bbt);
			bobbbt.setLargoBobinaCuellos(val_bobina_largoBobinaCuellos_bbt);
			bobbbt.setEspesorFisicoBobina(val_bobina_espesorFisico_bbt);
			bobbbt.setnDuctosCompletos(val_bobina_ductosCompletos_bbt);
			bobbbt.setnDuctosCabezal(val_bobina_ductosCabezal_bbt);
			bobbbt.setnDuctosNubTat(val_bobina_ductosNubTat_bbt);
			bobbbt.setnDuctosCabezaLatBt(val_bobina_ductosCabezalAtbt_bbt);
			bobbbt.setCondAxiales(val_bobina_condAxiales_bbt);
			bobbbt.setnCapasDisco(val_bobina_capasDiscos_bbt);

			Bobina bobbat = new Bobina();
			bobbat.setDiseno(val_datosgenerales_codigo);
			bobbat.setTipoBobina(2);
			bobbat.setCodigoCondCu(val_bobina_MaterialBat);
			bobbat.setDimensionAxial(val_batAxial);
			bobbat.setDimensionRadial(val_batRadial);
			bobbat.setIncrementoPapel(val_bobina_incrementoPapel_bat);
			bobbat.setLargoEspiraMedia(val_bobina_largoEpiraMedia_bat);
			bobbat.setVueltasXfase(val_bobina_vueltasXfase_bat);
			bobbat.setnPrincipiosXcapa(val_bobina_principiosXcapas_bat);
			bobbat.setCondParalelo(val_bobina_condParalelo_bat);
			bobbat.setLargoBobinaCuellos(val_bobina_largoBobinaCuellos_bat);
			bobbat.setEspesorFisicoBobina(val_bobina_espesorFisico_bat);
			bobbat.setnDuctosCompletos(val_bobina_ductosCompletos_bat);
			bobbat.setnDuctosCabezal(val_bobina_ductosCabezal_bat);
			bobbat.setnDuctosNubTat(val_bobina_ductosNubTat_bat);
			bobbat.setnDuctosCabezaLatBt(val_bobina_ductosCabezalAtbt_bat);
			bobbat.setCondAxiales(val_bobina_condAxiales_bat);
			bobbat.setnCapasDisco(val_bobina_capasDiscos_bat);

			Bobina bobbte = new Bobina();
			bobbte.setDiseno(val_datosgenerales_codigo);
			bobbte.setTipoBobina(3);
			bobbte.setCodigoCondCu(val_bobina_MaterialBte);
			bobbte.setDimensionAxial(val_bteAxial);
			bobbte.setDimensionRadial(val_bteRadial);
			bobbte.setIncrementoPapel(val_bobina_incrementoPapel_bte);
			bobbte.setLargoEspiraMedia(val_bobina_largoEpiraMedia_bte);
			bobbte.setVueltasXfase(val_bobina_vueltasXfase_bte);
			bobbte.setnPrincipiosXcapa(val_bobina_principiosXcapas_bte);
			bobbte.setCondParalelo(val_bobina_condParalelo_bte);
			bobbte.setLargoBobinaCuellos(val_bobina_largoBobinaCuellos_bte);
			bobbte.setEspesorFisicoBobina(val_bobina_espesorFisico_bte);
			bobbte.setnDuctosCompletos(val_bobina_ductosCompletos_bte);
			bobbte.setnDuctosCabezal(val_bobina_ductosCabezal_bte);
			bobbte.setnDuctosNubTat(val_bobina_ductosNubTat_bte);
			bobbte.setnDuctosCabezaLatBt(val_bobina_ductosCabezalAtbt_bte);
			bobbte.setCondAxiales(val_bobina_condAxiales_bte);
			bobbte.setnCapasDisco(val_bobina_capasDiscos_bte);

			Presspan prebbt = new Presspan();
			prebbt.setDiseno(val_datosgenerales_codigo);
			prebbt.setBobina(1);
			prebbt.setCanEspesoresCapa1(val_bbtEspesoresCapa1);
			prebbt.setCodCapa1(val_bbtCodCapa1);
			prebbt.setCanEspesoresCapa2(val_bbtEspesoresCapa2);
			prebbt.setCodCapa2(val_bbtCodCapa2);
			prebbt.setCanEspesoresCapa3(val_bbtEspesoresCapa3);
			prebbt.setCodCapa3(val_bbtCodCapa3);
			prebbt.setCanEspesoresAtbt1(val_espesoresAtbt1);
			prebbt.setCodAtbt1(val_codAtbt1);
			prebbt.setCanEspesoresAtbt2(val_espesoresAtbt2);
			prebbt.setCodAtbt2(val_codAtbt2);
			prebbt.setCanEspesoresAtbt3(val_espesoresAtbt3);
			prebbt.setCodAtbt3(val_codAtbt3);
			prebbt.setCanEspesoresBase1(val_espesoresBase1);
			prebbt.setCodBase1(val_codBase1);
			prebbt.setCanEspesoresBase2(val_espesoresBase2);
			prebbt.setCodBase2(val_codBase2);
			prebbt.setCanEspesoresBase3(val_espesoresBase3);
			prebbt.setCodBase3(val_codBase3);

			Presspan prebat = new Presspan();
			prebat.setDiseno(val_datosgenerales_codigo);
			prebat.setBobina(2);
			prebat.setCanEspesoresCapa1(val_batEspesoresCapa1);
			prebat.setCodCapa1(val_batCodCapa1);
			prebat.setCanEspesoresCapa2(val_batEspesoresCapa2);
			prebat.setCodCapa2(val_batCodCapa2);
			prebat.setCanEspesoresCapa3(val_batEspesoresCapa3);
			prebat.setCodCapa3(val_batCodCapa3);
			prebat.setCanEspesoresAtbt1(val_batespesoresAtbt1);
			prebat.setCodAtbt1(val_batcodAtbt1);
			prebat.setCanEspesoresAtbt2(val_batespesoresAtbt2);
			prebat.setCodAtbt2(val_batcodAtbt2);
			prebat.setCanEspesoresAtbt3(val_batespesoresAtbt3);
			prebat.setCodAtbt3(val_batcodAtbt3);
			prebat.setCanEspesoresBase1(val_batespesoresBase1);
			prebat.setCodBase1(val_batcodBase1);
			prebat.setCanEspesoresBase2(val_batespesoresBase2);
			prebat.setCodBase2(val_batcodBase2);
			prebat.setCanEspesoresBase3(val_batespesoresBase3);
			prebat.setCodBase3(val_batcodBase3);

			Presspan prebte = new Presspan();
			prebte.setDiseno(val_datosgenerales_codigo);
			prebte.setBobina(3);
			prebte.setCanEspesoresCapa1(val_bteEspesoresCapa1);
			prebte.setCodCapa1(val_bteCodCapa1);
			prebte.setCanEspesoresCapa2(val_bteEspesoresCapa2);
			prebte.setCodCapa2(val_bteCodCapa2);
			prebte.setCanEspesoresCapa3(val_bteEspesoresCapa3);
			prebte.setCodCapa3(val_bteCodCapa3);
			prebte.setCanEspesoresAtbt1(val_bteespesoresAtbt1);
			prebte.setCodAtbt1(val_btecodAtbt1);
			prebte.setCanEspesoresAtbt2(val_bteespesoresAtbt2);
			prebte.setCodAtbt2(val_btecodAtbt2);
			prebte.setCanEspesoresAtbt3(val_bteespesoresAtbt3);
			prebte.setCodAtbt3(val_btecodAtbt3);
			prebte.setCanEspesoresBase1(val_bteespesoresBase1);
			prebte.setCodBase1(val_btecodBase1);
			prebte.setCanEspesoresBase2(val_bteespesoresBase2);
			prebte.setCodBase2(val_btecodBase2);
			prebte.setCanEspesoresBase3(val_bteespesoresBase3);
			prebte.setCodBase3(val_btecodBase3);

			NucleoFerreteria nucFerr = new NucleoFerreteria();
			nucFerr.setDiseno(val_datosgenerales_codigo);
			nucFerr.setTipo(val_tipoNucleo);
			nucFerr.setSoporte(val_radioSoporte);
			nucFerr.setCodigoSilicoso1(val_silicoso1);
			nucFerr.setPesoNucleo1(val_acesilipesonucleo1);
			nucFerr.setCantidad1(val_acesilicant1);
			nucFerr.setCodigoSilicoso2(val_silicoso2);
			nucFerr.setPesoNucleo2(val_acesilipesonucleo2);
			nucFerr.setCantidad2(val_acesilicant2);
			nucFerr.setCodigoSilicoso3("0");
			nucFerr.setPesoNucleo3(0);
			nucFerr.setCantidad3(0);
			nucFerr.setCodigoSilicoso4("0");
			nucFerr.setPesoNucleo4(0);
			nucFerr.setCantidad4(0);
			nucFerr.setCodigoSilicoso5("0");
			nucFerr.setPesoNucleo5(0);
			nucFerr.setCantidad5(0);
			nucFerr.setCodigoSilicoso6("0");
			nucFerr.setPesoNucleo6(0);
			nucFerr.setCantidad6(0);
			nucFerr.setCodigoSilicoso7("0");
			nucFerr.setPesoNucleo7(0);
			nucFerr.setCantidad7(0);
			nucFerr.setCodigoSilicoso8("0");
			nucFerr.setPesoNucleo8(0);
			nucFerr.setCantidad8(0);
			nucFerr.setCodigoSilicoso9("0");
			nucFerr.setPesoNucleo9(0);
			nucFerr.setCantidad9(0);
			nucFerr.setCodigoSilicoso10("0");
			nucFerr.setPesoNucleo10(0);
			nucFerr.setCantidad10(0);
			if (val_tipoNucleo == '1') {
				nucFerr.setChapaMayorNucleo(val_nucferr_anchoCabezal);
				nucFerr.setApilamientoChapas(val_nucferr_largoVentana);
				nucFerr.setDiametroNucleo(0);
			}
			if (val_tipoNucleo == '0') {
				nucFerr.setChapaMayorNucleo(0);
				nucFerr.setApilamientoChapas(0);
				nucFerr.setDiametroNucleo(val_nucferr_diametro);
			}
			nucFerr.setEntreCentroNucleo(val_entreCentroNucleo);
			nucFerr.setVentanaNucleo(val_ventana_nucleo);
			nucFerr.setCorte(val_corte);

			EstanqueTapa estTapa = new EstanqueTapa();
			estTapa.setDiseno(val_datosgenerales_codigo);
			estTapa.setLargoEstanque(val_estanquetapa_largo);
			estTapa.setAnchoEstanque(val_estanquetapa_ancho);
			estTapa.setAltoEstanque(val_estanquetapa_alto);
			estTapa.setEspFondo(val_esp_fondo);
			estTapa.setEspManto(val_esp_manto);
			estTapa.setEspTapa(val_esp_tapa);
			estTapa.setCodFondo(val_cod_fondo);
			estTapa.setCodManto(val_cod_manto);
			estTapa.setCodTapa(val_cod_tapa);
			estTapa.setPin1(val_codPin1);
			estTapa.setPin2(val_codPin2);
			estTapa.setPin3(val_codPin3);
			estTapa.setPin4(val_codPin4);

			int a = isus.updateSeco(dis, especificacion, nucFerr, estTapa,
					bobbbt, bobbat, bobbte, prebbt, prebat, prebte);
			if (a > 0) {
				result = "<script>";
				result += "alert('Datos actualizados')";
				result += "</script>";
			} else {
				result = "<script>";
				result += "alert('No se pudo actualizar')";
				result += "</script>";
			}

		} catch (Exception e) {
			logger.error("ingenieria-insertar-diseno-aceite", e);
			result = "<script>";
			result += "alert('Error al actualizar')";
			result += "</script>";
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-update-diseno-aceite", method = RequestMethod.GET)
	public @ResponseBody String updateDisenoAceite(Model model,
			@RequestParam String val_datosgenerales_diseno,
			@RequestParam String val_datosgenerales_codigo,
			@RequestParam int val_datosgenerales_distribucion,
			@RequestParam double val_datosgenerales_potencia,
			@RequestParam int val_datosgenerales_fases,
			@RequestParam int val_datosgenerales_refrigerante,
			@RequestParam double val_datosgenerales_kv,
			@RequestParam int val_especificaciones_derivacion,
			@RequestParam int val_especificaciones_impulso1,
			@RequestParam int val_especificaciones_impulso2,
			@RequestParam int val_especificaciones_impulso3,
			@RequestParam String val_bobina_MaterialBbt,
			@RequestParam String val_bobina_MaterialBat,
			@RequestParam String val_bobina_MaterialBte,
			@RequestParam double val_bbtAxial,
			@RequestParam double val_batAxial,
			@RequestParam double val_bteAxial,
			@RequestParam double val_bbtRadial,
			@RequestParam double val_batRadial,
			@RequestParam double val_bteRadial,
			@RequestParam double val_bobina_incrementoPapel_bbt,
			@RequestParam double val_bobina_incrementoPapel_bat,
			@RequestParam double val_bobina_incrementoPapel_bte,
			@RequestParam double val_bobina_largoEpiraMedia_bbt,
			@RequestParam double val_bobina_largoEpiraMedia_bat,
			@RequestParam double val_bobina_largoEpiraMedia_bte,
			@RequestParam int val_bobina_vueltasXfase_bbt,
			@RequestParam int val_bobina_vueltasXfase_bat,
			@RequestParam int val_bobina_vueltasXfase_bte,
			@RequestParam int val_bobina_principiosXcapas_bbt,
			@RequestParam int val_bobina_principiosXcapas_bat,
			@RequestParam int val_bobina_principiosXcapas_bte,
			@RequestParam int val_bobina_condParalelo_bbt,
			@RequestParam int val_bobina_condParalelo_bat,
			@RequestParam int val_bobina_condParalelo_bte,
			@RequestParam double val_bobina_largoBobinaCuellos_bbt,
			@RequestParam double val_bobina_largoBobinaCuellos_bat,
			@RequestParam double val_bobina_largoBobinaCuellos_bte,
			@RequestParam double val_bobina_espesorFisico_bbt,
			@RequestParam double val_bobina_espesorFisico_bat,
			@RequestParam double val_bobina_espesorFisico_bte,
			@RequestParam int val_bobina_ductosCompletos_bbt,
			@RequestParam int val_bobina_ductosCompletos_bat,
			@RequestParam int val_bobina_ductosCompletos_bte,
			@RequestParam int val_bobina_ductosCabezal_bbt,
			@RequestParam int val_bobina_ductosCabezal_bat,
			@RequestParam int val_bobina_ductosCabezal_bte,
			@RequestParam int val_bobina_ductosNubTat_bbt,
			@RequestParam int val_bobina_ductosNubTat_bat,
			@RequestParam int val_bobina_ductosNubTat_bte,
			@RequestParam int val_bobina_ductosCabezalAtbt_bbt,
			@RequestParam int val_bobina_ductosCabezalAtbt_bat,
			@RequestParam int val_bobina_ductosCabezalAtbt_bte,
			@RequestParam int val_bobina_condAxiales_bbt,
			@RequestParam int val_bobina_condAxiales_bat,
			@RequestParam int val_bobina_condAxiales_bte,
			@RequestParam double val_bobina_capasDiscos_bbt,
			@RequestParam double val_bobina_capasDiscos_bat,
			@RequestParam double val_bobina_capasDiscos_bte,
			@RequestParam int val_estanquetapa_largo,
			@RequestParam int val_estanquetapa_ancho,
			@RequestParam int val_estanquetapa_alto,
			@RequestParam double val_esp_manto,
			@RequestParam String val_cod_manto,
			@RequestParam String val_material_pintura_plancha_fierro_manto,
			@RequestParam String val_material_pintura_plancha_fierro_tapa,
			@RequestParam String val_material_pintura_plancha_fierro_fondo,
			@RequestParam double val_esp_tapa,
			@RequestParam String val_cod_tapa,
			@RequestParam double val_esp_fondo,
			@RequestParam String val_cod_fondo,
			@RequestParam String val_codPin1, @RequestParam String val_codPin2,
			@RequestParam String val_codPin3, @RequestParam String val_codPin4,
			@RequestParam double val_bbtEspesoresCapa1,
			@RequestParam String val_bbtCodCapa1,
			@RequestParam double val_bbtEspesoresCapa2,
			@RequestParam String val_bbtCodCapa2,
			@RequestParam double val_bbtEspesoresCapa3,
			@RequestParam String val_bbtCodCapa3,
			@RequestParam double val_batEspesoresCapa1,
			@RequestParam String val_batCodCapa1,
			@RequestParam double val_batEspesoresCapa2,
			@RequestParam String val_batCodCapa2,
			@RequestParam double val_batEspesoresCapa3,
			@RequestParam String val_batCodCapa3,
			@RequestParam double val_bteEspesoresCapa1,
			@RequestParam String val_bteCodCapa1,
			@RequestParam double val_bteEspesoresCapa2,
			@RequestParam String val_bteCodCapa2,
			@RequestParam double val_bteEspesoresCapa3,
			@RequestParam String val_bteCodCapa3,
			@RequestParam int val_espesoresAtbt1,
			@RequestParam String val_codAtbt1,
			@RequestParam int val_espesoresAtbt2,
			@RequestParam String val_codAtbt2,
			@RequestParam int val_espesoresAtbt3,
			@RequestParam String val_codAtbt3,
			@RequestParam int val_batespesoresAtbt1,
			@RequestParam String val_batcodAtbt1,
			@RequestParam int val_batespesoresAtbt2,
			@RequestParam String val_batcodAtbt2,
			@RequestParam int val_batespesoresAtbt3,
			@RequestParam String val_batcodAtbt3,
			@RequestParam int val_bteespesoresAtbt1,
			@RequestParam String val_btecodAtbt1,
			@RequestParam int val_bteespesoresAtbt2,
			@RequestParam String val_btecodAtbt2,
			@RequestParam int val_bteespesoresAtbt3,
			@RequestParam String val_btecodAtbt3,
			@RequestParam int val_espesoresBase1,
			@RequestParam String val_codBase1,
			@RequestParam int val_espesoresBase2,
			@RequestParam String val_codBase2,
			@RequestParam int val_espesoresBase3,
			@RequestParam String val_codBase3,
			@RequestParam int val_batespesoresBase1,
			@RequestParam String val_batcodBase1,
			@RequestParam int val_batespesoresBase2,
			@RequestParam String val_batcodBase2,
			@RequestParam int val_batespesoresBase3,
			@RequestParam String val_batcodBase3,
			@RequestParam int val_bteespesoresBase1,
			@RequestParam String val_btecodBase1,
			@RequestParam int val_bteespesoresBase2,
			@RequestParam String val_btecodBase2,
			@RequestParam int val_bteespesoresBase3,
			@RequestParam String val_btecodBase3,
			@RequestParam String val_silicoso1,
			@RequestParam int val_acesilicant1,
			@RequestParam double val_acesilipesonucleo1,
			@RequestParam String val_silicoso2,
			@RequestParam int val_acesilicant2,
			@RequestParam double val_acesilipesonucleo2,
			@RequestParam int val_radioSoporte,
			@RequestParam char val_tipoNucleo,
			@RequestParam int val_nucferr_anchoCabezal,
			@RequestParam double val_nucferr_largoVentana,
			@RequestParam int val_nucferr_diametro,
			@RequestParam int val_entreCentroNucleo,
			@RequestParam int val_ventana_nucleo, @RequestParam int val_corte,
			@RequestParam String val_silicoso3,
			@RequestParam int val_acesilicant3,
			@RequestParam double val_acesilipesonucleo3,
			@RequestParam String val_silicoso4,
			@RequestParam int val_acesilicant4,
			@RequestParam double val_acesilipesonucleo4,
			@RequestParam String val_silicoso5,
			@RequestParam int val_acesilicant5,
			@RequestParam double val_acesilipesonucleo5,
			@RequestParam String val_silicoso6,
			@RequestParam int val_acesilicant6,
			@RequestParam double val_acesilipesonucleo6,
			@RequestParam String val_silicoso7,
			@RequestParam int val_acesilicant7,
			@RequestParam double val_acesilipesonucleo7,
			@RequestParam String val_silicoso8,
			@RequestParam int val_acesilicant8,
			@RequestParam double val_acesilipesonucleo8,
			@RequestParam String val_silicoso9,
			@RequestParam int val_acesilicant9,
			@RequestParam double val_acesilipesonucleo9,
			@RequestParam String val_silicoso10,
			@RequestParam int val_acesilicant10,
			@RequestParam double val_acesilipesonucleo10,
			@RequestParam String comboMaterialRadiador,
			@RequestParam int val_radiador_num,
			@RequestParam int val_radiador_elementos

	) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Diseno dis = new Diseno();
			dis.setIdDiseno(val_datosgenerales_codigo);
			dis.setCodigo1(val_datosgenerales_diseno);
			dis.setIdDistribucion(val_datosgenerales_distribucion);
			dis.setPotencia(val_datosgenerales_potencia);
			dis.setKv(val_datosgenerales_kv);
			dis.setFase(val_datosgenerales_fases);
			dis.setIdRefrigerante(val_datosgenerales_refrigerante);

			Especificacion especificacion = new Especificacion();
			especificacion.setDiseno(val_datosgenerales_codigo);
			especificacion.setIdDerivacion(val_especificaciones_derivacion);
			especificacion.setNivelImpulso1(val_especificaciones_impulso1);
			especificacion.setNivelImpulso2(val_especificaciones_impulso2);
			especificacion.setNivelImpulso3(val_especificaciones_impulso3);

			Bobina bobbbt = new Bobina();
			bobbbt.setDiseno(val_datosgenerales_codigo);
			bobbbt.setTipoBobina(1);
			bobbbt.setCodigoCondCu(val_bobina_MaterialBbt);
			bobbbt.setDimensionAxial(val_bbtAxial);
			bobbbt.setDimensionRadial(val_bbtRadial);
			bobbbt.setIncrementoPapel(val_bobina_incrementoPapel_bbt);
			bobbbt.setLargoEspiraMedia(val_bobina_largoEpiraMedia_bbt);
			bobbbt.setVueltasXfase(val_bobina_vueltasXfase_bbt);
			bobbbt.setnPrincipiosXcapa(val_bobina_principiosXcapas_bbt);
			bobbbt.setCondParalelo(val_bobina_condParalelo_bbt);
			bobbbt.setLargoBobinaCuellos(val_bobina_largoBobinaCuellos_bbt);
			bobbbt.setEspesorFisicoBobina(val_bobina_espesorFisico_bbt);
			bobbbt.setnDuctosCompletos(val_bobina_ductosCompletos_bbt);
			bobbbt.setnDuctosCabezal(val_bobina_ductosCabezal_bbt);
			bobbbt.setnDuctosNubTat(val_bobina_ductosNubTat_bbt);
			bobbbt.setnDuctosCabezaLatBt(val_bobina_ductosCabezalAtbt_bbt);
			bobbbt.setCondAxiales(val_bobina_condAxiales_bbt);
			bobbbt.setnCapasDisco(val_bobina_capasDiscos_bbt);

			Bobina bobbat = new Bobina();
			bobbat.setDiseno(val_datosgenerales_codigo);
			bobbat.setTipoBobina(2);
			bobbat.setCodigoCondCu(val_bobina_MaterialBat);
			bobbat.setDimensionAxial(val_batAxial);
			bobbat.setDimensionRadial(val_batRadial);
			bobbat.setIncrementoPapel(val_bobina_incrementoPapel_bat);
			bobbat.setLargoEspiraMedia(val_bobina_largoEpiraMedia_bat);
			bobbat.setVueltasXfase(val_bobina_vueltasXfase_bat);
			bobbat.setnPrincipiosXcapa(val_bobina_principiosXcapas_bat);
			bobbat.setCondParalelo(val_bobina_condParalelo_bat);
			bobbat.setLargoBobinaCuellos(val_bobina_largoBobinaCuellos_bat);
			bobbat.setEspesorFisicoBobina(val_bobina_espesorFisico_bat);
			bobbat.setnDuctosCompletos(val_bobina_ductosCompletos_bat);
			bobbat.setnDuctosCabezal(val_bobina_ductosCabezal_bat);
			bobbat.setnDuctosNubTat(val_bobina_ductosNubTat_bat);
			bobbat.setnDuctosCabezaLatBt(val_bobina_ductosCabezalAtbt_bat);
			bobbat.setCondAxiales(val_bobina_condAxiales_bat);
			bobbat.setnCapasDisco(val_bobina_capasDiscos_bat);

			Bobina bobbte = new Bobina();
			bobbte.setDiseno(val_datosgenerales_codigo);
			bobbte.setTipoBobina(3);
			bobbte.setCodigoCondCu(val_bobina_MaterialBte);
			bobbte.setDimensionAxial(val_bteAxial);
			bobbte.setDimensionRadial(val_bteRadial);
			bobbte.setIncrementoPapel(val_bobina_incrementoPapel_bte);
			bobbte.setLargoEspiraMedia(val_bobina_largoEpiraMedia_bte);
			bobbte.setVueltasXfase(val_bobina_vueltasXfase_bte);
			bobbte.setnPrincipiosXcapa(val_bobina_principiosXcapas_bte);
			bobbte.setCondParalelo(val_bobina_condParalelo_bte);
			bobbte.setLargoBobinaCuellos(val_bobina_largoBobinaCuellos_bte);
			bobbte.setEspesorFisicoBobina(val_bobina_espesorFisico_bte);
			bobbte.setnDuctosCompletos(val_bobina_ductosCompletos_bte);
			bobbte.setnDuctosCabezal(val_bobina_ductosCabezal_bte);
			bobbte.setnDuctosNubTat(val_bobina_ductosNubTat_bte);
			bobbte.setnDuctosCabezaLatBt(val_bobina_ductosCabezalAtbt_bte);
			bobbte.setCondAxiales(val_bobina_condAxiales_bte);
			bobbte.setnCapasDisco(val_bobina_capasDiscos_bte);

			Presspan prebbt = new Presspan();
			prebbt.setDiseno(val_datosgenerales_codigo);
			prebbt.setBobina(1);
			prebbt.setCanEspesoresCapa1(val_bbtEspesoresCapa1);
			prebbt.setCodCapa1(val_bbtCodCapa1);
			prebbt.setCanEspesoresCapa2(val_bbtEspesoresCapa2);
			prebbt.setCodCapa2(val_bbtCodCapa2);
			prebbt.setCanEspesoresCapa3(val_bbtEspesoresCapa3);
			prebbt.setCodCapa3(val_bbtCodCapa3);
			prebbt.setCanEspesoresAtbt1(val_espesoresAtbt1);
			prebbt.setCodAtbt1(val_codAtbt1);
			prebbt.setCanEspesoresAtbt2(val_espesoresAtbt2);
			prebbt.setCodAtbt2(val_codAtbt2);
			prebbt.setCanEspesoresAtbt3(val_espesoresAtbt3);
			prebbt.setCodAtbt3(val_codAtbt3);
			prebbt.setCanEspesoresBase1(val_espesoresBase1);
			prebbt.setCodBase1(val_codBase1);
			prebbt.setCanEspesoresBase2(val_espesoresBase2);
			prebbt.setCodBase2(val_codBase2);
			prebbt.setCanEspesoresBase3(val_espesoresBase3);
			prebbt.setCodBase3(val_codBase3);

			Presspan prebat = new Presspan();
			prebat.setDiseno(val_datosgenerales_codigo);
			prebat.setBobina(2);
			prebat.setCanEspesoresCapa1(val_batEspesoresCapa1);
			prebat.setCodCapa1(val_batCodCapa1);
			prebat.setCanEspesoresCapa2(val_batEspesoresCapa2);
			prebat.setCodCapa2(val_batCodCapa2);
			prebat.setCanEspesoresCapa3(val_batEspesoresCapa3);
			prebat.setCodCapa3(val_batCodCapa3);
			prebat.setCanEspesoresAtbt1(val_batespesoresAtbt1);
			prebat.setCodAtbt1(val_batcodAtbt1);
			prebat.setCanEspesoresAtbt2(val_batespesoresAtbt2);
			prebat.setCodAtbt2(val_batcodAtbt2);
			prebat.setCanEspesoresAtbt3(val_batespesoresAtbt3);
			prebat.setCodAtbt3(val_batcodAtbt3);
			prebat.setCanEspesoresBase1(val_batespesoresBase1);
			prebat.setCodBase1(val_batcodBase1);
			prebat.setCanEspesoresBase2(val_batespesoresBase2);
			prebat.setCodBase2(val_batcodBase2);
			prebat.setCanEspesoresBase3(val_batespesoresBase3);
			prebat.setCodBase3(val_batcodBase3);

			Presspan prebte = new Presspan();
			prebte.setDiseno(val_datosgenerales_codigo);
			prebte.setBobina(3);
			prebte.setCanEspesoresCapa1(val_bteEspesoresCapa1);
			prebte.setCodCapa1(val_bteCodCapa1);
			prebte.setCanEspesoresCapa2(val_bteEspesoresCapa2);
			prebte.setCodCapa2(val_bteCodCapa2);
			prebte.setCanEspesoresCapa3(val_bteEspesoresCapa3);
			prebte.setCodCapa3(val_bteCodCapa3);
			prebte.setCanEspesoresAtbt1(val_bteespesoresAtbt1);
			prebte.setCodAtbt1(val_btecodAtbt1);
			prebte.setCanEspesoresAtbt2(val_bteespesoresAtbt2);
			prebte.setCodAtbt2(val_btecodAtbt2);
			prebte.setCanEspesoresAtbt3(val_bteespesoresAtbt3);
			prebte.setCodAtbt3(val_btecodAtbt3);
			prebte.setCanEspesoresBase1(val_bteespesoresBase1);
			prebte.setCodBase1(val_btecodBase1);
			prebte.setCanEspesoresBase2(val_bteespesoresBase2);
			prebte.setCodBase2(val_btecodBase2);
			prebte.setCanEspesoresBase3(val_bteespesoresBase3);
			prebte.setCodBase3(val_btecodBase3);

			NucleoFerreteria nucFerr = new NucleoFerreteria();
			nucFerr.setDiseno(val_datosgenerales_codigo);
			nucFerr.setTipo(val_tipoNucleo);
			nucFerr.setSoporte(val_radioSoporte);
			nucFerr.setCodigoSilicoso1(val_silicoso1);
			nucFerr.setPesoNucleo1(val_acesilipesonucleo1);
			nucFerr.setCantidad1(val_acesilicant1);
			nucFerr.setCodigoSilicoso2(val_silicoso2);
			nucFerr.setPesoNucleo2(val_acesilipesonucleo2);
			nucFerr.setCantidad2(val_acesilicant2);
			nucFerr.setCodigoSilicoso3(val_silicoso3);
			nucFerr.setPesoNucleo3(val_acesilipesonucleo3);
			nucFerr.setCantidad3(val_acesilicant3);
			nucFerr.setCodigoSilicoso4(val_silicoso4);
			nucFerr.setPesoNucleo4(val_acesilipesonucleo4);
			nucFerr.setCantidad4(val_acesilicant4);
			nucFerr.setCodigoSilicoso5(val_silicoso5);
			nucFerr.setPesoNucleo5(val_acesilipesonucleo5);
			nucFerr.setCantidad5(val_acesilicant5);
			nucFerr.setCodigoSilicoso6(val_silicoso6);
			nucFerr.setPesoNucleo6(val_acesilipesonucleo6);
			nucFerr.setCantidad6(val_acesilicant6);
			nucFerr.setCodigoSilicoso7(val_silicoso7);
			nucFerr.setPesoNucleo7(val_acesilipesonucleo7);
			nucFerr.setCantidad7(val_acesilicant7);
			nucFerr.setCodigoSilicoso8(val_silicoso8);
			nucFerr.setPesoNucleo8(val_acesilipesonucleo8);
			nucFerr.setCantidad8(val_acesilicant8);
			nucFerr.setCodigoSilicoso9(val_silicoso9);
			nucFerr.setPesoNucleo9(val_acesilipesonucleo9);
			nucFerr.setCantidad9(val_acesilicant9);
			nucFerr.setCodigoSilicoso10(val_silicoso10);
			nucFerr.setPesoNucleo10(val_acesilipesonucleo10);
			nucFerr.setCantidad10(val_acesilicant10);
			if (val_tipoNucleo == '1') {
				nucFerr.setChapaMayorNucleo(val_nucferr_anchoCabezal);
				nucFerr.setApilamientoChapas(val_nucferr_largoVentana);
				nucFerr.setDiametroNucleo(0);
			}
			if (val_tipoNucleo == '0') {
				nucFerr.setChapaMayorNucleo(0);
				nucFerr.setApilamientoChapas(0);
				nucFerr.setDiametroNucleo(val_nucferr_diametro);
			}
			nucFerr.setEntreCentroNucleo(val_entreCentroNucleo);
			nucFerr.setVentanaNucleo(val_ventana_nucleo);
			nucFerr.setCorte(val_corte);

			EstanqueTapa estTapa = new EstanqueTapa();
			estTapa.setDiseno(val_datosgenerales_codigo);
			estTapa.setLargoEstanque(val_estanquetapa_largo);
			estTapa.setAnchoEstanque(val_estanquetapa_ancho);
			estTapa.setAltoEstanque(val_estanquetapa_alto);
			estTapa.setEspFondo(val_esp_fondo);
			estTapa.setEspManto(val_esp_manto);
			estTapa.setEspTapa(val_esp_tapa);
			estTapa.setCodFondo(val_cod_fondo);
			estTapa.setCodManto(val_cod_manto);
			estTapa.setCodTapa(val_cod_tapa);
			estTapa.setPin1(val_codPin1);
			estTapa.setPin2(val_codPin2);
			estTapa.setPin3(val_codPin3);
			estTapa.setPin4(val_codPin4);

			Radiador rad = new Radiador();
			rad.setDiseno(val_datosgenerales_codigo);
			rad.setCodigoRadiador(comboMaterialRadiador);
			rad.setNumRadiadores(val_radiador_num);
			rad.setNumElementos(val_radiador_elementos);

			int a = isus.updateAceite(dis, especificacion, nucFerr, estTapa,
					bobbbt, bobbat, bobbte, prebbt, prebat, prebte, rad);
			if (a > 0) {
				result = ViewGeneral.success("#", false);
			} else {
				result = ViewGeneral.error("#", false);
			}

		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"ingenieria-insertar-diseno-aceite"));
			result = ViewGeneral.error("#", false);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-tipo-radiador", method = RequestMethod.GET)
	public @ResponseBody String comboTipoRadiador(
			@RequestParam int comboTipoRadiador,
			@RequestParam String val_datosgenerales_codigo) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			if (comboTipoRadiador == 0) {
				Radiador rad = new Radiador();
				rad.setCodTipoRadiador(comboTipoRadiador);
				result = ViewIngenieria.radiador(isus.getTiposRadiadores(),
						rad.getCodTipoRadiador(),
						isus.getMaterialesRadiador(rad.getCodTipoRadiador()),
						rad.getCodigoRadiador(), rad);
			} else {
				Radiador rad = isus.getRadiador(val_datosgenerales_codigo);
				result = ViewIngenieria.radiador(isus.getTiposRadiadores(),
						rad.getCodTipoRadiador(),
						isus.getMaterialesRadiador(comboTipoRadiador),
						rad.getCodigoRadiador(), rad);
			}

		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"ingenieria-tipo-radiador"));
			result = "<script>";
			result += "alert('Error')";
			result += "</script>";
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-imprimir-cubicacion", method = RequestMethod.GET)
	public @ResponseBody ModelAndView printCubicacion(ModelMap model,
			ModelAndView modelAndView, @RequestParam String diseno,
			@RequestParam String potencia, @RequestParam String fases)
			throws JRException {
		JRDataSource jrDatasource;

		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			CubicacionDataSource ds = new CubicacionDataSource();
			ds.create(null);
			List<Costo> costos = isus.cubicacion(diseno);
			jrDatasource = ds.llenarLista(costos);
			// jrDatasource = null;
			model.addAttribute("datasource", jrDatasource);
			model.addAttribute("format", "pdf");
			model.addAttribute("CODIGO_DISENO", diseno);
			model.addAttribute("POTENCIA", potencia);
			model.addAttribute("FASES", fases);
			model.addAttribute("TOTALPRECIO",
					HelperIngenieria.totalPrecio(costos));
			modelAndView = new ModelAndView("multiViewReportCubica", model);
		} catch (Exception e) {
			logger.error("ingenieria-imprimir-cubicacion", e);
		}
		return modelAndView;
	}

	// DOCUMENTOS APLICABLES
	@RequestMapping(value = "/ingenieria-documentos-aplicables", method = RequestMethod.GET)
	public String guardarPlanoOdiseno(ModelMap model) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> auxs = isus.getPlanoDescripciones();
			String html = "";
			model.addAttribute("archivosDocApli", new CargarArchivo());
			model.addAttribute("cabeceraDocAplicableEnc", ViewIngenieria
					.cabeceraDocAplicableEnc(new DocAplicableEnc()));
			// model.addAttribute("comboDescripcionPlano",
			// ViewIngenieria.comboDescripcionPlano(auxs,
			// "val_descripcion_plano", 0));
			model.addAttribute("agregarPlanos", ViewIngenieria.agregarPlanos(
					ViewIngenieria.comboDescripcionPlano(auxs,
							"val_descripcion_plano", 0), "",
					"guardarDocAplicablesPlanos()", "Agregar", 0, 0));
			model.addAttribute("numDocApliEnc",
					"<input type='hidden' id='numDocApliEnc' name='numDocApliEnc' value='0'>");
			model.addAttribute("url", "ingenieria/documentosAplicables.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-documentos-aplicables", e);
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-documentos-aplicables-planos", method = RequestMethod.GET)
	public String docApliPlanos(ModelMap model) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			// model.addAttribute("subAreas",
			// ViewIngenieria.selectSubAreas(isus.geSubGroupPrincipal(3)));
			model.addAttribute("url", "ingenieria/planos.jsp");

		} catch (Exception e) {
			logger.error("ingenieria-documentos-aplicables-planos", e);
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-ingresar-descripcion-plano", method = RequestMethod.POST)
	public String insertarDescripcionPlano(ModelMap model,
			@RequestParam String val_descripcion_plano) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Auxiliar aux = new Auxiliar();
			aux.setAux9(val_descripcion_plano);
			if (!val_descripcion_plano.trim().equals("")) {
				int a = isus.insertPlanoDescripcion(aux);
				if (a > 0) {
					model.addAttribute("popup", ViewGeneral.success("#", false));
				} else {
					model.addAttribute("popup", ViewGeneral.info("#", false,
							"No se pudo insertar los datos"));
				}
			} else {
				model.addAttribute("popup", ViewGeneral.info("#", false,
						"Debe ingresar una descripci�n"));
			}
			model.addAttribute("url", "ingenieria/planos.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-ingresar-descripcion-plano", e);
			model.addAttribute("popup", ViewGeneral.error("#", false));
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-upload-archivo", method = RequestMethod.POST)
	public String upload(Model model, @RequestParam("upl") MultipartFile file)
			throws IOException {

		try {
			byte[] bytes = file.getBytes();
			CargarArchivo arch = new CargarArchivo();
			arch.setArchivo(bytes);
			model.addAttribute("archivosDocApli", arch);
		} catch (Exception e) {
			logger.error("comercial-upload-archivo", e);
		}

		return "home";
	}

	@RequestMapping(value = "/ingenieria-upload-guardar-planos", method = RequestMethod.POST)
	public String uploadGuardar(Model model, HttpSession session, @RequestParam String val_nombre_plano, Principal prin) throws IOException {

		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			if (!val_nombre_plano.trim().equals("")) {
				CargarArchivo arch = ((CargarArchivo) session
						.getAttribute("archivosDocApli"));
				int revision = isus.getPlanoRevision(val_nombre_plano);
				if (arch != null) {
					arch.setName(val_nombre_plano);
					arch.setRevision(revision);
					arch.setFecha(new Timestamp(new java.util.Date().getTime()));
					arch.setUsuario(prin.getName());
				}
				if (isus.insertArchivoDocApli(arch) > 0) {

					List<Auxiliar> equiposVigenteScha = isus.getEquiposVigentesTpmp();
					List<Auxiliar> equiposVigenteDoc = isus.getEquiposVigentes();

					for (int a = 0; a < equiposVigenteDoc.size(); a++) {
						Auxiliar doc = equiposVigenteDoc.get(a);
						String[] items = doc.getAux9().split("-");
						boolean est = false;
						for (int r = 0; r < items.length; r++) {
							est = HelperIngenieria.equipoVigente(equiposVigenteScha, doc.getAux1(), Integer.parseInt(items[r]));
							if (est) {
								break;
							}
						}
						if (!est) {
							isus.actualizarEstadoDocApli(doc.getAux2());
						}
					}

					List<Auxiliar> revis = isus
							.getDetPlanoActualizar(val_nombre_plano);
					int d = isus.getMaxPlanoRevision(val_nombre_plano);
					for (int i = 0; i < revis.size(); i++) {
						Auxiliar au = new Auxiliar();
						au.setAux1(d);
						au.setAux2(revis.get(i).getAux1());
						isus.actualizarRevisionPlanoRev(au);
						// Mandar mail
						List<Auxiliar> enviarMails = isus
								.getEnviarMailRevision(revis.get(i).getAux1());
						for (int f = 0; f < enviarMails.size(); f++) {
							String recipientAddress = enviarMails.get(f)
									.getAux9();
							String subject = "Nueva revisi�n de plano";
							String message = "";
							message += "Np:                "
									+ enviarMails.get(f).getAux1() + "\n";
							message += "Item:              "
									+ enviarMails.get(f).getAux10() + "\n";
							message += "Dise�o:            "
									+ enviarMails.get(f).getAux11() + "\n";
							message += "Descripci�n plano: "
									+ enviarMails.get(f).getAux12() + "\n";
							message += "";
							message += "";
							message += "";
							message += "Se hizo una revisi�n a un plano que imprimi�";
							// creates a simple e-mail object
							SimpleMailMessage email = new SimpleMailMessage();
							email.setTo(recipientAddress);
							email.setSubject(subject);
							email.setText(message);
							// sends the e-mail
							mailSender.send(email);
						}

					}
					model.addAttribute("popup", ViewGeneral.success("#", false));
				} else {
					model.addAttribute("popup",
							ViewGeneral.info("#", false, "No se pudo guardar"));
				}

			} else {
				model.addAttribute("popup", ViewGeneral.info("#", false,
						"Debe poner nombre al plano"));
			}
			model.addAttribute("url", "ingenieria/planos.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-upload-guardar-planos", e);
			model.addAttribute("popup", ViewGeneral.error("#", false));
			model.addAttribute("url", "ingenieria/planos.jsp");
		}

		return "home";
	}

	@RequestMapping(value = "/ingenieria-guardar-documento-aplicables-enc", method = RequestMethod.GET)
	public @ResponseBody String guardarDocAplicables(
			@RequestParam String val_diseno, @RequestParam int val_np,
			@RequestParam String val_item,
			@RequestParam String val_diseno_elec,
			@RequestParam String val_diseno_mec,
			@RequestParam int numDocApliEnc, Principal principal) {
		String result = "";
		int ins = 0;
		int numero = 0;
		try {
			String[] items = val_item.split("-");
			for (int i = 0; i < items.length; i++) {
				Integer.parseInt(items[i]);
			}
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			DocAplicableEnc enc = new DocAplicableEnc();
			if (numDocApliEnc == 0) {
				numero = isus.maxNumDocAplicable();
				enc.setNumero(numero);
				enc.setDisenoElectrico(val_diseno_elec);
				enc.setDisenoMecanico(val_diseno_mec);
				enc.setDisenoTrafo(val_diseno);
				enc.setNp(val_np);
				enc.setItem(val_item);
				enc.setFechaIngreso(new Timestamp(new java.util.Date()
						.getTime()));
				enc.setUsuario(principal.getName());
				ins = isus.insertDocAplicableEnc(enc);
			} else {
				enc.setNumero(numDocApliEnc);
				enc.setDisenoElectrico(val_diseno_elec);
				enc.setDisenoMecanico(val_diseno_mec);
				enc.setDisenoTrafo(val_diseno);
				enc.setNp(val_np);
				enc.setItem(val_item);
				ins = isus.actualizarDocAplicableEnc(enc);
				numero = numDocApliEnc;
			}
			if (ins > 0) {
				result += ViewGeneral.success("#", false);
				result += "<input type='hidden' id='numDocApliEnc' name='numDocApliEnc' value='"
						+ numero + "'>";
			} else {
				result = ViewGeneral.info("#", false, "No se pudo actualizar");
			}

		} catch (Exception e) {
			logger.error("ingenieria-guardar-documento-aplicables-enc", e);
			result = ViewGeneral.error("#", false);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-guardar-documento-aplicables-det", method = RequestMethod.GET)
	public @ResponseBody String guardarDocAplicablesPlanos(
			@RequestParam int numDocApliEnc,
			@RequestParam int val_descripcion_plano,
			@RequestParam String planosearch, Principal principal) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Integer revision = isus.getLastPlanoRevision(planosearch);
			if (revision != null) {
				DocAplicableDet det = new DocAplicableDet();
				det.setId(isus.getLastDocApliDet());
				det.setNumero(numDocApliEnc);
				det.setDescPlano(val_descripcion_plano);
				det.setFechaIngreso(new Timestamp(new java.util.Date()
						.getTime()));
				det.setIdPlano(planosearch);
				det.setUsuario(principal.getName());
				det.setRevision(revision);
				det.setIdPlanoRev(isus.getMaxPlanoRevision(planosearch));
				int ins = isus.insertDocAplicableDet(det);
				if (ins > 0) {
					List<DocAplicableDet> dets = isus
							.getDocAplicableDet(numDocApliEnc);
					List<Auxiliar> auxs = isus.getPlanoDescripciones();
					result = ViewIngenieria.detalleDocApliPlanos(dets, auxs);
				} else {
					result = ViewGeneral
							.info("#", false, "No se pudo insertar");
					List<DocAplicableDet> dets = isus
							.getDocAplicableDet(numDocApliEnc);
					List<Auxiliar> auxs = isus.getPlanoDescripciones();
					result += ViewIngenieria.detalleDocApliPlanos(dets, auxs);
				}
			} else {
				result = ViewGeneral.info("#", false, "No se pudo insertar");
				List<DocAplicableDet> dets = isus
						.getDocAplicableDet(numDocApliEnc);
				List<Auxiliar> auxs = isus.getPlanoDescripciones();
				result += ViewIngenieria.detalleDocApliPlanos(dets, auxs);
			}
		} catch (Exception e) {
			logger.error("ingenieria-guardar-documento-aplicables-det", e);
			result = ViewGeneral.error("#", false);
			try {
				IngenieriaServicio isus = (IngenieriaServicio) context
						.getBean("ingenieriaServicio");
				List<DocAplicableDet> dets = isus
						.getDocAplicableDet(numDocApliEnc);
				List<Auxiliar> auxs = isus.getPlanoDescripciones();
				result += ViewIngenieria.detalleDocApliPlanos(dets, auxs);
			} catch (Exception a) {
				logger.error("ingenieria-guardar-documento-aplicables-det", e);
			}
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-busqueda-doc-aplicable", method = RequestMethod.GET)
	public String ingresoBuscarDocAplicable(Model model) {
		try {
			model.addAttribute("buscarDocAplicable",
					ViewIngenieria.buscarDocAplicable("", ""));
			model.addAttribute("url", "ingenieria/buscarDocAplicable.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-busqueda-doc-aplicable", e);
		}

		return "home";
	}

	@RequestMapping(value = "/ingenieria-encontrar-doc-aplicable", method = RequestMethod.POST)
	public String encontrarDocAplicable(Model model,
			@RequestParam String val_cod_costo, @RequestParam int val_np) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Auxiliar aux = new Auxiliar();
			aux.setAux1(val_np);
			aux.setAux9(val_cod_costo);
			List<DocAplicableEnc> encs = isus.getDocsAplicablesEnc(aux);
			model.addAttribute("disenoBusquedaDataTable",
					ViewIngenieria.detalleDocApliEnc(encs));
			model.addAttribute("buscarDocAplicable",
					ViewIngenieria.buscarDocAplicable("", ""));
			model.addAttribute("url", "ingenieria/buscarDocAplicable.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-busqueda-doc-aplicable", e);
		}

		return "home";
	}

	@RequestMapping(value = "/ingenieria-documentos-aplicables-actualizar", method = RequestMethod.GET)
	public String modificarDocAplicableEnc(ModelMap model,
			@RequestParam int val_numero,
			@RequestParam String val_codigo_costo,
			@RequestParam String val_diseno_electrico,
			@RequestParam String val_diseno_mecanico, @RequestParam int val_np,
			@RequestParam String val_item) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> auxs = isus.getPlanoDescripciones();
			DocAplicableEnc enc = new DocAplicableEnc();
			enc.setNumero(val_numero);
			enc.setDisenoTrafo(val_codigo_costo);
			enc.setDisenoElectrico(val_diseno_electrico);
			enc.setDisenoMecanico(val_diseno_mecanico);
			enc.setNp(val_np);
			enc.setItem(val_item);

			model.addAttribute("archivosDocApli", new CargarArchivo());
			model.addAttribute("cabeceraDocAplicableEnc",
					ViewIngenieria.cabeceraDocAplicableEnc(enc));
			model.addAttribute("comboDescripcionPlano", ViewIngenieria
					.comboDescripcionPlano(auxs, "val_descripcion_plano", 0));
			model.addAttribute("numDocApliEnc",
					"<input type='hidden' id='numDocApliEnc' name='numDocApliEnc' value='"
							+ enc.getNumero() + "'>");
			List<DocAplicableDet> dets = isus.getDocAplicableDet(enc
					.getNumero());
			List<Auxiliar> auxss = isus.getPlanoDescripciones();
			model.addAttribute("detalleDocumentoAplicable",
					ViewIngenieria.detalleDocApliPlanos(dets, auxss));
			model.addAttribute("agregarPlanos", ViewIngenieria.agregarPlanos(
					ViewIngenieria.comboDescripcionPlano(auxs,
							"val_descripcion_plano", 0), "",
					"guardarDocAplicablesPlanos()", "Agregar", 0, val_numero));
			model.addAttribute("url", "ingenieria/documentosAplicables.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-documentos-aplicables-actualizar", e);
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-actualizar-doc-apli-det", method = RequestMethod.GET)
	public @ResponseBody String actualizarDocAplicableDet(ModelMap model,
			@RequestParam int id, @RequestParam int descPlano,
			@RequestParam String idPlano, @RequestParam int numero) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> auxs = isus.getPlanoDescripciones();
			DocAplicableDet det = new DocAplicableDet();
			det.setId(id);
			det.setDescPlano(descPlano);
			det.setIdPlano(idPlano);
			result = ViewIngenieria.agregarPlanos(ViewIngenieria
					.comboDescripcionPlano(auxs, "val_descripcion_plano",
							descPlano), idPlano, "#", "Actualizar", id, numero);
		} catch (Exception e) {
			logger.error("ingenieria-actualizar-doc-apli-det", e);
		}
		return result;
	}

	@RequestMapping(value = "/ingenieria-actualizar-doc-apli-det1", method = RequestMethod.POST)
	public String actualizarDocAplicableDet1(ModelMap model,
			@RequestParam int idDet, @RequestParam String planosearch,
			@RequestParam int num, Principal principal) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			DocAplicableDet det = new DocAplicableDet();
			det.setId(idDet);
			det.setFechaIngreso(new Timestamp(new java.util.Date().getTime()));
			det.setUsuario(principal.getName());
			det.setIdPlanoRev(isus.getMaxPlanoRevision(planosearch));
			int g = isus.actualizarEstadoDocApliDetRev(det);
			int ins = isus.insertDocApliDetRev(det);

			if (ins > 0) {
				List<Auxiliar> enviarMails = isus
						.getEnviarMailCambioPlano(idDet);
				for (int f = 0; f < enviarMails.size(); f++) {

					String recipientAddress = enviarMails.get(f).getAux9();
					String subject = "Cambio de plano";
					String message = "";
					message += "Np:                "
							+ enviarMails.get(f).getAux1() + "\n";
					message += "Item:              "
							+ enviarMails.get(f).getAux10() + "\n";
					message += "Dise�o:            "
							+ enviarMails.get(f).getAux11() + "\n";
					message += "Descripci�n plano: "
							+ enviarMails.get(f).getAux12() + "\n";
					message += "";
					message += "";
					message += "";
					message += "Se hizo un cambio a un plano que imprimi�";
					SimpleMailMessage email = new SimpleMailMessage();
					email.setTo(recipientAddress);
					email.setSubject(subject);
					email.setText(message);

					// sends the e-mail
					mailSender.send(email);
				}
				model.addAttribute("docAplicable",
						ViewGeneral.success("#", false));
			} else {
				model.addAttribute("docAplicable",
						ViewGeneral.info("#", false, "No se pudo insertar"));
			}

			List<Auxiliar> auxs = isus.getPlanoDescripciones();
			DocAplicableEnc enc = isus.getDocAplicableEnc(num);

			model.addAttribute("archivosDocApli", new CargarArchivo());
			model.addAttribute("cabeceraDocAplicableEnc",
					ViewIngenieria.cabeceraDocAplicableEnc(enc));
			model.addAttribute("comboDescripcionPlano", ViewIngenieria
					.comboDescripcionPlano(auxs, "val_descripcion_plano", 0));
			model.addAttribute("numDocApliEnc",
					"<input type='hidden' id='numDocApliEnc' name='numDocApliEnc' value='"
							+ enc.getNumero() + "'>");
			List<DocAplicableDet> dets = isus.getDocAplicableDet(enc
					.getNumero());
			List<Auxiliar> auxss = isus.getPlanoDescripciones();
			model.addAttribute("detalleDocumentoAplicable",
					ViewIngenieria.detalleDocApliPlanos(dets, auxss));
			model.addAttribute("agregarPlanos", ViewIngenieria.agregarPlanos(
					ViewIngenieria.comboDescripcionPlano(auxs,
							"val_descripcion_plano", 0), "",
					"guardarDocAplicablesPlanos()", "Agregar", 0, num));
			model.addAttribute("url", "ingenieria/documentosAplicables.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-actualizar-doc-apli-det1", e);
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-doc-apli-cargarArchivos", method = RequestMethod.GET)
	public void doDownload(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String val_id_plano,
			@RequestParam int val_revision, HttpSession session)
			throws Exception {

		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			Auxiliar aux = new Auxiliar();
			aux.setAux1(val_revision);
			aux.setAux9(val_id_plano);
			CargarArchivo archivo = isus.docApliDetArchivo(aux);

			response.setContentType("application/pdf");
			response.setContentLength(archivo.getArchivo().length);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(archivo.getArchivo(), 0,
					archivo.getArchivo().length);
			ouputStream.flush();
			ouputStream.close();
			/*
			 * response.setHeader("Content-Disposition", "attachment;filename="
			 * + val_id_plano);
			 * response.setContentLength(archivo.getArchivo().length);
			 * FileCopyUtils.copy(archivo.getArchivo(),
			 * response.getOutputStream());
			 */
		} catch (Exception e) {
			logger.error("ingenieria-doc-apli-cargarArchivos", e);
		}
	}

	@RequestMapping(value = "/ingenieria-administrar", method = RequestMethod.GET)
	public String administrarIng(Model model) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> permisos = isus.gePermisosXareaXusuario(2);
			List<Usuario> per = isus.geUsuariosXarea(2);
			model.addAttribute("administrar", ViewIngenieria
					.administracionUsuariosIngenieria(per, permisos));
			model.addAttribute("url", "ingenieria/administracion.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-administrar", e);
		}

		return "home";
	}

	@RequestMapping(value = "/ingenieria-administrar-produccion", method = RequestMethod.GET)
	public String administrar(Model model) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> permisos = isus.gePermisosXareaXusuario(3);
			List<Usuario> per = isus.geUsuariosXarea(3);
			model.addAttribute("administrar", ViewIngenieria
					.administracionUsuariosIngenieria(per, permisos));
			model.addAttribute("url", "ingenieria/administracion.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-administrar-produccion", e);
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-actualizar-permisos", method = RequestMethod.GET)
	public @ResponseBody String actualizarPermisos(Model model,
			@RequestParam String val_valorPermiso,
			@RequestParam String val_nombrePermiso,
			@RequestParam String val_user) {
		String result = "";
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			List<Auxiliar> permisos = isus.gePermisosXareaXusuario(2);
			Auxiliar aux = new Auxiliar();

			aux.setAux9(val_user);
			if (val_nombrePermiso.trim().equals("ING_ADMIN")) {
				aux.setAux1(3);
			} else if (val_nombrePermiso.trim().equals("ING_PLANOS")) {
				aux.setAux1(4);
			} else if (val_nombrePermiso.trim().equals("ING_DOC_APLI")) {
				aux.setAux1(5);
			} else if (val_nombrePermiso.trim().equals("ING_CUBICAR")) {
				aux.setAux1(6);
			}
			if (HelperIngenieria.checked(permisos, val_user.trim(),
					val_nombrePermiso).equals("checked")) {
				int a = isus.eliminarPermiso(aux);
			} else {
				int a = isus.insertPermiso(aux);
			}
			permisos = isus.gePermisosXareaXusuario(2);
			List<Usuario> per = isus.geUsuariosXarea(2);
			result = ViewIngenieria.administracionUsuariosIngenieria(per,
					permisos);
		} catch (Exception e) {
			logger.error("ingenieria-actualizar-permisos", e);
			result = ViewGeneral.error("ingenieria-administrar", true);
		}

		return result;
	}

	@RequestMapping(value = "/ingenieria-upload-modificar-plano", method = RequestMethod.POST)
	public String actualizarPlano(Model model,
			@RequestParam String planosearch, HttpSession session) {
		try {
			IngenieriaServicio isus = (IngenieriaServicio) context
					.getBean("ingenieriaServicio");
			CargarArchivo arch = ((CargarArchivo) session
					.getAttribute("archivosDocApli"));
			arch.setName(planosearch);
			if (isus.actualizarPlano(arch) > 0) {
				model.addAttribute("popup", ViewGeneral.success("#", false));
			} else {
				model.addAttribute("popup",
						ViewGeneral.info("#", false, "No se pudo actualizar"));
			}
			model.addAttribute("url", "ingenieria/planos.jsp");
		} catch (Exception e) {
			logger.error("ingenieria-administrar-produccion", e);
			model.addAttribute("popup", ViewGeneral.error("#", false));
			model.addAttribute("url", "ingenieria/planos.jsp");
		}
		return "home";
	}

	@RequestMapping(value = "/ingenieria-getMateriales-rest", method = RequestMethod.GET)
	public @ResponseBody List<Tag> getMaterialesRest(
			@RequestParam String tagName) {
		return searchResult(tagName.toUpperCase());
	}

	@RequestMapping(value = "/ingenieria-getPlanos", method = RequestMethod.GET)
	public @ResponseBody List<Tag> getIdPlanos(@RequestParam String tagName) {
		List<Tag> getIdPlanos = null;
		try {
			getIdPlanos = searchPlanos(tagName.toLowerCase());
		} catch (Exception e) {
			logger.error("ingenieria-getMateriales-rest", e);
		}
		return getIdPlanos;
	}

	private List<Tag> searchPlanos(String tagName) throws Exception {
		List<Tag> result = new ArrayList<Tag>();
		IngenieriaServicio isus = (IngenieriaServicio) context
				.getBean("ingenieriaServicio");
		if (tagName.length() > 2) {
			List<Auxiliar> auxs = isus.getIdPlanos(tagName.toLowerCase());
			for (int i = 0; i < auxs.size(); i++) {
				result.add(new Tag(i + 1, auxs.get(i).getAux9()));
			}
		}
		return result;
	}

	private List<LinkedHashMap> materialesPorCodigoRest(String nombre) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(
				SERVER_URI + "/material/" + nombre.toUpperCase(), List.class);
	}

	private List<LinkedHashMap> detalleMaterialesRest(String nombre) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/detalleMaterial/"
				+ nombre.toUpperCase(), List.class);
	}

	private List<Tag> searchResult(String tagName) {
		int incre = 1;
		List<Tag> result = new ArrayList<Tag>();
		data.clear();
		if (tagName.length() > 2) {
			for (LinkedHashMap map : materialesPorCodigoRest(tagName
					.toUpperCase())) {
				data.add(new Tag(incre, (String) map.get("aux10") + "---"
						+ (String) map.get("aux9")));
				incre++;
			}
		}
		for (Tag tag : data) {
			if (tag.getTagName().contains(tagName)) {
				result.add(tag);
			}
		}

		return result;
	}

	private List<Auxiliar> detalleMateriales(String codigo) {
		List<Auxiliar> result = new ArrayList<Auxiliar>();
		List<LinkedHashMap> map1 = detalleMaterialesRest(codigo);
		for (LinkedHashMap map : map1) {
			Auxiliar aux = new Auxiliar();
			aux.setAux9((String) map.get("aux9"));
			aux.setAux10((String) map.get("aux10"));
			aux.setAux11((String) map.get("aux11"));
			result.add(aux);
		}
		return result;
	}

	private List<LinkedHashMap> equiposVigentesRest() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/equiposVigentes/",
				List.class);
	}

	/*
	 * private List<Auxiliar> equiposVigentes() { List<Auxiliar> result = new
	 * ArrayList<Auxiliar>(); List<LinkedHashMap> map1 = equiposVigentesRest();
	 * for (LinkedHashMap map : map1) { Auxiliar aux = new Auxiliar();
	 * aux.setAux1((Integer) map.get("aux1")); aux.setAux2((Integer)
	 * map.get("aux2")); result.add(aux); } return result; }
	 */

}
