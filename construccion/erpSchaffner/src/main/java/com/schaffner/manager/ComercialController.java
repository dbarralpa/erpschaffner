package com.schaffner.manager;

import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.schaffner.core.servicio.ComercialServicio;
import com.schaffner.manager.util.HelperComercial;
import com.schaffner.manager.util.ProyectoVentaDetDataSource;
import com.schaffner.manager.util.Tag;
import com.schaffner.manager.util.ViewComercial;
import com.schaffner.manager.util.ViewGeneral;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Persona;
import com.schaffner.modelo.ProyectoComercialTransformador;
import com.schaffner.modelo.ProyectoComercialVentaDet;
import com.schaffner.modelo.ProyectoComercialVentaEnc;
import com.schaffner.modelo.Usuario;

@Controller
@SessionAttributes({ "cliente", "proyectosVentas", "proyectoVenta",
		"listaProyectosVentasDet", "archivos" })
public class ComercialController {
	final String SERVER_URI = "http://bitnami-wildfly-1208.cloudapp.net/middleware1";
	List<Tag> data = new ArrayList<Tag>();
	private static final Logger logger = Logger
			.getLogger(ComercialController.class);

	@RequestMapping(value = "/comercial-solicitudCotizacion-proyecto", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpSession session) {
		try {

			Persona per = new Persona();
			model.addAttribute("proyectoVenta", new ProyectoComercialVentaEnc());
			model.addAttribute("cliente", per);
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> tipoMonedas = isus.tipoMoneda();
			String html = ViewComercial.selectTipoMoneda(tipoMonedas, 0, "");
			model.addAttribute("tipoMoneda", html);
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			html = ViewComercial.selectTipoCotizacion(tipoCotizaciones, 0, "");
			model.addAttribute("tipoCotizacion", html);
			List<Auxiliar> formaPago = isus.formaPago();
			html = ViewComercial.selectFormaPago(formaPago, 0, "");
			model.addAttribute("formaPago", html);
			List<Persona> vendedores = vendedores();
			html = ViewComercial.selectVendedores(vendedores, 0, "");
			model.addAttribute("vendedores", html);
			html = ViewComercial.selectContactosVacio();
			model.addAttribute("contactos", html);
			html = ViewComercial.selectContactosEmailVacio();
			model.addAttribute("contactoEmail", html);
			model.addAttribute("url", "comercial/proyecto.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-solicitudCotizacion-proyecto"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-transformadores", method = RequestMethod.GET)
	public String transformadores(Model model) {
		model.addAttribute("cliente", new Persona());
		model.addAttribute("proyectoVenta", new ProyectoComercialVentaEnc());
		model.addAttribute("url", "comercial/listadoDePrecios1.jsp");
		return "home";
	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-IngresoProyecto", method = RequestMethod.POST)
	public String ingresoProyecto(Model model,
			@RequestParam String val_proyecto,
			@RequestParam String val_tipoCotizacion,
			@RequestParam String val_fechaCliente,
			@RequestParam String val_tipoMoneda,
			@RequestParam String val_formaPago,
			@RequestParam String val_vendedor, @RequestParam String val_email1,
			@RequestParam String val_contacto1, HttpSession session) {
		try {
			Persona per = ((Persona) session.getAttribute("cliente"));
			ComercialServicio isus = new ComercialServicio();
			ProyectoComercialVentaEnc pro = new ProyectoComercialVentaEnc();
			int num = isus.ultimoNumeroProyectoVenta() + 1;
			pro.setIdProyecto(num);
			pro.setIdCliente(Long.parseLong(per.getRut()));
			pro.setNombreProyecto(val_proyecto);
			pro.setIdTipoCotizacion(Integer.parseInt(val_tipoCotizacion));
			pro.setFechaCliente(val_fechaCliente);
			pro.setIdTipoMoneda(Integer.parseInt(val_tipoMoneda));
			pro.setIdFormaPago(Integer.parseInt(val_formaPago));
			pro.setIdEstado(1);
			Usuario user = ((Usuario) session.getAttribute("usuarioLogeado"));
			pro.setUsuario(user.getUsuario());
			String[] vendedor = val_vendedor.split(",");
			pro.setIdVendedor(Integer.parseInt(vendedor[0]));
			pro.setContacto(val_contacto1);
			pro.setEmail(val_email1);
			pro.setFechaEnvioIng("");
			pro.setFechaIngEnregara("");
			pro.setFechaIngEntrego("");
			pro.setValidezOferta("");
			pro.setPlazoEntrega("");
			if (isus.insertProyectoVenta(pro) > 0
					&& session.getAttribute("archivos") != null) {
				for (int i = 0; i < ((List) session.getAttribute("archivos"))
						.size(); i++) {
					CargarArchivo arc = ((List<CargarArchivo>) session
							.getAttribute("archivos")).get(i);
					arc.setIdProyecto(num);
					isus.insertProyectoVentaArchivos(arc);
				}
				model.addAttribute("archivos", new CargarArchivo());
			}
			model.addAttribute("popup", ViewGeneral.success(
					"comercial-solicitudCotizacion-proyecto", true));
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-solicitudCotizacion-IngresoProyecto"));
			model.addAttribute("popup", ViewGeneral.error(
					"comercial-solicitudCotizacion-proyecto", true));
		}
		return "information";
	}

	@RequestMapping(value = "/comercial-getClientes", method = RequestMethod.GET)
	public @ResponseBody List<Tag> getClientesRest(@RequestParam String tagName) {
		return searchResult(tagName.toUpperCase());

	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-cliente", method = RequestMethod.POST)
	public String cliente(Model model,
			@RequestParam("winputsearch") String winputsearch) {
		try {
			Persona per = clienteRest(winputsearch.trim().toUpperCase());
			List<Auxiliar> contactos = HelperComercial
					.contactos(clienteContactosRest(per.getRut()));
			String html = ViewComercial.selectContactos(contactos, "0", "");
			model.addAttribute("contactos", html);
			html = ViewComercial.selectContactosEmail(contactos, "0", "");
			model.addAttribute("contactoEmail", html);
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> tipoMonedas = isus.tipoMoneda();
			html = ViewComercial.selectTipoMoneda(tipoMonedas, 0, "");
			model.addAttribute("tipoMoneda", html);
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			html = ViewComercial.selectTipoCotizacion(tipoCotizaciones, 0, "");
			model.addAttribute("tipoCotizacion", html);
			List<Auxiliar> formaPago = isus.formaPago();
			html = ViewComercial.selectFormaPago(formaPago, 0, "");
			model.addAttribute("formaPago", html);
			List<Persona> vendedores = vendedores();
			html = ViewComercial.selectVendedores(vendedores, 0, "");
			model.addAttribute("vendedores", html);
			model.addAttribute("cliente", per);
			model.addAttribute("url", "comercial/proyecto.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-solicitudCotizacion-cliente"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-clienteTransformadores", method = RequestMethod.POST)
	public String clienteTransformadores(Model model,
			@RequestParam("winputsearch") String winputsearch) {

		try {
			Persona per = clienteRest(winputsearch.trim().toUpperCase());
			model.addAttribute("cliente", per);
			/*
			 * List<Auxiliar> tipoMonedas = isus.tipoMoneda(); List<Auxiliar>
			 * tipoCotizaciones = isus.tipoCotizacion(); List<Auxiliar>
			 * formaPago = isus.formaPago(); List<Persona> vendedores =
			 * vendedores();
			 * 
			 * 
			 * 
			 * 
			 * model.addAttribute("proyectoTrafos", ViewComercial
			 * .proyectoVentasTransfos(per, new ProyectoComercialVentaEnc(),
			 * ViewComercial .selectTipoCotizacion(tipoCotizaciones, 0, ""),
			 * ViewComercial .selectTipoMoneda(tipoMonedas, 0, ""),
			 * ViewComercial.selectFormaPago(formaPago, 0, ""),
			 * ViewComercial.selectVendedores(vendedores, 0, ""), contactos));
			 */
			model.addAttribute("proyectoVenta", new ProyectoComercialVentaEnc());
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> tipoMonedas = isus.tipoMoneda();
			String html = ViewComercial.selectTipoMoneda(tipoMonedas, 0, "");
			model.addAttribute("tipoMoneda", html);
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			html = ViewComercial.selectTipoCotizacion(tipoCotizaciones, 0, "");
			model.addAttribute("tipoCotizacion", html);
			List<Auxiliar> formaPago = isus.formaPago();
			html = ViewComercial.selectFormaPago(formaPago, 0, "");
			model.addAttribute("formaPago", html);
			List<Persona> vendedores = vendedores();
			html = ViewComercial.selectVendedores(vendedores, 0, "");
			model.addAttribute("vendedores", html);
			List<Auxiliar> contactos = HelperComercial
					.contactos(clienteContactosRest(per.getRut()));
			html = ViewComercial.selectContactos(contactos, "0", "");
			model.addAttribute("contactos", html);
			html = ViewComercial.selectContactosEmail(contactos, "0", "");
			model.addAttribute("contactoEmail", html);
			model.addAttribute("url", "comercial/proyectoTrafo.jsp");
			// session.setAttribute("proyectoVenta", null);
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-solicitudCotizacion-clienteTransformadores"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-editarCotizacion", method = RequestMethod.GET)
	public String editarCotizacion(Locale locale, Model model) {
		try {
			String html = "";
			ComercialServicio isus = new ComercialServicio();
			model.addAttribute("url", "comercial/editarCotizacion.jsp");
			List<ProyectoComercialVentaEnc> proyectosVentas = isus
					.proyectosVentas();
			List<Auxiliar> estadoCotizacion = isus.estadoCotizacion();
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			List<Auxiliar> tipoMoneda = isus.tipoMoneda();
			List<Auxiliar> formaPago = isus.formaPago();
			List<Persona> vendedores = vendedores();
			for (int i = 0; i < proyectosVentas.size(); i++) {
				html += ViewComercial.modalVentas(proyectosVentas.get(i), i,
						tipoCotizaciones, tipoMoneda, formaPago, vendedores);
				proyectosVentas.get(i).setId(i);

			}
			model.addAttribute("modalVentas", html);

			html = ViewComercial.editarCotizaciones(proyectosVentas,
					estadoCotizacion, tipoCotizaciones, vendedores);
			model.addAttribute("proyectosVentasDatatables", html);
			model.addAttribute("proyectosVentas", proyectosVentas);

		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion"));
		}
		return "home";
	}
	
	@RequestMapping(value = "/comercial-editarCotizacion-trafo", method = RequestMethod.GET)
	public String editarCotizacionTrafo(Locale locale, Model model) {
		try {
			String html = "";
			ComercialServicio isus = new ComercialServicio();
			model.addAttribute("url", "comercial/editarCotizacionTrafo.jsp");
			List<ProyectoComercialVentaEnc> proyectosVentas = isus
					.proyectosVentas();
			List<Auxiliar> estadoCotizacion = isus.estadoCotizacion();
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			List<Persona> vendedores = vendedores();
			
            for(int i=0;i<proyectosVentas.size();i++){
            	((ProyectoComercialVentaEnc)proyectosVentas.get(i)).setId(i);
            }
			html = ViewComercial.editarCotizacionesTrafo(proyectosVentas,
					estadoCotizacion, tipoCotizaciones, vendedores);
			model.addAttribute("proyectosVentasDatatables", html);
			model.addAttribute("proyectosVentas", proyectosVentas);

		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-vendedores-progresoOferta", method = RequestMethod.GET)
	public String progresoOferta(Locale locale, Model model) {
		try {
			String html = "";
			ComercialServicio isus = new ComercialServicio();
			model.addAttribute("url", "comercial/progresoOferta.jsp");
			model.addAttribute("proyectoVenta", new ProyectoComercialVentaEnc());
			List<ProyectoComercialVentaEnc> proyectosVentas = isus
					.proyectosVentas();
			List<Auxiliar> estadoCotizacion = isus.estadoCotizacion();
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			List<Persona> vendedores = vendedores();
			for (int i = 0; i < proyectosVentas.size(); i++) {
				proyectosVentas.get(i).setId(i);
			}
			html = ViewComercial.progresoOferta(proyectosVentas,
					estadoCotizacion, tipoCotizaciones, vendedores);
			model.addAttribute("proyectosVentasDatatables", html);
			model.addAttribute("proyectosVentas", proyectosVentas);

		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-vendedores-progresoOferta"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-editarCotizacion-update", method = RequestMethod.POST)
	public String editarCotizacionUpdate(Locale locale, Model model,
			@RequestParam String val_proyecto,
			@RequestParam String val_tipoCotizacion,
			@RequestParam String val_fechaCliente,
			@RequestParam String val_tipoMoneda,
			@RequestParam String val_formaPago,
			@RequestParam String val_fechaIngEntregara,
			@RequestParam String val_fechaIngEntrego,
			@RequestParam int idProyecto,
			@RequestParam String val_fechaEnvioIng,
			@RequestParam String val_vendedor) {
		try {
			String html = "";
			ComercialServicio isus = new ComercialServicio();
			model.addAttribute("url", "comercial/editarCotizacion.jsp");
			ProyectoComercialVentaEnc enc = new ProyectoComercialVentaEnc();
			enc.setIdProyecto(idProyecto);
			enc.setNombreProyecto(val_proyecto);
			enc.setIdTipoCotizacion(Integer.parseInt(val_tipoCotizacion));
			String fecha[] = val_fechaCliente.split("/");
			java.sql.Date.valueOf(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
			enc.setFechaCliente(val_fechaCliente);
			enc.setIdTipoMoneda(Integer.parseInt(val_tipoMoneda));
			enc.setIdFormaPago(Integer.parseInt(val_formaPago));
			if (!val_fechaIngEntregara.trim().equals("")) {
				String fecha1[] = val_fechaIngEntregara.trim().split("/");
				java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1] + "-"
						+ fecha1[0]);
			}
			enc.setFechaIngEnregara(val_fechaIngEntregara.trim());
			if (!val_fechaIngEntrego.trim().equals("")) {
				String fecha1[] = val_fechaIngEntrego.trim().split("/");
				java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1] + "-"
						+ fecha1[0]);
			}
			enc.setFechaIngEntrego(val_fechaIngEntrego.trim());
			if (!val_fechaEnvioIng.trim().equals("")) {
				String fecha1[] = val_fechaEnvioIng.trim().split("/");
				java.sql.Date.valueOf(fecha1[2] + "-" + fecha1[1] + "-"
						+ fecha1[0]);
			}
			enc.setFechaEnvioIng(val_fechaEnvioIng.trim());
			String[] vendedor = val_vendedor.split(",");
			enc.setIdVendedor(Integer.parseInt(vendedor[0]));
			isus.updateProyectoVenta(enc);
			/*
			 * List<ProyectoComercialVentaEnc> proyectosVentas = isus
			 * .proyectosVentas(); List<Auxiliar> estadoCotizacion =
			 * isus.estadoCotizacion(); List<Auxiliar> tipoCotizaciones =
			 * isus.tipoCotizacion(); List<Auxiliar> tipoMoneda =
			 * isus.tipoMoneda(); List<Auxiliar> formaPago = isus.formaPago();
			 * List<Persona> vendedores = vendedores(); for (int i = 0; i <
			 * proyectosVentas.size(); i++) { html +=
			 * ViewComercial.modalVentas(proyectosVentas.get(i), i,
			 * tipoCotizaciones, tipoMoneda, formaPago, vendedores);
			 * proyectosVentas.get(i).setId(i);
			 * 
			 * } model.addAttribute("modalVentas", html); html =
			 * ViewComercial.editarCotizaciones(proyectosVentas,
			 * estadoCotizacion, tipoCotizaciones, vendedores);
			 * model.addAttribute("proyectosVentasDatatables", html);
			 */
			model.addAttribute("popup",
					ViewGeneral.success("comercial-editarCotizacion", true));
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-update"));
			model.addAttribute("popup",
					ViewGeneral.error("comercial-editarCotizacion", true));
		}
		return "information";
	}

	@RequestMapping(value = "/comercial-editarCotizacion-resumenCostos", method = RequestMethod.GET)
	public String resumenCostos(Locale locale, Model model,
			@RequestParam int id, HttpSession session) {
		ProyectoComercialVentaEnc pro = ((List<ProyectoComercialVentaEnc>) session
				.getAttribute("proyectosVentas")).get(id);
		try {
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			List<Persona> vendedores = vendedores();
			List<Auxiliar> parametros = isus.parametros();
			List<Auxiliar> margenes = isus.margenes();
			List<ProyectoComercialVentaDet> proyecVenDet = isus
					.proyectosVentasDet(pro.getIdProyecto());
			List<ProyectoComercialTransformador> proyecVenTrafos = isus
					.proyectosVentasTrafos(pro.getIdProyecto());
			Persona cliente = clientePorRutRest(pro.getIdCliente());
			List<Auxiliar> familias = familias();
			List<Auxiliar> normas = isus.normas();
			List<Auxiliar> temPerdidas = isus.temPerdidas();
			List<Auxiliar> ventilacion = isus.ventilacion();
			List<Auxiliar> temperaturas = isus.temperaturas();
			String html = ViewComercial.resumenCostosCuenta(pro, parametros,
					tipoCotizaciones, vendedores, cliente);
			model.addAttribute("cuenta", html);
			html = ViewComercial.tablaMargenes(margenes);
			model.addAttribute("margenes", html);
			html = ViewComercial.selectMargenes(margenes, 0, "");
			model.addAttribute("selectMargenes", html);
			model.addAttribute("proyectoVenta", pro);
			List<CargarArchivo> archivos = isus.proyectosVentasArchivos(pro
					.getIdProyecto());
			model.addAttribute("archivos", archivos);
			html = ViewComercial.proyectosVentasArchivos(archivos);
			model.addAttribute("archivoss", html);
			html = "";
			for (int i = 0; i < proyecVenDet.size(); i++) {
				proyecVenDet.get(i).setId(i);
				html += ViewComercial.modalVentasDet(proyecVenDet.get(i), i,
						margenes);

			}
			model.addAttribute("modalVentas", html);
			html = "";

			if (!proyecVenTrafos.isEmpty()) {
				for (int i = 0; i < proyecVenDet.size(); i++) {
					ProyectoComercialTransformador proTrafo = HelperComercial
							.proyectoTrafo(proyecVenTrafos, proyecVenDet.get(i)
									.getIdProyecto());
					if (proTrafo.getIdProyectoVentaDet() > 0) {
						html += ViewComercial.modalVentasTrafos(proTrafo,
								proyecVenDet.get(i), familias, normas,
								temPerdidas, ventilacion, temperaturas,
								"disabled");

					}
				}
				model.addAttribute("modalVentasTrafos", html);
			}

			model.addAttribute("listaProyectosVentasDet", proyecVenDet);
			html = ViewComercial.tablaDetalleProyectoVenta(proyecVenDet);
			model.addAttribute("listaProyectosVentasDetHtml", html);
			html = ViewComercial.datosGenerales(pro);
			model.addAttribute("datosGenerales", html);
			model.addAttribute("url", "comercial/resumenCostos.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-resumenCostos"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-vendedores-editarProgresoOferta", method = RequestMethod.GET)
	public String editarProgresoOferta(Model model, @RequestParam int id,
			HttpSession session) {
		ProyectoComercialVentaEnc pro = ((List<ProyectoComercialVentaEnc>) session
				.getAttribute("proyectosVentas")).get(id);
		try {
			ComercialServicio isus = new ComercialServicio();
			int oo = pro.getIdProyecto();
			String html = ViewComercial.tablaDetalleProgresoProyecto(isus
					.progresoProyecto(oo));
			model.addAttribute("detalleProgresoOferta", html);
			model.addAttribute("proyectoVenta", pro);
			model.addAttribute("url", "comercial/editarProgresoOferta.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-vendedores-editarProgresoOferta"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-vendedores-editarProgresoOferta-update", method = RequestMethod.POST)
	public String editarProgresoOfertaUpdate(Model model,
			@RequestParam int val_progreso,
			@RequestParam String val_progreso_obser, HttpSession session,
			HttpServletRequest req) {
		try {
			ComercialServicio isus = new ComercialServicio();
			Auxiliar aux = new Auxiliar();
			aux.setAux1(val_progreso);
			aux.setAux9(val_progreso_obser);
			aux.setAux10(req.getRemoteAddr());
			aux.setAux2(((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta")).getIdProyecto());
			Usuario user = (Usuario) session.getAttribute("usuarioLogeado");
			aux.setAux11(user.getUsuario());
			aux.setAux12(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
					.format(new Date()));
			isus.insertProgresoProyecto(aux);
			String html = ViewComercial.tablaDetalleProgresoProyecto(isus
					.progresoProyecto(((ProyectoComercialVentaEnc) session
							.getAttribute("proyectoVenta")).getIdProyecto()));
			model.addAttribute("detalleProgresoOferta", html);
			model.addAttribute("url", "comercial/editarProgresoOferta.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-vendedores-editarProgresoOferta-update"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-editarCotizacion-resumenCostos-cargarArchivos", method = RequestMethod.GET)
	public void doDownload(HttpServletRequest request,
			HttpServletResponse response, @RequestParam int arch,
			HttpSession session) throws Exception {

		try {

			// File pdf = new File(fileName);
			// response.setContentType(adjunto.contenType(fileName));

			/*
			 * response.addHeader("Content-Disposition", "attachment; filename="
			 * + fileName);
			 */
			// response.setContentLength((int) pdf.length());
			CargarArchivo archivo = ((List<CargarArchivo>) session
					.getAttribute("archivos")).get(arch);
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename="
					+ archivo.getName());
			response.setContentLength(archivo.getArchivo().length);

			FileCopyUtils
					.copy(archivo.getArchivo(), response.getOutputStream());
		} catch (Exception ioe) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-resumenCostos-cargarArchivos"));
		}

	}

	@RequestMapping(value = "/comercial-editarCotizacion-resumenCostos-agregar", method = RequestMethod.POST)
	public String resumenCostosAgregar(Locale locale, Model model,
			@RequestParam String text_tag,
			@RequestParam String textarea_descripcion,
			@RequestParam String text_cubica, @RequestParam String text_diseno,
			@RequestParam String val_margen, @RequestParam int text_cant,
			@RequestParam int text_cu_usd, HttpSession session) {

		try {
			/*
			 * ApplicationContext context = new ClassPathXmlApplicationContext(
			 * "com/schaffner/core/beans/comercial.xml"); IComercialServicio
			 * isus = (IComercialServicio) context
			 * .getBean("comercialServicio");
			 */
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> parametros = isus.parametros();
			List<Auxiliar> margenes = isus.margenes();

			ProyectoComercialVentaDet det = new ProyectoComercialVentaDet();
			det.setId(((List) session.getAttribute("listaProyectosVentasDet"))
					.size());
			det.setIdProyecto(isus.ultimoNumeroProyectoVentaDet() + 1);
			det.setTag(text_tag);
			det.setDescripcion(textarea_descripcion);
			det.setCodigoCubica(text_cubica);
			det.setDiseno(text_diseno);
			det.setIdMargen(val_margen);
			det.setCantidad(text_cant);
			det.setCostoUnitario(text_cu_usd);
			det.setCostoTotal(text_cant * text_cu_usd);
			float val = HelperComercial.cantMargen(margenes, val_margen);
			if (val == 0) {
				det.setPrecioUniDolar(text_cu_usd);
				det.setPrecioTotalDolar(text_cant * text_cu_usd);
			} else {
				det.setPrecioUniDolar(Math.round((text_cu_usd / (1 - val))));
				det.setPrecioTotalDolar(text_cant
						* Math.round((text_cu_usd / (1 - val))));
			}
			det.setPrecioUniPeso(det.getPrecioUniDolar()
					* HelperComercial.precioUniPeso(parametros));
			det.setPrecioTotalPeso(det.getPrecioUniPeso() * det.getCantidad());
			det.setPrecioUniUf(Math.round(det.getPrecioUniPeso()
					/ HelperComercial.precioUniUf(parametros)));
			det.setPrecioTotalUf(det.getPrecioUniUf() * det.getCantidad());
			det.setIdProyectoEnc(((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta")).getIdProyecto());
			isus.insertProyectoVentaDet(det);

			model.addAttribute("popup", ViewGeneral.success(
					"comercial-editarCotizacion-resumenCostos-actual", true));
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-resumenCostos-agregar"));
			model.addAttribute("popup", ViewGeneral.error(
					"comercial-editarCotizacion-resumenCostos-actual", true));
		}
		return "information";
	}

	@RequestMapping(value = "/comercial-editarCotizacion-proyectoVentasDet-update", method = RequestMethod.POST)
	public String resumenCostosUpdate(Locale locale, Model model,
			@RequestParam String text_tag,
			@RequestParam String textarea_descripcion,
			@RequestParam String text_cubica, @RequestParam String text_diseno,
			@RequestParam String val_margen, @RequestParam int text_cant,
			@RequestParam int text_cu_usd, @RequestParam int idProyectoDet,
			@RequestParam String update, HttpSession session) {

		try {
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> margenes = isus.margenes();
			List<Auxiliar> parametros = isus.parametros();

			if (update.trim().equals("update")) {
				ProyectoComercialVentaDet det = new ProyectoComercialVentaDet();
				det.setIdProyecto(idProyectoDet);
				det.setTag(text_tag);
				det.setDescripcion(textarea_descripcion);
				det.setCodigoCubica(text_cubica);
				det.setDiseno(text_diseno);
				det.setIdMargen(val_margen);
				det.setCantidad(text_cant);
				det.setCostoUnitario(text_cu_usd);
				det.setCostoTotal(text_cant * text_cu_usd);
				float val = HelperComercial.cantMargen(margenes, val_margen);
				if (val == 0) {
					det.setPrecioUniDolar(text_cu_usd);
					det.setPrecioTotalDolar(text_cant * text_cu_usd);
				} else {
					det.setPrecioUniDolar(Math.round((text_cu_usd / (1 - val))));
					det.setPrecioTotalDolar(text_cant
							* Math.round((text_cu_usd / (1 - val))));
				}
				det.setPrecioUniPeso(det.getPrecioUniDolar()
						* HelperComercial.precioUniPeso(parametros));
				det.setPrecioTotalPeso(det.getPrecioUniPeso()
						* det.getCantidad());
				det.setPrecioUniUf(Math.round(det.getPrecioUniPeso()
						/ HelperComercial.precioUniUf(parametros)));
				det.setPrecioTotalUf(det.getPrecioUniUf() * det.getCantidad());
				isus.updateProyectoVentaDet(det);
			} else if (update.trim().equals("delete")) {
				isus.deleteProyectoVentaTrafo(idProyectoDet);
				isus.deleteProyectoVentaDet(idProyectoDet);
			}

			model.addAttribute("popup", ViewGeneral.success(
					"comercial-editarCotizacion-resumenCostos-actual", true));
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-proyectoVentasDet-update"));
			model.addAttribute("popup", ViewGeneral.error(
					"comercial-editarCotizacion-resumenCostos-actual", true));
		}
		return "information";
	}

	@RequestMapping(value = "/comercial-editarCotizacion-resumenCostos-datosGenerales", method = RequestMethod.POST)
	public String resumenCostosDatosGenerales(Locale locale, Model model,
			@RequestParam String text_entrega,
			@RequestParam String text_validez, HttpSession session) {

		try {
			ComercialServicio isus = new ComercialServicio();
			ProyectoComercialVentaEnc pro = (ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta");

			pro.setPlazoEntrega(text_entrega);
			pro.setValidezOferta(text_validez);
			if (isus.updateProyectoVentaEncDatosGenerales(pro) > 0) {
				session.setAttribute("proyectoVenta", pro);
			}
			model.addAttribute("popup", ViewGeneral.success(
					"comercial-editarCotizacion-resumenCostos-actual", true));
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-resumenCostos-datosGenerales"));
			model.addAttribute("popup", ViewGeneral.error(
					"comercial-editarCotizacion-resumenCostos-actual", true));
		}
		return "information";
	}

	@RequestMapping(value = "/comercial-editarCotizacion-resumenCostos-actual", method = RequestMethod.GET)
	public String resumenCostosAgregarError(Locale locale, Model model,
			HttpSession session) {

		try {
			ComercialServicio isus = new ComercialServicio();
			List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion();
			List<Persona> vendedores = vendedores();
			List<Auxiliar> parametros = isus.parametros();
			List<Auxiliar> margenes = isus.margenes();
			ProyectoComercialVentaEnc pro = (ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta");
			List<ProyectoComercialVentaDet> proyecVenDet = isus
					.proyectosVentasDet(pro.getIdProyecto());
			List<ProyectoComercialTransformador> proyecVenTrafos = isus
					.proyectosVentasTrafos(pro.getIdProyecto());
			Persona cliente = clientePorRutRest(pro.getIdCliente());
			List<Auxiliar> familias = familias();
			List<Auxiliar> normas = isus.normas();
			List<Auxiliar> temPerdidas = isus.temPerdidas();
			List<Auxiliar> ventilacion = isus.ventilacion();
			List<Auxiliar> temperaturas = isus.temperaturas();
			String html = ViewComercial.resumenCostosCuenta(pro, parametros,
					tipoCotizaciones, vendedores, cliente);
			model.addAttribute("cuenta", html);
			html = ViewComercial.tablaMargenes(margenes);
			model.addAttribute("margenes", html);
			html = ViewComercial.selectMargenes(margenes, 0, "");
			model.addAttribute("selectMargenes", html);
			model.addAttribute("proyectoVenta", pro);
			List<CargarArchivo> archivos = isus.proyectosVentasArchivos(pro
					.getIdProyecto());
			model.addAttribute("archivos", archivos);
			html = ViewComercial.proyectosVentasArchivos(archivos);
			model.addAttribute("archivoss", html);
			html = "";
			for (int i = 0; i < proyecVenDet.size(); i++) {
				proyecVenDet.get(i).setId(i);
				html += ViewComercial.modalVentasDet(proyecVenDet.get(i), i,
						margenes);

			}
			model.addAttribute("modalVentas", html);
			html = "";
			if (!proyecVenTrafos.isEmpty()) {
				for (int i = 0; i < proyecVenDet.size(); i++) {
					ProyectoComercialTransformador proTrafo = HelperComercial
							.proyectoTrafo(proyecVenTrafos, proyecVenDet.get(i)
									.getIdProyecto());
					if (proTrafo.getIdProyectoVentaDet() > 0) {
						html += ViewComercial.modalVentasTrafos(proTrafo,
								proyecVenDet.get(i), familias, normas,
								temPerdidas, ventilacion, temperaturas,
								"disabled");
					}
				}
				model.addAttribute("modalVentasTrafos", html);
			}

			model.addAttribute("listaProyectosVentasDet", proyecVenDet);
			html = ViewComercial.tablaDetalleProyectoVenta(proyecVenDet);
			model.addAttribute("listaProyectosVentasDetHtml", html);
			html = ViewComercial.datosGenerales(pro);
			model.addAttribute("datosGenerales", html);
			model.addAttribute("url", "comercial/resumenCostos.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-resumenCostos-actual"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-proyectoTrafoGuardar", method = RequestMethod.POST)
	public String ingresoProyectoTrafosGuardar(Model model,
			@RequestParam String val_proyecto,
			@RequestParam String val_tipoCotizacion,
			@RequestParam String val_fechaCliente,
			@RequestParam String val_tipoMoneda,
			@RequestParam String val_formaPago,
			@RequestParam String val_vendedor,
			@RequestParam String val_contacto1,
			@RequestParam String val_email1, HttpSession session) {
		try {
			Persona per = ((Persona) session.getAttribute("cliente"));
			ComercialServicio isus = new ComercialServicio();
			ProyectoComercialVentaEnc pro = new ProyectoComercialVentaEnc();
			int num = isus.ultimoNumeroProyectoVenta() + 1;
			pro.setIdProyecto(num);
			pro.setIdCliente(Long.parseLong(per.getRut()));
			pro.setNombreProyecto(val_proyecto);
			pro.setIdTipoCotizacion(Integer.parseInt(val_tipoCotizacion));
			pro.setFechaCliente(val_fechaCliente);
			pro.setIdTipoMoneda(Integer.parseInt(val_tipoMoneda));
			pro.setIdFormaPago(Integer.parseInt(val_formaPago));
			pro.setIdEstado(1);
			Usuario user = ((Usuario) session.getAttribute("usuarioLogeado"));
			pro.setUsuario(user.getUsuario());
			String[] vendedor = val_vendedor.split(",");
			pro.setIdVendedor(Integer.parseInt(vendedor[0]));
			pro.setFechaEnvioIng("");
			pro.setFechaIngEnregara("");
			pro.setFechaIngEntrego("");
			pro.setValidezOferta("");
			pro.setPlazoEntrega("");
			pro.setContacto(val_contacto1);
			pro.setEmail(val_email1);
			model.addAttribute("proyectoVenta", pro);
			List<Auxiliar> familias = familias();
			List<Auxiliar> normas = isus.normas();
			List<Auxiliar> temPerdidas = isus.temPerdidas();
			List<Auxiliar> ventilacion = isus.ventilacion();
			List<Auxiliar> temperaturas = isus.temperaturas();
			model.addAttribute("familias",
					ViewComercial.familias(familias, "0", ""));
			model.addAttribute("normas", ViewComercial.normas(normas, 0, ""));
			model.addAttribute("temPerdidas",
					ViewComercial.temPerdidas(temPerdidas, 0, ""));
			model.addAttribute("ventilacion",
					ViewComercial.ventilacion(ventilacion, 0, ""));
			model.addAttribute("temperaturas",
					ViewComercial.temperaturas(temperaturas, 0, ""));
			model.addAttribute("url", "comercial/listadoDePrecios.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-solicitudCotizacion-proyectoTrafoGuardar"));
		}
		return "home";
	}
	
	@RequestMapping(value = "/comercial-editarCotizacion-trafoEdit", method = RequestMethod.GET)
	public String proyectoTrafoEdit(Model model,
			@RequestParam int trafoId, HttpSession session) {
		try {
			ComercialServicio isus = new ComercialServicio();
			ProyectoComercialVentaEnc pro = ((List<ProyectoComercialVentaEnc>)session.getAttribute("proyectosVentas")).get(trafoId);
			model.addAttribute("proyectoVenta",pro);
			List<Auxiliar> familias = familias();
			List<Auxiliar> normas = isus.normas();
			List<Auxiliar> temPerdidas = isus.temPerdidas();
			List<Auxiliar> ventilacion = isus.ventilacion();
			List<Auxiliar> temperaturas = isus.temperaturas();
			model.addAttribute("familias",
					ViewComercial.familias(familias, "0", ""));
			model.addAttribute("normas", ViewComercial.normas(normas, 0, ""));
			model.addAttribute("temPerdidas",
					ViewComercial.temPerdidas(temPerdidas, 0, ""));
			model.addAttribute("ventilacion",
					ViewComercial.ventilacion(ventilacion, 0, ""));
			model.addAttribute("temperaturas",
					ViewComercial.temperaturas(temperaturas, 0, ""));
			
			List<ProyectoComercialTransformador> proTrafos = isus
					.proyectosVentasTrafos(pro.getIdProyecto());
			List<ProyectoComercialVentaDet> proDets = isus
					.proyectosVentasDet(pro.getIdProyecto());
			String html = "";
			for (int i = 0; i < proDets.size(); i++) {
				proDets.get(i).setId(i);
			}
			
			html = ViewComercial.tablaDetalleTrafos(proTrafos, proDets,
					normas, temPerdidas, ventilacion, temperaturas);
			model.addAttribute("transformadores", html);
			html = "";
			if (!proTrafos.isEmpty()) {
				for (int i = 0; i < proDets.size(); i++) {
					ProyectoComercialTransformador proTrafo = HelperComercial
							.proyectoTrafo(proTrafos, proDets.get(i)
									.getIdProyecto());
					if (proTrafo.getIdProyectoVentaDet() > 0) {
						html += ViewComercial.modalVentasTrafos2(proTrafo,
								proDets.get(i), familias, normas,
								temPerdidas, ventilacion, temperaturas, "");
					}
				}
				model.addAttribute("modalVentasDetTrafos", html);
			}
			
			
			model.addAttribute("url", "comercial/listadoDePrecios.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-editarCotizacion-trafoEdit"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-IngresoProyectoTrafos", method = RequestMethod.POST)
	public String ingresoProyectoTrafos(Model model, @RequestParam String tipo,
			@RequestParam int norma, @RequestParam int temPerdidas,
			@RequestParam int ventilacion, @RequestParam int temperatura,
			@RequestParam int val_potencia,
			@RequestParam int val_voltaje_primario,
			@RequestParam int val_voltaje_secundario,
			@RequestParam int val_impedancia, @RequestParam int val_frecuencia,
			@RequestParam String tag, @RequestParam int val_cantidad,
			@RequestParam String observacion, HttpSession session) {
		try {
			// int num = 0;
			Persona per = ((Persona) session.getAttribute("cliente"));
			ComercialServicio isus = new ComercialServicio();
			ProyectoComercialVentaEnc pro = ((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta"));
			if (isus.idProyectoEncExiste(pro.getIdProyecto()) == 0) {
				/*
				 * num = isus.ultimoNumeroProyectoVenta() + 1;
				 * pro.setIdProyecto(num);
				 * pro.setIdCliente(Long.parseLong(per.getRut()));
				 * pro.setNombreProyecto(val_proyecto);
				 * pro.setIdTipoCotizacion(Integer
				 * .parseInt(val_tipoCotizacion));
				 * pro.setFechaCliente(val_fechaCliente);
				 * pro.setIdTipoMoneda(Integer.parseInt(val_tipoMoneda));
				 * pro.setIdFormaPago(Integer.parseInt(val_formaPago));
				 * pro.setIdEstado(1); Usuario user = ((Usuario) session
				 * .getAttribute("usuarioLogeado"));
				 * pro.setUsuario(user.getUsuario()); String[] vendedor =
				 * val_vendedor.split(",");
				 * pro.setIdVendedor(Integer.parseInt(vendedor[0]));
				 * pro.setFechaEnvioIng(""); pro.setFechaIngEnregara("");
				 * pro.setFechaIngEntrego(""); pro.setValidezOferta("");
				 * pro.setPlazoEntrega(""); pro.setContacto(val_contacto1);
				 * pro.setEmail(val_email1);
				 */

				ProyectoComercialVentaDet det = new ProyectoComercialVentaDet();
				int numDet = isus.ultimoNumeroProyectoVentaDet() + 1;
				det.setTag(tag);
				det.setDescripcion("");
				det.setCodigoCubica("");
				det.setDiseno("");
				det.setIdMargen("SIN");
				det.setCantidad(val_cantidad);
				det.setCostoUnitario(0);
				det.setIdProyectoEnc(pro.getIdProyecto());
				det.setPrecioUniDolar(0);
				det.setPrecioTotalDolar(0);
				det.setPrecioUniPeso(0);
				det.setPrecioTotalPeso(0);
				det.setPrecioUniUf(0);
				det.setPrecioTotalUf(0);
				det.setIdProyecto(numDet);
				// isus.insertProyectoVentaDet(det);

				ProyectoComercialTransformador tra = new ProyectoComercialTransformador();
				tra.setIdFamilia(tipo);
				tra.setIdNorma(norma);
				tra.setIdTemPerdidas(temPerdidas);
				tra.setIdVentilacion(ventilacion);
				tra.setIdTemperatura(temperatura);
				tra.setPotencia(val_potencia);
				tra.setVoltajePrimario(val_voltaje_primario);
				tra.setVoltajeSecundario(val_voltaje_secundario);
				tra.setImpedancia(val_impedancia);
				tra.setFrecuencia(val_frecuencia);
				tra.setObservacion(observacion);
				tra.setIdProyectoVentaDet(numDet);
				tra.setIdProyectoVentaEnc(pro.getIdProyecto());
				// isus.insertProyectoVentaTrafos(tra);
				if (isus.insertProyectoEncDetTrafo(pro, det, tra) > 0) {
					model.addAttribute("proyectoVenta", pro);
					if (session.getAttribute("archivos") != null) {
						for (int i = 0; i < ((List) session
								.getAttribute("archivos")).size(); i++) {
							CargarArchivo arc = ((List<CargarArchivo>) session
									.getAttribute("archivos")).get(i);
							arc.setIdProyecto(pro.getIdProyecto());
							isus.insertProyectoVentaArchivos(arc);
						}
						model.addAttribute("archivos", new CargarArchivo());
					}
				}

			} else {
				int num = ((ProyectoComercialVentaEnc) session
						.getAttribute("proyectoVenta")).getIdProyecto();

				ProyectoComercialVentaDet det = new ProyectoComercialVentaDet();
				int numDet = isus.ultimoNumeroProyectoVentaDet() + 1;
				det.setTag(tag);
				det.setDescripcion("");
				det.setCodigoCubica("");
				det.setDiseno("");
				det.setIdMargen("SIN");
				det.setCantidad(val_cantidad);
				det.setCostoUnitario(0);
				det.setIdProyectoEnc(num);
				det.setPrecioUniDolar(0);
				det.setPrecioTotalDolar(0);
				det.setPrecioUniPeso(0);
				det.setPrecioTotalPeso(0);
				det.setPrecioUniUf(0);
				det.setPrecioTotalUf(0);
				det.setIdProyecto(numDet);
				// isus.insertProyectoVentaDet(det);

				ProyectoComercialTransformador tra = new ProyectoComercialTransformador();
				tra.setIdFamilia(tipo);
				tra.setIdNorma(norma);
				tra.setIdTemPerdidas(temPerdidas);
				tra.setIdVentilacion(ventilacion);
				tra.setIdTemperatura(temperatura);
				tra.setPotencia(val_potencia);
				tra.setVoltajePrimario(val_voltaje_primario);
				tra.setVoltajeSecundario(val_voltaje_secundario);
				tra.setImpedancia(val_impedancia);
				tra.setFrecuencia(val_frecuencia);
				tra.setObservacion(observacion);
				tra.setIdProyectoVentaDet(numDet);
				tra.setIdProyectoVentaEnc(num);
				isus.insertProyectoDetTrafo(det, tra);
			}
			List<Auxiliar> familias1 = familias();
			List<Auxiliar> normas1 = isus.normas();
			List<Auxiliar> temPerdidas1 = isus.temPerdidas();
			List<Auxiliar> ventilacion1 = isus.ventilacion();
			List<Auxiliar> temperaturas1 = isus.temperaturas();
			model.addAttribute("familias",
					ViewComercial.familias(familias1, "0", ""));
			model.addAttribute("normas", ViewComercial.normas(normas1, 0, ""));
			model.addAttribute("temPerdidas",
					ViewComercial.temPerdidas(temPerdidas1, 0, ""));
			model.addAttribute("ventilacion",
					ViewComercial.ventilacion(ventilacion1, 0, ""));
			model.addAttribute("temperaturas",
					ViewComercial.temperaturas(temperaturas1, 0, ""));
			List<ProyectoComercialTransformador> proTrafos = isus
					.proyectosVentasTrafos(pro.getIdProyecto());
			List<ProyectoComercialVentaDet> proDets = isus
					.proyectosVentasDet(pro.getIdProyecto());

			model.addAttribute("popup", ViewGeneral.success("", false));

			String html = "";
			if (!proTrafos.isEmpty()) {
				for (int i = 0; i < proDets.size(); i++) {
					proDets.get(i).setId(i);
					ProyectoComercialTransformador proTrafo = HelperComercial
							.proyectoTrafo(proTrafos, proDets.get(i)
									.getIdProyecto());
					if (proTrafo.getIdProyectoVentaDet() > 0) {
						html += ViewComercial.modalVentasTrafos2(proTrafo,
								proDets.get(i), familias1, normas1,
								temPerdidas1, ventilacion1, temperaturas1, "");
					}
				}
				model.addAttribute("modalVentasDetTrafos", html);
			}
			html = "";
			html = ViewComercial.tablaDetalleTrafos(proTrafos, proDets,
					normas1, temPerdidas1, ventilacion1, temperaturas1);
			model.addAttribute("transformadores", html);

			model.addAttribute("url", "comercial/listadoDePrecios.jsp");
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-solicitudCotizacion-IngresoProyectoTrafos"));
		}
		return "home";
	}

	@RequestMapping(value = "/comercial-solicitudCotizacion-actualizarProyectoTrafos", method = RequestMethod.POST)
	public String actualizarProyectoTrafos(Model model,
			@RequestParam String tipo, @RequestParam int norma,
			@RequestParam int temPerdidas, @RequestParam int ventilacion,
			@RequestParam int temperatura, @RequestParam int val_potencia,
			@RequestParam int val_voltaje_primario,
			@RequestParam int val_voltaje_secundario,
			@RequestParam int val_impedancia, @RequestParam int val_frecuencia,
			@RequestParam String observacion, @RequestParam int idTrafo,
			@RequestParam int idProDet,@RequestParam int val_cantidad,@RequestParam String tag , @RequestParam String actualizar,
			HttpSession session) {
		try {
			ComercialServicio isus = new ComercialServicio();
			if (actualizar.equals("actualizar")) {
				ProyectoComercialTransformador tra = new ProyectoComercialTransformador();
				tra.setIdTransformador(idTrafo);
				tra.setIdFamilia(tipo);
				tra.setIdNorma(norma);
				tra.setIdTemPerdidas(temPerdidas);
				tra.setIdVentilacion(ventilacion);
				tra.setIdTemperatura(temperatura);
				tra.setPotencia(val_potencia);
				tra.setVoltajePrimario(val_voltaje_primario);
				tra.setVoltajeSecundario(val_voltaje_secundario);
				tra.setImpedancia(val_impedancia);
				tra.setFrecuencia(val_frecuencia);
				tra.setObservacion(observacion);
				ProyectoComercialVentaDet det = new ProyectoComercialVentaDet();
				det.setIdProyecto(idProDet);
				det.setCantidad(val_cantidad);
				det.setTag(tag);
				if(isus.updateProyectoVentaDetTrafo(tra,det) > 0){
					model.addAttribute("popup", ViewGeneral.success("", false));
				}else{
					model.addAttribute("popup", ViewGeneral.error("", false));
				}
			}
			if (actualizar.equals("delete")) {
				if(isus.deleteProyectoDetTrafo(idProDet) > 0){
					model.addAttribute("popup", ViewGeneral.success("", false));
				}else{
					model.addAttribute("popup", ViewGeneral.error("", false));
				}
			}
			
			List<Auxiliar> familias = familias();
			List<Auxiliar> normas = isus.normas();
			List<Auxiliar> temPerdidas1 = isus.temPerdidas();
			List<Auxiliar> ventilacion1 = isus.ventilacion();
			List<Auxiliar> temperaturas = isus.temperaturas();
			model.addAttribute("familias",
					ViewComercial.familias(familias, "0", ""));
			model.addAttribute("normas", ViewComercial.normas(normas, 0, ""));
			model.addAttribute("temPerdidas",
					ViewComercial.temPerdidas(temPerdidas1, 0, ""));
			model.addAttribute("ventilacion",
					ViewComercial.ventilacion(ventilacion1, 0, ""));
			model.addAttribute("temperaturas",
					ViewComercial.temperaturas(temperaturas, 0, ""));
			model.addAttribute("url", "comercial/listadoDePrecios.jsp");
			List<ProyectoComercialTransformador> proTrafos = isus
					.proyectosVentasTrafos(((ProyectoComercialVentaEnc)session.getAttribute("proyectoVenta")).getIdProyecto());
			List<ProyectoComercialVentaDet> proDets = isus
					.proyectosVentasDet(((ProyectoComercialVentaEnc)session.getAttribute("proyectoVenta")).getIdProyecto());
			String html = "";
			for (int i = 0; i < proDets.size(); i++) {
				proDets.get(i).setId(i);
			}
			
			html = ViewComercial.tablaDetalleTrafos(proTrafos, proDets,
					normas, temPerdidas1, ventilacion1, temperaturas);
			model.addAttribute("transformadores", html);
			html = "";
			if (!proTrafos.isEmpty()) {
				for (int i = 0; i < proDets.size(); i++) {
					ProyectoComercialTransformador proTrafo = HelperComercial
							.proyectoTrafo(proTrafos, proDets.get(i)
									.getIdProyecto());
					if (proTrafo.getIdProyectoVentaDet() > 0) {
						html += ViewComercial.modalVentasTrafos2(proTrafo,
								proDets.get(i), familias, normas,
								temPerdidas1, ventilacion1, temperaturas, "");
					}
				}
				model.addAttribute("modalVentasDetTrafos", html);
			}
		} catch (Exception e) {

		}
		return "home";
	}

	/*
	 * @RequestMapping(value =
	 * "/comercial-solicitudCotizacion-clienteTransformadores1", method =
	 * RequestMethod.GET) public String clienteTransformadores1(Model model,
	 * HttpSession session) {
	 * 
	 * try { Persona per = (Persona) session.getAttribute("cliente");
	 * ProyectoComercialVentaEnc enc = ((ProyectoComercialVentaEnc) session
	 * .getAttribute("proyectoVenta")); ComercialServicio isus = new
	 * ComercialServicio(); List<Auxiliar> tipoMonedas = isus.tipoMoneda();
	 * List<Auxiliar> tipoCotizaciones = isus.tipoCotizacion(); List<Auxiliar>
	 * formaPago = isus.formaPago(); List<Persona> vendedores = vendedores();
	 * List<Auxiliar> contactos = HelperComercial
	 * .contactos(clienteContactosRest(per.getRut())); List<Auxiliar> familias =
	 * familias(); List<Auxiliar> normas = isus.normas(); List<Auxiliar>
	 * temPerdidas = isus.temPerdidas(); List<Auxiliar> ventilacion =
	 * isus.ventilacion(); List<Auxiliar> temperaturas = isus.temperaturas();
	 * model.addAttribute("familias", ViewComercial.familias(familias, "0",
	 * "")); model.addAttribute("normas", ViewComercial.normas(normas, 0, ""));
	 * model.addAttribute("temPerdidas", ViewComercial.temPerdidas(temPerdidas,
	 * 0, "")); model.addAttribute("ventilacion",
	 * ViewComercial.ventilacion(ventilacion, 0, ""));
	 * model.addAttribute("temperaturas",
	 * ViewComercial.temperaturas(temperaturas, 0, ""));
	 * model.addAttribute("proyectoTrafos", ViewComercial
	 * .proyectoVentasTransfos(per, enc, ViewComercial
	 * .selectTipoCotizacion(tipoCotizaciones, enc.getIdTipoCotizacion(),
	 * "readonly"), ViewComercial .selectTipoMoneda(tipoMonedas,
	 * enc.getIdTipoMoneda(), "readonly"),
	 * ViewComercial.selectFormaPago(formaPago, enc.getIdFormaPago(),
	 * "readonly"), ViewComercial.selectVendedores(vendedores,
	 * enc.getIdVendedor(), "readonly"), contactos)); model.addAttribute("url",
	 * "comercial/listadoDePrecios.jsp"); } catch (Exception e) {
	 * logger.error("Mensaje de error", new Exception(
	 * "comercial-solicitudCotizacion-clienteTransformadores1")); } return
	 * "home"; }
	 */

	@RequestMapping(value = "/comercial-editarCotizacion-resumenCostos-caratula", method = RequestMethod.GET)
	public @ResponseBody ModelAndView printWelcome(ModelMap model,
			ModelAndView modelAndView, HttpSession session) throws JRException {
		/*
		 * JRDataSource jrDatasource; StudentDataSource dsStudent = new
		 * StudentDataSource(); jrDatasource = dsStudent.create(null);
		 * model.addAttribute("datasource", jrDatasource);
		 * model.addAttribute("format", "pdf"); return "multiViewReport";
		 */

		List<ProyectoComercialVentaDet> det = ((List<ProyectoComercialVentaDet>) session
				.getAttribute("listaProyectosVentasDet"));
		long precio = 0;
		String formaPago = "";
		for (int i = 0; i < det.size(); i++) {
			if (((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta")).getIdTipoMoneda() == 1) {
				precio += det.get(i).getPrecioTotalPeso();
				formaPago = "Peso";
				det.get(i).setCostoTotalMoneda(det.get(i).getPrecioTotalPeso());
			} else if (((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta")).getIdTipoMoneda() == 2) {
				precio += det.get(i).getPrecioTotalDolar();
				formaPago = "D�lar";
				det.get(i)
						.setCostoTotalMoneda(det.get(i).getPrecioTotalDolar());
			} else if (((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta")).getIdTipoMoneda() == 3) {
				precio += det.get(i).getPrecioTotalUf();
				formaPago = "Uf";
				det.get(i).setCostoTotalMoneda(det.get(i).getPrecioTotalUf());
			}
		}
		Persona vende = HelperComercial.vendedor(vendedores(),
				((ProyectoComercialVentaEnc) session
						.getAttribute("proyectoVenta")).getIdVendedor());

		Persona per = clientePorRutRest(((ProyectoComercialVentaEnc) session
				.getAttribute("proyectoVenta")).getIdCliente());
		JRDataSource jrDatasource;
		ProyectoVentaDetDataSource ds = new ProyectoVentaDetDataSource();
		ds.create(null);
		jrDatasource = ds.llenarLista(det);
		// jrDatasource = null;
		model.addAttribute("datasource", jrDatasource);
		model.addAttribute("format", "pdf");
		model.addAttribute("P_CONTACTO", ((ProyectoComercialVentaEnc) session
				.getAttribute("proyectoVenta")).getContacto());
		model.addAttribute("P_EMAIL_CONTACTO",
				((ProyectoComercialVentaEnc) session
						.getAttribute("proyectoVenta")).getEmail());
		model.addAttribute("P_PRECIOS", precio);
		model.addAttribute("P_FORMA_PAGO", formaPago);
		model.addAttribute("P_PLAZO_ENTREGA",
				((ProyectoComercialVentaEnc) session
						.getAttribute("proyectoVenta")).getPlazoEntrega());
		model.addAttribute("P_VALIDEZ_OFERTA",
				((ProyectoComercialVentaEnc) session
						.getAttribute("proyectoVenta")).getValidezOferta());
		model.addAttribute("P_PROYECTO", ((ProyectoComercialVentaEnc) session
				.getAttribute("proyectoVenta")).getNombreProyecto());
		model.addAttribute("P_TELEFONO", vende.getTelefono());
		model.addAttribute("P_NOMBRE", vende.getNombre());
		model.addAttribute("P_EMAIL_VENDEDOR", vende.getEmail());
		/*
		 * String imagen = "resources/" + ((ProyectoComercialVentaEnc) session
		 * .getAttribute("proyectoVenta")).getIdVendedor() + ".jpg";
		 */
		/*
		 * model.addAttribute("P_FIRMA_DIGITAL", this.getClass()
		 * .getResourceAsStream(imagen));
		 */
		modelAndView = new ModelAndView(
				"multiViewReport"
						+ ((ProyectoComercialVentaEnc) session
								.getAttribute("proyectoVenta")).getIdVendedor(),
				model);
		return modelAndView;
	}

	@RequestMapping(value = "/comercial-upload", method = RequestMethod.POST)
	public @ResponseBody MultipartFile upload(Model model,
			@RequestParam("upl") MultipartFile file,
			HttpServletResponse response, HttpSession session)
			throws IOException {

		try {
			byte[] bytes = file.getBytes();

			if (session.getAttribute("archivos") == null) {
				List<CargarArchivo> archis = new ArrayList<CargarArchivo>();
				CargarArchivo arch = new CargarArchivo();
				arch.setName(file.getOriginalFilename());
				arch.setArchivo(bytes);
				archis.add(arch);
				session.setAttribute("archivos", archis);
			} else {
				CargarArchivo arch = new CargarArchivo();
				arch.setName(file.getOriginalFilename());
				arch.setArchivo(bytes);
				((List<CargarArchivo>) session.getAttribute("archivos"))
						.add(arch);

			}
			/*
			 * ComercialServicio com = new ComercialServicio(); CargarArchivo
			 * arch = new CargarArchivo();
			 * arch.setName(file.getOriginalFilename()); arch.setArchivo(bytes);
			 * com.insertProyectoVentaArchivos(arch);
			 */
		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception("comercial-upload"));
		}

		return file;
	}

	@RequestMapping(value = "/comercial-upload-guardar", method = RequestMethod.POST)
	public String uploadGuardar(Model model,
			@RequestParam("upl") MultipartFile file, HttpSession session)
			throws IOException {

		try {
			byte[] bytes = file.getBytes();
			ComercialServicio isus = new ComercialServicio();
			CargarArchivo arch = new CargarArchivo();
			arch.setName(file.getOriginalFilename());
			arch.setArchivo(bytes);
			arch.setIdProyecto(((ProyectoComercialVentaEnc) session
					.getAttribute("proyectoVenta")).getIdProyecto());
			if (isus.insertProyectoVentaArchivos(arch) > 0) {
				((List<CargarArchivo>) session.getAttribute("archivos"))
						.add(arch);
			}

		} catch (Exception e) {
			logger.error("Mensaje de error", new Exception(
					"comercial-upload-guardar"));
		}

		return "home";
	}

	private List<Tag> searchResult(String tagName) {
		int incre = 1;
		List<Tag> result = new ArrayList<Tag>();
		data.clear();
		if (tagName.length() > 2) {
			for (LinkedHashMap map : clientesPorNombreRest(tagName
					.toUpperCase())) {
				data.add(new Tag(incre, (String) map.get("nombre")));
				incre++;
			}
		}
		for (Tag tag : data) {
			if (tag.getTagName().contains(tagName)) {
				result.add(tag);
			}
		}

		return result;
	}

	private List<Persona> vendedores() {
		int incre = 1;
		List<Persona> vendedores = new ArrayList<Persona>();
		data.clear();

		for (LinkedHashMap map : vendedoresRest()) {
			Persona per = new Persona();
			per.setId((Integer) map.get("id"));
			per.setNombre((String) map.get("nombre"));
			per.setEmail((String) map.get("email"));
			per.setTelefono((String) map.get("telefono"));
			vendedores.add(per);
			incre++;
		}

		return vendedores;
	}

	private List<Auxiliar> familias() {
		int incre = 1;
		List<Auxiliar> familias = new ArrayList<Auxiliar>();
		data.clear();

		for (LinkedHashMap map : familiasRest()) {
			Auxiliar per = new Auxiliar();
			per.setAux9((String) map.get("aux9"));
			per.setAux10((String) map.get("aux10"));
			familias.add(per);
			incre++;
		}
		return familias;
	}

	private List<LinkedHashMap> clientesPorNombreRest(String nombre) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/clientesPorNombre/"
				+ nombre.toUpperCase(), List.class);
	}

	private List<LinkedHashMap> vendedoresRest() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/vendedores/",
				List.class);
	}

	private List<LinkedHashMap> familiasRest() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/familias/", List.class);
	}

	private Persona clienteRest(String nombre) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/cliente/" + nombre,
				Persona.class);
	}

	private Persona clientePorRutRest(long rut) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/clientePorRut/" + rut,
				Persona.class);
	}

	private Auxiliar clienteContactosRest(String rut) {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(SERVER_URI + "/contactos/" + rut,
				Auxiliar.class);
	}

}