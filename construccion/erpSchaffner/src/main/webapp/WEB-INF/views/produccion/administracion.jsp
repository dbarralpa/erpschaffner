<script src="resources/js/jquery.autocomplete.min.js"></script>
<script src="resources/js/pages/tablesDatatables.js"></script>
<script>
	function actualizarPermisos(valorPermiso,nombrePermiso,userName) {
		$("#actualizarPremisos").html("<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'produccion-actualizar-permisos',
			data : "val_valorPermiso=" + valorPermiso.value
				 + "&val_nombrePermiso=" + nombrePermiso
				 + "&val_user=" + userName,
				 
			success : function(data) {
				$('#actualizarPremisos').html(data);
			}
		});
	}
	
	
	function actualizarPermisosSubArea1(valorPermiso,nombrePermiso,userName,val) {
		$("#actualizarPremisos").html("<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'produccion-actualizar-permisos-subarea1',
			data : "val_valorPermiso=" + valorPermiso.value
				 + "&val_nombrePermiso=" + nombrePermiso
				 + "&val_user=" + userName,
		    type: "POST",	 
			success : function(data) {
				$('#actualizarPremisos').html(data);
			}
		});
	}
	
	function validarUsuarioImprimir() {
		$("#validarPermiso").html("<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'produccion-validacion-permiso-usuario',
			data : "val_plano_search=" + document.getElementById("planosearch").value
				 + "&val_nombreUsuario=" + document.getElementById("val_vali_usuario_impresion").value,
			success : function(data) {
				$('#validarPermiso').html(data);
			}
		});
	}
</script>

<div class="row">
	<div class="col-md-9">
		<div class="block full">
			<div class="block-title">
				<ul class="nav nav-tabs" data-toggle="tabs">
					<li class="active"><a href="#search-tab-administrar">Administrar</a></li>
					<li><a href="#search-tab-impresion-planos">Imprimir planos</a></li>
				</ul>
			</div>
			
			<div class="tab-content">
				<!-- Projects Search -->
				<div class='tab-pane active' id='search-tab-administrar'>
					<div id="actualizarPremisos">
					 ${administrar}
					</div>
				</div>
				
				
				<div class='tab-pane' id='search-tab-impresion-planos'>
				<div id='validarPermiso'></div>
					<fieldset>
							<legend>Validar impresión a usuario</legend>
							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
									
										<div class="input-group">
												<input type="text" id="planosearch" name="planosearch"
													class="form-control" placeholder="Plano..">
										</div>
									
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										
											<div class="input-group">
												${usuariosImpresionPlanos}
											</div>
										
									</div>
								</div>
							</div>
							
							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										
											<div class="input-group">
												<button type="button" class="btn btn-sm btn-primary"
													onclick="validarUsuarioImprimir()">
													<i class="fa fa-angle-right"></i> Validar impresión
												</button>
											</div>
									
									</div>
								</div>
							</div>
						</fieldset>
				</div>	
				
			</div>
		</div>
	</div>
</div>
<br/>
<br/>
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$('#planosearch')
								.autocomplete(
										{
											serviceUrl : '${pageContext.request.contextPath}/ingenieria-getPlanos',
											paramName : "tagName",
											delimiter : ",",
											transformResult : function(response) {

												return {

													suggestions : $
															.map(
																	$
																			.parseJSON(response),
																	function(
																			item) {

																		return {
																			value : item.tagName,
																			data : item.id
																		};
																	})

												};

											}

										});

					});
</script>