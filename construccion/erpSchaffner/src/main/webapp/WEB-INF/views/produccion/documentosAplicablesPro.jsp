<script src="resources/js/pages/tablesDatatables.js"></script>
<script>
	function deshabilitar(val){
		val.href="#";
	}
</script>
<div class="row">
	<div class="col-md-9">
		<div class="block full">
			<div class="block-title">
			${popup}
				<ul class="nav nav-tabs" data-toggle="tabs">
					<li class="active"><a href="#search-tab-documento-aplicable">Documento
							aplicable</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<!-- Projects Search -->
				<div class="tab-pane active" id="search-tab-documento-aplicable">
					<div id="docAplicable">${docAplicable}</div>
					${cabeceraDocAplicableEnc}
					<div class="row">
						<fieldset>
							<legend> LISTADO DE PLANOS</legend>
						</fieldset>
						<div id="detalleDocumentoAplicable">${detalleDocumentoAplicable}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br /> <br />
</div>
<br />
<br />
