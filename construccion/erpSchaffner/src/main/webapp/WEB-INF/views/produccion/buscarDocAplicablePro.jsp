<script src="resources/js/pages/tablesDatatables.js"></script>
<script type="text/javascript">
	$(function() {
		TablesDatatables.init();
	});

</script>
<div class="row">
	<div class="col-md-9">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>B�squeda</strong>
				</h2>
			</div>
			${buscarDocAplicable}
		</div>
	</div>
</div>
<br />
<div class="row">
	<div class="block full">
		<div class="block-title">
			<h2>
				<strong>Documentos aplicables</strong>
			</h2>
		</div>
		<div class='table-responsive'>
			<table id='example-datatable'
				class='table table-vcenter table-condensed table-bordered'>
				<thead>
					<tr>
						<th class='text-center' data-toggle='tooltip' title='N�mero del documento'>N�mero</th>
						<th class='text-center' data-toggle='tooltip' title='Nota de pedido'>Np</th>
						<th class='text-center' data-toggle='tooltip' title='Item de nota de pedido'>Item</th>
						<th class='text-center' data-toggle='tooltip' title='Dise�o el�ctrico'>Dise�o el�ctrico</th>
						<th class='text-center' data-toggle='tooltip' title='Dise�o mec�nico'>Dise�o mec�nico</th>
						<th class='text-center' data-toggle='tooltip' title='C�digo costo'>C�digo costo</th>
						<th class='text-center' data-toggle='tooltip' title='Refrigerante'>Fecha ingreso</th>
						<th class='text-center' data-toggle='tooltip' title='Usuario que cre� el documento'>Usuario</th>
					</tr>
				</thead>
				<tbody>${disenoBusquedaDataTable}
				</tbody>
			</table>
		</div>
	</div>
</div>
<br />
<br />

