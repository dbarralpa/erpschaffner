<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Erp Schaffner</title>

<meta name="description"
	content="ProUI is a Responsive Admin Dashboard Template created by pixelcave and published on Themeforest.">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport"
	content="width=device-width,initial-scale=1,maximum-scale=1.0">
<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon" href="resources/img/icon57.png"
	sizes="57x57">
<link rel="apple-touch-icon" href="resources/img/icon72.png"
	sizes="72x72">
<link rel="apple-touch-icon" href="resources/img/icon76.png"
	sizes="76x76">
<link rel="apple-touch-icon" href="resources/img/icon114.png"
	sizes="114x114">
<link rel="apple-touch-icon" href="resources/img/icon120.png"
	sizes="120x120">
<link rel="apple-touch-icon" href="resources/img/icon144.png"
	sizes="144x144">
<link rel="apple-touch-icon" href="resources/img/icon152.png"
	sizes="152x152">

<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/plugins.css">
<link rel="stylesheet" href="resources/css/main.css">
<link rel="stylesheet" href="resources/css/themes.css">
<script src="resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
<script src="resources/js/vendor/jquery-1.11.0.min.js"></script>
<script>
	!window.jQuery
			&& document
					.write(unescape('%3Cscript src="resources/js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));
</script>
<script src="resources/js/vendor/bootstrap.min.js"></script>
</head>
<body>
	<div id="login-background">
		<img src="resources/img/placeholders/headers/lock_header.jpg"
			alt="Lock Background" class="animation-pulseSlow">
	</div>
	
	
	<div id="login-container" class="animation-slideDown">
		${popup}
	</div>
	
	<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
	<script src="resources/js/plugins.js"></script>
	<script src="resources/js/app.js"></script>
</body>