<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Login</title>

<meta name="description"
	content="ProUI is a Responsive Admin Dashboard Template created by pixelcave and published on Themeforest.">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport"
	content="width=device-width,initial-scale=1,maximum-scale=1.0">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon" href="resources/img/icon57.png"
	sizes="57x57">
<link rel="apple-touch-icon" href="resources/img/icon72.png"
	sizes="72x72">
<link rel="apple-touch-icon" href="resources/img/icon76.png"
	sizes="76x76">
<link rel="apple-touch-icon" href="resources/img/icon114.png"
	sizes="114x114">
<link rel="apple-touch-icon" href="resources/img/icon120.png"
	sizes="120x120">
<link rel="apple-touch-icon" href="resources/img/icon144.png"
	sizes="144x144">
<link rel="apple-touch-icon" href="resources/img/icon152.png"
	sizes="152x152">
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/plugins.css">
<link rel="stylesheet" href="resources/css/main.css">
<link rel="stylesheet" href="resources/css/themes.css">
<script src="resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
<script src="resources/js/vendor/jquery-1.11.0.min.js"></script>
<script>
	!window.jQuery
			&& document
					.write(unescape('%3Cscript src="resources/js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));
</script>
<script src="resources/js/vendor/bootstrap.min.js"></script>
<script src="resources/js/pages/formsValidation.js"></script>
<script>
	$(function() {
		FormsValidation.init();
	});
</script>
</head>
<body onload='document.loginForm.j_username.focus();'>
	<!-- Login Background -->
	<div id="login-background">
		<!-- For best results use an image with a resolution of 2560x400 pixels (prefer a blurred image for smaller file size) -->
		<img src="resources/img/placeholders/headers/login_header.jpg"
			alt="Login Background" class="animation-pulseSlow">
	</div>
	<!-- END Login Background -->

	<!-- Login Container -->
	<div id="login-container" class="animation-fadeIn">
		<!-- Login Title -->
		<div class="login-title text-center">
			<h1>
				<i class="gi gi-flash"></i> <strong>Login</strong><br> <small>Ingrese
					los datos para entrar al sistema</small>
			</h1>
		</div>
		<!-- END Login Title -->

		<!-- Login Block -->
		<div class="block remove-margin">
			<%
				String errorString = (String) request.getAttribute("error");
				if (errorString != null && errorString.trim().equals("true")) {
					out.println("Usuario o contraseņa incorrecta. Vuelva a intentarlo.");
				}
			%>
			<form action="<c:url value='j_spring_security_check'/>" method="post"
				id="form-login" name="loginForm"
				class="form-horizontal form-bordered form-control-borderless">
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
							<input type="text" id="j_username" name="j_username" autocomplete="off"
								class="form-control input-lg" placeholder="Nombre de usuario..">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
							<input type="password" id="j_password" name="j_password" autocomplete="off"
								class="form-control input-lg" placeholder="Contraseņa..">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-8 text-right">
						<button type="submit" id="entrarErp" name="entrarErp" value="entrarErp" class="btn btn-sm btn-primary">
							<i class="fa fa-angle-right"></i> Entrar
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>