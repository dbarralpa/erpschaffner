<script src="resources/js/pages/tablesDatatables.js"></script>
<div class="row">
	<div class="col-md-12">
		<div class="block full">
			<div class="block-title">
				<ul class="nav nav-tabs" data-toggle="tabs">
					<li class="active"><a
						href="#search-tab-insertar-descripcion-plano">Insertar
							descripción plano</a></li>
					<li><a href="#search-tab-insertar-plano">Insertar plano</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<!-- Projects Search -->
				${popup}
				<div class="tab-pane active"
					id="search-tab-insertar-descripcion-plano">
					<form id="form-validation"
						action="ingenieria-ingresar-descripcion-plano" method="post"
						class="form-inline">
						<div class="form-group">
							<input type="text" id="val_descripcion_plano"
								name="val_descripcion_plano" class="form-control"
								placeholder="Descripción plano..">
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-sm btn-primary">
								<i class="fa fa-angle-right"></i> Ingresar descripción de plano
							</button>
						</div>
					</form>
				</div>
				<div class="tab-pane" id="search-tab-insertar-plano">
					<form id="form-validation" action="ingenieria-upload-guardar-planos"
						method="post" class="form-inline">
						<div class="form-group">
							<input type="text" id="val_nombre_plano" name="val_nombre_plano"
								class="form-control" placeholder="Nombre plano..">
							&nbsp;&nbsp;&nbsp; <label class='control-label'><a
								href='#modal-cargar-archivos' data-toggle='modal'>Cargar
									Archivos</a></label> &nbsp;&nbsp;&nbsp;
							<button type="submit" class="btn btn-primary">
								<i class="fa fa-user"></i> Guardar plano
							</button>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
	<br /> <br />
	<div id="modal-cargar-archivos" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="block full">
						<!-- Dropzone Title -->
						<div class="block-title">
							<div class="block-options pull-right">
								<a href="javascript:void(0)"
									class="btn btn-alt btn-sm btn-default" data-toggle="tooltip"
									title="Cargar archivos"><i class="gi gi-cogwheel"></i></a>
							</div>
							<h2>
								<strong>Cargar archivos</strong>
							</h2>
						</div>
						<div id="dropzone">
							<form id="upload" method="post" action="ingenieria-upload-archivo"
								enctype="multipart/form-data">
								<div id="drop">
									<a>Busque archivos</a> <input type="file" name="upl" multiple />
								</div>
								<ul>
								</ul>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br />
<br />
${numDocApliEnc}