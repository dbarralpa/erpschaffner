<script src="resources/js/pages/tablesDatatables.js"></script>
<script>
	function actualizarPermisos(valorPermiso,nombrePermiso,userName) {
		$("#actualizarPremisos").html("<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'ingenieria-actualizar-permisos',
			data : "val_valorPermiso=" + valorPermiso.value
				 + "&val_nombrePermiso=" + nombrePermiso
				 + "&val_user=" + userName,
			success : function(data) {
				$('#actualizarPremisos').html(data);
			}
		});
	}
</script>

<div class="row">
	<div class="col-md-9">
		<div class="block full">
			<div class="block-title">
				<ul class="nav nav-tabs" data-toggle="tabs">
					<li class="active"><a href="#search-tab-administrar">Administrar</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<!-- Projects Search -->
				 <div id="actualizarPremisos">
				 ${administrar}
				</div>
			</div>
		</div>
	</div>
</div>