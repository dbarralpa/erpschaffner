<script src="resources/js/jquery.autocomplete.min.js"></script>
<script src="resources/js/pages/tablesDatatables.js"></script>
<script>
	/*function nombrePlano() {
		document.getElementById("val_nom_plano").value = document
				.getElementById("val_nombre_plano").value;
	}*/
	
	function guardarDocAplicables() {
		$("#docAplicable")
				.html(
						"<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'ingenieria-guardar-documento-aplicables-enc',
			data : "val_diseno=" + document.getElementById("val_diseno").value
					+ "&val_np=" + document.getElementById("val_np").value
					+ "&val_item=" + document.getElementById("val_item").value
					+ "&val_diseno_elec="
					+ document.getElementById("val_diseno_elec").value
					+ "&val_diseno_mec="
					+ document.getElementById("val_diseno_mec").value
					+ "&numDocApliEnc=" + document.getElementById("numDocApliEnc").value,
			success : function(data) {
				$('#docAplicable').html(data);
			}
		});
	}

	function guardarDocAplicablesPlanos() {
		$("#docAplicablePlanos")
				.html(
						"<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'ingenieria-guardar-documento-aplicables-det',
			data : "numDocApliEnc="
					+ document.getElementById("numDocApliEnc").value
					+ "&val_descripcion_plano="
					+ document.getElementById("val_descripcion_plano").value
					+ "&planosearch="
					+ document.getElementById("planosearch").value,
			success : function(data) {
				$('#docAplicablePlanos').html(data);
			}
		});
	}
	
	function marcarParaActualizarDocApliDet(id,descPlano,idPlano,numero) {
		$("#agregarPlanos")
				.html(
						"<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Ejecutando...");
		$.ajax({
			url : 'ingenieria-actualizar-doc-apli-det',
			data : "id=" + id
					+ "&descPlano=" + descPlano
					+ "&idPlano=" + idPlano
					+ "&numero="  + numero,
			success : function(data) {
				$('#agregarPlanos').html(data);
			}
		});
	}
	
</script>

<div class="row">
	<div class="col-md-9">
		<div class="block full">
			<div class="block-title">
				<ul class="nav nav-tabs" data-toggle="tabs">
					<li class="active"><a href="#search-tab-documento-aplicable">Documento
							aplicable</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<!-- Projects Search -->
				<div class="tab-pane active" id="search-tab-documento-aplicable">
					<div id="docAplicable">${docAplicable}</div>
					${cabeceraDocAplicableEnc}
					<div class="row">
						<fieldset>
							<legend> AGREGAR PLANO </legend>
						</fieldset>
						<div id="agregarPlanos">${agregarPlanos}</div>
					</div>
					<div class="row">
						<fieldset>
							<legend> LISTADO DE PLANOS</legend>
						</fieldset>
						<div id="docAplicablePlanos">${detalleDocumentoAplicable}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br /> <br />
</div>
<br />
<br />
${numDocApliEnc}
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$('#planosearch')
								.autocomplete(
										{
											serviceUrl : '${pageContext.request.contextPath}/ingenieria-getPlanos',
											paramName : "tagName",
											delimiter : ",",
											transformResult : function(response) {

												return {

													suggestions : $
															.map(
																	$
																			.parseJSON(response),
																	function(
																			item) {

																		return {
																			value : item.tagName,
																			data : item.id
																		};
																	})

												};

											}

										});

					});
</script>