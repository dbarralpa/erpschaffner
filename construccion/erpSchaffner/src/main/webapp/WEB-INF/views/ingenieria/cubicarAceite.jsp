<script src="resources/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript">
	function bobinaConductores(codigo, tipoBobina) {
		$.ajax({
			url : 'ingenieria-bobina-conductores',
			data : "val_codigo=" + codigo.value + "&val_bobina=" + tipoBobina
					+ "&val_bbtAxial="
					+ document.getElementById("val_bbtAxial").value
					+ "&val_batAxial="
					+ document.getElementById("val_batAxial").value
					+ "&val_bteAxial="
					+ document.getElementById("val_bteAxial").value
					+ "&val_bbtRadial="
					+ document.getElementById("val_bbtRadial").value
					+ "&val_batRadial="
					+ document.getElementById("val_batRadial").value
					+ "&val_bteRadial="
					+ document.getElementById("val_bteRadial").value,
			success : function(data) {
				$('#result').html(data);
			}
		});
	}

	function cambiarMaterialPinturaPlanchaFierro(codigo, tipoEspesor) {
		$.ajax({
			url : 'ingenieria-material-pintura-plancha-fierro',
			data : "val_codigo=" + codigo.value + "&val_espesor=" + tipoEspesor
					+ "&val_esp_fondo="
					+ document.getElementById("val_esp_fondo").value
					+ "&val_cod_fondo="
					+ document.getElementById("val_cod_fondo").value
					+ "&val_esp_manto="
					+ document.getElementById("val_esp_manto").value
					+ "&val_cod_manto="
					+ document.getElementById("val_cod_manto").value
					+ "&val_esp_tapa="
					+ document.getElementById("val_esp_tapa").value
					+ "&val_cod_tapa="
					+ document.getElementById("val_cod_tapa").value,
			success : function(data) {
				$('#materialesPinturaPlanchaFierro').html(data);
			}
		});
	}

	function cambiarMaterialPresspanEntreCapasBbt(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-entrecapas-bbt',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_bbtEspesoresCapa1="
					+ document.getElementById("val_bbtEspesoresCapa1").value
					+ "&val_bbtCodCapa1="
					+ document.getElementById("val_bbtCodCapa1").value
					+ "&val_bbtDescCapa1="
					+ document.getElementById("val_bbtDescCapa1").value
					+ "&val_bbtEspesoresCapa2="
					+ document.getElementById("val_bbtEspesoresCapa2").value
					+ "&val_bbtCodCapa2="
					+ document.getElementById("val_bbtCodCapa2").value
					+ "&val_bbtDescCapa2="
					+ document.getElementById("val_bbtDescCapa2").value
					+ "&val_bbtEspesoresCapa3="
					+ document.getElementById("val_bbtEspesoresCapa3").value
					+ "&val_bbtCodCapa3="
					+ document.getElementById("val_bbtCodCapa3").value
					+ "&val_bbtDescCapa3="
					+ document.getElementById("val_bbtDescCapa3").value,
			success : function(data) {
				$('#entreCapasBbt').html(data);
			}
		});
	}

	function aislacionExternaBbt(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-aislacion-externa-bbt',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_espesoresAtbt1="
					+ document.getElementById("val_espesoresAtbt1").value
					+ "&val_codAtbt1="
					+ document.getElementById("val_codAtbt1").value
					+ "&val_descAtbt1="
					+ document.getElementById("val_descAtbt1").value
					+ "&val_espesoresAtbt2="
					+ document.getElementById("val_espesoresAtbt2").value
					+ "&val_codAtbt2="
					+ document.getElementById("val_codAtbt2").value
					+ "&val_descAtbt2="
					+ document.getElementById("val_descAtbt2").value
					+ "&val_espesoresAtbt3="
					+ document.getElementById("val_espesoresAtbt3").value
					+ "&val_codAtbt3="
					+ document.getElementById("val_codAtbt3").value
					+ "&val_descAtbt3="
					+ document.getElementById("val_descAtbt3").value,
			success : function(data) {
				$('#aislacionExternaBbt').html(data);
			}
		});
	}

	function alislacionBaseBbt(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-aislacion-base-bbt',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_espesoresBase1="
					+ document.getElementById("val_espesoresBase1").value
					+ "&val_codBase1="
					+ document.getElementById("val_codBase1").value
					+ "&val_descBase1="
					+ document.getElementById("val_descBase1").value
					+ "&val_espesoresBase2="
					+ document.getElementById("val_espesoresBase2").value
					+ "&val_codBase2="
					+ document.getElementById("val_codBase2").value
					+ "&val_descBase2="
					+ document.getElementById("val_descBase2").value
					+ "&val_espesoresBase3="
					+ document.getElementById("val_espesoresBase3").value
					+ "&val_codBase3="
					+ document.getElementById("val_codBase3").value
					+ "&val_descBase3="
					+ document.getElementById("val_descBase3").value,
			success : function(data) {
				$('#alislacionBaseBbt').html(data);
			}
		});
	}

	function cambiarMaterialPresspanEntreCapasBat(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-entrecapas-bat',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_batEspesoresCapa1="
					+ document.getElementById("val_batEspesoresCapa1").value
					+ "&val_batCodCapa1="
					+ document.getElementById("val_batCodCapa1").value
					+ "&val_batDescCapa1="
					+ document.getElementById("val_batDescCapa1").value
					+ "&val_batEspesoresCapa2="
					+ document.getElementById("val_batEspesoresCapa2").value
					+ "&val_batCodCapa2="
					+ document.getElementById("val_batCodCapa2").value
					+ "&val_batDescCapa2="
					+ document.getElementById("val_batDescCapa2").value
					+ "&val_batEspesoresCapa3="
					+ document.getElementById("val_batEspesoresCapa3").value
					+ "&val_batCodCapa3="
					+ document.getElementById("val_batCodCapa3").value
					+ "&val_batDescCapa3="
					+ document.getElementById("val_batDescCapa3").value,
			success : function(data) {
				$('#entreCapasBat').html(data);
			}
		});
	}

	function aislacionExternaBat(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-aislacion-externa-bat',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_batespesoresAtbt1="
					+ document.getElementById("val_batespesoresAtbt1").value
					+ "&val_batcodAtbt1="
					+ document.getElementById("val_batcodAtbt1").value
					+ "&val_batdescAtbt1="
					+ document.getElementById("val_batdescAtbt1").value
					+ "&val_batespesoresAtbt2="
					+ document.getElementById("val_batespesoresAtbt2").value
					+ "&val_batcodAtbt2="
					+ document.getElementById("val_batcodAtbt2").value
					+ "&val_batdescAtbt2="
					+ document.getElementById("val_batdescAtbt2").value
					+ "&val_batespesoresAtbt3="
					+ document.getElementById("val_batespesoresAtbt3").value
					+ "&val_batcodAtbt3="
					+ document.getElementById("val_batcodAtbt3").value
					+ "&val_batdescAtbt3="
					+ document.getElementById("val_batdescAtbt3").value,
			success : function(data) {
				$('#aislacionExternaBat').html(data);
			}
		});
	}

	function alislacionBaseBat(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-aislacion-base-bat',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_batespesoresBase1="
					+ document.getElementById("val_batespesoresBase1").value
					+ "&val_batcodBase1="
					+ document.getElementById("val_batcodBase1").value
					+ "&val_batdescBase1="
					+ document.getElementById("val_batdescBase1").value
					+ "&val_batespesoresBase2="
					+ document.getElementById("val_batespesoresBase2").value
					+ "&val_batcodBase2="
					+ document.getElementById("val_batcodBase2").value
					+ "&val_batdescBase2="
					+ document.getElementById("val_batdescBase2").value
					+ "&val_batespesoresBase3="
					+ document.getElementById("val_batespesoresBase3").value
					+ "&val_batcodBase3="
					+ document.getElementById("val_batcodBase3").value
					+ "&val_batdescBase3="
					+ document.getElementById("val_batdescBase3").value,
			success : function(data) {
				$('#alislacionBaseBat').html(data);
			}
		});
	}

	function entreCapasBte(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-entrecapas-bte',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_bteEspesoresCapa1="
					+ document.getElementById("val_bteEspesoresCapa1").value
					+ "&val_bteCodCapa1="
					+ document.getElementById("val_bteCodCapa1").value
					+ "&val_bteDescCapa1="
					+ document.getElementById("val_bteDescCapa1").value
					+ "&val_bteEspesoresCapa2="
					+ document.getElementById("val_bteEspesoresCapa2").value
					+ "&val_bteCodCapa2="
					+ document.getElementById("val_bteCodCapa2").value
					+ "&val_bteDescCapa2="
					+ document.getElementById("val_bteDescCapa2").value
					+ "&val_bteEspesoresCapa3="
					+ document.getElementById("val_bteEspesoresCapa3").value
					+ "&val_bteCodCapa3="
					+ document.getElementById("val_bteCodCapa3").value
					+ "&val_bteDescCapa3="
					+ document.getElementById("val_bteDescCapa3").value,
			success : function(data) {
				$('#entreCapasBte').html(data);
			}
		});
	}

	function aislacionExternaBte(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-aislacion-externa-bte',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_bteespesoresAtbt1="
					+ document.getElementById("val_bteespesoresAtbt1").value
					+ "&val_btecodAtbt1="
					+ document.getElementById("val_btecodAtbt1").value
					+ "&val_btedescAtbt1="
					+ document.getElementById("val_btedescAtbt1").value
					+ "&val_bteespesoresAtbt2="
					+ document.getElementById("val_bteespesoresAtbt2").value
					+ "&val_btecodAtbt2="
					+ document.getElementById("val_btecodAtbt2").value
					+ "&val_btedescAtbt2="
					+ document.getElementById("val_btedescAtbt2").value
					+ "&val_bteespesoresAtbt3="
					+ document.getElementById("val_bteespesoresAtbt3").value
					+ "&val_btecodAtbt3="
					+ document.getElementById("val_btecodAtbt3").value
					+ "&val_btedescAtbt3="
					+ document.getElementById("val_btedescAtbt3").value,
			success : function(data) {
				$('#aislacionExternaBte').html(data);
			}
		});
	}

	function alislacionBaseBte(codigo, tipoPresspan) {
		$.ajax({
			url : 'ingenieria-material-presspan-aislacion-base-bte',
			data : "val_codigo=" + codigo.value + "&val_tipo_presspan="
					+ tipoPresspan + "&val_bteespesoresBase1="
					+ document.getElementById("val_bteespesoresBase1").value
					+ "&val_btecodBase1="
					+ document.getElementById("val_btecodBase1").value
					+ "&val_btedescBase1="
					+ document.getElementById("val_btedescBase1").value
					+ "&val_bteespesoresBase2="
					+ document.getElementById("val_bteespesoresBase2").value
					+ "&val_btecodBase2="
					+ document.getElementById("val_btecodBase2").value
					+ "&val_btedescBase2="
					+ document.getElementById("val_btedescBase2").value
					+ "&val_bteespesoresBase3="
					+ document.getElementById("val_bteespesoresBase3").value
					+ "&val_btecodBase3="
					+ document.getElementById("val_btecodBase3").value
					+ "&val_btedescBase3="
					+ document.getElementById("val_btedescBase3").value,
			success : function(data) {
				$('#alislacionBaseBte').html(data);
			}
		});
	}

	function tipoNucleo(tipoNucleo) {
		$.ajax({
			url : 'ingenieria-nuc-ferr-tipo-nucleo',
			data : "tipoNucleo=" + tipoNucleo.value + "&val_entreCentroNucleo="
					+ document.getElementById("val_entreCentroNucleo").value
					+ "&val_ventana_nucleo="
					+ document.getElementById("val_ventana_nucleo").value,
			success : function(data) {
				$('#tipoNucleo').html(data);
			}
		});
	}

	function aceroSilicoso(codigo, cantidad, pesoNucleo, i) {
		$.ajax({
			url : 'ingenieria-nuc-ferr-tipo-acero-silicoso',
			data : "val_codigo_silicoso=" + codigo.value + "&val_acesilicant="
					+ document.getElementById(cantidad).value
					+ "&val_acesilipesonucleo="
					+ document.getElementById(pesoNucleo).value + "&i=" + i
					+ "&val_tipo_nucleo="
					+ document.getElementById("val_tipoNucleo").value,
			success : function(data) {
				if (i == 1) {
					$('#aceroSilicoso1').html(data);
				} else if (i == 2) {
					$('#aceroSilicoso2').html(data);
				} else if (i == 3) {
					$('#aceroSilicoso3').html(data);
				} else if (i == 4) {
					$('#aceroSilicoso4').html(data);
				} else if (i == 5) {
					$('#aceroSilicoso5').html(data);
				} else if (i == 6) {
					$('#aceroSilicoso6').html(data);
				} else if (i == 7) {
					$('#aceroSilicoso7').html(data);
				} else if (i == 8) {
					$('#aceroSilicoso8').html(data);
				} else if (i == 9) {
					$('#aceroSilicoso9').html(data);
				} else if (i == 10) {
					$('#aceroSilicoso10').html(data);
				}
			}
		});
	}

	function cambiarMaterialPinturaEstanque(codigo, i) {
		$.ajax({
			url : 'ingenieria-material-pintura-estanque',
			data : "val_codigo=" + codigo.value + "&val_codPin1="
					+ document.getElementById("val_codPin1").value
					+ "&val_codPin2="
					+ document.getElementById("val_codPin2").value
					+ "&val_codPin3="
					+ document.getElementById("val_codPin3").value
					+ "&val_codPin4="
					+ document.getElementById("val_codPin4").value + "&i=" + i,
			success : function(data) {
				$('#pinturaEstanque').html(data);
			}
		});
	}

	function obtenerCodigo() {
		if (document.getElementById("modificarAccesorio").value == '0') {
			var dato = document.getElementById("materialsearch").value;
			var elem = dato.split("---");
			document.getElementById("val_codigo_accesorio").value = elem[1];
			document.getElementById("val_descri_accesorio").value = elem[0];
		}

	}

	function accesoriosDatatables() {
		$
				.ajax({
					url : 'ingenieria-accesorios-datatable',
					data : "val_cantidad_accesorio="
							+ document.getElementById("val_cantidad_accesorio").value
							+ "&val_codigo_accesorio="
							+ document.getElementById("val_codigo_accesorio").value
							+ "&val_datosgenerales_codigo="
							+ document
									.getElementById("val_datosgenerales_codigo").value
							+ "&modificarAccesorio="
							+ document.getElementById("modificarAccesorio").value,
					success : function(data) {
						$('#accesoriosDatatable').html(data);
					}
				});
	}

	function accesoriosModificar(codigo, descripcion, cantidad) {
		$.ajax({
			url : 'ingenieria-accesorios-modificar',
			data : "val_cantidad_accesorio=" + cantidad
					+ "&val_codigo_accesorio=" + codigo
					+ "&val_descripcion_accesorio=" + descripcion,
			success : function(data) {
				$('#modificarListAccesorios').html(data);
			}
		});
	}

	function accesoriosLimpiar() {
		document.getElementById("materialsearch").value = "";
		$.ajax({
			url : 'ingenieria-accesorios-limpiar',
			data : "",
			success : function(data) {
				$('#modificarListAccesorios').html(data);
			}
		});

	}

	function accesoriosDelete(codigo) {
		$
				.ajax({
					url : 'ingenieria-accesorios-delete',
					data : "val_codigo_accesorio="
							+ codigo
							+ "&val_datosgenerales_codigo="
							+ document
									.getElementById("val_datosgenerales_codigo").value,
					success : function(data) {
						$('#accesoriosDatatable').html(data);
					}
				});
	}

	function codigoTrafoCoti() {
		$.ajax({
			url : 'ingenieria-codigo-trafo-cotizacion',
			data : "val_combo0=" + document.getElementById("combo0").value
					+ "&val_combo1=" + document.getElementById("combo1").value
					+ "&val_combo2=" + document.getElementById("combo2").value
					+ "&val_combo3=" + document.getElementById("combo3").value
					+ "&val_combo4=" + document.getElementById("combo4").value
					+ "&val_combo5=" + document.getElementById("combo5").value,
			success : function(data) {
				$('#codigoTrafoCoti').html(data);
			}
		});
	}

	function codigoTrafoCotiGenerar(val) {
		$
				.ajax({
					url : 'ingenieria-codigo-trafo-cotizacion-generar',
					data : "val_combo0="
							+ document.getElementById("combo0").value
							+ "&val_combo1="
							+ document.getElementById("combo1").value
							+ "&val_combo2="
							+ document.getElementById("combo2").value
							+ "&val_combo3="
							+ document.getElementById("combo3").value
							+ "&val_combo4="
							+ document.getElementById("combo4").value
							+ "&val_combo5="
							+ document.getElementById("combo5").value
							+ "&diseno2="
							+ document
									.getElementById("val_datosgenerales_codigo").value
							+ "&seAc=" + val,
					success : function(data) {
						$('#codigoTrafoCotiGenerar').html(data);
					}
				});
	}

	function actualizarDisenoAceite() {
		$("#actualizarDisenoAceite").html("<i class='fa fa-spinner fa-4x fa-spin'></i>&nbsp;&nbsp;Actualizando...");
		$
				.ajax({
					url : 'ingenieria-update-diseno-aceite',
					data : "val_datosgenerales_diseno="
							+ document
									.getElementById("val_datosgenerales_diseno").value
							+ "&val_datosgenerales_codigo="
							+ document
									.getElementById("val_datosgenerales_codigo").value
							+ "&val_datosgenerales_distribucion="
							+ document
									.getElementById("val_datosgenerales_distribucion").value
							+ "&val_datosgenerales_potencia="
							+ document
									.getElementById("val_datosgenerales_potencia").value
							+ "&val_datosgenerales_fases="
							+ document
									.getElementById("val_datosgenerales_fases").value
							+ "&val_datosgenerales_refrigerante="
							+ document
									.getElementById("val_datosgenerales_refrigerante").value
							+ "&val_datosgenerales_kv="
							+ document.getElementById("val_datosgenerales_kv").value
							+ "&val_especificaciones_derivacion="
							+ document
									.getElementById("val_especificaciones_derivacion").value
							+ "&val_especificaciones_impulso1="
							+ document
									.getElementById("val_especificaciones_impulso1").value
							+ "&val_especificaciones_impulso2="
							+ document
									.getElementById("val_especificaciones_impulso2").value
							+ "&val_especificaciones_impulso3="
							+ document
									.getElementById("val_especificaciones_impulso3").value
							+ "&val_bobina_MaterialBbt="
							+ document.getElementById("val_bobina_MaterialBbt").value
							+ "&val_bobina_MaterialBat="
							+ document.getElementById("val_bobina_MaterialBat").value
							+ "&val_bobina_MaterialBte="
							+ document.getElementById("val_bobina_MaterialBte").value
							+ "&val_bbtAxial="
							+ document.getElementById("val_bbtAxial").value
							+ "&val_batAxial="
							+ document.getElementById("val_batAxial").value
							+ "&val_bteAxial="
							+ document.getElementById("val_bteAxial").value
							+ "&val_bbtRadial="
							+ document.getElementById("val_bbtRadial").value
							+ "&val_batRadial="
							+ document.getElementById("val_batRadial").value
							+ "&val_bteRadial="
							+ document.getElementById("val_bteRadial").value
							+ "&val_bobina_incrementoPapel_bbt="
							+ document
									.getElementById("val_bobina_incrementoPapel_bbt").value
							+ "&val_bobina_incrementoPapel_bat="
							+ document
									.getElementById("val_bobina_incrementoPapel_bat").value
							+ "&val_bobina_incrementoPapel_bte="
							+ document
									.getElementById("val_bobina_incrementoPapel_bte").value
							+ "&val_bobina_largoEpiraMedia_bbt="
							+ document
									.getElementById("val_bobina_largoEpiraMedia_bbt").value
							+ "&val_bobina_largoEpiraMedia_bat="
							+ document
									.getElementById("val_bobina_largoEpiraMedia_bat").value
							+ "&val_bobina_largoEpiraMedia_bte="
							+ document
									.getElementById("val_bobina_largoEpiraMedia_bte").value
							+ "&val_bobina_vueltasXfase_bbt="
							+ document
									.getElementById("val_bobina_vueltasXfase_bbt").value
							+ "&val_bobina_vueltasXfase_bat="
							+ document
									.getElementById("val_bobina_vueltasXfase_bat").value
							+ "&val_bobina_vueltasXfase_bte="
							+ document
									.getElementById("val_bobina_vueltasXfase_bte").value
							+ "&val_bobina_principiosXcapas_bbt="
							+ document
									.getElementById("val_bobina_principiosXcapas_bbt").value
							+ "&val_bobina_principiosXcapas_bat="
							+ document
									.getElementById("val_bobina_principiosXcapas_bat").value
							+ "&val_bobina_principiosXcapas_bte="
							+ document
									.getElementById("val_bobina_principiosXcapas_bte").value
							+ "&val_bobina_condParalelo_bbt="
							+ document
									.getElementById("val_bobina_condParalelo_bbt").value
							+ "&val_bobina_condParalelo_bat="
							+ document
									.getElementById("val_bobina_condParalelo_bat").value
							+ "&val_bobina_condParalelo_bte="
							+ document
									.getElementById("val_bobina_condParalelo_bte").value
							+ "&val_bobina_largoBobinaCuellos_bbt="
							+ document
									.getElementById("val_bobina_largoBobinaCuellos_bbt").value
							+ "&val_bobina_largoBobinaCuellos_bat="
							+ document
									.getElementById("val_bobina_largoBobinaCuellos_bat").value
							+ "&val_bobina_largoBobinaCuellos_bte="
							+ document
									.getElementById("val_bobina_largoBobinaCuellos_bte").value
							+ "&val_bobina_espesorFisico_bbt="
							+ document
									.getElementById("val_bobina_espesorFisico_bbt").value
							+ "&val_bobina_espesorFisico_bat="
							+ document
									.getElementById("val_bobina_espesorFisico_bat").value
							+ "&val_bobina_espesorFisico_bte="
							+ document
									.getElementById("val_bobina_espesorFisico_bte").value
							+ "&val_bobina_ductosCompletos_bbt="
							+ document
									.getElementById("val_bobina_ductosCompletos_bbt").value
							+ "&val_bobina_ductosCompletos_bat="
							+ document
									.getElementById("val_bobina_ductosCompletos_bat").value
							+ "&val_bobina_ductosCompletos_bte="
							+ document
									.getElementById("val_bobina_ductosCompletos_bte").value
							+ "&val_bobina_ductosCabezal_bbt="
							+ document
									.getElementById("val_bobina_ductosCabezal_bbt").value
							+ "&val_bobina_ductosCabezal_bat="
							+ document
									.getElementById("val_bobina_ductosCabezal_bat").value
							+ "&val_bobina_ductosCabezal_bte="
							+ document
									.getElementById("val_bobina_ductosCabezal_bte").value
							+ "&val_bobina_ductosNubTat_bbt="
							+ document
									.getElementById("val_bobina_ductosNubTat_bbt").value
							+ "&val_bobina_ductosNubTat_bat="
							+ document
									.getElementById("val_bobina_ductosNubTat_bat").value
							+ "&val_bobina_ductosNubTat_bte="
							+ document
									.getElementById("val_bobina_ductosNubTat_bte").value
							+ "&val_bobina_ductosCabezalAtbt_bbt="
							+ document
									.getElementById("val_bobina_ductosCabezalAtbt_bbt").value
							+ "&val_bobina_ductosCabezalAtbt_bat="
							+ document
									.getElementById("val_bobina_ductosCabezalAtbt_bat").value
							+ "&val_bobina_ductosCabezalAtbt_bte="
							+ document
									.getElementById("val_bobina_ductosCabezalAtbt_bte").value
							+ "&val_bobina_condAxiales_bbt="
							+ document
									.getElementById("val_bobina_condAxiales_bbt").value
							+ "&val_bobina_condAxiales_bat="
							+ document
									.getElementById("val_bobina_condAxiales_bat").value
							+ "&val_bobina_condAxiales_bte="
							+ document
									.getElementById("val_bobina_condAxiales_bte").value
							+ "&val_bobina_capasDiscos_bbt="
							+ document
									.getElementById("val_bobina_capasDiscos_bbt").value
							+ "&val_bobina_capasDiscos_bat="
							+ document
									.getElementById("val_bobina_capasDiscos_bat").value
							+ "&val_bobina_capasDiscos_bte="
							+ document
									.getElementById("val_bobina_capasDiscos_bte").value
							+ "&val_estanquetapa_largo="
							+ document.getElementById("val_estanquetapa_largo").value
							+ "&val_estanquetapa_ancho="
							+ document.getElementById("val_estanquetapa_ancho").value
							+ "&val_estanquetapa_alto="
							+ document.getElementById("val_estanquetapa_alto").value
							+ "&val_esp_manto="
							+ document.getElementById("val_esp_manto").value
							+ "&val_cod_manto="
							+ document.getElementById("val_cod_manto").value
							+ "&val_material_pintura_plancha_fierro_manto="
							+ document
									.getElementById("val_material_pintura_plancha_fierro_manto").value
							+ "&val_material_pintura_plancha_fierro_tapa="
							+ document
									.getElementById("val_material_pintura_plancha_fierro_tapa").value
							+ "&val_material_pintura_plancha_fierro_fondo="
							+ document
									.getElementById("val_material_pintura_plancha_fierro_fondo").value
							+ "&val_esp_tapa="
							+ document.getElementById("val_esp_tapa").value
							+ "&val_cod_tapa="
							+ document.getElementById("val_cod_tapa").value
							+ "&val_esp_fondo="
							+ document.getElementById("val_esp_fondo").value
							+ "&val_cod_fondo="
							+ document.getElementById("val_cod_fondo").value
							+ "&val_codPin1="
							+ document.getElementById("val_codPin1").value
							+ "&val_codPin2="
							+ document.getElementById("val_codPin2").value
							+ "&val_codPin3="
							+ document.getElementById("val_codPin3").value
							+ "&val_codPin4="
							+ document.getElementById("val_codPin4").value
							+ "&val_bbtEspesoresCapa1="
							+ document.getElementById("val_bbtEspesoresCapa1").value
							+ "&val_bbtCodCapa1="
							+ document.getElementById("val_bbtCodCapa1").value
							+ "&val_bbtEspesoresCapa2="
							+ document.getElementById("val_bbtEspesoresCapa2").value
							+ "&val_bbtCodCapa2="
							+ document.getElementById("val_bbtCodCapa2").value
							+ "&val_bbtEspesoresCapa3="
							+ document.getElementById("val_bbtEspesoresCapa3").value
							+ "&val_bbtCodCapa3="
							+ document.getElementById("val_bbtCodCapa3").value
							+ "&val_batEspesoresCapa1="
							+ document.getElementById("val_batEspesoresCapa1").value
							+ "&val_batCodCapa1="
							+ document.getElementById("val_batCodCapa1").value
							+ "&val_batEspesoresCapa2="
							+ document.getElementById("val_batEspesoresCapa2").value
							+ "&val_batCodCapa2="
							+ document.getElementById("val_batCodCapa2").value
							+ "&val_batEspesoresCapa3="
							+ document.getElementById("val_batEspesoresCapa3").value
							+ "&val_batCodCapa3="
							+ document.getElementById("val_batCodCapa3").value
							+ "&val_bteEspesoresCapa1="
							+ document.getElementById("val_bteEspesoresCapa1").value
							+ "&val_bteCodCapa1="
							+ document.getElementById("val_bteCodCapa1").value
							+ "&val_bteEspesoresCapa2="
							+ document.getElementById("val_bteEspesoresCapa2").value
							+ "&val_bteCodCapa2="
							+ document.getElementById("val_bteCodCapa2").value
							+ "&val_bteEspesoresCapa3="
							+ document.getElementById("val_bteEspesoresCapa3").value
							+ "&val_bteCodCapa3="
							+ document.getElementById("val_bteCodCapa3").value
							+ "&val_espesoresAtbt1="
							+ document.getElementById("val_espesoresAtbt1").value
							+ "&val_codAtbt1="
							+ document.getElementById("val_codAtbt1").value
							+ "&val_espesoresAtbt2="
							+ document.getElementById("val_espesoresAtbt2").value
							+ "&val_codAtbt2="
							+ document.getElementById("val_codAtbt2").value
							+ "&val_espesoresAtbt3="
							+ document.getElementById("val_espesoresAtbt3").value
							+ "&val_codAtbt3="
							+ document.getElementById("val_codAtbt3").value
							+ "&val_batespesoresAtbt1="
							+ document.getElementById("val_batespesoresAtbt1").value
							+ "&val_batcodAtbt1="
							+ document.getElementById("val_batcodAtbt1").value
							+ "&val_batespesoresAtbt2="
							+ document.getElementById("val_batespesoresAtbt2").value
							+ "&val_batcodAtbt2="
							+ document.getElementById("val_batcodAtbt2").value
							+ "&val_batespesoresAtbt3="
							+ document.getElementById("val_batespesoresAtbt3").value
							+ "&val_batcodAtbt3="
							+ document.getElementById("val_batcodAtbt3").value
							+ "&val_bteespesoresAtbt1="
							+ document.getElementById("val_bteespesoresAtbt1").value
							+ "&val_btecodAtbt1="
							+ document.getElementById("val_btecodAtbt1").value
							+ "&val_bteespesoresAtbt2="
							+ document.getElementById("val_bteespesoresAtbt2").value
							+ "&val_btecodAtbt2="
							+ document.getElementById("val_btecodAtbt2").value
							+ "&val_bteespesoresAtbt3="
							+ document.getElementById("val_bteespesoresAtbt3").value
							+ "&val_btecodAtbt3="
							+ document.getElementById("val_btecodAtbt3").value
							+ "&val_espesoresBase1="
							+ document.getElementById("val_espesoresBase1").value
							+ "&val_codBase1="
							+ document.getElementById("val_codBase1").value
							+ "&val_espesoresBase2="
							+ document.getElementById("val_espesoresBase2").value
							+ "&val_codBase2="
							+ document.getElementById("val_codBase2").value
							+ "&val_espesoresBase3="
							+ document.getElementById("val_espesoresBase3").value
							+ "&val_codBase3="
							+ document.getElementById("val_codBase3").value
							+ "&val_batespesoresBase1="
							+ document.getElementById("val_batespesoresBase1").value
							+ "&val_batcodBase1="
							+ document.getElementById("val_batcodBase1").value
							+ "&val_batespesoresBase2="
							+ document.getElementById("val_batespesoresBase2").value
							+ "&val_batcodBase2="
							+ document.getElementById("val_batcodBase2").value
							+ "&val_batespesoresBase3="
							+ document.getElementById("val_batespesoresBase3").value
							+ "&val_batcodBase3="
							+ document.getElementById("val_batcodBase3").value
							+ "&val_bteespesoresBase1="
							+ document.getElementById("val_bteespesoresBase1").value
							+ "&val_btecodBase1="
							+ document.getElementById("val_btecodBase1").value
							+ "&val_bteespesoresBase2="
							+ document.getElementById("val_bteespesoresBase2").value
							+ "&val_btecodBase2="
							+ document.getElementById("val_btecodBase2").value
							+ "&val_bteespesoresBase3="
							+ document.getElementById("val_bteespesoresBase3").value
							+ "&val_btecodBase3="
							+ document.getElementById("val_btecodBase3").value
							+ "&val_silicoso1="
							+ document.getElementById("val_silicoso1").value
							+ "&val_acesilicant1="
							+ document.getElementById("val_acesilicant1").value
							+ "&val_acesilipesonucleo1="
							+ document.getElementById("val_acesilipesonucleo1").value
							+ "&val_silicoso2="
							+ document.getElementById("val_silicoso2").value
							+ "&val_acesilicant2="
							+ document.getElementById("val_acesilicant2").value
							+ "&val_acesilipesonucleo2="
							+ document.getElementById("val_acesilipesonucleo2").value
							+ "&val_radioSoporte="
							+ document.nucleoFerreteria.val_radioSoporte.value
							+ "&val_tipoNucleo="
							+ document.getElementById("val_tipoNucleo").value
							+ "&val_nucferr_anchoCabezal="
							+ document
									.getElementById("val_nucferr_anchoCabezal").value
							+ "&val_nucferr_largoVentana="
							+ document
									.getElementById("val_nucferr_largoVentana").value
							+ "&val_nucferr_diametro="
							+ document.getElementById("val_nucferr_diametro").value
							+ "&val_entreCentroNucleo="
							+ document.getElementById("val_entreCentroNucleo").value
							+ "&val_ventana_nucleo="
							+ document.getElementById("val_ventana_nucleo").value
							+ "&val_corte="
							+ document.nucleoFerreteria.val_corte.value

							+ "&val_silicoso3="
							+ document.getElementById("val_silicoso3").value
							+ "&val_acesilicant3="
							+ document.getElementById("val_acesilicant3").value
							+ "&val_acesilipesonucleo3="
							+ document.getElementById("val_acesilipesonucleo3").value
							+ "&val_silicoso4="
							+ document.getElementById("val_silicoso4").value
							+ "&val_acesilicant4="
							+ document.getElementById("val_acesilicant4").value
							+ "&val_acesilipesonucleo4="
							+ document.getElementById("val_acesilipesonucleo4").value
							+ "&val_silicoso5="
							+ document.getElementById("val_silicoso5").value
							+ "&val_acesilicant5="
							+ document.getElementById("val_acesilicant5").value
							+ "&val_acesilipesonucleo5="
							+ document.getElementById("val_acesilipesonucleo5").value
							+ "&val_silicoso6="
							+ document.getElementById("val_silicoso6").value
							+ "&val_acesilicant6="
							+ document.getElementById("val_acesilicant6").value
							+ "&val_acesilipesonucleo6="
							+ document.getElementById("val_acesilipesonucleo6").value
							+ "&val_silicoso7="
							+ document.getElementById("val_silicoso7").value
							+ "&val_acesilicant7="
							+ document.getElementById("val_acesilicant7").value
							+ "&val_acesilipesonucleo7="
							+ document.getElementById("val_acesilipesonucleo7").value
							+ "&val_silicoso8="
							+ document.getElementById("val_silicoso8").value
							+ "&val_acesilicant8="
							+ document.getElementById("val_acesilicant8").value
							+ "&val_acesilipesonucleo8="
							+ document.getElementById("val_acesilipesonucleo8").value
							+ "&val_silicoso9="
							+ document.getElementById("val_silicoso9").value
							+ "&val_acesilicant9="
							+ document.getElementById("val_acesilicant9").value
							+ "&val_acesilipesonucleo9="
							+ document.getElementById("val_acesilipesonucleo9").value
							+ "&val_silicoso10="
							+ document.getElementById("val_silicoso10").value
							+ "&val_acesilicant10="
							+ document.getElementById("val_acesilicant10").value
							+ "&val_acesilipesonucleo10="
							+ document
									.getElementById("val_acesilipesonucleo10").value
							+ "&comboMaterialRadiador="
							+ document.getElementById("comboMaterialRadiador").value
							+ "&val_radiador_num="
							+ document.getElementById("val_radiador_num").value
							+ "&val_radiador_elementos="
							+ document.getElementById("val_radiador_elementos").value,
					success : function(data) {
						$('#actualizarDisenoAceite').html(data);
					}
				});
	}

	function comboTipoRadiador1() {
		$
				.ajax({
					url : 'ingenieria-tipo-radiador',
					data : "comboTipoRadiador="
							+ document.getElementById("comboTipoRadiador").value
							+ "&val_datosgenerales_codigo="
							+ document
									.getElementById("val_datosgenerales_codigo").value,
					success : function(data) {
						$('#comboTipoRadiador2').html(data);
					}
				});
	}

	function cubicar()
	{
		var diseno = document.getElementById("val_datosgenerales_codigo").value;
		var potencia = document.getElementById("val_datosgenerales_potencia").value;
		var fases = document.getElementById("val_datosgenerales_fases").value;
		window.open("ingenieria-imprimir-cubicacion?diseno="+diseno+"&potencia="+potencia+"&fases="+fases);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="block">
			<div class="block-title">
				<ul class="nav nav-tabs" data-toggle="tabs">
				    <li class="active"><a href="#search-tab-acceso">Accesorios</a></li>
					<li><a href="#search-tab-datos-generales">Datos generales</a></li>
					<li><a href="#search-tab-espe">Especificaci�n</a></li>
					<li><a href="#search-tab-bobinas">Bobinas</a></li>
					<li><a href="#search-tab-est-tapa">Estanque tapa</a></li>
					<li><a href="#search-tab-presspan">Presspan</a></li>
					<li><a href="#search-tab-radiador">Radiadores</a></li>
					<li><a href="#search-tab-nuc-ferr">N�cleo ferreteria</a></li>
					<li><a href="#search-tab-guardar">Control</a></li>
				</ul>
			</div>

			<div class="tab-content">
				<!-- Projects Search -->
				<div class="tab-pane active" id="search-tab-acceso">
					<div class="row">
						<form action="#" method="post" class="form-horizontal">
							<fieldset>
								<legend>
									<i class="fa fa-angle-right"></i> Buscar material para agregar
									a accesorios. <br /></br> <input type="text" id="materialsearch"
										name="materialsearch" class="col-md-9"
										placeholder="buscar material.." required>
								</legend>
							</fieldset>
						</form>
						<br /> <br />
						<div id="modificarListAccesorios">${modificarListAccesorios}</div>
						<br /> <br />
						<div id="accesoriosDatatable">${accesoriosDataTable}</div>
					</div>
				</div>
				
				<div class="tab-pane" id="search-tab-datos-generales">
					<div class="row">
						<div class="col-md-6">
							<div class="block-section">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_diseno">Dise�o <span
										class="text-danger">*</span>
									</label>
									<div class="input-group">
										<input type="text" id="val_datosgenerales_diseno"
											name="val_datosgenerales_diseno"
											value="${Diseno.getCodigo1()}" class="form-control"
											placeholder="Dise�o..">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="block-section">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_codigo">C�digo<span
										class="text-danger">*</span></label>
									<div class="input-group">
										<input type="text" id="val_datosgenerales_codigo"
											name="val_datosgenerales_codigo" READONLY
											value="${Diseno.getIdDiseno()}" class="form-control"
											placeholder="C�digo..">

									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="block-section">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_distribucion">Distribuci�n<span
										class="text-danger">*</span></label>
									<div class="input-group">${grillaDistribucion}</div>
								</div>
							</div>
						</div>


						<div class="col-md-6">
							<div class="block-section">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_potencia">Potencia<span
										class="text-danger">*</span></label>
									<div class="input-group">
										<input type="text" id="val_datosgenerales_potencia"
											name="val_datosgenerales_potencia"
											value="${Diseno.getPotencia()}" class="form-control"
											placeholder="Potencia..">

									</div>
								</div>
							</div>
						</div>



						<div class="col-md-6">
							<div class="block-section">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_fases">Fases<span
										class="text-danger">*</span></label>
									<div class="input-group">
										<input type="text" id="val_datosgenerales_fases"
											name="val_datosgenerales_fases" value="${Diseno.getFase()}"
											class="form-control" placeholder="Fases..">

									</div>
								</div>

							</div>
						</div>

						<div class="col-md-6">
							<div class="block-section">

								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_disposicion">Disposici�n
										aisladores<span class="text-danger">*</span>
									</label>
									<div class="input-group">${grillaAisladores}</div>
								</div>

							</div>
						</div>


						<div class="col-md-6">
							<div class="block-section">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_refrigerante">Refrigerante<span
										class="text-danger">*</span></label>
									<div class="input-group">${grillaRefrigerante}</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="block-section">

								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_datosgenerales_kv">Nivel de tensi�n<span
										class="text-danger">*</span>
									</label>
									<div class="input-group">
										<input type="text" id="val_datosgenerales_kv"
											name="val_datosgenerales_kv" value="${Diseno.getKv()}"
											class="form-control" placeholder="Nivel de tensi�n..">
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="block-section">${bobinaBBT}</div>
						</div>

						<div class="col-md-6">
							<div class="block-section">${bobinaBAT}</div>
						</div>

						<div class="col-md-6">
							<div class="block-section">${bobinaBTE}</div>
						</div>
					</div>
				</div>


				<div class="tab-pane" id="search-tab-espe">
					<div class="row">
						<div class="col-md-6">
							<form action="#" method="post" enctype="multipart/form-data"
								class="form-horizontal form-bordered">
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_especificaciones_derivacion">Derivacion <span
										class="text-danger">*</span>
									</label>
									<div class="col-md-6">
										<div class="input-group">${Derivaciones}</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_especificaciones_impulso1">Impulso 1<span
										class="text-danger">*</span>
									</label>
									<div class="col-md-6">
										<div class="input-group">
											<input type="text" id="val_especificaciones_impulso1"
												name="val_especificaciones_impulso1"
												value="${Especificacion.getNivelImpulso1()}"
												class="form-control" placeholder="C�digo..">

										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_especificaciones_impulso2">Impulso 2<span
										class="text-danger">*</span>
									</label>
									<div class="col-md-6">
										<div class="input-group">
											<input type="text" id="val_especificaciones_impulso2"
												name="val_especificaciones_impulso2"
												value="${Especificacion.getNivelImpulso2()}"
												class="form-control" placeholder="C�digo..">

										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-4 control-label"
										for="val_especificaciones_impulso3">Impulso 3<span
										class="text-danger">*</span>
									</label>
									<div class="col-md-6">
										<div class="input-group">
											<input type="text" id="val_especificaciones_impulso3"
												name="val_especificaciones_impulso3"
												value="${Especificacion.getNivelImpulso3()}"
												class="form-control" placeholder="C�digo..">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>







				<div class="tab-pane" id="search-tab-bobinas">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">&nbsp;</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">BBT</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">BAT</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">BTE</div>
								</div>
							</div>
						</div>





						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Conductor cobre" class="form-control">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">${matBbt}</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">${matBat}</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">${matBte}</div>
								</div>
							</div>
						</div>





						<div id="result">${dimensionesBobinas}</div>





						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Incremento de papel" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_incrementoPapel_bbt"
											name="val_bobina_incrementoPapel_bbt"
											value="${bbt.getIncrementoPapel()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_incrementoPapel_bat"
											name="val_bobina_incrementoPapel_bat"
											value="${bat.getIncrementoPapel()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_incrementoPapel_bte"
											name="val_bobina_incrementoPapel_bte"
											value="${bte.getIncrementoPapel()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>







						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Largo espira media" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_largoEpiraMedia_bbt"
											name="val_bobina_largoEpiraMedia_bbt"
											value="${bbt.getLargoEspiraMedia()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_largoEpiraMedia_bat"
											name="val_bobina_largoEpiraMedia_bat"
											value="${bat.getLargoEspiraMedia()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_largoEpiraMedia_bte"
											name="val_bobina_largoEpiraMedia_bte"
											value="${bte.getLargoEspiraMedia()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>








						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Vueltas X fase" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_vueltasXfase_bbt"
											name="val_bobina_vueltasXfase_bbt"
											value="${bbt.getVueltasXfase()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_vueltasXfase_bat"
											name="val_bobina_vueltasXfase_bat"
											value="${bat.getVueltasXfase()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_vueltasXfase_bte"
											name="val_bobina_vueltasXfase_bte"
											value="${bte.getVueltasXfase()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>








						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Principios  por capa" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_principiosXcapas_bbt"
											name="val_bobina_principiosXcapas_bbt"
											value="${bbt.getnPrincipiosXcapa()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_principiosXcapas_bat"
											name="val_bobina_principiosXcapas_bat"
											value="${bat.getnPrincipiosXcapa()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_principiosXcapas_bte"
											name="val_bobina_principiosXcapas_bte"
											value="${bte.getnPrincipiosXcapa()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>











						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Conductor paralelo" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_condParalelo_bbt"
											name="val_bobina_condParalelo_bbt"
											value="${bbt.getCondParalelo()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_condParalelo_bat"
											name="val_bobina_condParalelo_bat"
											value="${bat.getCondParalelo()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_condParalelo_bte"
											name="val_bobina_condParalelo_bte"
											value="${bte.getCondParalelo()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>









						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Largo bobina cuellos" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_largoBobinaCuellos_bbt"
											name="val_bobina_largoBobinaCuellos_bbt"
											value="${bbt.getLargoBobinaCuellos()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_largoBobinaCuellos_bat"
											name="val_bobina_largoBobinaCuellos_bat"
											value="${bat.getLargoBobinaCuellos()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_largoBobinaCuellos_bte"
											name="val_bobina_largoBobinaCuellos_bte"
											value="${bte.getLargoBobinaCuellos()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>









						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Espesor f�sico bobinas" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_espesorFisico_bbt"
											name="val_bobina_espesorFisico_bbt"
											value="${bbt.getEspesorFisicoBobina()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_espesorFisico_bat"
											name="val_bobina_espesorFisico_bat"
											value="${bat.getEspesorFisicoBobina()}" class="form-control"
											placeholder="Espesor..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_espesorFisico_bte"
											name="val_bobina_espesorFisico_bte"
											value="${bte.getEspesorFisicoBobina()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>









						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Ductos completos" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCompletos_bbt"
											name="val_bobina_ductosCompletos_bbt"
											value="${bbt.getnDuctosCompletos()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCompletos_bat"
											name="val_bobina_ductosCompletos_bat"
											value="${bat.getnDuctosCompletos()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCompletos_bte"
											name="val_bobina_ductosCompletos_bte"
											value="${bte.getnDuctosCompletos()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>









						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Ductos cabezal" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCabezal_bbt"
											name="val_bobina_ductosCabezal_bbt"
											value="${bbt.getnDuctosCabezal()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCabezal_bat"
											name="val_bobina_ductosCabezal_bat"
											value="${bat.getnDuctosCabezal()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCabezal_bte"
											name="val_bobina_ductosCabezal_bte"
											value="${bte.getnDuctosCabezal()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>










						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Ductos Nubtat" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosNubTat_bbt"
											name="val_bobina_ductosNubTat_bbt"
											value="${bbt.getnDuctosNubTat()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosNubTat_bat"
											name="val_bobina_ductosNubTat_bat"
											value="${bat.getnDuctosNubTat()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosNubTat_bte"
											name="val_bobina_ductosNubTat_bte"
											value="${bte.getnDuctosNubTat()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>









						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Ductos cabezal atbt" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCabezalAtbt_bbt"
											name="val_bobina_ductosCabezalAtbt_bbt"
											value="${bbt.getnDuctosCabezaLatBt()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCabezalAtbt_bat"
											name="val_bobina_ductosCabezalAtbt_bat"
											value="${bat.getnDuctosCabezaLatBt()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_ductosCabezalAtbt_bte"
											name="val_bobina_ductosCabezalAtbt_bte"
											value="${bte.getnDuctosCabezaLatBt()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>









						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled
											value="Conductores axiales" class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_condAxiales_bbt"
											name="val_bobina_condAxiales_bbt"
											value="${bbt.getCondAxiales()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_condAxiales_bat"
											name="val_bobina_condAxiales_bat"
											value="${bat.getCondAxiales()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_condAxiales_bte"
											name="val_bobina_condAxiales_bte"
											value="${bte.getCondAxiales()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>







						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="" name="" disabled value="Capas discos"
											class="form-control">
									</div>
								</div>

							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_capasDiscos_bbt"
											name="val_bobina_capasDiscos_bbt"
											value="${bbt.getnCapasDisco()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_capasDiscos_bat"
											name="val_bobina_capasDiscos_bat"
											value="${bat.getnCapasDisco()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<div class="col-md-6">
									<div class="input-group">
										<input type="text" id="val_bobina_capasDiscos_bte"
											name="val_bobina_capasDiscos_bte"
											value="${bte.getnCapasDisco()}" class="form-control"
											placeholder="Dise�o..">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>



				<div class="tab-pane" id="search-tab-est-tapa">
					<div class="row">
						<fieldset>
							<legend>Dimensiones generales</legend>
							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										<label class="col-md-6 control-label"
											for="val_estanquetapa_largo">Largo estanque<span
											class="text-danger">*</span>
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="val_estanquetapa_largo"
													name="val_estanquetapa_largo"
													value="${EstanqueTapa.getLargoEstanque()}"
													class="form-control" placeholder="C�digo..">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										<label class="col-md-6 control-label"
											for="val_estanquetapa_ancho">Ancho estanque<span
											class="text-danger">*</span>
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="val_estanquetapa_ancho"
													name="val_estanquetapa_ancho"
													value="${EstanqueTapa.getAnchoEstanque()}"
													class="form-control" placeholder="C�digo..">

											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										<label class="col-md-6 control-label"
											for="val_estanquetapa_alto">Alto estanque<span
											class="text-danger">*</span>
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="val_estanquetapa_alto"
													name="val_estanquetapa_alto"
													value="${EstanqueTapa.getAltoEstanque()}"
													class="form-control" placeholder="C�digo..">

											</div>
										</div>
									</div>
								</div>
							</div>




							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										<label class="col-md-6 control-label"
											for="val_estanquetapa_altoBaseFalsa">Alto base falsa<span
											class="text-danger">*</span>
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="val_estanquetapa_altoBaseFalsa"
													name="val_estanquetapa_altoBaseFalsa"
													value="${EstanqueTapa.getAltoBaseFalsa()}"
													class="form-control" placeholder="C�digo..">

											</div>
										</div>
									</div>
								</div>
							</div>




							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										<label class="col-md-6 control-label"
											for="val_estanquetapa_altoColchon">Alto colch�n<span
											class="text-danger">*</span>
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="val_estanquetapa_altoColchon"
													name="val_estanquetapa_altoColchon"
													value="${EstanqueTapa.getAltoBaseFalsa()}"
													class="form-control" placeholder="C�digo..">

											</div>
										</div>
									</div>
								</div>
							</div>



							<div class="col-md-4">
								<div class="block-section">
									<div class="form-group">
										<label class="col-md-6 control-label"
											for="val_estanquetapa_diametroCircular">Di�metro
											circular<span class="text-danger">*</span>
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<input type="text" id="val_estanquetapa_diametroCircular"
													name="val_estanquetapa_diametroCircular"
													value="${EstanqueTapa.getDiametroCircular()}"
													class="form-control" placeholder="C�digo..">

											</div>
										</div>
									</div>
								</div>
							</div>
						</fieldset>


						<fieldset>
							<legend>Materiales y espesor</legend>
							<div id="materialesPinturaPlanchaFierro">
								${materialesPinturaPlanchaFierro}</div>
						</fieldset>

						<fieldset>
							<legend>Pintura</legend>
							<div id="pinturaEstanque">${pinturaEstanque}</div>
						</fieldset>
					</div>
				</div>



				<div class="tab-pane" id="search-tab-presspan">
					<div class="row">
						<div class="col-md-12">
							<fieldset>
								<legend>BBT</legend>
								<div class="form-group">
									<label class="col-md-12 control-label" for=""> Entre
										capas </label>
								</div>
								<div id="entreCapasBbt">${entreCapasBbt}</div>
								<div class="form-group">
									<label class="col-md-12 control-label" for="">
										Aislaci�n Ext. </label>
								</div>
								<div id="aislacionExternaBbt">${aislacionExternaBbt}</div>
								<div class="form-group">
									<label class="col-md-12 control-label" for="">
										Aislaci�n base </label>
								</div>
								<div id="alislacionBaseBbt">${alislacionBaseBbt}</div>
							</fieldset>




							<fieldset>
								<legend>BAT</legend>
								<div class="form-group">
									<label class="col-md-12 control-label" for=""> Entre
										capas </label>
								</div>
								<div id="entreCapasBat">${entreCapasBat}</div>
								<div class="form-group">
									<label class="col-md-12 control-label" for="">
										Aislaci�n Ext. </label>
								</div>
								<div id="aislacionExternaBat">${aislacionExternaBat}</div>
								<div class="form-group">
									<label class="col-md-12 control-label" for="">
										Aislaci�n base </label>
								</div>
								<div id="alislacionBaseBat">${alislacionBaseBat}</div>
							</fieldset>
							<fieldset>
								<legend>BTE</legend>
								<div class="form-group">
									<label class="col-md-12 control-label" for=""> Entre
										capas </label>
								</div>
								<div id="entreCapasBte">${entreCapasBte}</div>
								<div class="form-group">
									<label class="col-md-12 control-label" for="">
										Aislaci�n Ext. </label>
								</div>
								<div id="aislacionExternaBte">${aislacionExternaBte}</div>
								<div class="form-group">
									<label class="col-md-12 control-label" for="">
										Aislaci�n base </label>
								</div>
								<div id="alislacionBaseBte">${alislacionBaseBte}</div>
							</fieldset>
						</div>
					</div>
				</div>




				<div class="tab-pane" id="search-tab-radiador">
					<div class="row">
						<div class="col-md-6">
							<form action="#" method="post"
								class="form-horizontal form-bordered">
								<div class='form-group'>
									<label class='col-md-4 control-label' for=''>Tipo <span
										class='text-danger'>*</span>
									</label>
									<div class='col-md-6'>
										<div class='input-group'>${comboTipoRadiador}</div>
									</div>
								</div>
								<div id="comboTipoRadiador2">${radiador}</div>
							</form>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="search-tab-nuc-ferr">
					<div class="row">
						<form action="#" method="post" name="nucleoFerreteria">
							<div class="col-md-12">
								<fieldset>
									<legend>Acero silicoso</legend>
									<div class='form-group'>
										<label class='col-md-6 control-label' for=''> Silicoso
										</label> <label class='col-md-3 control-label' for=''>
											Cantidad </label> <label class='col-md-3 control-label' for=''>
											Peso n�cleo </label>
									</div>
									<div id="aceroSilicoso1">${aceroSilicoso1}</div>
									<div id="aceroSilicoso2">${aceroSilicoso2}</div>
									<div id="aceroSilicoso3">${aceroSilicoso3}</div>
									<div id="aceroSilicoso4">${aceroSilicoso4}</div>
									<div id="aceroSilicoso5">${aceroSilicoso5}</div>
									<div id="aceroSilicoso6">${aceroSilicoso6}</div>
									<div id="aceroSilicoso7">${aceroSilicoso7}</div>
									<div id="aceroSilicoso8">${aceroSilicoso8}</div>
									<div id="aceroSilicoso9">${aceroSilicoso9}</div>
									<div id="aceroSilicoso10">${aceroSilicoso10}</div>
								</fieldset>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<fieldset>
										<legend>Soporte</legend>
										<div class="col-md-6">${soporte}</div>
									</fieldset>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<fieldset>
										<legend>N�cleo</legend>
										<div class="col-md-9">
											<div id=tipoNucleo>${nucleo}</div>
										</div>
									</fieldset>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="tab-pane" id="search-tab-guardar">
					<div class="row">
						<div class="col-md-8">
							<h3 class="sub-header">
								<strong>Guardar Cambios</strong>
							</h3>
							<div id="faq2" class="panel-group">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<i class="fa fa-angle-right"></i> <a class="accordion-toggle"
												data-toggle="collapse" data-parent="#faq2" href="#faq2_q1">Guardar
												C�mo?</a>
										</h4>
									</div>
									<div id="faq2_q1" class="panel-collapse collapse">
										<div class="panel-body">

											<div class='form-group'>
												<select id="combo0" name="combo0" class="form-control"
													onchange="codigoTrafoCoti()">
													<option value="">Area</option>
													<option value="T">Producci�n</option>
													<option value="C">Cotizaci�n</option>
												</select>
											</div>
											<div class='form-group'>
												<select id="combo2" name="combo2" class="form-control"
													onchange="codigoTrafoCoti()">
													<option value="">Tipo</option>
													<option value="1">Monof�sico, Bif�sico</option>
													<option value="2">ECM</option>
													<option value="3">Trif�sico</option>
													<option value="4">Equipos Especiales</option>
												</select>
											</div>

											<div class='form-group'>
												<select id="combo4" name="combo4" class="form-control"
													onchange="codigoTrafoCoti()">
													<option value="">Refrigerante</option>
													<option value="1">Aceite</option>
													<option value="2">Silicona</option>
													<option value="3">Aceite vegetal FR3</option>
												</select>
											</div>

											<div id="codigoTrafoCoti">
												<div class='form-group'>${familia}</div>
												<div class='form-group'>${tabla_c1}</div>
												<div class='form-group'>${tabla_c2}</div>
											</div>
											<div class='form-group'>
												<button type="button" class="btn btn-sm btn-primary"
													onclick='codigoTrafoCotiGenerar(0)'>
													<i class="fa fa-angle-right"></i> Generar c�digo
												</button>
											</div>
											<div id="codigoTrafoCotiGenerar"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<i class="fa fa-angle-right"></i> <a class="accordion-toggle"
											data-toggle="collapse" data-parent="#faq2" href="#faq2_q2">Actualizar
											dise�o</a>
									</h4>
								</div>
								<div id="faq2_q2" class="panel-collapse collapse">
									<div class="panel-body">
										<button type="button" class="btn btn-sm btn-primary"
											onclick="actualizarDisenoAceite()">
											<i class="fa fa-angle-right"></i> Actualizar
										</button>
									</div>
								</div>
							</div>
							<div id="actualizarDisenoAceite"></div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<i class="fa fa-angle-right"></i> <a class="accordion-toggle"
											data-toggle="collapse" data-parent="#faq2" href="#faq2_q3">Cubicar</a>
									</h4>
								</div>
								<div id="faq2_q3" class="panel-collapse collapse">
									<div class="panel-body">
										<button type="button" class="btn btn-sm btn-primary" onclick="cubicar()">										
										<i class="fa fa-angle-right"></i> Cubicar
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br />
<br />
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$('#materialsearch')
								.autocomplete(
										{
											serviceUrl : '${pageContext.request.contextPath}/ingenieria-getMateriales-rest',
											paramName : "tagName",
											delimiter : ",",
											transformResult : function(response) {

												return {

													suggestions : $
															.map(
																	$
																			.parseJSON(response),
																	function(
																			item) {

																		return {
																			value : item.tagName,
																			data : item.id
																		};
																	})

												};

											}

										});

					});
</script>