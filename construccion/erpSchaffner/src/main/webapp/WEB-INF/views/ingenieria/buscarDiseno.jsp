<script src="resources/js/pages/tablesDatatables.js"></script>
<script type="text/javascript">
	$(function() {
		TablesDatatables.init();
	});

</script>
<div class="row">
	<div class="col-md-9">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>B�squeda</strong>
				</h2>
			</div>
			${theadDiseno}
		</div>
	</div>
</div>
<br />
<div class="row">
	<div class="block full">
		<div class="block-title">
			<h2>
				<strong>Dise�os</strong>
			</h2>
		</div>
		<div class='table-responsive'>
			<table id='example-datatable'
				class='table table-vcenter table-condensed table-bordered'>
				<thead>
					<tr>
						<th class='text-center' data-toggle='tooltip' title='Dise�o'>Dise�o</th>
						<th class='text-center' data-toggle='tooltip' title='C�digo'>C�digo</th>
						<th class='text-center' data-toggle='tooltip'
							title='Clasificaci�n'>Cla</th>
						<th class='text-center' data-toggle='tooltip' title='Potencia'>Pot</th>
						<th class='text-center' data-toggle='tooltip' title='KV'>KV</th>
						<th class='text-center' data-toggle='tooltip' title='Refrigerante'>Ref</th>
						<th class='text-center' data-toggle='tooltip'
							title='Voltaje primario'>Vol. pri</th>
						<th class='text-center' data-toggle='tooltip'
							title='Voltaje secundario'>Vol. sec</th>
						<th class='text-center' data-toggle='tooltip' title='Impedancia'>Imp</th>
						<th class='text-center' data-toggle='tooltip'
							title='Altura de instalaci�n'>Alt. ins</th>
						<th class='text-center' data-toggle='tooltip' title='Conductor BT'>Conductor
							BT</th>
						<th class='text-center' data-toggle='tooltip' title='Conductor AT'>Conductor
							AT</th>
					</tr>
				</thead>
				<tbody>${disenoBusquedaDataTable}
				</tbody>
			</table>
		</div>
	</div>
</div>
<br />
<br />

