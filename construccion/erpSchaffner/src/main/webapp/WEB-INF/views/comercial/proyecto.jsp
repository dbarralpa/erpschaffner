<script src="resources/js/pages/formsValidation.js"></script>
<script src="resources/js/jquery.autocomplete.min.js"></script>
<script>
	$(function() {
		FormsValidation.init();
	});
	function contacto() {
		document.getElementById("val_email1").selectedIndex = document.getElementById("val_contacto1").selectedIndex;
		
	}
</script>
<div class="row">
	<div class="col-md-9">
		<form action="comercial-solicitudCotizacion-cliente" method="post"
			class="form-horizontal form-bordered">
			<fieldset>
				<legend>
					<i class="fa fa-angle-right"></i> Agregar cliente a proyecto <br />
					<br /> <input type="text" id="winputsearch" name="winputsearch"
						size="85" placeholder="buscar cliente.." required> <span>
						<button type="submit" class="btn btn-sm btn-primary">
							<i class="fa fa-angle-right"></i> Buscar
						</button>
					</span>

				</legend>
			</fieldset>
		</form>
	</div>
	<div class="col-md-9">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Ingreso Proyecto</strong>
				</h2>
			</div>
			<form id="form-validation"
				action="comercial-solicitudCotizacion-IngresoProyecto" method="post"
				class="form-horizontal form-bordered">

				<div class="form-group">
					<label class="col-md-4 control-label" for=val_cliente>Cliente
						<span class="text-danger">*</span>
					</label>
					<div class="col-md-6">
						<div class="input-group">
							<input type="text" id="val_cliente" name="val_cliente" readonly
								class="form-control" placeholder="cliente.."
								value="${cliente.getNombre()}"> <span
								class="input-group-addon"><i class="gi gi-user"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_contacto1">Contacto

					</label>
					<div class="col-md-6">
						<div class="input-group">
							${contactos} <span class="input-group-addon"><i
								class="gi gi-asterisk"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_email1">Email

					</label>
					<div class="col-md-6">
						<div class="input-group">
							${contactoEmail} <span class="input-group-addon"><i
								class="gi gi-envelope"></i></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for=val_proyecto>Nombre
						proyecto <span class="text-danger">*</span>
					</label>
					<div class="col-md-6">
						<div class="input-group">
							<input type="text" id="val_proyecto" name="val_proyecto"
								class="form-control" placeholder="nombre proyecto.."> <span
								class="input-group-addon"><i class="gi gi-asterisk"></i></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="val_tipoCotizacion">Tipo
						cotizaci&oacute;n <span class="text-danger">*</span>
					</label>
					<div class="col-md-6">${tipoCotizacion}</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for=val_fechaCliente>Fecha
						cliente <span class="text-danger">*</span>
					</label>
					<div class="col-md-6">
						<div class="input-group">
							<input type="text" id="val_fechaCliente" name="val_fechaCliente"
								class="form-control input-datepicker-close"
								data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" readonly>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_tipoMoneda">Tipo
						moneda <span class="text-danger">*</span>
					</label>
					<div class="col-md-6">${tipoMoneda}</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_formaPago">Forma
						de pago<span class="text-danger">*</span>
					</label>
					<div class="col-md-6">${formaPago}</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_vendedor">
						Vendedor <span class="text-danger">*</span>
					</label>
					<div class="col-md-6">${vendedores}</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <a
						href="#modal-cargar-archivos" data-toggle="modal">Cargar
							Archivos</a>
					</label>
				</div>
				<div class="form-group form-actions">
					<div class="col-md-8 col-md-offset-4">
						<button type="submit" class="btn btn-sm btn-primary">
							<i class="fa fa-arrow-right"></i> Guardar
						</button>
					</div>
				</div>
				<input type="hidden" id="val_vendedor" name="val_vendedor">
			</form>
		</div>
	</div>



	<div id="modal-cargar-archivos" class="modal fade" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="block full">
						<!-- Dropzone Title -->
						<div class="block-title">
							<div class="block-options pull-right">
								<a href="javascript:void(0)"
									class="btn btn-alt btn-sm btn-default" data-toggle="tooltip"
									title="Cargar archivos"><i class="gi gi-cogwheel"></i></a>
							</div>
							<h2>
								<strong>Cargar archivos</strong>
							</h2>
						</div>
						<!--form id="dropzone-form" class="dropzone"
							enctype="multipart/form-data">

							<div
								class="dz-default dz-message file-dropzone text-center well col-sm-12">

								<span class="glyphicon glyphicon-paperclip"></span> <span>
									Arrastre archivos a esta zona</span><br> <span>o</span><br>
								<span>Haga clic</span>
							</div>

							<div class="dropzone-previews"></div>
						</form>
						<hr>
						<button id="upload-button" class="btn btn-primary">
							<span class="glyphicon glyphicon-upload"></span> Upload
						</button-->
						<!--div id="dropzone"><form action="upload" class="dropzone" id="demo-upload"></form></div-->
						<div id="dropzone">
							<form id="upload" method="post" action="comercial-upload"
								enctype="multipart/form-data">
								<div id="drop">
									<a>Busque archivos</a> <input type="file" name="upl" multiple />
								</div>

								<ul>

								</ul>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document)
				.ready(
						function() {

							$('#winputsearch')
									.autocomplete(
											{
												serviceUrl : '${pageContext.request.contextPath}/comercial-getClientes',
												paramName : "tagName",
												delimiter : ",",
												transformResult : function(
														response) {

													return {

														suggestions : $
																.map(
																		$
																				.parseJSON(response),
																		function(
																				item) {

																			return {
																				value : item.tagName,
																				data : item.id
																			};
																		})

													};

												}

											});

						});
	</script>
</div>


