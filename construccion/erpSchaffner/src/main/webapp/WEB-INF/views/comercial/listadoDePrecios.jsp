<script src="resources/js/pages/formsWizard.js"></script>
<script>
	$(function() {
		FormsWizard.init();
	});
</script>
${popup}
<div class="row">
	<div class="block">
		<div class="block-title">
			<h2>
				<strong>Transformadores</strong>
			</h2>
		</div>
		<div class="form-group">
			<a href="#modal-trafos" data-toggle="modal">Agregar transformador</a> &nbsp;proyecto: &nbsp;${proyectoVenta.getNombreProyecto()}
		</div>
		<div class="table-responsive">
			<table id="example-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center" data-toggle="tooltip"
							title="Actualizar solicitud de trafo">Id</th>
						<th class="text-center" data-toggle="tooltip"
							title="Familia del trafo">Tipo</th>
						<th class="text-center" data-toggle="tooltip" title="Norma">Nor</th>
						<th class="text-center" data-toggle="tooltip"
							title="Temperaturas perdidas">Tem Per</th>
						<th class="text-center" data-toggle="tooltip" title="Ventilación">Ven</th>
						<th class="text-center" data-toggle="tooltip" title="Temperatura">Tem</th>
						<th class="text-center" data-toggle="tooltip" title="Potencia">Pot</th>
						<th class="text-center" data-toggle="tooltip"
							title="Voltaje primario">Vol P</th>
						<th class="text-center" data-toggle="tooltip"
							title="Voltaje secundario">Vol S</th>
						<th class="text-center" data-toggle="tooltip" title="Impedancia">Imp</th>
						<th class="text-center" data-toggle="tooltip" title="Frecuencia">Fec</th>
						<th class="text-center" data-toggle="tooltip" title="Tag">Tag</th>
						<th class="text-center" data-toggle="tooltip" title="Cantidad">Cant</th>
						<th class="text-center" data-toggle="tooltip"
							title="Observaciones">Observaciones</th>
					</tr>
				</thead>
				${transformadores}
			</table>
		</div>
	</div>
</div>

<div id="modal-trafos" class="modal fade" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="block full">
					<div class="block-title">
						<form id="form-validation"
							action="comercial-solicitudCotizacion-IngresoProyectoTrafos"
							method="post" class="form-horizontal form-bordered">
							<h2>
								<strong>Transformador</strong>
							</h2>
							<div class="form-group">
								<label class="col-md-3 control-label" for="tipo">Tipo</label>
								<div class="col-md-9">${familias}</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Normas</label>
								<div class="col-md-9">${normas}</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Tem. perdidas</label>
								<div class="col-md-9">${temPerdidas}</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label">Ventilación</label>
								<div class="col-md-9">${ventilacion}</div>
							</div>


							<div class="form-group">
								<label class="col-md-3 control-label">Temperatura</label>
								<div class="col-md-9">${temperaturas}</div>
							</div>


							<div class="form-group">
								<label class="col-md-3 control-label" for="val_potencia">Potencia
									(KVA) <span class="text-danger">*</span>
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="val_potencia" name="val_potencia"
											class="form-control" placeholder="potencia..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="val_voltaje_primario">Voltaje
									Primario (V) (KVA) <span class="text-danger">*</span>
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="val_voltaje_primario"
											name="val_voltaje_primario" class="form-control"
											placeholder="voltaje..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label"
									for="val_voltaje_secundario">Voltaje secundario (V)
									(KVA) <span class="text-danger">*</span>
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="val_voltaje_secundario"
											name="val_voltaje_secundario" class="form-control"
											placeholder="voltaje..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="val_impedancia">Impedancia
									(%) (KVA) <span class="text-danger">*</span>
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="val_impedancia" name="val_impedancia"
											class="form-control" placeholder="impedancia..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="val_frecuencia">Frecuencia
									(Hz) (KVA) <span class="text-danger">*</span>
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="val_frecuencia" name="val_frecuencia"
											class="form-control" placeholder="frecuencia..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="tag">Tag </label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="tag" name="tag" class="form-control"
											placeholder="tag..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="val_cantidad">Cantidad
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" id="val_cantidad" name="val_cantidad"
											class="form-control" placeholder="cantidad..">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="observacion">Observaciones
								</label>
								<div class="col-md-9">
									<div class="input-group">
										<textarea id="observacion" name="observacion" rows="5"
											class="form-control" placeholder="observación.."></textarea>
									</div>
								</div>
							</div>
							<div class="form-group form-actions">
								<div class="col-md-8 col-md-offset-4">
									<button type="submit" class="btn btn-sm btn-primary">
										<i class="fa fa-arrow-right"></i> Guardar trafo
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
${modalVentasDetTrafos}
