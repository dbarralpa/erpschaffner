<script src="resources/js/jquery.autocomplete.min.js"></script>
<form action="comercial-solicitudCotizacion-clienteTransformadores"
	method="post" class="form-horizontal form-bordered">
	<fieldset>
		<legend>
			<i class="fa fa-angle-right"></i> Agregar cliente a listado de transformadores. 
			<br/></br>
			<input type="text" id="winputsearch"
				name="winputsearch" size="90" placeholder="buscar cliente.."
				required> <span>
				<button type="submit" class="btn btn-sm btn-primary">
					<i class="fa fa-angle-right"></i> Buscar
				</button>
			</span>

		</legend>
	</fieldset>
</form>

<script>
	$(document).ready(function() {

		$('#winputsearch').autocomplete({
			serviceUrl : '${pageContext.request.contextPath}/comercial-getClientes',
			paramName : "tagName",
			delimiter : ",",
			transformResult : function(response) {

				return {

					suggestions : $.map($.parseJSON(response), function(item) {

						return {
							value : item.tagName,
							data : item.id
						};
					})

				};

			}

		});

	});
</script>