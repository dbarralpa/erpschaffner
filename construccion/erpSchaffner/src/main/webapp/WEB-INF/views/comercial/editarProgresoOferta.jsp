<script src="resources/js/pages/formsValidation.js"></script>
<script src="resources/js/jquery.autocomplete.min.js"></script>
<script>
	$(function() {
		FormsValidation.init();
	});
</script>
<div class="row">
	<div class="col-md-9">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Avance de la oferta</strong>
				</h2>
			</div>
			<form id="form-validation"
				action="comercial-vendedores-editarProgresoOferta-update"
				method="post" class="form-horizontal form-bordered">

				<div class="form-group">
					<label class="col-md-4 control-label" for=val_cliente>Nombre
						proyecto </label>
					<div class="col-md-6">
						<div class="input-group">
							<input type="text" id="val_cliente" name="val_cliente" disabled
								class="form-control"
								value="${proyectoVenta.getNombreProyecto()}"> <span
								class="input-group-addon"><i class="gi gi-user"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_progreso">Progreso</label>
					<div class="col-md-6">
						<select id="val_progreso" name="val_progreso" class="form-control"
							size="1">
							<option value="">Elija progreso</option>
							<option value="25">25%</option>
							<option value="50">50%</option>
							<option value="75">75%</option>
							<option value="100">100%</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="val_progreso_obser">Observación</label>
					<div class="col-md-6">
						<textarea id="val_progreso_obser" name="val_progreso_obser"
							rows="4" class="form-control" placeholder="observación.."></textarea>
					</div>
				</div>
				<div class="form-group form-actions">
					<div class="col-md-8 col-md-offset-4">
						<button type="submit" class="btn btn-sm btn-primary">
							<i class="fa fa-arrow-right"></i> Guardar
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<br />
<div class="row">
	<div class="block full">
		<div class="block-title">
			<h2>
				<strong>Detalle avance</strong>
			</h2>
		</div>
		<div class="table-responsive">
			<table id="example-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center" data-toggle="tooltip"
							title="Porcentaje de avande de la oferta">% Avance</th>
						<th class="text-center" data-toggle="tooltip"
							title="Usuario que hizo la modificación">Usuario</th>
						<th class="text-center" data-toggle="tooltip"
							title="Fecha de ingreso del avance">Fecha</th>
						<th class="text-center" data-toggle="tooltip" title="">Observación</th>

					</tr>
				</thead>
				${detalleProgresoOferta}
			</table>
		</div>
	</div>
</div>
<br/>
<br/>

