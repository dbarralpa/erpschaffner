<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="resources/js/pages/tablesDatatables.js"></script>
<script src="resources/js/pages/formsValidation.js"></script>
<script>
	$(function() {
		TablesDatatables.init();
	});
	$(function() {
		FormsValidation.init();
	});
</script>
<div class="block full">
	<div class="block-title">
		<h2>
			<strong>Ofertas</strong>
		</h2>
	</div>
	<div class="table-responsive">
		<table id="example-datatable"
			class="table table-vcenter table-condensed table-bordered">
			<thead>
				<tr>
					<th class="text-center" data-toggle="tooltip" title="Editar oferta">Edi</th>
					<th class="text-center" data-toggle="tooltip"
						title="Estado de la cotizaci�n">Est</th>
					<th class="text-center" data-toggle="tooltip"
						title="N� de la oferta">Ofer</th>
					<th class="text-center" data-toggle="tooltip"
						title="Tipo de la oferta">Tipo</th>
					<th class="text-center" data-toggle="tooltip"
						title="Rut del cliente">Cliente</th>
					<th class="text-center" data-toggle="tooltip"
						title="Nombre del proyecto">Pro</th>
					<th class="text-center" data-toggle="tooltip"
						title="Vendedor asociado a la oferta">Ven</th>
					<th class="text-center" data-toggle="tooltip"
						title="Fecha que se envi� a ingenier�a">ing</th>
					<th class="text-center" data-toggle="tooltip"
						title="Fecha que debe entregar ingenier�a">ing 1</th>
					<th class="text-center" data-toggle="tooltip"
						title="Fecha entrega a cliente">Fec cli</th>
					<th class="text-center" data-toggle="tooltip"
						title="Fecha en que ingenier�a entregar� la propuesta">ing 2</th>
					<th class="text-center" data-toggle="tooltip"
						title="Fecha en que ingenier�a entreg� la propuesta">ing 3</th>
					<th class="text-center" data-toggle="tooltip"
						title="atraso entre la fecha de compromiso y la entrega real de ingenier�a">atra</th>
					<th class="text-center" data-toggle="tooltip"
						title="atraso entre la fecha de compromiso y el d�a que se envi� a ingenier�a">atra	1</th>
				</tr>
			</thead>
			${proyectosVentasDatatables}
		</table>
	</div>
</div>
${modalVentas}