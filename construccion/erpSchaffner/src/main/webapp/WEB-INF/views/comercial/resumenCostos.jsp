<script src="resources/js/pages/formsGeneral.js"></script>
<script src="resources/js/pages/formsValidation.js"></script>
<script src="resources/js/pages/tablesDatatables.js"></script>
<script>
	$(function() {
		FormsGeneral.init();
	});

	$(function() {
		FormsValidation.init();
	});

	$(function() {
		TablesDatatables.init();
	});
</script>

<div class="row">
	<div class="col-md-6">
		<div class="block">
			<div class="block-title">
				<div class="block-options pull-right">
					<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default"
						data-toggle="tooltip" title="Settings"><i
						class="gi gi-cogwheel"></i></a>
				</div>
				<h2>
					<strong>Resumen de costos para cotizaci�n</strong>
				</h2>
			</div>
			<form action="#" method="post"
				class="form-horizontal .toggle-bordered" onsubmit="return false;">
				${cuenta}</form>
		</div>
	</div>



	<div class="col-md-6">
		<div class="block">
			<div class="block-title">
				<div class="block-options pull-right">
					<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default"
						data-toggle="tooltip" title="Settings"><i
						class="gi gi-cogwheel"></i></a>
				</div>
				<h2>
					<strong>Tabla de m�rgenes</strong>
				</h2>
			</div>
			<div class="table-responsive">
				<table id="datatable"
					class="table table-vcenter table-condensed table-bordered">
					<thead>
						<tr>
							<!--th class="text-center" data-toggle="tooltip" title="Descripci�n">Descrip</th-->
							<th class="text-center" data-toggle="tooltip"
								title="Id del margen">Id</th>
							<th class="text-center" data-toggle="tooltip" title="Margen">%mg</th>
							<th class="text-center" data-toggle="tooltip"
								title="Cantidad d�lares">CT US</th>
						</tr>
					</thead>
					<tbody>${margenes}
					</tbody>
				</table>
			</div>

		</div>
	</div>
</div>
<div class="row">
	<div class="block full">
		<div class="widget">
			<div class="widget-header text-center themed-background-dark">
				<h4 class="widget-content-light">
					<a href="#modal-cargar-archivos" data-toggle="modal">Archivos adjuntos</a><br>
				</h4>
			</div>
			<div class="widget-main">
				<div class="list-group remove-margin">${archivoss}</div>
			</div>
		</div>
	</div>
</div>
<br />
<div class="row">
	<div class="block full">
		<!-- Inline Form Title -->
		<div class="block-title">
			<h2>
				<strong>Detalle de costos</strong> &nbsp;&nbsp;&nbsp;&nbsp; <a
					href="comercial-editarCotizacion-resumenCostos-caratula"
					target="_blank" data-toggle="tooltip"
					title="Generar car�tula del vendedor"
					class="btn btn-xs btn-default">Generar car�tula</a>
			</h2>
		</div>
		<form id="form-validation"
			action="comercial-editarCotizacion-resumenCostos-agregar"
			method="post" class="form-inline">
			<div class="form-group">
				<input type="text" id="text_tag" name="text_tag"
					class="form-control" placeholder="Tag..">
			</div>
			<div class="form-group">
				<textarea id="textarea_descripcion" name="textarea_descripcion"
					rows="1" class="form-control" placeholder="Descripci�n.."></textarea>
			</div>
			<div class="form-group">
				<input type="text" id="text_cubica" name="text_cubica"
					class="form-control" placeholder="C�digo cubica..">
			</div>
			<div class="form-group">

				<input type="text" id="text_diseno" name="text_diseno"
					class="form-control" placeholder="Dise�o..">

			</div>
			<div class="form-group">${selectMargenes}</div>
			<div class="form-group">
				<div class="input-group">
					<input type="text" id="text_cant" name="text_cant"
						class="form-control" placeholder="Cantidad..">
				</div>

			</div>
			<div class="form-group">
				<div class="input-group">
					<input type="text" id="text_cu_usd" name="text_cu_usd"
						class="form-control" placeholder="Costo unitario d�lares..">
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">
					<i class="fa fa-user"></i> Agregar
				</button>
			</div>
		</form>
	</div>
</div>
<br />
<div class="row">
	<div class="block full">
		<div class="table-responsive">
			<table id="example-datatable"
				class="table table-vcenter table-condensed table-bordered">
				<thead>
					<tr>
						<th class="text-center" data-toggle="tooltip" title="Editar item">Edit</th>
						<th class="text-center" data-toggle="tooltip"
							title="Mostrar trafo">Trafo</th>
						<th class="text-center" data-toggle="tooltip" title="Tag">Tag</th>
						<th class="text-center" data-toggle="tooltip" title="Descripci�n">Descri</th>
						<th class="text-center">Dise�o</th>
						<th class="text-center" data-toggle="tooltip"
							title="C�digo cubica">Cub</th>
						<th class="text-center" data-toggle="tooltip" title="Id margen">ID</th>
						<th class="text-center" data-toggle="tooltip"
							title="Cantidad en unidades">Cant</th>
						<th class="text-center" data-toggle="tooltip"
							title="Costo unitario en d�lares">CU US$</th>
						<th class="text-center" data-toggle="tooltip"
							title="Costo total en d�lares">CT US$</th>
						<th class="text-center" data-toggle="tooltip"
							title="Precio unitario en d�lares">PU US$</th>
						<th class="text-center" data-toggle="tooltip"
							title="Precio total en d�lares">PT US$</th>
						<th class="text-center" data-toggle="tooltip"
							title="Precio unitario en pesos chilenos">PU Ch$</th>
						<th class="text-center" data-toggle="tooltip"
							title="Precio total en pesos chilenos">PT Ch$</th>
						<th class="text-center" data-toggle="tooltip"
							title="Precio unitario en UF">PU UF</th>
						<th class="text-center" data-toggle="tooltip"
							title="Precio total en UF">PT UF</th>
					</tr>
				</thead>
				${listaProyectosVentasDetHtml}
			</table>
		</div>
		<br />
	</div>
</div>
<div class="row">
	<div class="col-md-9">
		<div class="block">
			<div class="block-title">
				<h2>
					<strong>Datos generales</strong>
				</h2>
			</div>
			<form id="form-validation"
				action="comercial-editarCotizacion-resumenCostos-datosGenerales"
				method="post" class="form-horizontal">
				${datosGenerales}
				<div class="form-group">
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-user"></i> Actualizar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<br />
${modalVentas} ${modalVentasTrafos}
<div id="modal-cargar-archivos" class="modal fade" tabindex="-1"
	role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="block full">
					<!-- Dropzone Title -->
					<div class="block-title">
						<div class="block-options pull-right">
							<a href="javascript:void(0)"
								class="btn btn-alt btn-sm btn-default" data-toggle="tooltip"
								title="Cargar archivos"><i class="gi gi-cogwheel"></i></a>
						</div>
						<h2>
							<strong>Cargar archivos</strong>&nbsp;&nbsp;<a href="comercial-editarCotizacion-resumenCostos-actual">Actualizar</a>
						</h2>
					</div>
					<div id="dropzone">
						<form id="upload" method="post" action="comercial-upload-guardar"
							enctype="multipart/form-data">
							<div id="drop">
								<a>Busque archivos</a> <input type="file" name="upl" multiple />
							</div>

							<ul>

							</ul>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
