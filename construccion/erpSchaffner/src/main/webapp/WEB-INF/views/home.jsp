<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Erp Schaffner</title>

<meta name="description"
	content="ProUI is a Responsive Admin Dashboard Template created by pixelcave and published on Themeforest.">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<meta name="viewport"
	content="width=device-width,initial-scale=1,maximum-scale=1.0">
<link rel="shortcut icon" href="resources/img/favicon.ico">
<link rel="apple-touch-icon" href="resources/img/icon57.png"
	sizes="57x57">
<link rel="apple-touch-icon" href="resources/img/icon72.png"
	sizes="72x72">
<link rel="apple-touch-icon" href="resources/img/icon76.png"
	sizes="76x76">
<link rel="apple-touch-icon" href="resources/img/icon114.png"
	sizes="114x114">
<link rel="apple-touch-icon" href="resources/img/icon120.png"
	sizes="120x120">
<link rel="apple-touch-icon" href="resources/img/icon144.png"
	sizes="144x144">
<link rel="apple-touch-icon" href="resources/img/icon152.png"
	sizes="152x152">

<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/plugins.css">
<link rel="stylesheet" href="resources/css/main.css">
<link rel="stylesheet" href="resources/css/themes.css">
<link rel="stylesheet" href="resources/css/assets/style.css" />
<script src="resources/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
<script src="resources/js/vendor/jquery-1.11.0.min.js"></script>
<script>
	!window.jQuery
			&& document
					.write(unescape('%3Cscript src="resources/js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));
</script>
<script src="resources/js/vendor/bootstrap.min.js"></script>
<script src="resources/js/vendor/assets/jquery.knob.js"></script>
<script src="resources/js/vendor/assets/jquery.ui.widget.js"></script>
<script src="resources/js/vendor/assets/jquery.iframe-transport.js"></script>
<script src="resources/js/vendor/assets/jquery.fileupload.js"></script>
<script src="resources/js/vendor/assets/script.js"></script>
</head>
<body>
	<div id="page-container"
		class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
		<div id="sidebar-alt">
			<div class="sidebar-scroll"></div>
		</div>
		<div id="sidebar">
			<!-- Wrapper for scrolling functionality -->
			<div class="sidebar-scroll">
				<!-- Sidebar Content -->
				<div class="sidebar-content">
					<!-- Brand -->
					<a href="#" class="sidebar-brand"> <i
						class="gi gi-flash"></i>Schaffner
					</a>

					<div class="sidebar-section sidebar-user clearfix">
						<div class="sidebar-user-avatar">
							<a href="#"> <img
								src="resources/img/placeholders/avatars/avatar2.jpg"
								alt="avatar">
							</a>
						</div>
						<div class="sidebar-user-name">${usuarioLogeado.getUsuario()}</div>
						<div class="sidebar-user-links">
							<a href="#" data-toggle="tooltip" data-placement="bottom"
								title="Profile"><i class="gi gi-user"></i></a> <a href="#"
								data-toggle="tooltip" data-placement="bottom" title="Messages"><i
								class="gi gi-envelope"></i></a> <a href="#modal-user-settings"
								data-toggle="modal" class="enable-tooltip"
								data-placement="bottom" title="Settings"><i
								class="gi gi-cogwheel"></i></a> <a href="#" data-toggle="tooltip"
								data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
						</div>
					</div>
					<ul class="sidebar-section sidebar-themes clearfix">
						<li class="active"><a href="javascript:void(0)"
							class="themed-background-dark-default themed-border-default"
							data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
						</li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-night themed-border-night"
							data-theme="resources/css/themes/night.css" data-toggle="tooltip"
							title="Night"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-amethyst themed-border-amethyst"
							data-theme="resources/css/themes/amethyst.css"
							data-toggle="tooltip" title="Amethyst"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-modern themed-border-modern"
							data-theme="resources/css/themes/modern.css"
							data-toggle="tooltip" title="Modern"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-autumn themed-border-autumn"
							data-theme="resources/css/themes/autumn.css"
							data-toggle="tooltip" title="Autumn"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-flatie themed-border-flatie"
							data-theme="resources/css/themes/flatie.css"
							data-toggle="tooltip" title="Flatie"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-spring themed-border-spring"
							data-theme="resources/css/themes/spring.css"
							data-toggle="tooltip" title="Spring"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-fancy themed-border-fancy"
							data-theme="resources/css/themes/fancy.css" data-toggle="tooltip"
							title="Fancy"></a></li>
						<li><a href="javascript:void(0)"
							class="themed-background-dark-fire themed-border-fire"
							data-theme="resources/css/themes/fire.css" data-toggle="tooltip"
							title="Fire"></a></li>
					</ul>
					<ul class="sidebar-nav">
						<li><a href="#" class=" active"><i
								class="gi gi-stopwatch sidebar-nav-icon"></i>Dashboard</a></li>
						<!--li class="sidebar-header">
                                <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                                <span class="sidebar-header-title">Comercial</span>
                            </li-->
                            
                            <!-- COMERCIAL -->
						<li><a href="#" class="sidebar-nav-menu"><i
								class="fa fa-angle-left sidebar-nav-indicator"></i><i
								class="gi gi-certificate sidebar-nav-icon"></i>Comercial</a>
							<ul>
								<li><a href="#" class="sidebar-nav-submenu"><i
										class="fa fa-angle-left sidebar-nav-indicator"></i>Ventas</a>
									<ul>
										<li><a href="#" class="sidebar-nav-submenu"><i
												class="fa fa-angle-left sidebar-nav-indicator"></i>Solicitud
												de cotización</a>
											<ul>
												<li><a href="comercial-solicitudCotizacion-proyecto">Proyecto</a></li>
												<li><a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Transformadores</a>
												<ul>
												<li><a href="comercial-solicitudCotizacion-transformadores">Nueva</a></li>
												<li><a href="comercial-editarCotizacion-trafo">Editar</a></li>
												</ul>
												</li>
											</ul></li>

										<li><a href="comercial-editarCotizacion">Editar
												cotización</a></li>
										<li><a href="comercial-vendedores-progresoOferta">Vendedores</a></li>
									</ul></li>
							</ul>
						</li>
						
						<!-- INGENIERIA -->
						<li><a href="#" class="sidebar-nav-menu"><i
								class="fa fa-angle-left sidebar-nav-indicator"></i><i
								class="gi gi-certificate sidebar-nav-icon"></i>Ingeniería</a>
							<ul>
								<li><a href="ingenieria-administrar">Administración</a></li>
								<li><a href="ingenieria-busqueda-doc-aplicable">Documentos aplicables</a></li>
								<li><a href="ingenieria-documentos-aplicables-planos">Planos</a></li>
								<li><a href="ingenieria-busqueda">Cubicación</a></li>
							</ul>
						</li>
						<!-- PRODUCCION -->
						<li><a href="#" class="sidebar-nav-menu"><i
								class="fa fa-angle-left sidebar-nav-indicator"></i><i
								class="gi gi-certificate sidebar-nav-icon"></i>Producción</a>
							<ul>
								<li><a href="produccion-administrar" >Administración</a></li>
								<li><a href="produccion-busqueda-doc-aplicable" >Documentos aplicables</a></li>
							</ul>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
		<div id="main-container">
			<header class="navbar navbar-default">
				<ul class="nav navbar-nav-custom">
					<li><a href="javascript:void(0)"
						onclick="App.sidebar('toggle-sidebar');"> <i
							class="fa fa-bars fa-fw"></i>
					</a></li>
				</ul>
				<form action="#" method="post" class="navbar-form-custom"
					role="search">
					<div class="form-group">
						<input type="text" id="top-search" name="top-search"
							class="form-control" placeholder="Search..">
					</div>
				</form>
				<ul class="nav navbar-nav-custom pull-right">
					<li class="dropdown"><a href="javascript:void(0)"
						class="dropdown-toggle" data-toggle="dropdown"> <img
							src="resources/img/placeholders/avatars/avatar2.jpg" alt="avatar">
							<i class="fa fa-angle-down"></i>
					</a>
						<ul class="dropdown-menu dropdown-custom dropdown-menu-right">
							<li class="dropdown-header text-center">Configuración</li>

							<li class="divider"></li>
							<li><a href="#"><i class="fa fa-lock fa-fw pull-right"></i>
									Lock Account</a> <a href="j_spring_security_logout"><i
									class="fa fa-ban fa-fw pull-right"> </i> Logout</a></li>
						</ul></li>
				</ul>
			</header>
			<div id="page-content">
				<div class="content-header content-header-media">
					<div class="header-section">
						<div class="row">
							<div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
								<h1>
									Bienvenido <br> <small>${usuarioLogeado.getNombre()}</small>
								</h1>
							</div>
							<div class="col-md-8 col-lg-6"></div>
						</div>
					</div>
					<!--img src="resources/img/placeholders/headers/dashboard_header.jpg"
						alt="header image" class="animation-pulseSlow"-->
				</div>







				<!-- AQUÍ VA EL CÓDIGO -->
				<jsp:include page="${url}">
					<jsp:param name="" value="" />
				</jsp:include>
				<!-- FIN CÓDIGO -->




			</div>


			<footer class="clearfix">
				<div class="pull-right">
					Schaffner S.A. 
				</div>
				<!--div class="pull-left">
					<span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC"
						target="_blank">ProUI 1.2</a>
				</div-->
			</footer>
		</div>
	</div>
	<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
	<script src="resources/js/plugins.js"></script>
	<script src="resources/js/app.js"></script>
	<script src="resources/js/ckeditor/ckeditor.js"></script>
</body>
</html>