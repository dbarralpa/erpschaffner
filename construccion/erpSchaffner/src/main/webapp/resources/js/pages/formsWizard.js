/*
 *  Document   : formsWizard.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Wizard page
 */

var FormsWizard = function() {

    return {
        init: function() {
            /*
             *  Jquery Wizard, Check out more examples and documentation at http://www.thecodemine.org
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Basic Wizard */
            $('#basic-wizard').formwizard({focusFirstInput: true, disableUIStyles: true, inDuration: 0, outDuration: 0});

            /* Initialize Advanced Wizard with Validation */
            $('#advanced-wizard').formwizard({
                disableUIStyles: true,
                validationEnabled: true,
                validationOptions: {
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'span',
                    errorPlacement: function(error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function(e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function(e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                    rules: {
                        val_username: {
                            required: true,
                            minlength: 2
                        },
                        val_cliente: {
                            required: true
                        },
                        val_tipoCotizacion: {
                            required: true
                        },
                        val_fechaCliente: {
                            required: true
                        },
                        val_entrega: {
                            required: true
                        },
                        val_validezOferta: {
                            required: true
                        },
                        val_formaPago: {
                            required: true
                        },
                        val_vendedor: {
                            required: true
                        },
                        val_potencia: {
                            required: true,
                            number: true
                        },
                        val_tipoMoneda: {
                            required: true
                        },
                        val_proyecto: {
                            required: true
                        },
                        val_password: {
                            required: true,
                            minlength: 5
                        },
                        val_confirm_password: {
                            required: true,
                            equalTo: '#val_password'
                        },
                        val_email: {
                            required: true,
                            email: true
                        },
                        val_terms: {
                            required: true
                        },
                        val_voltaje_primario: {
                            required: true,
                            number: true
                        },
                        val_voltaje_secundario: {
                            required: true,
                            number: true
                        },
                        val_impedancia: {
                            required: true,
                            number: true
                        },
                        val_frecuencia: {
                            required: true,
                            number: true
                        },
                        tipo: {
                            required: true
                        },
                        norma: {
                            required: true
                        },
                        temPerdidas: {
                            required: true
                        },
                        ventilacion: {
                            required: true
                        },
                        temperatura: {
                            required: true
                        },
                        val_cantidad: {
                            required: true,
                            number: true
                        },
                        val_contacto1: {
                            required: true
                        }
                    },
                    messages: {
                        val_username: {
                            required: 'Please enter a username',
                            minlength: 'Your username must consist of at least 2 characters'
                        },
                        val_cliente: {
                            required: 'por favor entre un cliente valido'
                        },
                        val_proyecto: {
                            required: 'Por favor ingrese un nombre de proyecto'
                        },
                        val_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long'
                        },
                        val_confirm_password: {
                            required: 'Please provide a password',
                            minlength: 'Your password must be at least 5 characters long',
                            equalTo: 'Please enter the same password as above'
                        },
                        val_email: 'Por favor entre un email valido',
                        val_formaPago: 'Seleccione una forma de pago',
                        val_tipoMoneda: 'Seleccione tipo de moneda',
                        val_fechaCliente: 'Seleccione fecha cliente',
                        val_tipoCotizacion: 'Seleccione tipo cotizacion',
                        val_entrega: 'Debe poner una fecha de entrega',
                        val_validezOferta: 'Debe poner validez de la oferta',
                        val_vendedor: 'Debe ingresar vendedor',
                        val_potencia: 'Entre un numero',
                        val_voltaje_primario: 'Entre un numero',
                        val_voltaje_secundario: 'Entre un numero',
                        val_impedancia: 'Entre un numero',
                        val_frecuencia: 'Entre un numero',
                        tipo: 'Ingrese tipo',
                        norma: 'Ingrese norma',
                        temPerdidas: 'Ingrese temperaturas perdidas',
                        ventilacion: 'Ingrese ventilacion',
                        temperatura: 'Ingrese temperatura',
                        val_cantidad: 'Entre un numero',
                        val_contacto1: 'Debe ingresar un contacto',
                        val_terms: 'Please accept the terms to continue'
                    }
                },
                inDuration: 0,
                outDuration: 0
            });
        }
    };
}();