package com.schaffner.modelo;

public class ProyectoComercialVentaDet {
	private int id;
	private int idProyecto;
	private String tag;
	private String descripcion;
	private String codigoCubica;
	private String diseno;
	private String idMargen;
	private int cantidad;
	private int costoUnitario;
	private long costoTotal;
	private int precioUniDolar;
	private long precioTotalDolar;
	private int precioUniPeso;
	private long precioTotalPeso;
	private int precioUniUf;
	private long precioTotalUf;
	private int idProyectoEnc;
	private long costoTotalMoneda;

	public ProyectoComercialVentaDet() {

	}

	public ProyectoComercialVentaDet(String tag, String descripcion,
			int cantidad, long costoTotalMoneda) {
		this.tag = tag;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
		this.costoTotalMoneda = costoTotalMoneda;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigoCubica() {
		return codigoCubica;
	}

	public void setCodigoCubica(String codigoCubica) {
		this.codigoCubica = codigoCubica;
	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public String getIdMargen() {
		return idMargen;
	}

	public void setIdMargen(String idMargen) {
		this.idMargen = idMargen;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(int costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public long getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(long costoTotal) {
		this.costoTotal = costoTotal;
	}

	public int getPrecioUniDolar() {
		return precioUniDolar;
	}

	public void setPrecioUniDolar(int precioUniDolar) {
		this.precioUniDolar = precioUniDolar;
	}

	public long getPrecioTotalDolar() {
		return precioTotalDolar;
	}

	public void setPrecioTotalDolar(long precioTotalDolar) {
		this.precioTotalDolar = precioTotalDolar;
	}

	public int getPrecioUniPeso() {
		return precioUniPeso;
	}

	public void setPrecioUniPeso(int precioUniPeso) {
		this.precioUniPeso = precioUniPeso;
	}

	public long getPrecioTotalPeso() {
		return precioTotalPeso;
	}

	public void setPrecioTotalPeso(long precioTotalPeso) {
		this.precioTotalPeso = precioTotalPeso;
	}

	public int getPrecioUniUf() {
		return precioUniUf;
	}

	public void setPrecioUniUf(int precioUniUf) {
		this.precioUniUf = precioUniUf;
	}

	public long getPrecioTotalUf() {
		return precioTotalUf;
	}

	public void setPrecioTotalUf(long precioTotalUf) {
		this.precioTotalUf = precioTotalUf;
	}

	public int getIdProyectoEnc() {
		return idProyectoEnc;
	}

	public void setIdProyectoEnc(int idProyectoEnc) {
		this.idProyectoEnc = idProyectoEnc;
	}

	public long getCostoTotalMoneda() {
		return costoTotalMoneda;
	}

	public void setCostoTotalMoneda(long costoTotalMoneda) {
		this.costoTotalMoneda = costoTotalMoneda;
	}

}
