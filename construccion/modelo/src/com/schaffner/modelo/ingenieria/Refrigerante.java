package com.schaffner.modelo.ingenieria;

public class Refrigerante {
	private int codRefrigerante;
	private String desRefrigerante;

	public Refrigerante() {
		desRefrigerante = "";
	}

	public int getCodRefrigerante() {
		return codRefrigerante;
	}

	public void setCodRefrigerante(int codRefrigerante) {
		this.codRefrigerante = codRefrigerante;
	}

	public String getDesRefrigerante() {
		return desRefrigerante;
	}

	public void setDesRefrigerante(String desRefrigerante) {
		this.desRefrigerante = desRefrigerante;
	}

}
