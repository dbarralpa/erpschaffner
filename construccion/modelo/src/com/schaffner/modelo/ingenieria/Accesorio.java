package com.schaffner.modelo.ingenieria;

public class Accesorio {
	private String diseno;
	private String codigo;
	private int cantidad;
	private String descripcion;

	public Accesorio() {
		diseno = "";
		codigo = "";
		descripcion = "";
	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
