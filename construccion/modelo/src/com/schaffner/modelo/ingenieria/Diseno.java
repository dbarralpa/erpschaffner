package com.schaffner.modelo.ingenieria;

public class Diseno {
	private String idDiseno;
	private int idDistribucion;
	private int idRefrigerante;
	private String codigo1;
	private int fase;
	private double potencia;
	private byte valorBbt;
	private byte valorBat;
	private byte valorBte;
	private int codCondBbt;
	private int codCondBat;
	private int codCondBte;
	private int aislador;
	private double kv;
	private String fecha;
	private String usuario;

	public Diseno() {
		idDiseno = "";
		codigo1 = "";
		fecha = "";
		usuario = "";
	}

	public String getIdDiseno() {
		return idDiseno;
	}

	public void setIdDiseno(String idDiseno) {
		this.idDiseno = idDiseno;
	}

	public int getIdDistribucion() {
		return idDistribucion;
	}

	public void setIdDistribucion(int idDistribucion) {
		this.idDistribucion = idDistribucion;
	}

	public int getIdRefrigerante() {
		return idRefrigerante;
	}

	public void setIdRefrigerante(int idRefrigerante) {
		this.idRefrigerante = idRefrigerante;
	}

	public String getCodigo1() {
		return codigo1;
	}

	public void setCodigo1(String codigo1) {
		this.codigo1 = codigo1;
	}

	public int getFase() {
		return fase;
	}

	public void setFase(int fase) {
		this.fase = fase;
	}

	public double getPotencia() {
		return potencia;
	}

	public void setPotencia(double potencia) {
		this.potencia = potencia;
	}

	public byte getValorBbt() {
		return valorBbt;
	}

	public void setValorBbt(byte valorBbt) {
		this.valorBbt = valorBbt;
	}

	public byte getValorBat() {
		return valorBat;
	}

	public void setValorBat(byte valorBat) {
		this.valorBat = valorBat;
	}

	public byte getValorBte() {
		return valorBte;
	}

	public void setValorBte(byte valorBte) {
		this.valorBte = valorBte;
	}

	public int getCodCondBbt() {
		return codCondBbt;
	}

	public void setCodCondBbt(int codCondBbt) {
		this.codCondBbt = codCondBbt;
	}

	public int getCodCondBat() {
		return codCondBat;
	}

	public void setCodCondBat(int codCondBat) {
		this.codCondBat = codCondBat;
	}

	public int getCodCondBte() {
		return codCondBte;
	}

	public void setCodCondBte(int codCondBte) {
		this.codCondBte = codCondBte;
	}

	public int getAislador() {
		return aislador;
	}

	public void setAislador(int aislador) {
		this.aislador = aislador;
	}

	public double getKv() {
		return kv;
	}

	public void setKv(double kv) {
		this.kv = kv;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}
