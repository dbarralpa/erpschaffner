package com.schaffner.modelo.ingenieria;

import java.sql.Timestamp;

public class DocAplicableEnc {
	private int numero;
	private int np;
	private String item;
	private String disenoElectrico;
	private String disenoMecanico;
	private String disenoTrafo;
	private Timestamp fechaIngreso;
	private String usuario;

	public DocAplicableEnc() {
		disenoElectrico = "";
		disenoMecanico = "";
		disenoTrafo = "";
		usuario = "";
		item = "";
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Timestamp getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Timestamp fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getNp() {
		return np;
	}

	public void setNp(int np) {
		this.np = np;
	}

	public String getDisenoElectrico() {
		return disenoElectrico;
	}

	public void setDisenoElectrico(String disenoElectrico) {
		this.disenoElectrico = disenoElectrico;
	}

	public String getDisenoMecanico() {
		return disenoMecanico;
	}

	public void setDisenoMecanico(String disenoMecanico) {
		this.disenoMecanico = disenoMecanico;
	}

	public String getDisenoTrafo() {
		return disenoTrafo;
	}

	public void setDisenoTrafo(String disenoTrafo) {
		this.disenoTrafo = disenoTrafo;
	}

}
