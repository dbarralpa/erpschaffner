package com.schaffner.modelo.ingenieria;

public class Especificacion {
	private String diseno;
	private String norma;
	private String tiporef;
	private int frecuencia;
	private int voltajePrimario;
	private String voltajeSecundario;
	private double corrientePrimaria;
	private double corrienteSecundaria;
	private int potenciaTerciario;
	private int voltajeTerciario;
	private int corrienteTerciario;
	private String conexionPrimaria;
	private String conexionSecundaria;
	private String conexionTerciaria;
	private String grupoVectorial;
	private int nivelImpulso1;
	private int nivelImpulso2;
	private int nivelImpulso3;
	private int aumentotAceite;
	private int aumentotResistencia;
	private int alturaInstalacion;
	private int perdidasVacio;
	private int perdidasCobre;
	private double impedancia;
	private int perdidaTotal;
	private int idDerivacion;

	public Especificacion() {
		diseno = "";
		norma = "";
		tiporef = "";
		voltajeSecundario = "";
	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public int getDerivacion() {
		return idDerivacion;
	}

	public void setDerivacion(int derivacion) {
		this.idDerivacion = derivacion;
	}

	public int getNivelImpulso1() {
		return nivelImpulso1;
	}

	public void setNivelImpulso1(int nivelImpulso1) {
		this.nivelImpulso1 = nivelImpulso1;
	}

	public int getNivelImpulso2() {
		return nivelImpulso2;
	}

	public void setNivelImpulso2(int nivelImpulso2) {
		this.nivelImpulso2 = nivelImpulso2;
	}

	public int getNivelImpulso3() {
		return nivelImpulso3;
	}

	public void setNivelImpulso3(int nivelImpulso3) {
		this.nivelImpulso3 = nivelImpulso3;
	}

	public String getNorma() {
		return norma;
	}

	public void setNorma(String norma) {
		this.norma = norma;
	}

	public String getTiporef() {
		return tiporef;
	}

	public void setTiporef(String tiporef) {
		this.tiporef = tiporef;
	}

	public int getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}

	public int getVoltajePrimario() {
		return voltajePrimario;
	}

	public void setVoltajePrimario(int voltajePrimario) {
		this.voltajePrimario = voltajePrimario;
	}

	public String getVoltajeSecundario() {
		return voltajeSecundario;
	}

	public void setVoltajeSecundario(String voltajeSecundario) {
		this.voltajeSecundario = voltajeSecundario;
	}

	public double getCorrientePrimaria() {
		return corrientePrimaria;
	}

	public void setCorrientePrimaria(double corrientePrimaria) {
		this.corrientePrimaria = corrientePrimaria;
	}

	public double getCorrienteSecundaria() {
		return corrienteSecundaria;
	}

	public void setCorrienteSecundaria(double corrienteSecundaria) {
		this.corrienteSecundaria = corrienteSecundaria;
	}

	public int getPotenciaTerciario() {
		return potenciaTerciario;
	}

	public void setPotenciaTerciario(int potenciaTerciario) {
		this.potenciaTerciario = potenciaTerciario;
	}

	public int getVoltajeTerciario() {
		return voltajeTerciario;
	}

	public void setVoltajeTerciario(int voltajeTerciario) {
		this.voltajeTerciario = voltajeTerciario;
	}

	public int getCorrienteTerciario() {
		return corrienteTerciario;
	}

	public void setCorrienteTerciario(int corrienteTerciario) {
		this.corrienteTerciario = corrienteTerciario;
	}

	public String getConexionPrimaria() {
		return conexionPrimaria;
	}

	public void setConexionPrimaria(String conexionPrimaria) {
		this.conexionPrimaria = conexionPrimaria;
	}

	public String getConexionSecundaria() {
		return conexionSecundaria;
	}

	public void setConexionSecundaria(String conexionSecundaria) {
		this.conexionSecundaria = conexionSecundaria;
	}

	public String getConexionTerciaria() {
		return conexionTerciaria;
	}

	public void setConexionTerciaria(String conexionTerciaria) {
		this.conexionTerciaria = conexionTerciaria;
	}

	public String getGrupoVectorial() {
		return grupoVectorial;
	}

	public void setGrupoVectorial(String grupoVectorial) {
		this.grupoVectorial = grupoVectorial;
	}

	public int getAumentotAceite() {
		return aumentotAceite;
	}

	public void setAumentotAceite(int aumentotAceite) {
		this.aumentotAceite = aumentotAceite;
	}

	public int getAumentotResistencia() {
		return aumentotResistencia;
	}

	public void setAumentotResistencia(int aumentotResistencia) {
		this.aumentotResistencia = aumentotResistencia;
	}

	public int getAlturaInstalacion() {
		return alturaInstalacion;
	}

	public void setAlturaInstalacion(int alturaInstalacion) {
		this.alturaInstalacion = alturaInstalacion;
	}

	public int getPerdidasVacio() {
		return perdidasVacio;
	}

	public void setPerdidasVacio(int perdidasVacio) {
		this.perdidasVacio = perdidasVacio;
	}

	public int getPerdidasCobre() {
		return perdidasCobre;
	}

	public void setPerdidasCobre(int perdidasCobre) {
		this.perdidasCobre = perdidasCobre;
	}

	public double getImpedancia() {
		return impedancia;
	}

	public void setImpedancia(double impedancia) {
		this.impedancia = impedancia;
	}

	public int getPerdidaTotal() {
		return perdidaTotal;
	}

	public void setPerdidaTotal(int perdidaTotal) {
		this.perdidaTotal = perdidaTotal;
	}

	public int getIdDerivacion() {
		return idDerivacion;
	}

	public void setIdDerivacion(int idDerivacion) {
		this.idDerivacion = idDerivacion;
	}

}
