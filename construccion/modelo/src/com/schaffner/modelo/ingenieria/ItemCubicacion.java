package com.schaffner.modelo.ingenieria;

public class ItemCubicacion {
	private String descripcion;
	private String item;
	private int kv1;
	private int kv2;
	private double cant;
	private String area;
	private String unidad;
	private double dim1;
	private double dim2;
	private double dim3;
	private double costoUnitario;
	private int al;

	public ItemCubicacion() {
		descripcion = "";
		area = "";
		unidad = "";
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int getKv1() {
		return kv1;
	}

	public void setKv1(int kv1) {
		this.kv1 = kv1;
	}

	public int getKv2() {
		return kv2;
	}

	public void setKv2(int kv2) {
		this.kv2 = kv2;
	}

	public double getCant() {
		return cant;
	}

	public void setCant(double cant) {
		this.cant = cant;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public double getDim1() {
		return dim1;
	}

	public void setDim1(double dim1) {
		this.dim1 = dim1;
	}

	public double getDim2() {
		return dim2;
	}

	public void setDim2(double dim2) {
		this.dim2 = dim2;
	}

	public double getDim3() {
		return dim3;
	}

	public void setDim3(double dim3) {
		this.dim3 = dim3;
	}

	public int getAl() {
		return al;
	}

	public void setAl(int al) {
		this.al = al;
	}

	public double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

}
