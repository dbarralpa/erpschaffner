package com.schaffner.modelo.ingenieria;

public class NucleoFerreteria {
	private String diseno;
	private char tipo;
	private int soporte;
	private String codigoSilicoso1;
	private String codigoSilicoso2;
	private String codigoSilicoso3;
	private String codigoSilicoso4;
	private String codigoSilicoso5;
	private String codigoSilicoso6;
	private String codigoSilicoso7;
	private String codigoSilicoso8;
	private String codigoSilicoso9;
	private String codigoSilicoso10;
	private int cantidad1;
	private int cantidad2;
	private int cantidad3;
	private int cantidad4;
	private int cantidad5;
	private int cantidad6;
	private int cantidad7;
	private int cantidad8;
	private int cantidad9;
	private int cantidad10;
	private double pesoNucleo1;
	private double pesoNucleo2;
	private double pesoNucleo3;
	private double pesoNucleo4;
	private double pesoNucleo5;
	private double pesoNucleo6;
	private double pesoNucleo7;
	private double pesoNucleo8;
	private double pesoNucleo9;
	private double pesoNucleo10;
	private int chapaMayorNucleo;
	private double apilamientoChapas;
	private int diametroNucleo;
	private int entreCentroNucleo;
	private int ventanaNucleo;
	private int corte;
	public NucleoFerreteria() {
		diseno = "";
		codigoSilicoso1 = "";
		codigoSilicoso2 = "";
		codigoSilicoso3 = "";
		codigoSilicoso4 = "";
		codigoSilicoso5 = "";
		codigoSilicoso6 = "";
		codigoSilicoso7 = "";
		codigoSilicoso8 = "";
		codigoSilicoso9 = "";
		codigoSilicoso10 = "";
	}
	public String getDiseno() {
		return diseno;
	}
	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	public int getSoporte() {
		return soporte;
	}
	public void setSoporte(int soporte) {
		this.soporte = soporte;
	}
	public String getCodigoSilicoso1() {
		return codigoSilicoso1;
	}
	public void setCodigoSilicoso1(String codigoSilicoso1) {
		this.codigoSilicoso1 = codigoSilicoso1;
	}
	public String getCodigoSilicoso2() {
		return codigoSilicoso2;
	}
	public void setCodigoSilicoso2(String codigoSilicoso2) {
		this.codigoSilicoso2 = codigoSilicoso2;
	}
	public String getCodigoSilicoso3() {
		return codigoSilicoso3;
	}
	public void setCodigoSilicoso3(String codigoSilicoso3) {
		this.codigoSilicoso3 = codigoSilicoso3;
	}
	public String getCodigoSilicoso4() {
		return codigoSilicoso4;
	}
	public void setCodigoSilicoso4(String codigoSilicoso4) {
		this.codigoSilicoso4 = codigoSilicoso4;
	}
	public String getCodigoSilicoso5() {
		return codigoSilicoso5;
	}
	public void setCodigoSilicoso5(String codigoSilicoso5) {
		this.codigoSilicoso5 = codigoSilicoso5;
	}
	public String getCodigoSilicoso6() {
		return codigoSilicoso6;
	}
	public void setCodigoSilicoso6(String codigoSilicoso6) {
		this.codigoSilicoso6 = codigoSilicoso6;
	}
	public String getCodigoSilicoso7() {
		return codigoSilicoso7;
	}
	public void setCodigoSilicoso7(String codigoSilicoso7) {
		this.codigoSilicoso7 = codigoSilicoso7;
	}
	public String getCodigoSilicoso8() {
		return codigoSilicoso8;
	}
	public void setCodigoSilicoso8(String codigoSilicoso8) {
		this.codigoSilicoso8 = codigoSilicoso8;
	}
	public String getCodigoSilicoso9() {
		return codigoSilicoso9;
	}
	public void setCodigoSilicoso9(String codigoSilicoso9) {
		this.codigoSilicoso9 = codigoSilicoso9;
	}
	public String getCodigoSilicoso10() {
		return codigoSilicoso10;
	}
	public void setCodigoSilicoso10(String codigoSilicoso10) {
		this.codigoSilicoso10 = codigoSilicoso10;
	}
	public int getCantidad1() {
		return cantidad1;
	}
	public void setCantidad1(int cantidad1) {
		this.cantidad1 = cantidad1;
	}
	public int getCantidad2() {
		return cantidad2;
	}
	public void setCantidad2(int cantidad2) {
		this.cantidad2 = cantidad2;
	}
	public int getCantidad3() {
		return cantidad3;
	}
	public void setCantidad3(int cantidad3) {
		this.cantidad3 = cantidad3;
	}
	public int getCantidad4() {
		return cantidad4;
	}
	public void setCantidad4(int cantidad4) {
		this.cantidad4 = cantidad4;
	}
	public int getCantidad5() {
		return cantidad5;
	}
	public void setCantidad5(int cantidad5) {
		this.cantidad5 = cantidad5;
	}
	public int getCantidad6() {
		return cantidad6;
	}
	public void setCantidad6(int cantidad6) {
		this.cantidad6 = cantidad6;
	}
	public int getCantidad7() {
		return cantidad7;
	}
	public void setCantidad7(int cantidad7) {
		this.cantidad7 = cantidad7;
	}
	public int getCantidad8() {
		return cantidad8;
	}
	public void setCantidad8(int cantidad8) {
		this.cantidad8 = cantidad8;
	}
	public int getCantidad9() {
		return cantidad9;
	}
	public void setCantidad9(int cantidad9) {
		this.cantidad9 = cantidad9;
	}
	public int getCantidad10() {
		return cantidad10;
	}
	public void setCantidad10(int cantidad10) {
		this.cantidad10 = cantidad10;
	}
	public double getPesoNucleo1() {
		return pesoNucleo1;
	}
	public void setPesoNucleo1(double pesoNucleo1) {
		this.pesoNucleo1 = pesoNucleo1;
	}
	public double getPesoNucleo2() {
		return pesoNucleo2;
	}
	public void setPesoNucleo2(double pesoNucleo2) {
		this.pesoNucleo2 = pesoNucleo2;
	}
	public double getPesoNucleo3() {
		return pesoNucleo3;
	}
	public void setPesoNucleo3(double pesoNucleo3) {
		this.pesoNucleo3 = pesoNucleo3;
	}
	public double getPesoNucleo4() {
		return pesoNucleo4;
	}
	public void setPesoNucleo4(double pesoNucleo4) {
		this.pesoNucleo4 = pesoNucleo4;
	}
	public double getPesoNucleo5() {
		return pesoNucleo5;
	}
	public void setPesoNucleo5(double pesoNucleo5) {
		this.pesoNucleo5 = pesoNucleo5;
	}
	public double getPesoNucleo6() {
		return pesoNucleo6;
	}
	public void setPesoNucleo6(double pesoNucleo6) {
		this.pesoNucleo6 = pesoNucleo6;
	}
	public double getPesoNucleo7() {
		return pesoNucleo7;
	}
	public void setPesoNucleo7(double pesoNucleo7) {
		this.pesoNucleo7 = pesoNucleo7;
	}
	public double getPesoNucleo8() {
		return pesoNucleo8;
	}
	public void setPesoNucleo8(double pesoNucleo8) {
		this.pesoNucleo8 = pesoNucleo8;
	}
	public double getPesoNucleo9() {
		return pesoNucleo9;
	}
	public void setPesoNucleo9(double pesoNucleo9) {
		this.pesoNucleo9 = pesoNucleo9;
	}
	public double getPesoNucleo10() {
		return pesoNucleo10;
	}
	public void setPesoNucleo10(double pesoNucleo10) {
		this.pesoNucleo10 = pesoNucleo10;
	}
	public int getChapaMayorNucleo() {
		return chapaMayorNucleo;
	}
	public void setChapaMayorNucleo(int chapaMayorNucleo) {
		this.chapaMayorNucleo = chapaMayorNucleo;
	}
	public double getApilamientoChapas() {
		return apilamientoChapas;
	}
	public void setApilamientoChapas(double apilamientoChapas) {
		this.apilamientoChapas = apilamientoChapas;
	}
	public int getDiametroNucleo() {
		return diametroNucleo;
	}
	public void setDiametroNucleo(int diametroNucleo) {
		this.diametroNucleo = diametroNucleo;
	}
	public int getEntreCentroNucleo() {
		return entreCentroNucleo;
	}
	public void setEntreCentroNucleo(int entreCentroNucleo) {
		this.entreCentroNucleo = entreCentroNucleo;
	}
	public int getVentanaNucleo() {
		return ventanaNucleo;
	}
	public void setVentanaNucleo(int ventanaNucleo) {
		this.ventanaNucleo = ventanaNucleo;
	}
	public int getCorte() {
		return corte;
	}
	public void setCorte(int corte) {
		this.corte = corte;
	}

	

}
