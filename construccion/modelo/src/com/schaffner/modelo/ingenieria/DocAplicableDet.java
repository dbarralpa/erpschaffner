package com.schaffner.modelo.ingenieria;

import java.sql.Timestamp;

public class DocAplicableDet {
	private int id;
	private int numero;
	private int descPlano;
	private String idPlano;
	private short numRevision;
	private Timestamp fechaIngreso;
	private String usuario;
	private int count;
	private int permisoImprimir;
	private String lastIdPlano;
	private String valImprimir;
	private int revision;
	private int idPlanoRev;
	private int idDetRev;

	public DocAplicableDet() {
		idPlano = "";
		usuario = "";
		lastIdPlano = "";
		valImprimir = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Timestamp getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Timestamp fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getDescPlano() {
		return descPlano;
	}

	public void setDescPlano(int descPlano) {
		this.descPlano = descPlano;
	}

	public String getIdPlano() {
		return idPlano;
	}

	public void setIdPlano(String idPlano) {
		this.idPlano = idPlano;
	}

	public short getNumRevision() {
		return numRevision;
	}

	public void setNumRevision(short numRevision) {
		this.numRevision = numRevision;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getLastIdPlano() {
		return lastIdPlano;
	}

	public void setLastIdPlano(String lastIdPlano) {
		this.lastIdPlano = lastIdPlano;
	}

	public int getPermisoImprimir() {
		return permisoImprimir;
	}

	public void setPermisoImprimir(int permisoImprimir) {
		this.permisoImprimir = permisoImprimir;
	}

	public String getValImprimir() {
		return valImprimir;
	}

	public void setValImprimir(String valImprimir) {
		this.valImprimir = valImprimir;
	}

	public int getRevision() {
		return revision;
	}

	public void setRevision(int revision) {
		this.revision = revision;
	}

	public int getIdPlanoRev() {
		return idPlanoRev;
	}

	public void setIdPlanoRev(int idPlanoRev) {
		this.idPlanoRev = idPlanoRev;
	}

	public int getIdDetRev() {
		return idDetRev;
	}

	public void setIdDetRev(int idDetRev) {
		this.idDetRev = idDetRev;
	}
	
}
