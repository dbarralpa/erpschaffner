package com.schaffner.modelo.ingenieria;

public class Tolerancia {
	private int idTolerancia;
	private double f1;
	private double f2;
	private double f3;
	private double f4;
	private double f5;
	private double f6;
	private double f7;
	private double f8;
	private double f9;
	private double f10;
	private double f11;
	private double f12;
	private double f13;
	private double f14;
	private double f15;
	private double f16;
	private double f17;

	public Tolerancia() {

	}

	public int getIdTolerancia() {
		return idTolerancia;
	}

	public void setIdTolerancia(int idTolerancia) {
		this.idTolerancia = idTolerancia;
	}

	public double getF1() {
		return f1;
	}

	public void setF1(double f1) {
		this.f1 = f1;
	}

	public double getF2() {
		return f2;
	}

	public void setF2(double f2) {
		this.f2 = f2;
	}

	public double getF3() {
		return f3;
	}

	public void setF3(double f3) {
		this.f3 = f3;
	}

	public double getF4() {
		return f4;
	}

	public void setF4(double f4) {
		this.f4 = f4;
	}

	public double getF5() {
		return f5;
	}

	public void setF5(double f5) {
		this.f5 = f5;
	}

	public double getF6() {
		return f6;
	}

	public void setF6(double f6) {
		this.f6 = f6;
	}

	public double getF7() {
		return f7;
	}

	public void setF7(double f7) {
		this.f7 = f7;
	}

	public double getF8() {
		return f8;
	}

	public void setF8(double f8) {
		this.f8 = f8;
	}

	public double getF9() {
		return f9;
	}

	public void setF9(double f9) {
		this.f9 = f9;
	}

	public double getF10() {
		return f10;
	}

	public void setF10(double f10) {
		this.f10 = f10;
	}

	public double getF11() {
		return f11;
	}

	public void setF11(double f11) {
		this.f11 = f11;
	}

	public double getF12() {
		return f12;
	}

	public void setF12(double f12) {
		this.f12 = f12;
	}

	public double getF13() {
		return f13;
	}

	public void setF13(double f13) {
		this.f13 = f13;
	}

	public double getF14() {
		return f14;
	}

	public void setF14(double f14) {
		this.f14 = f14;
	}

	public double getF15() {
		return f15;
	}

	public void setF15(double f15) {
		this.f15 = f15;
	}

	public double getF16() {
		return f16;
	}

	public void setF16(double f16) {
		this.f16 = f16;
	}

	public double getF17() {
		return f17;
	}

	public void setF17(double f17) {
		this.f17 = f17;
	}

}
