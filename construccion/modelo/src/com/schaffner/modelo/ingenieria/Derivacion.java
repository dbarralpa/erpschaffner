package com.schaffner.modelo.ingenieria;

public class Derivacion {
	private int id;
	private String descripcion;

	public Derivacion() {
		descripcion = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
