package com.schaffner.modelo.ingenieria;

public class TipoAislador {
	private int codTipoAislador;
	private String descripcionAislador;

	public TipoAislador() {
		descripcionAislador = "";
	}

	public int getCodTipoAislador() {
		return codTipoAislador;
	}

	public void setCodTipoAislador(int codTipoAislador) {
		this.codTipoAislador = codTipoAislador;
	}

	public String getDescripcionAislador() {
		return descripcionAislador;
	}

	public void setDescripcionAislador(String descripcionAislador) {
		this.descripcionAislador = descripcionAislador;
	}

}
