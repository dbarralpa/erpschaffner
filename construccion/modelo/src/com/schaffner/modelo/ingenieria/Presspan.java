package com.schaffner.modelo.ingenieria;

public class Presspan {
	private String diseno;
	private int canEspesoresBase1;
	private int canEspesoresBase2;
	private int canEspesoresBase3;
	private double espesorBase1;
	private double espesorBase2;
	private double espesorBase3;
	private String codBase1;
	private String codBase2;
	private String codBase3;
	private int canEspesoresAtbt1;
	private int canEspesoresAtbt2;
	private int canEspesoresAtbt3;
	private double canEspesoresCapa1;
	private double canEspesoresCapa2;
	private double canEspesoresCapa3;
	private double espesorAtbt1;
	private double espesorAtbt2;
	private double espesorAtbt3;
	private double espesorCapa1;
	private double espesorCapa2;
	private double espesorCapa3;
	private String codAtbt1;
	private String codAtbt2;
	private String codAtbt3;
	private String codCapa1;
	private String codCapa2;
	private String codCapa3;
	private String descBase1;
	private String descBase2;
	private String descBase3;
	private String descAtbt1;
	private String descAtbt2;
	private String descAtbt3;
	private String descCapa1;
	private String descCapa2;
	private String descCapa3;
	private int bobina;

	public Presspan() {
		diseno = "";
		codBase1 = "";
		codBase2 = "";
		codBase2 = "";
		codAtbt1 = "";
		codAtbt2 = "";
		codAtbt3 = "";
		codCapa1 = "";
		codCapa2 = "";
		codCapa3 = "";
		descBase1 = "";
		descBase2 = "";
		descBase3 = "";
		descAtbt1 = "";
		descAtbt2 = "";
		descAtbt3 = "";

	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public int getCanEspesoresBase1() {
		return canEspesoresBase1;
	}

	public void setCanEspesoresBase1(int canEspesoresBase1) {
		this.canEspesoresBase1 = canEspesoresBase1;
	}

	public int getCanEspesoresBase2() {
		return canEspesoresBase2;
	}

	public void setCanEspesoresBase2(int canEspesoresBase2) {
		this.canEspesoresBase2 = canEspesoresBase2;
	}

	public int getCanEspesoresBase3() {
		return canEspesoresBase3;
	}

	public void setCanEspesoresBase3(int canEspesoresBase3) {
		this.canEspesoresBase3 = canEspesoresBase3;
	}

	public double getEspesorBase1() {
		return espesorBase1;
	}

	public void setEspesorBase1(double espesorBase1) {
		this.espesorBase1 = espesorBase1;
	}

	public double getEspesorBase2() {
		return espesorBase2;
	}

	public void setEspesorBase2(double espesorBase2) {
		this.espesorBase2 = espesorBase2;
	}

	public double getEspesorBase3() {
		return espesorBase3;
	}

	public void setEspesorBase3(double espesorBase3) {
		this.espesorBase3 = espesorBase3;
	}

	public String getCodBase1() {
		return codBase1;
	}

	public void setCodBase1(String codBase1) {
		this.codBase1 = codBase1;
	}

	public String getCodBase2() {
		return codBase2;
	}

	public void setCodBase2(String codBase2) {
		this.codBase2 = codBase2;
	}

	public String getCodBase3() {
		return codBase3;
	}

	public void setCodBase3(String codBase3) {
		this.codBase3 = codBase3;
	}

	public int getCanEspesoresAtbt1() {
		return canEspesoresAtbt1;
	}

	public void setCanEspesoresAtbt1(int canEspesoresAtbt1) {
		this.canEspesoresAtbt1 = canEspesoresAtbt1;
	}

	public int getCanEspesoresAtbt2() {
		return canEspesoresAtbt2;
	}

	public void setCanEspesoresAtbt2(int canEspesoresAtbt2) {
		this.canEspesoresAtbt2 = canEspesoresAtbt2;
	}

	public int getCanEspesoresAtbt3() {
		return canEspesoresAtbt3;
	}

	public void setCanEspesoresAtbt3(int canEspesoresAtbt3) {
		this.canEspesoresAtbt3 = canEspesoresAtbt3;
	}

	public double getCanEspesoresCapa1() {
		return canEspesoresCapa1;
	}

	public void setCanEspesoresCapa1(double canEspesoresCapa1) {
		this.canEspesoresCapa1 = canEspesoresCapa1;
	}

	public double getCanEspesoresCapa2() {
		return canEspesoresCapa2;
	}

	public void setCanEspesoresCapa2(double canEspesoresCapa2) {
		this.canEspesoresCapa2 = canEspesoresCapa2;
	}

	public double getCanEspesoresCapa3() {
		return canEspesoresCapa3;
	}

	public void setCanEspesoresCapa3(double canEspesoresCapa3) {
		this.canEspesoresCapa3 = canEspesoresCapa3;
	}

	public double getEspesorAtbt1() {
		return espesorAtbt1;
	}

	public void setEspesorAtbt1(double espesorAtbt1) {
		this.espesorAtbt1 = espesorAtbt1;
	}

	public double getEspesorAtbt2() {
		return espesorAtbt2;
	}

	public void setEspesorAtbt2(double espesorAtbt2) {
		this.espesorAtbt2 = espesorAtbt2;
	}

	public double getEspesorAtbt3() {
		return espesorAtbt3;
	}

	public void setEspesorAtbt3(double espesorAtbt3) {
		this.espesorAtbt3 = espesorAtbt3;
	}

	public double getEspesorCapa1() {
		return espesorCapa1;
	}

	public void setEspesorCapa1(double espesorCapa1) {
		this.espesorCapa1 = espesorCapa1;
	}

	public double getEspesorCapa2() {
		return espesorCapa2;
	}

	public void setEspesorCapa2(double espesorCapa2) {
		this.espesorCapa2 = espesorCapa2;
	}

	public double getEspesorCapa3() {
		return espesorCapa3;
	}

	public void setEspesorCapa3(double espesorCapa3) {
		this.espesorCapa3 = espesorCapa3;
	}

	public String getCodAtbt1() {
		return codAtbt1;
	}

	public void setCodAtbt1(String codAtbt1) {
		this.codAtbt1 = codAtbt1;
	}

	public String getCodAtbt2() {
		return codAtbt2;
	}

	public void setCodAtbt2(String codAtbt2) {
		this.codAtbt2 = codAtbt2;
	}

	public String getCodAtbt3() {
		return codAtbt3;
	}

	public void setCodAtbt3(String codAtbt3) {
		this.codAtbt3 = codAtbt3;
	}

	public String getCodCapa1() {
		return codCapa1;
	}

	public void setCodCapa1(String codCapa1) {
		this.codCapa1 = codCapa1;
	}

	public String getCodCapa2() {
		return codCapa2;
	}

	public void setCodCapa2(String codCapa2) {
		this.codCapa2 = codCapa2;
	}

	public String getCodCapa3() {
		return codCapa3;
	}

	public void setCodCapa3(String codCapa3) {
		this.codCapa3 = codCapa3;
	}

	public String getDescBase1() {
		return descBase1;
	}

	public void setDescBase1(String descBase1) {
		this.descBase1 = descBase1;
	}

	public String getDescBase2() {
		return descBase2;
	}

	public void setDescBase2(String descBase2) {
		this.descBase2 = descBase2;
	}

	public String getDescBase3() {
		return descBase3;
	}

	public void setDescBase3(String descBase3) {
		this.descBase3 = descBase3;
	}

	public String getDescAtbt1() {
		return descAtbt1;
	}

	public void setDescAtbt1(String descAtbt1) {
		this.descAtbt1 = descAtbt1;
	}

	public String getDescAtbt2() {
		return descAtbt2;
	}

	public void setDescAtbt2(String descAtbt2) {
		this.descAtbt2 = descAtbt2;
	}

	public String getDescAtbt3() {
		return descAtbt3;
	}

	public void setDescAtbt3(String descAtbt3) {
		this.descAtbt3 = descAtbt3;
	}

	public String getDescCapa1() {
		return descCapa1;
	}

	public void setDescCapa1(String descCapa1) {
		this.descCapa1 = descCapa1;
	}

	public String getDescCapa2() {
		return descCapa2;
	}

	public void setDescCapa2(String descCapa2) {
		this.descCapa2 = descCapa2;
	}

	public String getDescCapa3() {
		return descCapa3;
	}

	public void setDescCapa3(String descCapa3) {
		this.descCapa3 = descCapa3;
	}

	public int getBobina() {
		return bobina;
	}

	public void setBobina(int bobina) {
		this.bobina = bobina;
	}
}
