package com.schaffner.modelo.ingenieria;

public class EstanqueTapa {
	private String diseno;
	private int largoEstanque;
	private int anchoEstanque;
	private int altoEstanque;
	private int altoBaseFalsa;
	private int altoColchon;
	private int diametroCircular;
	private String codManto;
	private double espManto;
	private String codTapa;
	private double espTapa;
	private String codFondo;
	private double espFondo;
	private String pin1;
	private String pin2;
	private String pin3;
	private String pin4;
	private char tipo;

	public EstanqueTapa() {
		diseno = "";
		codManto = "";
		codTapa = "";
		codFondo = "";
		pin1 = "";
		pin2 = "";
		pin3 = "";
		pin4 = "";
	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public int getLargoEstanque() {
		return largoEstanque;
	}

	public void setLargoEstanque(int largoEstanque) {
		this.largoEstanque = largoEstanque;
	}

	public int getAnchoEstanque() {
		return anchoEstanque;
	}

	public void setAnchoEstanque(int anchoEstanque) {
		this.anchoEstanque = anchoEstanque;
	}

	public int getAltoEstanque() {
		return altoEstanque;
	}

	public void setAltoEstanque(int altoEstanque) {
		this.altoEstanque = altoEstanque;
	}

	public int getAltoBaseFalsa() {
		return altoBaseFalsa;
	}

	public void setAltoBaseFalsa(int altoBaseFalsa) {
		this.altoBaseFalsa = altoBaseFalsa;
	}

	public int getAltoColchon() {
		return altoColchon;
	}

	public void setAltoColchon(int altoColchon) {
		this.altoColchon = altoColchon;
	}

	public int getDiametroCircular() {
		return diametroCircular;
	}

	public void setDiametroCircular(int diametroCircular) {
		this.diametroCircular = diametroCircular;
	}

	public String getCodManto() {
		return codManto;
	}

	public void setCodManto(String codManto) {
		this.codManto = codManto;
	}

	public double getEspManto() {
		return espManto;
	}

	public void setEspManto(double espManto) {
		this.espManto = espManto;
	}

	public String getCodTapa() {
		return codTapa;
	}

	public void setCodTapa(String codTapa) {
		this.codTapa = codTapa;
	}

	public double getEspTapa() {
		return espTapa;
	}

	public void setEspTapa(double espTapa) {
		this.espTapa = espTapa;
	}

	public String getCodFondo() {
		return codFondo;
	}

	public void setCodFondo(String codFondo) {
		this.codFondo = codFondo;
	}

	public double getEspFondo() {
		return espFondo;
	}

	public void setEspFondo(double espFondo) {
		this.espFondo = espFondo;
	}

	public String getPin1() {
		return pin1;
	}

	public void setPin1(String pin1) {
		this.pin1 = pin1;
	}

	public String getPin2() {
		return pin2;
	}

	public void setPin2(String pin2) {
		this.pin2 = pin2;
	}

	public String getPin3() {
		return pin3;
	}

	public void setPin3(String pin3) {
		this.pin3 = pin3;
	}

	public String getPin4() {
		return pin4;
	}

	public void setPin4(String pin4) {
		this.pin4 = pin4;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
}
