package com.schaffner.modelo.ingenieria;

public class Distribucion {
	private int codDistribucion;
	private String distribucion;
	private char tipo;

	public Distribucion() {
		tipo = ' ';
	}

	public int getCodDistribucion() {
		return codDistribucion;
	}

	public void setCodDistribucion(int codDistribucion) {
		this.codDistribucion = codDistribucion;
	}

	public String getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(String distribucion) {
		this.distribucion = distribucion;
	}

	public char getTipo() {
		return tipo;
	}

	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

}
