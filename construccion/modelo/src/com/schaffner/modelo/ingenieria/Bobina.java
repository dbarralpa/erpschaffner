package com.schaffner.modelo.ingenieria;

public class Bobina {
	private String diseno;
	private int vueltasXfase;
	private String codigoCondCu;
	private double dimensionAxial;
	private double dimensionRadial;
	private double incrementoPapel;
	private int condParalelo;
	private int condAxiales;
	private double largoEspiraMedia;
	private double nCapasDisco;
	private int lducto;
	private int espesorDuctos;
	private String codDucto;
	private int nDuctosCompletos;
	private int nDuctosCabezal;
	private int nPrincipiosXcapa;
	private double largoFisicoBobina;
	private double espesorFisicoBobina;
	private int alma;
	private int lducto2;
	private int espesorDuctos2;
	private String codDucto2;
	private int nDuctosNubTat;
	private double largoBobinaCuellos;
	private int nDuctosCabezaLatBt;
	private double esplat;
	private double espcab;
	private int tipoBobina;
	
	public Bobina() {
		diseno = "";
		codigoCondCu = "";
		codDucto = "";
		codDucto2 = "";
	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public int getVueltasXfase() {
		return vueltasXfase;
	}

	public void setVueltasXfase(int vueltasXfase) {
		this.vueltasXfase = vueltasXfase;
	}

	public String getCodigoCondCu() {
		return codigoCondCu;
	}

	public void setCodigoCondCu(String codigoCondCu) {
		this.codigoCondCu = codigoCondCu;
	}

	public double getDimensionAxial() {
		return dimensionAxial;
	}

	public void setDimensionAxial(double dimensionAxial) {
		this.dimensionAxial = dimensionAxial;
	}

	public double getDimensionRadial() {
		return dimensionRadial;
	}

	public void setDimensionRadial(double dimensionRadial) {
		this.dimensionRadial = dimensionRadial;
	}

	public double getIncrementoPapel() {
		return incrementoPapel;
	}

	public void setIncrementoPapel(double incrementoPapel) {
		this.incrementoPapel = incrementoPapel;
	}

	public int getCondParalelo() {
		return condParalelo;
	}

	public void setCondParalelo(int condParalelo) {
		this.condParalelo = condParalelo;
	}

	public int getCondAxiales() {
		return condAxiales;
	}

	public void setCondAxiales(int condAxiales) {
		this.condAxiales = condAxiales;
	}

	public double getLargoEspiraMedia() {
		return largoEspiraMedia;
	}

	public void setLargoEspiraMedia(double largoEspiraMedia) {
		this.largoEspiraMedia = largoEspiraMedia;
	}

	public double getnCapasDisco() {
		return nCapasDisco;
	}

	public void setnCapasDisco(double nCapasDisco) {
		this.nCapasDisco = nCapasDisco;
	}

	public int getLducto() {
		return lducto;
	}

	public void setLducto(int lducto) {
		this.lducto = lducto;
	}

	public int getEspesorDuctos() {
		return espesorDuctos;
	}

	public void setEspesorDuctos(int espesorDuctos) {
		this.espesorDuctos = espesorDuctos;
	}

	public String getCodDucto() {
		return codDucto;
	}

	public void setCodDucto(String codDucto) {
		this.codDucto = codDucto;
	}

	public int getnDuctosCompletos() {
		return nDuctosCompletos;
	}

	public void setnDuctosCompletos(int nDuctosCompletos) {
		this.nDuctosCompletos = nDuctosCompletos;
	}

	public int getnDuctosCabezal() {
		return nDuctosCabezal;
	}

	public void setnDuctosCabezal(int nDuctosCabezal) {
		this.nDuctosCabezal = nDuctosCabezal;
	}

	public int getnPrincipiosXcapa() {
		return nPrincipiosXcapa;
	}

	public void setnPrincipiosXcapa(int nPrincipiosXcapa) {
		this.nPrincipiosXcapa = nPrincipiosXcapa;
	}

	public double getLargoFisicoBobina() {
		return largoFisicoBobina;
	}

	public void setLargoFisicoBobina(double largoFisicoBobina) {
		this.largoFisicoBobina = largoFisicoBobina;
	}

	public double getEspesorFisicoBobina() {
		return espesorFisicoBobina;
	}

	public void setEspesorFisicoBobina(double espesorFisicoBobina) {
		this.espesorFisicoBobina = espesorFisicoBobina;
	}

	public int getLducto2() {
		return lducto2;
	}

	public void setLducto2(int lducto2) {
		this.lducto2 = lducto2;
	}

	public int getEspesorDuctos2() {
		return espesorDuctos2;
	}

	public void setEspesorDuctos2(int espesorDuctos2) {
		this.espesorDuctos2 = espesorDuctos2;
	}

	public String getCodDucto2() {
		return codDucto2;
	}

	public void setCodDucto2(String codDucto2) {
		this.codDucto2 = codDucto2;
	}

	public int getnDuctosNubTat() {
		return nDuctosNubTat;
	}

	public void setnDuctosNubTat(int nDuctosNubTat) {
		this.nDuctosNubTat = nDuctosNubTat;
	}

	public double getLargoBobinaCuellos() {
		return largoBobinaCuellos;
	}

	public void setLargoBobinaCuellos(double largoBobinaCuellos) {
		this.largoBobinaCuellos = largoBobinaCuellos;
	}

	public int getnDuctosCabezaLatBt() {
		return nDuctosCabezaLatBt;
	}

	public void setnDuctosCabezaLatBt(int nDuctosCabezaLatBt) {
		this.nDuctosCabezaLatBt = nDuctosCabezaLatBt;
	}

	public int getTipoBobina() {
		return tipoBobina;
	}

	public void setTipoBobina(int tipoBobina) {
		this.tipoBobina = tipoBobina;
	}

	public int getAlma() {
		return alma;
	}

	public void setAlma(int alma) {
		this.alma = alma;
	}

	public double getEsplat() {
		return esplat;
	}

	public void setEsplat(double esplat) {
		this.esplat = esplat;
	}

	public double getEspcab() {
		return espcab;
	}

	public void setEspcab(double espcab) {
		this.espcab = espcab;
	}
}
