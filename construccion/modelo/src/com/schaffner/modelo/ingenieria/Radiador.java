package com.schaffner.modelo.ingenieria;

public class Radiador {
	private String diseno;
	private int codTipoRadiador;
	private String codigoRadiador;
	private String descripcionRadiador;
	private int anchoRadiador;
	private int altoRadiador;
	private int numRadiadores;
	private int numElementos;
	private double kilo;
	private double litros;

	public Radiador() {
		diseno = "";
		descripcionRadiador = "";
		codigoRadiador = "";
	}

	public String getDiseno() {
		return diseno;
	}

	public void setDiseno(String diseno) {
		this.diseno = diseno;
	}

	public int getCodTipoRadiador() {
		return codTipoRadiador;
	}

	public void setCodTipoRadiador(int codTipoRadiador) {
		this.codTipoRadiador = codTipoRadiador;
	}

	public String getCodigoRadiador() {
		return codigoRadiador;
	}

	public void setCodigoRadiador(String codigoRadiador) {
		this.codigoRadiador = codigoRadiador;
	}

	public String getDescripcionRadiador() {
		return descripcionRadiador;
	}

	public void setDescripcionRadiador(String descripcionRadiador) {
		this.descripcionRadiador = descripcionRadiador;
	}

	public int getAnchoRadiador() {
		return anchoRadiador;
	}

	public void setAnchoRadiador(int anchoRadiador) {
		this.anchoRadiador = anchoRadiador;
	}

	public int getAltoRadiador() {
		return altoRadiador;
	}

	public void setAltoRadiador(int altoRadiador) {
		this.altoRadiador = altoRadiador;
	}

	public int getNumRadiadores() {
		return numRadiadores;
	}

	public void setNumRadiadores(int numRadiadores) {
		this.numRadiadores = numRadiadores;
	}

	public int getNumElementos() {
		return numElementos;
	}

	public void setNumElementos(int numElementos) {
		this.numElementos = numElementos;
	}

	public double getKilo() {
		return kilo;
	}

	public void setKilo(double kilo) {
		this.kilo = kilo;
	}

	public double getLitros() {
		return litros;
	}

	public void setLitros(double litros) {
		this.litros = litros;
	}

	
}
