package com.schaffner.modelo;

import java.io.Serializable;

public class Persona implements Serializable{
	private static final long serialVersionUID = -7788619177798333712L;
	private int id;
	private String nombre;
	private String rut;
	private String contacto;
	private String email;
	private String telefono;

	public Persona() {
      nombre = "";
      rut = "";
      contacto = "";
      email = "";
      telefono = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
