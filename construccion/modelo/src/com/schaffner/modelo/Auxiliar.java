package com.schaffner.modelo;

import java.sql.Timestamp;

public class Auxiliar {
	private int aux1;
	private int aux2;
	private int aux3;
	private int aux4;
	private int aux5;
	private int aux6;
	private int aux7;
	private int aux8;
	private String aux9;
	private String aux10;
	private String aux11;
	private String aux12;
	private String aux13;
	private String aux14;
	private String aux15;
	private String aux16;
	private String aux17;
	private String aux18;
	private String aux19;
	private String aux20;
	private char aux21;
	private char aux22;
	private char aux23;
	private char aux24;
	private char aux25;
	private char aux26;
	private char aux27;
	private double aux28;
	private double aux29;
	private double aux30;
	private double aux31;
	private double aux32;
	private double aux33;
	private double aux34;
	private Timestamp aux35;
	private Timestamp aux36;
	private Timestamp aux37;
	private boolean aux38;
	private boolean aux39;
	private boolean aux40;

	public char getAux21() {
		return aux21;
	}

	public void setAux21(char aux21) {
		this.aux21 = aux21;
	}

	public char getAux22() {
		return aux22;
	}

	public void setAux22(char aux22) {
		this.aux22 = aux22;
	}

	public char getAux23() {
		return aux23;
	}

	public void setAux23(char aux23) {
		this.aux23 = aux23;
	}

	public char getAux24() {
		return aux24;
	}

	public void setAux24(char aux24) {
		this.aux24 = aux24;
	}

	public char getAux25() {
		return aux25;
	}

	public void setAux25(char aux25) {
		this.aux25 = aux25;
	}

	public char getAux26() {
		return aux26;
	}

	public void setAux26(char aux26) {
		this.aux26 = aux26;
	}

	public char getAux27() {
		return aux27;
	}

	public void setAux27(char aux27) {
		this.aux27 = aux27;
	}

	public Auxiliar() {

	}

	public int getAux1() {
		return aux1;
	}

	public void setAux1(int aux1) {
		this.aux1 = aux1;
	}

	public int getAux2() {
		return aux2;
	}

	public void setAux2(int aux2) {
		this.aux2 = aux2;
	}

	public int getAux3() {
		return aux3;
	}

	public void setAux3(int aux3) {
		this.aux3 = aux3;
	}

	public int getAux4() {
		return aux4;
	}

	public void setAux4(int aux4) {
		this.aux4 = aux4;
	}

	public int getAux5() {
		return aux5;
	}

	public void setAux5(int aux5) {
		this.aux5 = aux5;
	}

	public int getAux6() {
		return aux6;
	}

	public void setAux6(int aux6) {
		this.aux6 = aux6;
	}

	public int getAux7() {
		return aux7;
	}

	public void setAux7(int aux7) {
		this.aux7 = aux7;
	}

	public int getAux8() {
		return aux8;
	}

	public void setAux8(int aux8) {
		this.aux8 = aux8;
	}

	public String getAux9() {
		return aux9;
	}

	public void setAux9(String aux9) {
		this.aux9 = aux9;
	}

	public String getAux10() {
		return aux10;
	}

	public void setAux10(String aux10) {
		this.aux10 = aux10;
	}

	public String getAux11() {
		return aux11;
	}

	public void setAux11(String aux11) {
		this.aux11 = aux11;
	}

	public String getAux12() {
		return aux12;
	}

	public void setAux12(String aux12) {
		this.aux12 = aux12;
	}

	public String getAux13() {
		return aux13;
	}

	public void setAux13(String aux13) {
		this.aux13 = aux13;
	}

	public String getAux14() {
		return aux14;
	}

	public void setAux14(String aux14) {
		this.aux14 = aux14;
	}

	public String getAux15() {
		return aux15;
	}

	public void setAux15(String aux15) {
		this.aux15 = aux15;
	}

	public String getAux16() {
		return aux16;
	}

	public void setAux16(String aux16) {
		this.aux16 = aux16;
	}

	public String getAux17() {
		return aux17;
	}

	public void setAux17(String aux17) {
		this.aux17 = aux17;
	}

	public String getAux18() {
		return aux18;
	}

	public void setAux18(String aux18) {
		this.aux18 = aux18;
	}

	public String getAux19() {
		return aux19;
	}

	public void setAux19(String aux19) {
		this.aux19 = aux19;
	}

	public String getAux20() {
		return aux20;
	}

	public void setAux20(String aux20) {
		this.aux20 = aux20;
	}

	public double getAux28() {
		return aux28;
	}

	public void setAux28(double aux28) {
		this.aux28 = aux28;
	}

	public double getAux29() {
		return aux29;
	}

	public void setAux29(double aux29) {
		this.aux29 = aux29;
	}

	public double getAux30() {
		return aux30;
	}

	public void setAux30(double aux30) {
		this.aux30 = aux30;
	}

	public double getAux31() {
		return aux31;
	}

	public void setAux31(double aux31) {
		this.aux31 = aux31;
	}

	public double getAux32() {
		return aux32;
	}

	public void setAux32(double aux32) {
		this.aux32 = aux32;
	}

	public double getAux33() {
		return aux33;
	}

	public void setAux33(double aux33) {
		this.aux33 = aux33;
	}

	public double getAux34() {
		return aux34;
	}

	public void setAux34(double aux34) {
		this.aux34 = aux34;
	}

	public Timestamp getAux35() {
		return aux35;
	}

	public void setAux35(Timestamp aux35) {
		this.aux35 = aux35;
	}

	public Timestamp getAux36() {
		return aux36;
	}

	public void setAux36(Timestamp aux36) {
		this.aux36 = aux36;
	}

	public Timestamp getAux37() {
		return aux37;
	}

	public void setAux37(Timestamp aux37) {
		this.aux37 = aux37;
	}

	public boolean isAux38() {
		return aux38;
	}

	public void setAux38(boolean aux38) {
		this.aux38 = aux38;
	}

	public boolean isAux39() {
		return aux39;
	}

	public void setAux39(boolean aux39) {
		this.aux39 = aux39;
	}

	public boolean isAux40() {
		return aux40;
	}

	public void setAux40(boolean aux40) {
		this.aux40 = aux40;
	}
	
}
