package com.schaffner.modelo;

public class ProyectoComercialVentaEnc {
	private int id;
	private int idProyecto;
	private long idCliente;
	private String nombreProyecto;
	private int idTipoCotizacion;
	private String fechaCliente;
	private int idTipoMoneda;
	private int idFormaPago;
	private String usuario;
	private int idVendedor;
	private int idEstado;
	private String fechaEnvioIng;
	private String fechaIngEnregara;
	private String fechaIngEntrego;
	private String validezOferta;
	private String plazoEntrega;
	private String contacto;
	private String email;

	public ProyectoComercialVentaEnc() {
		fechaCliente = "";
		nombreProyecto = "";
		fechaEnvioIng = "";
		fechaIngEnregara = "";
		fechaIngEntrego = "";
		validezOferta = "";
		plazoEntrega = "";
		contacto = "";
		email = "";
	}

	public String getValidezOferta() {
		return validezOferta;
	}

	public void setValidezOferta(String validezOferta) {
		this.validezOferta = validezOferta;
	}

	public String getPlazoEntrega() {
		return plazoEntrega;
	}

	public void setPlazoEntrega(String plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}

	public int getIdVendedor() {
		return idVendedor;
	}

	public void setIdVendedor(int idVendedor) {
		this.idVendedor = idVendedor;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public int getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}

	public String getFechaEnvioIng() {
		return fechaEnvioIng;
	}

	public void setFechaEnvioIng(String fechaEnvioIng) {
		this.fechaEnvioIng = fechaEnvioIng;
	}

	public String getFechaIngEnregara() {
		return fechaIngEnregara;
	}

	public void setFechaIngEnregara(String fechaIngEnregara) {
		this.fechaIngEnregara = fechaIngEnregara;
	}

	public String getFechaIngEntrego() {
		return fechaIngEntrego;
	}

	public void setFechaIngEntrego(String fechaIngEntrego) {
		this.fechaIngEntrego = fechaIngEntrego;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	public int getIdTipoCotizacion() {
		return idTipoCotizacion;
	}

	public void setIdTipoCotizacion(int idTipoCotizacion) {
		this.idTipoCotizacion = idTipoCotizacion;
	}

	public String getFechaCliente() {
		return fechaCliente;
	}

	public void setFechaCliente(String fechaCliente) {
		this.fechaCliente = fechaCliente;
	}

	public int getIdTipoMoneda() {
		return idTipoMoneda;
	}

	public void setIdTipoMoneda(int idTipoMoneda) {
		this.idTipoMoneda = idTipoMoneda;
	}

	public int getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(int idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
