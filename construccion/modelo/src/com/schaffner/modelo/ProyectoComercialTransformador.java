package com.schaffner.modelo;

public class ProyectoComercialTransformador {
	private int id;
	private int idTransformador;
	private String idFamilia;
	private int idNorma;
	private int idTemPerdidas;
	private int idVentilacion;
	private int idTemperatura;
	private int potencia;
	private int voltajePrimario;
	private int voltajeSecundario;
	private int impedancia;
	private int frecuencia;
	private String observacion;
	private int idProyectoVentaDet;
	private int idProyectoVentaEnc;

	public ProyectoComercialTransformador() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdTransformador() {
		return idTransformador;
	}

	public void setIdTransformador(int idTransformador) {
		this.idTransformador = idTransformador;
	}

	public String getIdFamilia() {
		return idFamilia;
	}

	public void setIdFamilia(String idFamilia) {
		this.idFamilia = idFamilia;
	}

	public int getIdNorma() {
		return idNorma;
	}

	public void setIdNorma(int idNorma) {
		this.idNorma = idNorma;
	}

	public int getIdTemPerdidas() {
		return idTemPerdidas;
	}

	public void setIdTemPerdidas(int idTemPerdidas) {
		this.idTemPerdidas = idTemPerdidas;
	}

	public int getIdVentilacion() {
		return idVentilacion;
	}

	public void setIdVentilacion(int idVentilacion) {
		this.idVentilacion = idVentilacion;
	}

	public int getIdTemperatura() {
		return idTemperatura;
	}

	public void setIdTemperatura(int idTemperatura) {
		this.idTemperatura = idTemperatura;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public int getVoltajePrimario() {
		return voltajePrimario;
	}

	public void setVoltajePrimario(int voltajePrimario) {
		this.voltajePrimario = voltajePrimario;
	}

	public int getVoltajeSecundario() {
		return voltajeSecundario;
	}

	public void setVoltajeSecundario(int voltajeSecundario) {
		this.voltajeSecundario = voltajeSecundario;
	}

	public int getImpedancia() {
		return impedancia;
	}

	public void setImpedancia(int impedancia) {
		this.impedancia = impedancia;
	}

	public int getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public int getIdProyectoVentaDet() {
		return idProyectoVentaDet;
	}

	public void setIdProyectoVentaDet(int idProyectoVentaDet) {
		this.idProyectoVentaDet = idProyectoVentaDet;
	}

	public int getIdProyectoVentaEnc() {
		return idProyectoVentaEnc;
	}

	public void setIdProyectoVentaEnc(int idProyectoVentaEnc) {
		this.idProyectoVentaEnc = idProyectoVentaEnc;
	}
}
