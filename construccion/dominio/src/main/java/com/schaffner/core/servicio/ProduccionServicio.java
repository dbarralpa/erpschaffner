package com.schaffner.core.servicio;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.schaffner.infraestructure.data.repositories.contracts.IIngenieriaDAO;
import com.schaffner.infraestructure.data.repositories.contracts.IProduccionDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.DocAplicableDet;

public class ProduccionServicio implements IProduccionServicio {

	private ApplicationContext context;
	
	public ProduccionServicio() throws Exception {
		context = new ClassPathXmlApplicationContext(
				"classpath:com/schaffner/persistencia/beans/produccion.xml");
	}
	public int eliminarPermiso(Auxiliar aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.eliminarPermiso(aux);
	}
	
	public int eliminarPermisoSubArea(Auxiliar aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.eliminarPermisoSubArea(aux);
	}
	
	public int insertPermiso(Auxiliar aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.insertPermiso(aux);
	}
	
	public int insertPermisoSubArea(Auxiliar aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.insertPermisoSubArea(aux);
	}
	
	public int insertImpresionUsuario(Auxiliar aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.insertImpresionUsuario(aux);
	}
	
	public int insertValPerUsuario(Auxiliar aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.insertValPerUsuario(aux);
	}
	
	public List<Auxiliar> geSubGroupPrincipal(int subGroupPrincipal) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.geSubGroupPrincipal(subGroupPrincipal);
	}
	
	public List<Usuario> geUsuariosXarea(int area) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.geUsuariosXarea(area);
	}
	
	public List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.gePermisosXareaXusuario(groupPrincipal);
	}
	
	public List<Auxiliar> gePermisosSubArea(int groupPrincipal) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.gePermisosSubArea(groupPrincipal);
	}
	
	public List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.getDocAplicableDet(aux);
	}
	
	public String puedeImprimir(Auxiliar idPlano) throws Exception {
		IProduccionDAO pro = (IProduccionDAO) context.getBean("produccionDao");
		return pro.puedeImprimir(idPlano);
	}
}
