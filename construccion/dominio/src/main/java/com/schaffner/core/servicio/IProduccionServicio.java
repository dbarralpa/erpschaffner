package com.schaffner.core.servicio;

import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Bobina;
import com.schaffner.modelo.ingenieria.Costo;
import com.schaffner.modelo.ingenieria.Derivacion;
import com.schaffner.modelo.ingenieria.Diseno;
import com.schaffner.modelo.ingenieria.Distribucion;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;
import com.schaffner.modelo.ingenieria.Especificacion;
import com.schaffner.modelo.ingenieria.EstanqueTapa;
import com.schaffner.modelo.ingenieria.ItemCubicacion;
import com.schaffner.modelo.ingenieria.NucleoFerreteria;
import com.schaffner.modelo.ingenieria.Presspan;
import com.schaffner.modelo.ingenieria.Radiador;
import com.schaffner.modelo.ingenieria.Refrigerante;
import com.schaffner.modelo.ingenieria.TipoAislador;
import com.schaffner.modelo.ingenieria.Tolerancia;
/*CUBICA*/
public interface IProduccionServicio {
	int eliminarPermiso(Auxiliar aux) throws Exception;
	int eliminarPermisoSubArea(Auxiliar aux) throws Exception;
	int insertPermiso(Auxiliar aux) throws Exception;
	int insertPermisoSubArea(Auxiliar aux) throws Exception;
	int insertImpresionUsuario(Auxiliar aux) throws Exception;
	int insertValPerUsuario(Auxiliar aux) throws Exception;
	List<Auxiliar> geSubGroupPrincipal(int subGroupPrincipal) throws Exception;
	List<Usuario> geUsuariosXarea(int area) throws Exception;
	List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception;
	List<Auxiliar> gePermisosSubArea(int groupPrincipal) throws Exception;
	List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception;
	String puedeImprimir(Auxiliar idPlano) throws Exception;
}
