package com.schaffner.core.servicio;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import prueba.Prueba;

import com.schaffner.infraestructure.data.repositories.contracts.IIngenieriaDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Bobina;
import com.schaffner.modelo.ingenieria.Costo;
import com.schaffner.modelo.ingenieria.Derivacion;
import com.schaffner.modelo.ingenieria.Diseno;
import com.schaffner.modelo.ingenieria.Distribucion;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;
import com.schaffner.modelo.ingenieria.Especificacion;
import com.schaffner.modelo.ingenieria.EstanqueTapa;
import com.schaffner.modelo.ingenieria.ItemCubicacion;
import com.schaffner.modelo.ingenieria.NucleoFerreteria;
import com.schaffner.modelo.ingenieria.Presspan;
import com.schaffner.modelo.ingenieria.Radiador;
import com.schaffner.modelo.ingenieria.Refrigerante;
import com.schaffner.modelo.ingenieria.TipoAislador;
import com.schaffner.modelo.ingenieria.Tolerancia;

public class IngenieriaServicio implements IIngenieriaServicio {

	private ApplicationContext context;
	private int RECTANGULAR = 1;
	private int CIRCULAR = 2;
	private int DE_DISCO = 3;
	private int PLETINA = 4;
	private int LAMINA = 5;
	private int diviSor;
	private double RendimientoSeccion;
	private Diseno dis = null;
	private Tolerancia tol = null;
	private Bobina bat = null;
	private Bobina bbt = null;
	private Bobina bte = null;
	private Presspan pbat = null;
	private Presspan pbbt = null;
	private Presspan pbte = null;
	private Bobina bob = new Bobina();
	private Presspan press = new Presspan();
	private Especificacion espe = null;
	private ItemCubicacion itemCubi = null;
	private NucleoFerreteria nucFerr = null;
	private EstanqueTapa estTapa = null;
	private Radiador rad = null;
	private double TolCobreRec, TolCobreCir, TolAceroSil, TolMatAisl,
			TolLiquidoAis, TolEstanque, PotenciaX, perimetro, PesoEsmalte,
			PesoPapel, cantidadcobre, Cantidad, VDolar;
	private double DenPresspan, DenSi, DenCobre, DenAlum, DenNomex, DenMadera,
			KilosPactiva, DenAceroSil, DenEstanque, DenAceite, DenSilicona,
			FImpreg, LitrosAceite, KilosAceite = 0d, DesplVolumetricoCobre = 0,
			DespVolumetricoCobre = 0, KilosPeso, DespVolumetrico;
	private String nDise�oE, Detax, FleXG, Parte2, Parte3, Parte4, Parte5,
			Parte6, Parte7, Parte8, Parte9, Parte10, ParteX;
	private int[] Seplar = new int[6];
	private int[] Separ = new int[6];
	private double[] codSepar = new double[6];
	private int NumParte, tipoXX = 0;
	private String Parte = "";
	private String CModt = "";
	private int bill = 0;
	private String CText;
	private String diseno = "";
	private double KilosRadiadores = 0d;
	private double KilosEstanque = 0;
	private ArrayList<Costo> costos = new ArrayList<Costo>();
	private SchaffnerServicio scha = null;
	IIngenieriaDAO isus = null;
	DecimalFormat decimales = new DecimalFormat("0.00");

	public IngenieriaServicio() throws Exception {
		context = new ClassPathXmlApplicationContext(
				"classpath:com/schaffner/persistencia/beans/ingenieria.xml");
	}

	/* CUBICA */
	public int insertSeco(List<Accesorio> accesorios, Diseno diseno,
			Especificacion especificacion, NucleoFerreteria nucFerr,
			EstanqueTapa estTapa, Bobina bobinabbt, Bobina bobinabat,
			Bobina bobinabte, Presspan presspanBbt, Presspan presspanBat,
			Presspan presspanBte) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertSeco(accesorios, diseno, especificacion, nucFerr,
				estTapa, bobinabbt, bobinabat, bobinabte, presspanBbt,
				presspanBat, presspanBte);
	}

	public int insertAceite(List<Accesorio> accesorios, Diseno diseno,
			Especificacion especificacion, NucleoFerreteria nucFerr,
			EstanqueTapa estTapa, Bobina bobinabbt, Bobina bobinabat,
			Bobina bobinabte, Presspan presspanBbt, Presspan presspanBat,
			Presspan presspanBte, Radiador rad) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertAceite(accesorios, diseno, especificacion, nucFerr,
				estTapa, bobinabbt, bobinabat, bobinabte, presspanBbt,
				presspanBat, presspanBte, rad);
	}

	public int insertAccesorio(Accesorio accesorio) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertAccesorio(accesorio);
	}

	public int actualizarAccesorios(Accesorio accesorio) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarAccesorios(accesorio);
	}

	public int updateSeco(Diseno diseno, Especificacion especificacion,
			NucleoFerreteria nucFerr, EstanqueTapa estTapa, Bobina bobinabbt,
			Bobina bobinabat, Bobina bobinabte, Presspan presspanBbt,
			Presspan presspanBat, Presspan presspanBte) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.updateSeco(diseno, especificacion, nucFerr, estTapa,
				bobinabbt, bobinabat, bobinabte, presspanBbt, presspanBat,
				presspanBte);
	}

	public int updateAceite(Diseno diseno, Especificacion especificacion,
			NucleoFerreteria nucFerr, EstanqueTapa estTapa, Bobina bobinabbt,
			Bobina bobinabat, Bobina bobinabte, Presspan presspanBbt,
			Presspan presspanBat, Presspan presspanBte, Radiador rad)
			throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.updateSeco(diseno, especificacion, nucFerr, estTapa,
				bobinabbt, bobinabat, bobinabte, presspanBbt, presspanBat,
				presspanBte);
	}

	/*
	 * public int actualizarBobinaXdisenoXbobina(Bobina bob) throws Exception {
	 * IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
	 * return ing.actualizarBobinaXdisenoXbobina(bob); }
	 * 
	 * public int actualizarDisenoXdiseno(Diseno diseno) throws Exception {
	 * IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
	 * return ing.actualizarDisenoXdiseno(diseno); }
	 */
	public int actualizarPlano(CargarArchivo arch) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarPlano(arch);
	}

	public int deleteAccesorio(Accesorio acces) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.deleteAccesorio(acces);
	}

	public Diseno getDiseno(String diseno) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDiseno(diseno);
	}

	public Bobina getBobina(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getBobina(aux);
	}

	public NucleoFerreteria getNucleoFerreteria(String diseno) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		// List<Auxiliar> auxs = ing.getCodigosPrueba();
		NucleoFerreteria nucFerr = ing.getNucleoFerreteria(diseno);

		return nucFerr;
	}

	public Especificacion getEspecificacion(String diseno) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEspecificacion(diseno);
	}

	public Presspan getPresspan(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getPresspan(aux);
	}

	public Tolerancia getTolerancia(int id) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTolerancia(id);
	}

	public ItemCubicacion getItemCubicacion(String item) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getItemCubicacion(item);
	}

	public EstanqueTapa getEstanqueTapa(String diseno) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEstanqueTapa(diseno);
	}

	public Radiador getRadiador(String diseno) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getRadiador(diseno);
	}

	public List<Accesorio> getAccesorios(String diseno) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getAccesorios(diseno);
	}

	public List<ItemCubicacion> getItemAdicionales(double potencia)
			throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getItemAdicionales(potencia);
	}

	public List<Distribucion> getDistribuciones(String tipo) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDistribuciones(tipo);
	}

	public List<TipoAislador> getTipoAisladores() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTipoAisladores();
	}

	public List<Refrigerante> getRefrigerantes() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getRefrigerantes();
	}

	public List<Derivacion> getDerivaciones() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDerivaciones();
	}

	public List<Auxiliar> getMaterialesXclasificacionAceite() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaterialesXclasificacionAceite();
	}

	public List<Auxiliar> getMaterialesXclasificacionSeco() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaterialesXclasificacionSeco();
	}

	public List<Auxiliar> getMaterialesPintura() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaterialesPintura();
	}

	public List<Auxiliar> getMaterialesPlanchaFierro() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaterialesPlanchaFierro();
	}

	public List<Auxiliar> getMaterialesPresspan() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaterialesPresspan();
	}

	public List<Auxiliar> getTiposRadiadores() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTiposRadiadores();
	}

	public List<Auxiliar> getMaterialesRadiador(int tipo) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaterialesRadiador(tipo);
	}

	public List<Auxiliar> getAceroSilicoso() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getAceroSilicoso();
	}

	public List<Auxiliar> getTipoSoporte() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTipoSoporte();
	}

	public List<Auxiliar> getTipoNucleo() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTipoNucleo();
	}

	public List<Auxiliar> getAislaciones() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getAislaciones();
	}

	public List<Distribucion> getDistribucionesAll() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDistribucionesAll();
	}

	public List<Auxiliar> getDisenoBusqueda() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDisenoBusqueda();
	}

	public List<Auxiliar> getTablaC1() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTablaC1();
	}

	public List<Auxiliar> getTablaC2() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTablaC2();
	}

	public List<Auxiliar> getTablaC3() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTablaC3();
	}

	public List<Auxiliar> getTablaC4() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getTablaC4();
	}

	public int getMaxCodigoDiseno(String diseno) throws Exception {
		if (((IIngenieriaDAO) context.getBean("ingenieriaDao"))
				.getMaxCodigoDiseno(diseno) != null) {
			return ((IIngenieriaDAO) context.getBean("ingenieriaDao"))
					.getMaxCodigoDiseno(diseno);
		} else {
			return 1;
		}
	}

	public List<Auxiliar> getCodigosPrueba() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getCodigosPrueba();
	}

	public List<Costo> cubicacion(String diseno) throws Exception {
		isus = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		this.diseno = diseno;
		scha = new SchaffnerServicio();
		// diseno = "C229919901";
		// diseno = "T410101501";
		dis = isus.getDiseno(diseno);
		tol = isus.getTolerancia(1);
		espe = isus.getEspecificacion(diseno);
		Cubixx();
		return costos;
	}

	/*
	 * public static void main(String[] args) { //
	 * System.out.println(Math.sqrt(0.03 * 3.67 * 3.79 / (1 - 3.1416 / 4))); new
	 * Prueba();
	 * 
	 * }
	 */

	private void cubicarAceite() throws Exception {
		Auxiliar aux = new Auxiliar();
		aux.setAux1(2);
		aux.setAux9(diseno);
		bat = isus.getBobina(aux);
		aux.setAux1(1);
		bbt = isus.getBobina(aux);
		aux.setAux1(3);
		bte = isus.getBobina(aux);
		datos();
		NumParte = 1;
		Parte = "BOBINA DE BAJA TENSION";
		CModt = " BT - N�cleo";
		CubicaBXX(1);
		NumParte = 2;
		Parte = "BOBINA DE ALTA TENSION";
		CModt = " BT - AT";
		CubicaBXX(2);
		NumParte = 3;
		Parte = "BOBINA TERCIARIA";
		CModt = " BT - TE";
		CubicaBXX(3);
		diviSor = 1;
		NumParte = 4;
		Parte = "NUCLEO";
		CubicaNucleo();
		NumParte = 5;
		Parte = "FERRETERIA";
		// Call CubicaFerreteria;
		NumParte = 6;
		Parte = "ESTANQUE Y TAPA";
		estanqueyTapa();
		if (dis.getPotencia() < 600 && dis.getFase() == 3) {
			AcumCosto22();
		}
		NumParte = 7;
		Parte = "RADIADORES";
		RadAceite();
		NumParte = 8;
		Parte = "DISPOSICION DE AISLACIONES";
		Aislaciones();
		NumParte = 9;
		Parte = "REFRIGERANTE";
		Refrigerante();
		NumParte = 10;
		Parte = "ACCESORIOS";
		ACCESORIOS();
		/*
		 * Diseno dise = new Diseno(); dise.setKilosPeso(KilosPeso);
		 * dise.setKilosRadiadores(KilosRadiadores);
		 * dise.setKilosEstanque(KilosEstanque);
		 * dise.setLitrosAceite(LitrosAceite); dise.setKilosAceite(KilosAceite);
		 * dise.setKilosPactiva(KilosPactiva); dise.setIdDiseno(diseno);
		 * dise.setCubic(100); int as = isus.actualizarDisenoXdiseno(dise);
		 */
		//imprimir();

	}

	private void ACCESORIOS() throws Exception {
		List<Accesorio> acces = isus.getAccesorios(diseno);
		Parte2 = "XXX";
		FleXG = "ENV";

		for (int i = 0; i < acces.size(); i++) {
			Cantidad = acces.get(i).getCantidad();
			if (Cantidad > 0) {
				AcumCosto(acces.get(i).getCodigo());
			}
		}

	}

	private void Refrigerante() throws Exception {

		/*
		 * '==========================================================================
		 * ' Calculo refrigerante
		 * '==========================================================================
		 */
		// 'Volumen de Estanque
		double VolumenEstanque = 0;
		double VolumenRadiadores = 0;

		if (estTapa.getDiametroCircular() > 0) {
			/*
			 * VolumenEstanque = 3.14 / 4 *
			 * Math.pow((estTapa.getDiametroCircular()), 2 *
			 * (estTapa.getAltoEstanque() - estTapa.getAltoColchon())
			 * Math.pow(10, -6));
			 */
			VolumenEstanque = 3.14 / 4
					* Math.pow((estTapa.getDiametroCircular()), 2)
					* (estTapa.getAltoEstanque() - estTapa.getAltoColchon())
					* Math.pow(10, -6);
		} else {
			if (estTapa.getTipo() == '1') {
				VolumenEstanque = (Math.pow(
						(estTapa.getLargoEstanque() - estTapa
								.getAnchoEstanque())
								* estTapa.getAnchoEstanque()
								+ estTapa.getAnchoEstanque(), 2 / 4 * 3.1416))
						* (estTapa.getAltoEstanque() - estTapa.getAltoColchon())
						* Math.pow(10, -6);
			} else {
				VolumenEstanque = estTapa.getLargoEstanque()
						* estTapa.getAnchoEstanque()
						* (estTapa.getAltoEstanque() - estTapa.getAltoColchon())
						* Math.pow(10, -6);
			}
		}
		if (rad.getCodTipoRadiador() == 5) {
			// VolumenRadiadores = rad.getMetrosTubo() * 0.96;
		} else {
			VolumenRadiadores = rad.getLitros() * rad.getNumRadiadores()
					* rad.getNumElementos();
		}

		// ' Aceite
		Cantidad = (VolumenEstanque + VolumenRadiadores - DespVolumetrico)
				* TolLiquidoAis;
		if (dis.getIdRefrigerante() == 1) {
			Parte2 = "TEN";
			FleXG = "ENV";
			AcumCosto("9012003010");
			LitrosAceite = Cantidad;
			KilosAceite = LitrosAceite * DenAceite;
		} else {
			if (dis.getIdRefrigerante() == 2) {
				// ' Peso Especifico Silicona
				LitrosAceite = Cantidad;
				Cantidad = Cantidad * DenSilicona;
				KilosAceite = Cantidad;
				Parte2 = "TEN";
				FleXG = "ENV";
				AcumCosto("9012003020");
			} else {
				if (dis.getIdRefrigerante() == 3) {
					// ' Peso Especifico FR3
					LitrosAceite = Cantidad;
					KilosAceite = Cantidad * 0.92;
					Parte2 = "TEN";
					FleXG = "ENV";
					AcumCosto("9012003030");
				}
			}
		}
		KilosPeso = KilosPeso + KilosAceite;
	}

	private void Aislaciones() throws Exception {
		/*
		 * ' ' Disposicion de Aislaciones ' Z_CT, Cantidad de tacos ' Z_LTC,
		 * Largo de tacos ' Presspan de 1.5 mm (Anillos)
		 * 
		 * '======================================================================
		 * ' Pantallas OK SEMI
		 * '======================================================================
		 */
		// Csql = "SELECT * FROM Especificaciones WHERE Dise�o = '" +
		// Dise�o.Dise�o + "'"
		// Set rsEspecificaciones = BD_SISTEMA.OpenRecordset(Csql)
		double espan = 0d;
		Cantidad = 0;
		if (nucFerr.getDiametroNucleo() != 0) {
			Cantidad = DenPresspan * TolMatAisl * dis.getFase() * 1.5
					* (Math.pow(nucFerr.getEntreCentroNucleo(), 2))
					* Math.pow(10, -6);
		}

		// ' Presspan de 3.0 mm (Pantallas AT-AT)
		if (espe.getNivelImpulso2() >= 0 && espe.getNivelImpulso2() <= 60) {
			espan = 1.5;
		}
		if (espe.getNivelImpulso2() >= 61 && espe.getNivelImpulso2() <= 95) {
			espan = 3;
		}

		if (espe.getNivelImpulso2() >= 96 && espe.getNivelImpulso2() <= 150) {
			espan = 4.5;
		}

		if (espe.getNivelImpulso2() >= 200 && espe.getNivelImpulso2() <= 50000) {
			espan = 6;
		}

		if (nucFerr.getDiametroNucleo() != 0) {
			Cantidad = Cantidad + DenPresspan * TolMatAisl
					* (dis.getFase() - 1) * nucFerr.getEntreCentroNucleo()
					* nucFerr.getVentanaNucleo() * espan * Math.pow(10, -6);
		} else {
			Cantidad = Cantidad + DenPresspan * TolMatAisl
					* (dis.getFase() - 1) * (estTapa.getAnchoEstanque() * 0.85)
					* nucFerr.getVentanaNucleo() * espan * Math.pow(10, -6);
		}
		Detax = "pantallas";
		Parte2 = "PAC";
		FleXG = "APA";
		AcumCosto("9001001010");
		DespVolumetrico = DespVolumetrico + Cantidad / (TolMatAisl * 1.25);
		KilosPeso = KilosPeso + Cantidad / TolMatAisl;
		KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;

		/*
		 * '=========================================================================
		 * ' Coductores Aisladores OK SEMI
		 * '=========================================================================
		 */
		// ' Cobre Terminal AT
		double largocable;
		if (dis.getAislador() == 1) {
			largocable = dis.getFase()
					* (estTapa.getAltoEstanque() - nucFerr.getVentanaNucleo() - 2 * nucFerr
							.getChapaMayorNucleo());
		} else {
			// ' Subestacion unitaria
			largocable = dis.getFase()
					* (estTapa.getAltoEstanque() - nucFerr.getVentanaNucleo()
							- 2 * nucFerr.getChapaMayorNucleo() + 1.5 * estTapa
							.getLargoEstanque());
		}
		// ' tipo y peso
		double cobrrre;
		if (dis.getCodCondBat() == CIRCULAR) {
			largocable = Math.pow(bat.getDimensionAxial(), 2) * 3.1416 / 4
					* largocable;
		} else {
			largocable = bat.getDimensionAxial() * bat.getDimensionRadial()
					* largocable;
		}
		cubicaCupp(bat.getCodigoCondCu());
		Cantidad = largocable * DenSi * Math.pow(10, -6);
		cobrrre = Cantidad;
		Detax = "salida aislador AT";
		AcumCosto(bat.getCodigoCondCu());
		DespVolumetrico = DespVolumetrico + Cantidad / DenSi;
		KilosPeso = KilosPeso + Cantidad;
		KilosPactiva = KilosPactiva + Cantidad;

		// ' CU Terminal BT
		double secc;
		if (dis.getCodCondBbt() == CIRCULAR) {
			secc = Math.pow(bbt.getDimensionAxial(), 2) * 3.1416 / 4;
		} else {
			secc = bbt.getDimensionAxial() * bbt.getDimensionRadial();
		}
		cubicaCupp(bbt.getCodigoCondCu());
		if (dis.getAislador() == 1) {
			Cantidad = secc
					* 2
					* DenSi
					* bbt.getCondParalelo()
					* dis.getFase()
					* (estTapa.getAltoEstanque() - 2 * nucFerr
							.getChapaMayorNucleo()) * Math.pow(10, -6);
		} else {
			Cantidad = secc
					* 2
					* DenSi
					* bbt.getCondParalelo()
					* dis.getFase()
					* (estTapa.getLargoEstanque() / 2 + nucFerr
							.getChapaMayorNucleo()) * Math.pow(10, -6);
		}
		cobrrre = Cantidad + cobrrre;
		Detax = "salida aislador BT";
		AcumCosto(bbt.getCodigoCondCu());
		DespVolumetrico = DespVolumetrico + Cantidad / DenSi;
		KilosPeso = KilosPeso + Cantidad;
		KilosPactiva = KilosPactiva + Cantidad;

		/*
		 * '============================================================================
		 * ' Soldaduras OK SEMI
		 * '============================================================================
		 */
		// ' Soldadura de plata
		Cantidad = cobrrre * 0.01;
		AcumCosto("9006001020");

		// ' Soldadura esta�o
		Cantidad = cobrrre * 0.1;
		AcumCosto("9006001010");

		// ' Spaguetti papel crepe AT
		if (bat.getCondAxiales() == 2) {
			Cantidad = largocable * 0.001;
			// ' AcumCosto 650
		} else {
			if (bat.getCondAxiales() == 1) {
				if ((bat.getDimensionAxial() + bat.getDimensionRadial()) < 15) {
					Cantidad = largocable * 0.001;
					// ' AcumCosto 650
				} else {
					Cantidad = largocable * 0.001;
					// ' AcumCosto 650
				}
			}
		}

		// 'Cinta Papel crepe
		Cantidad = ((double) Math
				.round((0.5 * Math.sqrt(dis.getPotencia() / 1000)) * 100)) / 100;
		AcumCosto("9001004010");

		// 'Amarras plasticas
		if (dis.getPotencia() >= 0 && dis.getPotencia() <= 100) {
			Cantidad = 15;
		}
		if (dis.getPotencia() >= 101 && dis.getPotencia() <= 500000) {
			Cantidad = (int) 10 + 15 * dis.getPotencia() / 300;
		}

		AcumCosto("9015005120");
	}

	private void RadAceite() throws Exception {
		if (rad.getCodTipoRadiador() > 0) {
			switch (rad.getCodTipoRadiador()) {
			case 1:
				Cantidad = rad.getNumRadiadores();
				KilosPeso = rad.getKilo() * rad.getNumRadiadores()
						* rad.getNumElementos() + KilosPeso;
				KilosRadiadores = rad.getKilo();
				break;
			case 2:
				Cantidad = rad.getNumElementos() * rad.getNumRadiadores()
						* rad.getKilo() * TolEstanque;
				KilosRadiadores = Cantidad;
				KilosPeso = Cantidad + KilosPeso;
				break;
			case 3:
				Cantidad = rad.getNumRadiadores();
				KilosPeso = rad.getKilo() * rad.getNumRadiadores()
						* rad.getNumElementos() + KilosPeso;
				KilosRadiadores = rad.getKilo();
				break;
			case 4:
				Cantidad = rad.getNumElementos() * rad.getNumRadiadores()
						* rad.getKilo() * TolEstanque;
				KilosRadiadores = Cantidad;
				KilosPeso = Cantidad + KilosPeso;
				break;
			/*
			 * case 5: Cantidad = rad.getMetrosTubo() * 1.35 * TolEstanque;
			 * KilosRadiadores = Cantidad; KilosPeso = Cantidad + KilosPeso;
			 * break;
			 */
			}
			Parte2 = "KRA";
			FleXG = "EST";
			Detax = "Radiadores";
			if (Long.parseLong(rad.getCodigoRadiador()) >= 0
					&& Long.parseLong(rad.getCodigoRadiador()) <= 9100000000d) {
				// ' AcumCosto Val(Radiadores.CodigoRadiador)
			}
			if (Long.parseLong(rad.getCodigoRadiador()) >= 9100000001d
					&& Long.parseLong(rad.getCodigoRadiador()) <= 9200000000d) {
				AcumCosto("9008001020");
			}
			if (Long.parseLong(rad.getCodigoRadiador()) >= 9200000001d
					&& Long.parseLong(rad.getCodigoRadiador()) <= 9500000010d) {
				AcumCosto("9008002020");
			}

			Detax = "";
		}
	}

	private void AcumCosto22() throws Exception {

		List<ItemCubicacion> itemAdic = isus.getItemAdicionales(dis
				.getPotencia());
		// While itemAdic

		// INSERTAR COSTOS

		for (int i = 0; i < itemAdic.size(); i++) {
			FleXG = itemAdic.get(i).getArea();
			Cantidad = itemAdic.get(i).getCant();
			AcumCosto22(itemAdic.get(i).getItem());
		}

		/*
		 * Costos.AddNew Costos.Dise�o = nDise�o Costos.flex = rs3!Area 'FleXG
		 * Costos.USUSARIO = UserX Costos.Parte2 = ParteX + Parte2
		 * Costos.NumParte = NumParte Costos.Parte = Parte
		 * 
		 * Costos.CodigoMaterial = rs3!CODIGOCUB Costos.Cantidad = rs3!cant
		 * Costos.diviSor = 1 Costos.Area = 0 Costos.Unidad = rs3!Unidad
		 * Costos.DescripcionMaterial = rs3!Descripcion Costos.preciocosto =
		 * rs3!dolar Costos.TotalCosto = (rs3!cant * rs3!dolar)
		 */
		//

		// Costos.Update
	}

	private void estanqueyTapa() throws Exception {
		rad = isus.getRadiador(diseno);

		if (estTapa.getDiametroCircular() > 0) {
			// 'tapa
			Cantidad = Math.pow(estTapa.getDiametroCircular(), 2) * 3.1416 / 4
					* estTapa.getEspTapa() * TolEstanque * DenEstanque
					* Math.pow(10, -6);

			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			Parte2 = "TPT";
			FleXG = "TAP";
			// Detax = "tapa"
			AcumCosto(estTapa.getCodTapa());
			// 'manto
			Cantidad = estTapa.getDiametroCircular() * 3.1416
					* (estTapa.getAltoEstanque() + estTapa.getAltoBaseFalsa())
					* estTapa.getEspManto() * TolEstanque * DenEstanque
					* Math.pow(10, -6);
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			// Detax = "manto";
			Parte2 = "KEP";
			FleXG = "EST";
			AcumCosto(estTapa.getCodManto());
			// 'fondo
			Cantidad = Math.pow(estTapa.getDiametroCircular(), 2) * 3.1416 / 4
					* estTapa.getEspFondo() * TolEstanque * DenEstanque
					* Math.pow(10, -6);
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			// Detax = "fondo"
			Parte2 = "KEP";
			FleXG = "EST";
			AcumCosto(estTapa.getCodFondo());
		} else {
			// 'tapa
			if (estTapa.getTipo() == '1') {
				Cantidad = (Math.pow(
						(estTapa.getLargoEstanque() - estTapa
								.getAnchoEstanque())
								* estTapa.getAnchoEstanque()
								+ estTapa.getAnchoEstanque(), 2 / 4 * 3.1416))
						* estTapa.getEspTapa()
						* TolEstanque
						* DenEstanque
						* Math.pow(10, -6);
			} else {
				Cantidad = estTapa.getLargoEstanque()
						* estTapa.getAnchoEstanque() * estTapa.getEspTapa()
						* TolEstanque * DenEstanque * Math.pow(10, -6);
			}
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			// Detax = "tapa"
			Parte2 = "TPT";
			FleXG = "TAP";
			AcumCosto(estTapa.getCodTapa());
			// 'manto
			if (estTapa.getTipo() == '1') {
				Cantidad = 2
						* (estTapa.getLargoEstanque()
								- estTapa.getAnchoEstanque() + estTapa
								.getAnchoEstanque() * 3.1416 / 2)
						* (estTapa.getAltoEstanque() + estTapa
								.getAltoBaseFalsa()) * estTapa.getEspManto()
						* TolEstanque * DenEstanque * Math.pow(10, -6);
			} else {
				Cantidad = 2
						* (estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque())
						* (estTapa.getAltoEstanque() + estTapa
								.getAltoBaseFalsa()) * estTapa.getEspManto()
						* TolEstanque * DenEstanque * Math.pow(10, -6);
			}
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			// Detax = "manto"
			Parte2 = "KEP";
			FleXG = "EST";
			AcumCosto(estTapa.getCodManto());
			// 'fondo
			if (estTapa.getTipo() == '1') {
				Cantidad = (Math.pow(
						(estTapa.getLargoEstanque() - estTapa
								.getAnchoEstanque())
								* estTapa.getAnchoEstanque()
								+ estTapa.getAnchoEstanque(), 2 / 4 * 3.1416))
						* estTapa.getEspFondo()
						* TolEstanque
						* DenEstanque
						* Math.pow(10, -6);
			} else {
				Cantidad = estTapa.getLargoEstanque()
						* estTapa.getAnchoEstanque() * estTapa.getEspFondo()
						* TolEstanque * DenEstanque * Math.pow(10, -6);
			}
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			// Detax = "fondo"
			Parte2 = "KEP";
			FleXG = "EST";
			AcumCosto(estTapa.getCodFondo());
		}

		// ' AcumCosto 400

		// '==========================================================================
		// ' Marco OK SEMI
		// '==========================================================================

		double EspMarco, AnchMarco;
		Parte2 = "KEP";
		FleXG = "EST";
		Detax = "marco";
		if (estTapa.getTipo() == '1') {
			if (dis.getPotencia() >= 0 && dis.getPotencia() <= 100) {
				EspMarco = 6.4;
				AnchMarco = 38.1;
				Cantidad = EspMarco
						* AnchMarco
						* (estTapa.getLargoEstanque() - estTapa
								.getAnchoEstanque()) * 2 * TolEstanque
						* DenEstanque * Math.pow(10, -6);
				AcumCosto("9008003010");
				KilosEstanque = Cantidad + KilosEstanque;
				KilosPeso = Cantidad + KilosPeso;
				Cantidad = 6 * AnchMarco
						* (estTapa.getAnchoEstanque() + AnchMarco) * 3.1416
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008001060");
			}

			if (dis.getPotencia() >= 101 && dis.getPotencia() <= 300) {
				EspMarco = 8;
				AnchMarco = 38.1;
				Cantidad = EspMarco
						* AnchMarco
						* (estTapa.getLargoEstanque()
								+ estTapa.getAnchoEstanque() + AnchMarco) * 2
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008003020");
				KilosEstanque = Cantidad + KilosEstanque;
				KilosPeso = Cantidad + KilosPeso;
				Cantidad = 8 * AnchMarco
						* (estTapa.getAnchoEstanque() + AnchMarco) * 3.1416
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008001070");
			}

			if (dis.getPotencia() >= 301 && dis.getPotencia() <= 500000) {
				EspMarco = 8;
				AnchMarco = 50.8;
				Cantidad = EspMarco
						* AnchMarco
						* (estTapa.getLargoEstanque()
								+ estTapa.getAnchoEstanque() + AnchMarco) * 2
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008003030");
				KilosEstanque = Cantidad + KilosEstanque;
				KilosPeso = Cantidad + KilosPeso;
				Cantidad = 8 * AnchMarco
						* (estTapa.getAnchoEstanque() + AnchMarco) * 3.1416
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008001070");
			}

		} else {
			if (dis.getPotencia() >= 0 && dis.getPotencia() <= 100) {
				EspMarco = 6.4;
				AnchMarco = 38.1;
				Cantidad = EspMarco
						* AnchMarco
						* (estTapa.getLargoEstanque()
								+ estTapa.getAnchoEstanque() + AnchMarco) * 2
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008003010");
			}

			if (dis.getPotencia() >= 101 && dis.getPotencia() <= 300) {
				EspMarco = 8;
				AnchMarco = 38.1;
				Cantidad = EspMarco
						* AnchMarco
						* (estTapa.getLargoEstanque()
								+ estTapa.getAnchoEstanque() + AnchMarco) * 2
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008003020");
			}

			if (dis.getPotencia() >= 301 && dis.getPotencia() <= 500000) {
				EspMarco = 8;
				AnchMarco = 50.8;
				Cantidad = EspMarco
						* AnchMarco
						* (estTapa.getLargoEstanque()
								+ estTapa.getAnchoEstanque() + AnchMarco) * 2
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				AcumCosto("9008003030");
			}

		}
		KilosEstanque = Cantidad + KilosEstanque;
		KilosPeso = Cantidad + KilosPeso;

		// '==========================================================================
		// ' ACCESORIOS estanque OK SEMI
		// '==========================================================================

		// ' Empaquetadura rodon ovalado
		if (estTapa.getDiametroCircular() > 0) {
			Cantidad = 3.14 * (estTapa.getDiametroCircular() + 25) * 0.001;
		} else {
			if (estTapa.getTipo() == '1') {
				Cantidad = 2 * (estTapa.getLargoEstanque()
						- estTapa.getAnchoEstanque() + (estTapa
						.getAnchoEstanque() + 50) / 2 * 3.1416) * 0.001;
			} else {
				Cantidad = 2 * (estTapa.getLargoEstanque()
						+ estTapa.getAnchoEstanque() + 50) * 0.001;
			}
		}
		Parte2 = "TEN";
		FleXG = "ENV";
		AcumCosto("9015005130");

		// '==========================================================================
		// ' Calculo superficie pintura
		// '==========================================================================

		double SuperficieRadiador = 0d;
		// ' De poder
		if (rad.getCodTipoRadiador() < 4) {
			SuperficieRadiador = 1.05 * 2 * rad.getNumRadiadores()
					* rad.getNumElementos() * rad.getAltoRadiador()
					* rad.getAnchoRadiador() * Math.pow(10, -6);
		}
		// ' Sumergible
		if (rad.getCodTipoRadiador() == 4) {
			SuperficieRadiador = 2 * rad.getNumRadiadores()
					* rad.getNumElementos() * (rad.getAltoRadiador() + 300)
					* rad.getAnchoRadiador() * Math.pow(10, -6);
		}
		// ' Tubo
		if (rad.getCodTipoRadiador() == 5) {
			// SuperficieRadiador = rad.getMetrosTubo() * 0.12;
		}

		// ' Superficie de estanque (para pintura)
		double Supext, Supint;
		String Area;
		if (estTapa.getDiametroCircular() > 0) {
			Supext = 3.14
					* estTapa.getDiametroCircular()
					* (estTapa.getAltoEstanque() + estTapa.getAltoBaseFalsa()
							* 2 + estTapa.getDiametroCircular() / 2)
					* Math.pow(10, -6);
			Supint = 3.14
					* estTapa.getDiametroCircular()
					* (estTapa.getAltoEstanque() + estTapa
							.getDiametroCircular() / 4) * Math.pow(10, -6);
		} else {
			if (estTapa.getTipo() == '1') {
				Supext = 2
						* (Math.pow(
								(estTapa.getLargoEstanque()
										- estTapa.getAnchoEstanque() + estTapa
										.getAnchoEstanque() / 2 * 3.1616)
										* (estTapa.getAltoEstanque() + 2 * estTapa
												.getAltoBaseFalsa())
										+ (estTapa.getLargoEstanque() - estTapa
												.getAnchoEstanque())
										* estTapa.getAnchoEstanque()
										+ estTapa.getAnchoEstanque(),
								2 / 8 * 3.1416)) * Math.pow(10, -6);
				Supint = 2
						* (Math.pow(
								(estTapa.getLargoEstanque()
										- estTapa.getAnchoEstanque() + estTapa
										.getAnchoEstanque() / 2 * 3.1616)
										* (estTapa.getAltoEstanque())
										+ (estTapa.getLargoEstanque() - estTapa
												.getAnchoEstanque())
										* estTapa.getAnchoEstanque()
										+ estTapa.getAnchoEstanque(),
								2 / 8 * 3.1416)) * Math.pow(10, -6);
			} else {
				Supext = 2
						* ((estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque())
								* (estTapa.getAltoEstanque() + 2 * estTapa
										.getAltoBaseFalsa()) + estTapa
								.getLargoEstanque()
								* estTapa.getAnchoEstanque())
						* Math.pow(10, -6);
				Supint = 2
						* ((estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque())
								* (estTapa.getAltoEstanque()) + estTapa
								.getLargoEstanque()
								* estTapa.getAnchoEstanque())
						* Math.pow(10, -6);
			}
		}

		// ' Pintura estanque/radiador
		// ItemCubicacion it1 = isus.getItemCubicacion(estTapa.getPin1());
		// if (it1 != null && it1.getUnidad().equals("KG")) {
		Auxiliar auxMaterial1 = scha.detalleMaterialesSchaffner(estTapa
				.getPin1());
		if (auxMaterial1.getAux11() != null
				&& auxMaterial1.getAux11().trim().equals("KG")) {
			Cantidad = (Supint) / 7;
		} else {
			Cantidad = 2 * (Supint) / 18 / 1.25;
		}
		if (!estTapa.getPin1().trim().equals("0")) {
			Area = "1";
			Detax = "interior";
			Parte2 = "EST";
			FleXG = "EST";
			AcumCosto(estTapa.getPin1());
		}

		// ItemCubicacion it2 = isus.getItemCubicacion(estTapa.getPin2());
		// if (it2 != null && it2.getUnidad().equals("KG")) {
		Auxiliar auxMaterial2 = scha.detalleMaterialesSchaffner(estTapa
				.getPin2());
		if (auxMaterial2.getAux11() != null
				&& auxMaterial2.getAux11().trim().equals("KG")) {
			Cantidad = (Supint) / 7;
		} else {
			Cantidad = 2 * (Supint) / 18 / 1.25;
		}
		if (!estTapa.getPin2().trim().equals("0")) {
			Area = "1";
			Detax = "interior";
			Parte2 = "EST";
			FleXG = "EST";
			AcumCosto(estTapa.getPin2());
		}
		// ItemCubicacion it3 = isus.getItemCubicacion(estTapa.getPin3());
		// if (it3 != null && it3.getUnidad().equals("KG")) {
		Auxiliar auxMaterial3 = scha.detalleMaterialesSchaffner(estTapa
				.getPin3());
		if (auxMaterial3.getAux11() != null
				&& auxMaterial3.getAux11().trim().equals("KG")) {
			Cantidad = (Supext + SuperficieRadiador) / 7;
		} else {
			Cantidad = 2 * (Supext + SuperficieRadiador) / 18 / 1.25;
		}
		if (!estTapa.getPin3().trim().equals("0")) {
			Area = "1";
			Parte2 = "EST";
			FleXG = "EST";
			Detax = "exterior";
			AcumCosto(estTapa.getPin3());
			// ' Parte2 = "TTE"
			// ' FleXG = "LTX"
			// ' Detax = "terminaciones"
			// ' AcumCosto EstanqueTapa.pin3
		}
		// ItemCubicacion it4 = isus.getItemCubicacion(estTapa.getPin4());
		// if (it4 != null && it4.getUnidad().equals("KG")) {
		Auxiliar auxMaterial4 = scha.detalleMaterialesSchaffner(estTapa
				.getPin4());
		if (auxMaterial4.getAux11() != null
				&& auxMaterial4.getAux11().trim().equals("KG")) {
			Cantidad = 2 * (Supext + SuperficieRadiador) / 7;
		} else {
			Cantidad = 2 * (Supext + SuperficieRadiador) / 18 / 1.25;
		}
		if (!estTapa.getPin4().trim().equals("0")) {
			Area = "1";
			Parte2 = "EST";
			FleXG = "EST";
			Detax = "exterior";
			AcumCosto(estTapa.getPin4());
			Parte2 = "TTE";
			FleXG = "LTX";
			Detax = "terminaciones";
			AcumCosto(estTapa.getPin4());
		}

		// ' Pernos de la tapa
		if (estTapa.getDiametroCircular() > 0) {
			Cantidad = (3.14 * estTapa.getDiametroCircular()) / 75;
		} else {
			if (estTapa.getTipo() == '1') {
				Cantidad = 2 * (estTapa.getLargoEstanque()
						- estTapa.getAnchoEstanque() + estTapa
						.getAnchoEstanque() / 2 * 3.1416) / 95;
			} else {
				Cantidad = 2 * (estTapa.getLargoEstanque() + estTapa
						.getAnchoEstanque()) / 95;
			}
		}
		Cantidad = (int) (Cantidad);
		if (dis.getPotencia() <= 500) {
			Parte2 = "TEN";
			FleXG = "ENV";
			AcumCosto("9011001010");
			AcumCosto("9011004010");
			AcumCosto("9011002010");
			Cantidad = Cantidad * 2;
			AcumCosto("9011003010");
		} else {
			Parte2 = "TEN";
			FleXG = "ENV";
			AcumCosto("9011001020");
			AcumCosto("9011004020");
			AcumCosto("9011002020");
			Cantidad = Cantidad * 2;
			AcumCosto("9011003020");
		}

		// ' Soldadura
		Cantidad = KilosEstanque * 0.05 * 0.7;
		Detax = "MIG";
		Parte2 = "KEP";
		FleXG = "EST";
		AcumCosto("9006001030");

		Cantidad = KilosEstanque * 0.05 * 0.3;
		Detax = "TIG";
		Parte2 = "KEP";
		AcumCosto("9006001040");
	}

	private void CubicaNucleo() throws Exception {
		double PesoNucleo = 0d;
		double pesonucleo2 = 0d;
		double pesonucleo3 = 0d;
		double pesonucleo4 = 0d;
		double pesonucleo5 = 0d;
		double pesonucleo6 = 0d;
		double pesonucleo7 = 0d;
		double pesonucleo8 = 0d;
		double pesonucleo9 = 0d;
		double pesonucleo10 = 0d;
		estTapa = isus.getEstanqueTapa(diseno);
		// '==============================================================================
		// ' Peso N�cleo OK SEMI
		// '==============================================================================

		// ' N�cleo enrollado
		// if Mid(Str(NucleoFerreteria.CodigoSilicoso1), 2, 7) = "9005001" Then
		if (nucFerr.getCodigoSilicoso1().substring(0, 7).equals("9005001")) {
			Auxiliar auxSilicoso1 = scha.detalleMaterialesSchaffner(nucFerr
					.getCodigoSilicoso1());
			if (auxSilicoso1.getAux11() != null
					&& auxSilicoso1.getAux11().trim().equals("KG")) {
				Cantidad = nucFerr.getCantidad1() * nucFerr.getPesoNucleo1();
			} else {
				Cantidad = nucFerr.getCantidad1();
				Parte2 = "PAC";
				FleXG = "APA";
			}
			PesoNucleo = nucFerr.getPesoNucleo1();
			AcumCosto(nucFerr.getCodigoSilicoso1());
			DespVolumetrico = DespVolumetrico + PesoNucleo / DenAceroSil;
			KilosPeso = KilosPeso + PesoNucleo;
			KilosPactiva = KilosPactiva + PesoNucleo;
			if (!nucFerr.getCodigoSilicoso2().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso2().trim()) > 0) {
				Auxiliar auxSilicoso2 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso2());
				if (auxSilicoso2.getAux11() != null
						&& auxSilicoso2.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad2()
							* nucFerr.getPesoNucleo2();
				} else {
					Cantidad = nucFerr.getCantidad2();
					Parte2 = "PAC";
					FleXG = "APA";
				}
				Cantidad = nucFerr.getCantidad2();
				PesoNucleo = nucFerr.getPesoNucleo2();
				AcumCosto(nucFerr.getCodigoSilicoso2());
				DespVolumetrico = DespVolumetrico + pesonucleo2 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo2;
				KilosPactiva = KilosPactiva + pesonucleo2;
			}
			if (!nucFerr.getCodigoSilicoso3().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso3().trim()) > 0) {
				Auxiliar auxSilicoso3 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso3());
				if (auxSilicoso3.getAux11() != null
						&& auxSilicoso3.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad3()
							* nucFerr.getPesoNucleo3();
				} else {
					Cantidad = nucFerr.getCantidad3();
					Parte3 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad3();
				PesoNucleo = nucFerr.getPesoNucleo3();
				AcumCosto(nucFerr.getCodigoSilicoso3());
				DespVolumetrico = DespVolumetrico + pesonucleo3 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo3;
				KilosPactiva = KilosPactiva + pesonucleo3;
			}
			if (!nucFerr.getCodigoSilicoso4().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso4().trim()) > 0) {
				Auxiliar auxSilicoso4 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso4());
				if (auxSilicoso4.getAux11() != null
						&& auxSilicoso4.getAux11().equals("KG")) {
					Cantidad = nucFerr.getCantidad4()
							* nucFerr.getPesoNucleo4();
				} else {
					Cantidad = nucFerr.getCantidad4();
					Parte4 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad4();
				PesoNucleo = nucFerr.getPesoNucleo4();
				AcumCosto(nucFerr.getCodigoSilicoso4());
				DespVolumetrico = DespVolumetrico + pesonucleo4 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo4;
				KilosPactiva = KilosPactiva + pesonucleo4;
			}
			if (!nucFerr.getCodigoSilicoso5().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso5().trim()) > 0) {
				Auxiliar auxSilicoso5 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso5());
				if (auxSilicoso5.getAux11() != null
						&& auxSilicoso5.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad5()
							* nucFerr.getPesoNucleo5();
				} else {
					Cantidad = nucFerr.getCantidad5();
					Parte5 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad5();
				PesoNucleo = nucFerr.getPesoNucleo5();
				AcumCosto(nucFerr.getCodigoSilicoso5());
				DespVolumetrico = DespVolumetrico + pesonucleo5 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo5;
				KilosPactiva = KilosPactiva + pesonucleo5;
			}
			if (!nucFerr.getCodigoSilicoso6().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso6().trim()) > 0) {
				Auxiliar auxSilicoso6 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso6());
				if (auxSilicoso6.getAux11() != null
						&& auxSilicoso6.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad6()
							* nucFerr.getPesoNucleo6();
				} else {
					Cantidad = nucFerr.getCantidad6();
					Parte6 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad6();
				PesoNucleo = nucFerr.getPesoNucleo6();
				AcumCosto(nucFerr.getCodigoSilicoso6());
				DespVolumetrico = DespVolumetrico + pesonucleo6 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo6;
				KilosPactiva = KilosPactiva + pesonucleo6;
			}
			if (!nucFerr.getCodigoSilicoso7().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso7().trim()) > 0) {
				Auxiliar auxSilicoso7 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso7());
				if (auxSilicoso7.getAux11() != null
						&& auxSilicoso7.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad7()
							* nucFerr.getPesoNucleo7();
				} else {
					Cantidad = nucFerr.getCantidad7();
					Parte7 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad7();
				PesoNucleo = nucFerr.getPesoNucleo7();
				AcumCosto(nucFerr.getCodigoSilicoso7());
				DespVolumetrico = DespVolumetrico + pesonucleo7 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo7;
				KilosPactiva = KilosPactiva + pesonucleo7;
			}
			if (!nucFerr.getCodigoSilicoso8().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso8().trim()) > 0) {
				Auxiliar auxSilicoso8 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso8());
				if (auxSilicoso8.getAux11() != null
						&& auxSilicoso8.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad8()
							* nucFerr.getPesoNucleo8();
				} else {
					Cantidad = nucFerr.getCantidad8();
					Parte8 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad8();
				PesoNucleo = nucFerr.getPesoNucleo8();
				AcumCosto(nucFerr.getCodigoSilicoso8());
				DespVolumetrico = DespVolumetrico + pesonucleo8 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo8;
				KilosPactiva = KilosPactiva + pesonucleo8;
			}
			if (!nucFerr.getCodigoSilicoso9().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso9().trim()) > 0) {
				Auxiliar auxSilicoso9 = scha.detalleMaterialesSchaffner(nucFerr
						.getCodigoSilicoso9());
				if (auxSilicoso9.getAux11() != null
						&& auxSilicoso9.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad9()
							* nucFerr.getPesoNucleo9();
				} else {
					Cantidad = nucFerr.getCantidad9();
					Parte9 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad9();
				PesoNucleo = nucFerr.getPesoNucleo9();
				AcumCosto(nucFerr.getCodigoSilicoso9());
				DespVolumetrico = DespVolumetrico + pesonucleo9 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo9;
				KilosPactiva = KilosPactiva + pesonucleo9;
			}
			if (!nucFerr.getCodigoSilicoso10().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso10().trim()) > 0) {
				Auxiliar auxSilicoso10 = scha
						.detalleMaterialesSchaffner(nucFerr
								.getCodigoSilicoso10());
				if (auxSilicoso10.getAux11() != null
						&& auxSilicoso10.getAux11().trim().equals("KG")) {
					Cantidad = nucFerr.getCantidad10()
							* nucFerr.getPesoNucleo10();
				} else {
					Cantidad = nucFerr.getCantidad10();
					Parte10 = "-PAC";
				}
				Cantidad = nucFerr.getCantidad10();
				PesoNucleo = nucFerr.getPesoNucleo10();
				AcumCosto(nucFerr.getCodigoSilicoso10());
				DespVolumetrico = DespVolumetrico + pesonucleo10 / DenAceroSil;
				KilosPeso = KilosPeso + pesonucleo10;
				KilosPactiva = KilosPactiva + pesonucleo10;
			}
		} else {
			// Normal
			double pesox, k, a, b = 0d;
			pesox = 0;
			a = nucFerr.getChapaMayorNucleo();
			k = nucFerr.getEntreCentroNucleo();
			b = nucFerr.getVentanaNucleo();
			if (nucFerr.getTipo() == '1') {
				if (nucFerr.getCorte() == 0) {
					pesox = a * (4 * k + 2 * (b + a) + b);
					pesox = 1 + a * a / pesox;
					Parte2 = "NTE";
					FleXG = "NUC";
				} else {
					pesox = 1.025;
					Parte2 = "NTE";
					FleXG = "NUC";
				}
			} else {
				a = nucFerr.getDiametroNucleo();
				pesox = 3.1416 * Math.pow((a / 2), 2)
						* (4 * k + 2 * (b + a) + b);
				pesox = 1 + 3.1416 * Math.pow((a / 2), 3) / pesox / 3;
				Parte2 = "NTE";
				FleXG = "NUC";
			}
			Cantidad = pesox * nucFerr.getPesoNucleo1()
					* nucFerr.getCantidad1() * TolAceroSil;
			PesoNucleo = Cantidad;
			KilosPeso = KilosPeso + (PesoNucleo / TolAceroSil / pesox);
			KilosPactiva = KilosPactiva + (PesoNucleo / TolAceroSil / pesox);
			AcumCosto(nucFerr.getCodigoSilicoso1());
			DespVolumetrico = DespVolumetrico + PesoNucleo
					/ (TolAceroSil * DenAceroSil);
			if (!nucFerr.getCodigoSilicoso2().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso2().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo2()
						* nucFerr.getCantidad2() * TolAceroSil;
				pesonucleo2 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso2());
				DespVolumetrico = DespVolumetrico + pesonucleo2
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo2 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo2 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso3().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso3().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo3()
						* nucFerr.getCantidad3() * TolAceroSil;
				pesonucleo3 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso3());
				DespVolumetrico = DespVolumetrico + pesonucleo3
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo3 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo3 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso4().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso4().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo4()
						* nucFerr.getCantidad4() * TolAceroSil;
				pesonucleo4 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso4());
				DespVolumetrico = DespVolumetrico + pesonucleo4
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo4 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo4 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso5().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso5().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo5()
						* nucFerr.getCantidad5() * TolAceroSil;
				pesonucleo5 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso5());
				DespVolumetrico = DespVolumetrico + pesonucleo5
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo5 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo5 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso6().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso6().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo6()
						* nucFerr.getCantidad6() * TolAceroSil;
				pesonucleo6 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso6());
				DespVolumetrico = DespVolumetrico + pesonucleo6
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo6 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo6 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso7().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso7().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo7()
						* nucFerr.getCantidad7() * TolAceroSil;
				pesonucleo7 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso7());
				DespVolumetrico = DespVolumetrico + pesonucleo7
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo7 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo7 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso8().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso8().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo8()
						* nucFerr.getCantidad8() * TolAceroSil;
				pesonucleo8 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso8());
				DespVolumetrico = DespVolumetrico + pesonucleo8
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo8 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo8 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso9().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso9().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo9()
						* nucFerr.getCantidad9() * TolAceroSil;
				pesonucleo9 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso9());
				DespVolumetrico = DespVolumetrico + pesonucleo9
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo9 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo9 / TolAceroSil / pesox);
			}
			if (!nucFerr.getCodigoSilicoso10().trim().equals("")
					&& Long.parseLong(nucFerr.getCodigoSilicoso10().trim()) > 0) {
				Cantidad = pesox * nucFerr.getPesoNucleo10()
						* nucFerr.getCantidad10() * TolAceroSil;
				pesonucleo10 = Cantidad;
				AcumCosto(nucFerr.getCodigoSilicoso10());
				DespVolumetrico = DespVolumetrico + pesonucleo10
						/ (TolAceroSil * DenAceroSil);
				KilosPeso = KilosPeso + (pesonucleo10 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo10 / TolAceroSil / pesox);
			}
		}

		/*
		 * '=============================================================================
		 * ' Presspan Nucleo - Ferreteria (Presspan de 1.5 mm.)OK SEMI
		 * '=============================================================================
		 */
		// 'ParteX = nDise�o
		Parte2 = "NTE";
		FleXG = "NUC";
		int CTuer = 0;
		String CodTuer = "";
		Cantidad = 0;
		if (dis.getFase() > 1) {
			Cantidad = DenPresspan
					* TolMatAisl
					* 3
					* 4
					* nucFerr.getChapaMayorNucleo()
					* (dis.getFase() * nucFerr.getEntreCentroNucleo() + nucFerr
							.getChapaMayorNucleo()) * Math.pow(10, -6);
			// Detax = "aislaci�n n�cleo"
			AcumCosto("9001001010");
		}

		// ' Desplazamiento volumetrico

		DespVolumetrico = DespVolumetrico + Cantidad
				/ (DenPresspan * TolMatAisl);
		KilosPeso = KilosPeso + Cantidad / TolMatAisl;
		KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;

		// '==========================================================================
		// ' Hilos y Tirantes SEMI OK
		// '==========================================================================

		// ' Conjunto hilo metro yugo
		if (nucFerr.getSoporte() == 0 || nucFerr.getSoporte() == 1) {
			Cantidad = 4 * (nucFerr.getApilamientoChapas() + 75) * 0.001;
			// Detax = "hilos apriete"
			CTuer = 8;
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				CodTuer = "10";
			}

			if (nucFerr.getChapaMayorNucleo() >= 81
					&& nucFerr.getChapaMayorNucleo() <= 100) {
				CodTuer = "20";
			}

			if (nucFerr.getChapaMayorNucleo() >= 101
					&& nucFerr.getChapaMayorNucleo() <= 5000) {
				CodTuer = "30";
			}

			AcumCosto("90100010" + CodTuer);
		}

		// ' Esparragos tirante
		double cabex, espex, ala = 0d, altura = 0d;
		if (nucFerr.getChapaMayorNucleo() > 80) {
			espex = 6;
		} else {
			espex = 5;
		}
		if (nucFerr.getChapaMayorNucleo() >= 0
				&& nucFerr.getChapaMayorNucleo() <= 60) {
			ala = 40;
			altura = 45;
		}

		if (nucFerr.getChapaMayorNucleo() >= 61
				&& nucFerr.getChapaMayorNucleo() <= 80) {
			ala = 45;
			altura = 60;
		}

		if (nucFerr.getChapaMayorNucleo() >= 81
				&& nucFerr.getChapaMayorNucleo() <= 100) {
			ala = 50;
			altura = 75;
		}

		if (nucFerr.getChapaMayorNucleo() >= 101
				&& nucFerr.getChapaMayorNucleo() <= 120) {
			ala = 55;
			altura = 95;
		}

		if (nucFerr.getChapaMayorNucleo() >= 121
				&& nucFerr.getChapaMayorNucleo() <= 140) {
			ala = 60;
			altura = 115;
		}

		if (nucFerr.getChapaMayorNucleo() >= 140
				&& nucFerr.getChapaMayorNucleo() <= 500) {
			ala = 60;
			altura = 130;
		}

		cabex = 4 * (estTapa.getLargoEstanque() - 15)
				* (ala * 2 + altura - 4 * espex) * espex * DenEstanque
				* TolEstanque * Math.pow(10, -6);
		switch (nucFerr.getSoporte()) {
		// 'Hilo Tirante
		case 0:
			CTuer = CTuer + 8;
			Cantidad = CTuer;
			FleXG = "NUC";
			AcumCosto("90100040" + CodTuer);
			AcumCosto("90100020" + CodTuer);
			Cantidad = CTuer * 2;
			AcumCosto("90100030" + CodTuer);
			Cantidad = 4 * (nucFerr.getVentanaNucleo() + 2
					* nucFerr.getChapaMayorNucleo() + 80) * 0.001;
			// Detax = "hilos tirantes"
			AcumCosto("90100010" + CodTuer);
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				Cantidad = cabex;
				Detax = "cabezal";
				Parte2 = "CTE";
				FleXG = "CAB";
				AcumCosto("9008001050");
				KilosPeso = KilosPeso + Cantidad;
				KilosPactiva = KilosPactiva + Cantidad;
			}

			if (nucFerr.getChapaMayorNucleo() >= 81
					&& nucFerr.getChapaMayorNucleo() <= 5000) {
				Cantidad = cabex;
				Detax = "cabezal";
				Parte2 = "CTE";
				FleXG = "CAB";
				AcumCosto("9008001060");
				KilosPeso = KilosPeso + Cantidad;
				KilosPactiva = KilosPactiva + Cantidad;

			}
			break;
		// 'Tirante Plano
		case 1:
			Parte2 = "NTE";
			FleXG = "NUC";
			CTuer = CTuer;
			Cantidad = CTuer;
			AcumCosto("90100040" + CodTuer);
			AcumCosto("90100020" + CodTuer);
			Cantidad = CTuer * 2;
			AcumCosto("90100030" + CodTuer);
			Cantidad = dis.getFase()
					* 2
					* nucFerr.getChapaMayorNucleo()
					* (2 * nucFerr.getChapaMayorNucleo() + nucFerr
							.getVentanaNucleo()) * DenEstanque * TolEstanque
					* Math.pow(10, -6);
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				Cantidad = Cantidad * 5;
				Cantidad = cabex + Cantidad;
				// Detax = "cabezal y tirantes"
				Parte2 = "CTE";
				FleXG = "CAB";
				AcumCosto("9008001050");
			}
			if (nucFerr.getChapaMayorNucleo() >= 81
					&& nucFerr.getChapaMayorNucleo() <= 10000) {
				Cantidad = Cantidad * 6;
				Cantidad = cabex + Cantidad;
				// Detax = "cabezal y tirantes"
				Parte2 = "CTE";
				FleXG = "CAB";
				AcumCosto("9008001060");
			}
			KilosPeso = KilosPeso + Cantidad;
			KilosPactiva = KilosPactiva + Cantidad;
			break;
		// 'Cajet�n 3f
		case 2:
			Parte2 = "NTE";
			FleXG = "APA";
			Cantidad = 8;
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				AcumCosto("9010005010");
				AcumCosto("9010002010");
				AcumCosto("9010004010");
				Cantidad = Cantidad * 2;
				AcumCosto("9010003010");

				AcumCosto("9010005020");
				AcumCosto("9010002020");
				AcumCosto("9010004020");
				Cantidad = Cantidad * 2;
				AcumCosto("9010003020");
			}
			Cantidad = dis.getFase()
					* 2
					* (nucFerr.getEntreCentroNucleo()
							+ estTapa.getAltoEstanque() - 40)
					* (nucFerr.getChapaMayorNucleo() + nucFerr
							.getApilamientoChapas()) * 2 * DenEstanque
					* TolEstanque * Math.pow(10, -6);

			KilosPeso = KilosPeso + Cantidad;
			KilosPactiva = KilosPactiva + Cantidad;
			// 'Planchas 1f
			break;
		case 3:
			Cantidad = dis.getFase()
					* 2
					* (nucFerr.getEntreCentroNucleo())
					* (nucFerr.getChapaMayorNucleo() + nucFerr
							.getApilamientoChapas()) * 2 * DenEstanque
					* TolEstanque * Math.pow(10, -6);
			// Detax = "cajet�n"
			Parte2 = "PAC";
			FleXG = "CAB";
			AcumCosto("9008001010");
			KilosPeso = KilosPeso + Cantidad;
			KilosPactiva = KilosPactiva + Cantidad;
			break;
		}

		// ' Cinta Poliglas
		if (dis.getFase() != 1 && dis.getPotencia() > 700) {
			if (nucFerr.getDiametroNucleo() == 0) {
				Cantidad = 3
						* dis.getFase()
						* 2
						* (nucFerr.getChapaMayorNucleo() + nucFerr
								.getApilamientoChapas())
						* nucFerr.getVentanaNucleo() / 150 * 0.001;
			} else {
				Cantidad = 3 * dis.getFase() * 3.14
						* nucFerr.getDiametroNucleo()
						* nucFerr.getVentanaNucleo() / 200 * 0.001;
			}
			Parte2 = "NTE";
			FleXG = "NUC";
			AcumCosto("9007002001");
		}

		// ' Madera Trupan aprete nucleo
		Cantidad = (dis.getFase() - 1) * 0.08
				* Math.sqrt(dis.getPotencia() / 1000);
		Parte2 = "NTE";
		FleXG = "NUC";
		AcumCosto("9001006010");
		KilosPeso = KilosPeso + Cantidad;
		KilosPactiva = KilosPactiva + Cantidad;

		ParteX = diseno;

	}

	public void Cubixx() {
		try {

			// System.out.print(enc.getIdRefrigerante());

			if (dis.getIdRefrigerante() == 0) {
				cubicarSeco();
			} else {
				cubicarAceite();
			}

		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	private void datos() {

		// dise�o
		PotenciaX = dis.getPotencia();
		nDise�oE = dis.getCodigo1();
		ParteX = nDise�oE;
		tipoXX = dis.getIdDistribucion();
		diviSor = dis.getFase();

		// tolerancia
		TolCobreRec = tol.getF1();
		TolCobreCir = tol.getF2();
		TolAceroSil = tol.getF3();
		TolMatAisl = tol.getF4();
		TolLiquidoAis = tol.getF5();
		TolEstanque = tol.getF6();
		DenCobre = tol.getF7();
		DenPresspan = tol.getF8();
		DenNomex = tol.getF9();
		DenMadera = tol.getF10();
		DenAceroSil = tol.getF11();
		DenEstanque = tol.getF12();
		DenAceite = tol.getF13();
		DenSilicona = tol.getF14();
		FImpreg = 1 - tol.getF15();
		VDolar = tol.getF16();
		DenAlum = tol.getF17();

		// BBT
		Seplar[0] = bbt.getLducto();
		Seplar[1] = bbt.getLducto2();
		Separ[0] = bbt.getEspesorDuctos();
		Separ[1] = bbt.getEspesorDuctos2();
		codSepar[0] = Double.parseDouble(bbt.getCodDucto());
		codSepar[1] = Double.parseDouble(bbt.getCodDucto2());

		// BAT
		Seplar[2] = bat.getLducto();
		Seplar[3] = bat.getLducto2();
		Separ[2] = bat.getEspesorDuctos();
		Separ[3] = bat.getEspesorDuctos2();
		codSepar[2] = Double.parseDouble(bat.getCodDucto());
		codSepar[3] = Double.parseDouble(bat.getCodDucto2());

		// BTE
		Seplar[4] = bte.getLducto();
		Seplar[5] = bte.getLducto2();
		Separ[4] = bte.getEspesorDuctos();
		Separ[5] = bte.getEspesorDuctos2();
		codSepar[4] = Double.parseDouble(bte.getCodDucto());
		codSepar[5] = Double.parseDouble(bte.getCodDucto2());
	}

	public int CubicaBXX(int i) throws Exception {
		int val = 0;
		String codigoCondCu = "";
		int discondbxx = 0;
		boolean est = true;
		switch (i) {
		case 1:
			bill = espe.getNivelImpulso1();
			discondbxx = dis.getCodCondBbt();
			bob = bbt;
			codigoCondCu = bbt.getCodigoCondCu();
			if (dis.getValorBbt() == 0) {
				est = false;
			}
			break;
		case 2:
			bill = espe.getNivelImpulso2();
			discondbxx = dis.getCodCondBat();
			bob = bat;
			codigoCondCu = bat.getCodigoCondCu();
			if (dis.getValorBat() == 0) {
				est = false;
			}
			break;
		case 3:
			bill = espe.getNivelImpulso3();
			discondbxx = dis.getCodCondBte();
			bob = bte;
			codigoCondCu = bte.getCodigoCondCu();
			if (dis.getValorBte() == 0) {
				est = false;
			}
			break;
		}

		if (est) {
			cubicaCupp(codigoCondCu);

			if (discondbxx == PLETINA || discondbxx == LAMINA) {
				RendimientoSeccion = 1;
			} else {
				if (discondbxx == RECTANGULAR) {
					RendimientoSeccion = 0.97;
				} else {
					RendimientoSeccion = 1;
				}
			}

			double Efox = 0;
			if (discondbxx == PLETINA || discondbxx == LAMINA) {
				perimetro = 2 * (bob.getDimensionRadial() + bob
						.getDimensionAxial());
			} else {
				Efox = Math.sqrt(0.03 * bob.getDimensionRadial()
						* bob.getDimensionAxial() / (1 - 3.1416 / 4));
				perimetro = 2 * (bob.getDimensionRadial()
						+ bob.getDimensionAxial() - Efox * (4 - 3.1416));
			}
			// Peso del Esmalte
			PesoEsmalte = (0.0086 * bob.getDimensionRadial()
					* bob.getDimensionAxial() - 0.089
					* bob.getDimensionRadial() + 1.3437) / 100;
			// Peso del papel
			if (discondbxx == RECTANGULAR) {
				PesoPapel = DenPresspan
						/ DenSi
						* (perimetro * bob.getIncrementoPapel() + 3.1416
								* bob.getIncrementoPapel()
								* bob.getIncrementoPapel())
						/ (bob.getDimensionAxial() * bob.getDimensionRadial() * RendimientoSeccion);
			}

			// Peso Conductor
			if (discondbxx == RECTANGULAR || discondbxx == PLETINA
					|| discondbxx == LAMINA) {

				cantidadcobre = DenSi * dis.getFase()
						* bob.getLargoEspiraMedia()
						* (bob.getVueltasXfase() + bob.getnPrincipiosXcapa())
						* (TolCobreRec + PesoPapel) * bob.getCondParalelo()
						* RendimientoSeccion * bob.getDimensionAxial()
						* bob.getDimensionRadial() * Math.pow(10, -6);

			}

			if (discondbxx == CIRCULAR) {

				/*
				 * cantidadcobre = DenSi * dis.getFase() *
				 * bob.getLargoEspiraMedia() (bob.getVueltasXfase() +
				 * bob.getnPrincipiosXcapa()) (TolCobreCir + PesoEsmalte) *
				 * bob.getCondParalelo() 3.1416 / 4 * bob.getDimensionAxial()
				 * bob.getDimensionRadial() * Math.pow(10, -6);
				 */

				cantidadcobre = DenSi * dis.getFase()
						* bob.getLargoEspiraMedia()
						* (bob.getVueltasXfase() + bob.getnPrincipiosXcapa())
						* (TolCobreCir + PesoEsmalte) * bob.getCondParalelo()
						* 3.1416 / 4 * bob.getDimensionRadial()
						* bob.getDimensionRadial() * Math.pow(10, -6);

			}

			// Cobre depende del conductor
			Cantidad = cantidadcobre;
			// Detax = "conductor bobina";
			switch (i) {
			case 1:
				Parte2 = "BO1";
				FleXG = "BBT";
				break;
			case 2:
				Parte2 = "BO2";
				FleXG = "BAT";
				break;
			case 3:
				Parte2 = "BO3";
				FleXG = "BTE";
				break;
			}

			AcumCosto(bob.getCodigoCondCu());

			// Salidas bobina
			nucFerr = isus.getNucleoFerreteria(diseno);
			if (discondbxx == RECTANGULAR || discondbxx == PLETINA
					|| discondbxx == LAMINA) {
				Cantidad = 2
						* dis.getFase()
						* (TolCobreRec + PesoPapel)
						* bob.getDimensionAxial()
						* bob.getDimensionRadial()
						* bob.getCondParalelo()
						* (nucFerr.getVentanaNucleo() + nucFerr
								.getChapaMayorNucleo() * 2) * DenSi
						* Math.pow(10, -6);

				AcumCosto(bob.getCodigoCondCu());

				cantidadcobre = cantidadcobre + Cantidad;
				DesplVolumetricoCobre = cantidadcobre
						/ ((TolCobreRec + PesoPapel) * DenSi);

			} else {
				Cantidad = 2
						* dis.getFase()
						* 3.1416
						/ 4
						* TolCobreCir
						* bob.getDimensionAxial()
						* bob.getDimensionAxial()
						* bob.getCondParalelo()
						* (nucFerr.getVentanaNucleo() + nucFerr
								.getChapaMayorNucleo() * 2) * DenSi
						* Math.pow(10, -6);

				AcumCosto(bob.getCodigoCondCu());

				cantidadcobre = cantidadcobre + Cantidad;
				DesplVolumetricoCobre = cantidadcobre / (TolCobreCir * DenSi);

			}

			KilosPactiva = KilosPactiva + cantidadcobre;
			KilosPeso = KilosPeso + cantidadcobre;
			DespVolumetrico = DespVolumetrico + DesplVolumetricoCobre;

			// Cable de salida de TAPS
			double Largoducto = 0;

			if (espe.getDerivacion() != 8) {
				if (i == 2) {
					Largoducto = dis.getFase()
							* (3 * nucFerr.getVentanaNucleo() + 18 * nucFerr
									.getChapaMayorNucleo());
					// Detax = "derivaciones"
					if (discondbxx == RECTANGULAR) {
						Cantidad = DenSi * dis.getFase() * Largoducto
								* (TolCobreRec + PesoPapel)
								* bob.getCondParalelo()
								* bob.getDimensionRadial()
								* bob.getDimensionAxial() * Math.pow(10, -6);
						AcumCosto(bob.getCodigoCondCu());
					} else {
						Cantidad = DenSi * dis.getFase() * Largoducto
								* TolCobreCir * bob.getCondParalelo()
								* bob.getDimensionAxial()
								* bob.getDimensionAxial() * 3.14 / 4
								* Math.pow(10, -6);
						AcumCosto(bob.getCodigoCondCu());
					}
					KilosPactiva = KilosPactiva + Cantidad;
					KilosPeso = KilosPeso + Cantidad;
				}
			}

			// PRESSPAN OK SEMI
			Auxiliar aux = new Auxiliar();
			aux.setAux9(diseno);
			switch (i) {
			case 1:
				Parte2 = "BO1";
				FleXG = "BBT";
				aux.setAux1(1);
				pbbt = isus.getPresspan(aux);
				press = pbbt;
				break;
			case 2:
				Parte2 = "BO2";
				FleXG = "BAT";
				aux.setAux1(2);
				pbat = isus.getPresspan(aux);
				press = pbat;
				break;
			case 3:
				Parte2 = "BO3";
				FleXG = "BTE";
				aux.setAux1(3);
				pbte = isus.getPresspan(aux);
				press = pbte;
				break;
			}

			// Presspan Base
			PBaseBXX(i);

			// Ducto Presspan (Bobina de disco No Lleva)
			double FactorLargo, Propcab = 0;
			if (nucFerr.getTipo() == '1') {
				Propcab = nucFerr.getChapaMayorNucleo()
						/ (nucFerr.getChapaMayorNucleo() + nucFerr
								.getApilamientoChapas());
			} else {
				Propcab = 0;
			}

			// Ducto Bobina
			// Detax = "ducto bobina"
			if (discondbxx != DE_DISCO) {
				FactorLargo = (int) (Seplar[2 * (i - 1)] / (bob
						.getLargoBobinaCuellos() - 10));
				if (FactorLargo == 0) {
					FactorLargo = 1;
				}
				Cantidad = TolMatAisl
						* dis.getFase()
						* ((bob.getLargoEspiraMedia() * (bob
								.getnDuctosCompletos() + bob
								.getnDuctosCabezal() * Propcab))) / 1000;
				if (Cantidad > 0) {
					Cantidad = Cantidad / FactorLargo;
				}
				if (Cantidad > 0) {
					long va = (long) codSepar[2 * (i - 1)];
					AcumCosto(String.valueOf(va));
				}
				/*
				 * if(Cantidad > 0){ CubicaSep String.valueof(codSepar[2 * (i -
				 * 1)]); .Edit .Textoducto = CText .Update }else{ .Edit
				 * .Textoducto = "Sin Ducto en Bobina" .Update }
				 */
			}

			// Ducto Interior
			// Detax = "ducto base"
			if (discondbxx != DE_DISCO) {
				FactorLargo = (int) (Seplar[2 * (i - 1) + 1] / (bob
						.getLargoBobinaCuellos() - 10));
				if (FactorLargo == 0) {
					FactorLargo = 1;
				}
				Cantidad = TolMatAisl
						* dis.getFase()
						* (((bob.getLargoEspiraMedia() - bob
								.getEspesorFisicoBobina() * 3.1416) * (bob
								.getnDuctosNubTat() + bob
								.getnDuctosCabezaLatBt() * Propcab))) / 1000;
				if (Cantidad > 0) {
					Cantidad = Cantidad / FactorLargo;
				}
				if (Cantidad > 0) {
					long va = (long) codSepar[2 * (i - 1) + 1];
					AcumCosto(String.valueOf(va));
				}
				/*
				 * if(Cantidad > 0){ CubicaSep (Val(codSepar(2 * (Index - 1) +
				 * 1))) .Edit .Textoducto2 = CText .Update Else .Edit
				 * .Textoducto2 = "Sin Ducto" & CModt .Update End If
				 */
			}

			PXXXX_BXX(i);
			PEC_BXX(i);

			// Cuello Presspan
			// Bobina de disco no lleva
			if (discondbxx != DE_DISCO) {
				if (bob.getCondAxiales() > 0) {
					Cantidad = DenPresspan
							* TolMatAisl
							* dis.getFase()
							* bob.getLargoEspiraMedia()
							* (bob.getLargoBobinaCuellos() - bob
									.getLargoFisicoBobina())
							* bob.getCondParalelo()
							/ bob.getCondAxiales()
							* (bob.getDimensionRadial() + bob
									.getIncrementoPapel())
							* bob.getnCapasDisco() * Math.pow(10, -6);
					if (discondbxx == CIRCULAR) {
						Cantidad = DenPresspan
								* TolMatAisl
								* dis.getFase()
								* bob.getLargoEspiraMedia()
								* (bob.getLargoBobinaCuellos() - bob
										.getLargoFisicoBobina())
								* bob.getCondParalelo() / bob.getCondAxiales()
								* bob.getDimensionAxial()
								* bob.getnCapasDisco() * Math.pow(10, -6);
					}
				}
				DespVolumetrico = DespVolumetrico
						+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
				KilosPeso = KilosPeso + Cantidad / TolMatAisl;
				KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
				// Detax = "Cuello"
				AcumCosto("9001001010");
			}

			// '==========================================================================
			// ' CINTAS SEMI OK
			// '==========================================================================

			switch (i) {
			case 1:
				Parte2 = "BO1";
				FleXG = "BBT";
				break;
			case 2:
				Parte2 = "BO2";
				FleXG = "BAT";
				break;
			case 3:
				Parte2 = "BO3";
				FleXG = "BTE";
				break;
			}

			// ' Cinta Algod�n 1"
			// ' Todas excepto disco y pletina

			// ' Paso = 12.5
			// ' Rollo = 50 mts.

			if (discondbxx != DE_DISCO && dis.getCodCondBbt() != PLETINA) {
				Cantidad = 1.15
						* dis.getFase()
						* (bob.getLargoEspiraMedia() + 3.14 * bob
								.getEspesorFisicoBobina())
						* (bob.getLargoBobinaCuellos() / 12.5) / 50 * 0.001;
				AcumCosto("9007001001");
			}

			// ' Cinta Poliglass 3/4"
			if (discondbxx == PLETINA) {
				Cantidad = dis.getFase() * bob.getLargoEspiraMedia()
						* bob.getVueltasXfase() * 0.001;
				AcumCosto("9007002001");
			}

			// '==========================================================================
			// ' Aislaci�n de Conductor y Soldadura
			// '==========================================================================

			// 'Ductos de Papel Crep�

			// ' Soldadura de esta�o
			Cantidad = 0.02;
			// Detax = ""
			AcumCosto("9006001010");

			// ' Accesorios Extras
			/*
			 * if(discondbxx == DE_DISCO){ Call BobDisco //al parecer no est�
			 * dentro del proyecto }
			 */
		}
		return val;
	}

	private void PXXXX_BXX(int i) throws Exception {
		// Presspan 1
		if (press.getCanEspesoresAtbt1() > 0) {
			Cantidad = DenPresspan
					* TolMatAisl
					* dis.getFase()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina())
							* press.getCanEspesoresAtbt1() + 50)
					* bob.getLargoBobinaCuellos() * press.getEspesorAtbt1()
					* Math.pow(10, -6);
			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Descripcion = "PRESSPAN AT-BT " + Str(.EspesorATBT1) + " mm."
			// Detax = "aislaci�n exterior"
			AcumCosto(String.valueOf(press.getCodAtbt1()));
		}
		// Presspan 2
		if (press.getCanEspesoresAtbt2() > 0) {
			Cantidad = DenPresspan
					* TolMatAisl
					* dis.getFase()
					* ((bob.getLargoEspiraMedia() + 3.14
							* bob.getEspesorFisicoBobina()
							* press.getCanEspesoresAtbt2() + 50)
							* bob.getLargoBobinaCuellos()
							* press.getEspesorAtbt2() * Math.pow(10, -6));
			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Descripcion = "PRESSPAN AT-BT " + Str(.EspesorATBT2) + " mm."
			// Detax = "aislaci�n exterior"
			AcumCosto(String.valueOf(press.getCodAtbt2()));
		}

		// Presspan 3
		if (press.getCanEspesoresAtbt3() > 0) {
			Cantidad = DenPresspan
					* TolMatAisl
					* dis.getFase()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina())
							* press.getCanEspesoresAtbt3() + 50)
					* bob.getLargoBobinaCuellos() * press.getEspesorAtbt3()
					* Math.pow(10, -6);
			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Descripcion = "PRESSPAN AT-BT " + Str(.EspesorATBT3) + " mm."
			// Detax = "aislaci�n exterior"
			AcumCosto(String.valueOf(press.getCodAtbt3()));
		}
	}

	private void PBaseBXX(int i) throws Exception {

		// Presspan 1
		if (press.getCanEspesoresBase1() > 0) {
			Cantidad = DenPresspan
					* TolMatAisl
					* dis.getFase()
					* bob.getLargoBobinaCuellos()
					* (bob.getLargoEspiraMedia() - 3.14
							* bob.getEspesorFisicoBobina() + 50)
					* press.getCanEspesoresBase1() * press.getEspesorBase1()
					* Math.pow(10, -6);

			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "aislaci�n base"
			AcumCosto(press.getCodBase1());
		}

		// Presspan2
		if (press.getCanEspesoresBase2() > 0) {
			Cantidad = DenPresspan
					* TolMatAisl
					* dis.getFase()
					* bob.getLargoBobinaCuellos()
					* (bob.getLargoEspiraMedia() - 3.14
							* bob.getEspesorFisicoBobina() + 50)
					* pbbt.getCanEspesoresBase2() * pbbt.getEspesorBase2()
					* Math.pow(10, -6);

			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "aislaci�n base"
			AcumCosto(press.getCodBase2());
		}

		// Presspan3
		if (press.getCanEspesoresBase3() > 0) {
			Cantidad = DenPresspan
					* TolMatAisl
					* dis.getFase()
					* bob.getLargoBobinaCuellos()
					* (bob.getLargoEspiraMedia() - 3.14
							* bob.getEspesorFisicoBobina() + 50)
					* press.getCanEspesoresBase3() * press.getEspesorBase3()
					* Math.pow(10, -6);

			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "aislaci�n base"
			AcumCosto(press.getCodBase3());
		}
	}

	private void PEC_BXX(int i) throws Exception {
		// Presspan1
		if (press.getCanEspesoresCapa1() > 0) {
			Cantidad = DenPresspan * TolMatAisl * dis.getFase()
					* (bob.getLargoEspiraMedia() + 50)
					* press.getCanEspesoresCapa1() * press.getEspesorCapa1()
					* bob.getLargoBobinaCuellos()
					* (bob.getnCapasDisco() - 1 - bob.getnDuctosCompletos())
					* Math.pow(10, -6);
			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "entre capas"
			AcumCosto(press.getCodCapa1());
		}

		// Presspan 2
		if (press.getCanEspesoresCapa2() > 0) {
			Cantidad = DenPresspan * TolMatAisl * dis.getFase()
					* (bob.getLargoEspiraMedia() + 50)
					* press.getCanEspesoresCapa2() * press.getEspesorCapa2()
					* bob.getLargoBobinaCuellos()
					* (bob.getnCapasDisco() - 1 - bob.getnDuctosCompletos())
					* Math.pow(10, -6);
			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "entre capas"
			AcumCosto(press.getCodCapa2());
		}

		// Presspan 3
		if (press.getCanEspesoresCapa3() > 0) {
			Cantidad = DenPresspan * TolMatAisl * dis.getFase()
					* (bob.getLargoEspiraMedia() + 50)
					* press.getCanEspesoresCapa3() * press.getEspesorCapa3()
					* bob.getLargoBobinaCuellos()
					* (bob.getnCapasDisco() - 1 - bob.getnDuctosCompletos())
					* Math.pow(10, -6);
			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenPresspan * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "entre capas"
			AcumCosto(press.getCodCapa3());
		}
	}

	private void cubicaCupp(String codigoCondCu1) throws Exception {
		ItemCubicacion item = isus.getItemCubicacion(codigoCondCu1);
		// if (item.getDescripcion().toUpperCase().indexOf("ALUMINIO") > -1) {
		if (item.getAl() == 1) {
			DenSi = DenAlum;
		} else {
			DenSi = DenCobre;
		}
		// CText = item.getDescripcion();
	}

	private void AcumCosto(String Item) throws Exception {
		itemCubi = isus.getItemCubicacion(Item);
		Auxiliar auxs1 = scha.detalleMaterialesSchaffner(Item);
		if (dis.getPotencia() < 600 && dis.getFase() == 3) {
			if (Item != null
					&& !Item.trim().equals("0")
					&& (Item.substring(0, 4).equals("9010")
							|| Item.substring(0, 4).equals("9011")
							|| Item.substring(0, 4).equals("9030")
							|| Item.equals("9001005010")
							|| Item.equals("9001005020") || Item
								.equals("9001001010"))) {
				// Exit Function
			} else {
				if (Cantidad == 0) {

				} else {
					Costo costo = new Costo();
					costo.setProceso(FleXG);
					costo.setCodigoMaterial(Item);
					costo.setUm(auxs1.getAux11());
					costo.setDescripcion(auxs1.getAux10());
					costo.setCantidad(Math.abs(Cantidad));
					costo.setCostoUnitario(itemCubi.getCostoUnitario());
					costo.setCostoTotal(Math.abs(Cantidad)
							* itemCubi.getCostoUnitario());
					costo.setDiseno(diseno);
					costos.add(costo);
				}
			}
		} else {
			if (Cantidad == 0) {

			} else {
				Costo costo = new Costo();
				costo.setProceso(FleXG);
				costo.setCodigoMaterial(Item);
				costo.setUm(auxs1.getAux11());
				costo.setDescripcion(auxs1.getAux10());
				costo.setCantidad(Math.abs(Cantidad));
				costo.setCostoUnitario(itemCubi.getCostoUnitario());
				costo.setCostoTotal(Math.abs(Cantidad)
						* itemCubi.getCostoUnitario());
				costo.setDiseno(diseno);
				costos.add(costo);
			}
		}

	}

	private void AcumCosto22(String Item) throws Exception {
		itemCubi = isus.getItemCubicacion(Item);
		Auxiliar auxs1 = scha.detalleMaterialesSchaffner(Item);
		if (Cantidad > 0) {
			Costo costo = new Costo();
			costo.setProceso(FleXG);
			costo.setCodigoMaterial(Item);
			costo.setUm(auxs1.getAux11());
			costo.setDescripcion(auxs1.getAux10());
			costo.setCantidad(Cantidad);
			costo.setCostoUnitario(itemCubi.getCostoUnitario());
			costo.setCostoTotal(Cantidad * itemCubi.getCostoUnitario());
			costo.setDiseno(diseno);
			costos.add(costo);
		}
	}

	public void imprimir() {
		for (int i = 0; i < costos.size(); i++) {
			System.out.println("-----------------------------------");
			System.out.println((i + 1) + ": ");
			System.out.println("Proceso: " + costos.get(i).getProceso());
			System.out
					.println("Material: " + costos.get(i).getCodigoMaterial());
			System.out.println("Cantidad: " + costos.get(i).getCantidad());
			System.out.println("Dise�o: " + costos.get(i).getDiseno());
			System.out.println("-----------------------------------");
			System.out.println("");
		}

	}

	// ************************************CUBICAR--SECOS*********************************************

	public void cubicarSeco() throws Exception {
		Auxiliar aux = new Auxiliar();
		aux.setAux1(2);
		aux.setAux9(diseno);
		bat = isus.getBobina(aux);
		aux.setAux1(1);
		bbt = isus.getBobina(aux);
		aux.setAux1(3);
		bte = isus.getBobina(aux);
		datos();
		NumParte = 1;
		Parte = "BOBINA DE BAJA TENSION";
		CModt = " BT - N�cleo";
		sCubicaBXX(1);
		NumParte = 2;
		Parte = "BOBINA DE ALTA TENSION";
		CModt = " BT - AT";
		sCubicaBXX(2);
		NumParte = 3;
		Parte = "BOBINA TERCIARIA";
		CModt = " BT - TE";
		sCubicaBXX(3);
		diviSor = 1;
		NumParte = 4;
		Parte = "NUCLEO";
		sCubicaNucleo();
		NumParte = 6;
		Parte = "ESTANQUE Y TAPA";
		sEstanqueyTapa();
		NumParte = 8;
		Parte = "DISPOSICION DE AISLACIONES";
		sAislaciones();
		NumParte = 10;
		Parte = "ACCESORIOS";
		ACCESORIOS();
		/*
		 * Diseno dise = new Diseno(); dise.setKilosPeso(KilosPeso);
		 * dise.setKilosRadiadores(KilosRadiadores);
		 * dise.setKilosEstanque(KilosEstanque);
		 * dise.setLitrosAceite(LitrosAceite); dise.setKilosAceite(KilosAceite);
		 * dise.setKilosPactiva(KilosPactiva); dise.setIdDiseno(diseno);
		 * dise.setCubic(100); int as = isus.actualizarDisenoXdiseno(dise);
		 */
		// imprimir();

	}

	public int sCubicaBXX(int i) throws Exception {
		int val = 0;
		String codigoCondCu = "";
		int discondbxx = 0;
		boolean est = true;
		switch (i) {
		case 1:
			bill = espe.getNivelImpulso1();
			discondbxx = dis.getCodCondBbt();
			bob = bbt;
			codigoCondCu = bbt.getCodigoCondCu();
			if (dis.getValorBbt() == 0) {
				est = false;
			}
			break;
		case 2:
			bill = espe.getNivelImpulso2();
			discondbxx = dis.getCodCondBat();
			bob = bat;
			codigoCondCu = bat.getCodigoCondCu();
			if (dis.getValorBat() == 0) {
				est = false;
			}
			break;
		case 3:
			bill = espe.getNivelImpulso3();
			discondbxx = dis.getCodCondBte();
			bob = bte;
			codigoCondCu = bte.getCodigoCondCu();
			if (dis.getValorBte() == 0) {
				est = false;
			}
			break;
		}

		if (est) {
			cubicaCupp(codigoCondCu);

			if (discondbxx == PLETINA || discondbxx == LAMINA) {
				RendimientoSeccion = 1;
			} else {
				if (discondbxx == RECTANGULAR) {
					RendimientoSeccion = 0.97;
				} else {
					RendimientoSeccion = 1;
				}
			}

			// 'Per�metro del Conductor
			double Efox = 0;
			if (discondbxx == PLETINA || discondbxx == LAMINA) {
				perimetro = 2 * (bob.getDimensionRadial() + bob
						.getDimensionAxial());
			} else {
				Efox = Math.sqrt(0.03 * bob.getDimensionRadial()
						* bob.getDimensionAxial() / (1 - 3.1416 / 4));
				perimetro = 2 * (bob.getDimensionRadial()
						+ bob.getDimensionAxial() - Efox * (4 - 3.1416));
			}

			// ' Peso del Esmalte
			PesoEsmalte = (0.0086 * bob.getDimensionRadial()
					* bob.getDimensionRadial() - 0.089
					* bob.getDimensionRadial() + 1.3437) / 100;

			// ' Peso del papel
			if (discondbxx == RECTANGULAR) {
				PesoPapel = DenNomex
						/ DenSi
						* (perimetro * bob.getIncrementoPapel() + 3.1416
								* bob.getIncrementoPapel()
								* bob.getIncrementoPapel())
						/ (bob.getDimensionAxial() * bob.getDimensionRadial() * RendimientoSeccion);
			}

			// ' Peso Conductor
			if (discondbxx == RECTANGULAR || discondbxx == PLETINA
					|| discondbxx == LAMINA) {

				cantidadcobre = DenSi * dis.getFase()
						* bob.getLargoEspiraMedia()
						* (bob.getVueltasXfase() + bob.getnPrincipiosXcapa())
						* (TolCobreRec + PesoPapel) * bob.getCondParalelo()
						* RendimientoSeccion * bob.getDimensionAxial()
						* bob.getDimensionRadial() * Math.pow(10, -6);

			}
			if (discondbxx == CIRCULAR) {

				cantidadcobre = DenSi * dis.getFase()
						* bob.getLargoEspiraMedia()
						* (bob.getVueltasXfase() + bob.getnPrincipiosXcapa())
						* (TolCobreCir + PesoEsmalte) * bob.getCondParalelo()
						* 3.1416 / 4 * bob.getDimensionRadial()
						* bob.getDimensionRadial() * Math.pow(10, -6);
				// 'MsgBox .VueltasXFase
			}

			// ' Cobre depende del conductor
			Cantidad = cantidadcobre;
			// Detax = "conductor bobina"
			switch (i) {
			case 1:
				Parte2 = "BO1";
				FleXG = "BBT";
				break;
			case 2:
				Parte2 = "BO2";
				FleXG = "BAT";
			case 3:
				Parte2 = "BO3";
				FleXG = "BTE";
				break;
			}
			sAcumCosto(bob.getCodigoCondCu());
			// ' Salidas bobina
			nucFerr = isus.getNucleoFerreteria(diseno);
			if (discondbxx == RECTANGULAR || discondbxx == PLETINA
					|| discondbxx == LAMINA) {
				Cantidad = 2
						* dis.getFase()
						* (TolCobreRec + PesoPapel)
						* bob.getDimensionAxial()
						* bob.getDimensionRadial()
						* bob.getCondParalelo()
						* (nucFerr.getVentanaNucleo() + nucFerr
								.getChapaMayorNucleo() * 2) * DenSi
						* Math.pow(10, -6);
				// Detax = "salida bobina"
				sAcumCosto(bob.getCodigoCondCu());
				cantidadcobre = cantidadcobre + Cantidad;
			} else {
				Cantidad = 2
						* dis.getFase()
						* 3.1416
						/ 4
						* TolCobreCir
						* bob.getDimensionAxial()
						* bob.getDimensionAxial()
						* bob.getCondParalelo()
						* (nucFerr.getVentanaNucleo() + nucFerr
								.getChapaMayorNucleo() * 2) * DenSi
						* Math.pow(10, -6);
				// Detax = "salida bobina"
				sAcumCosto(bob.getCodigoCondCu());

				cantidadcobre = cantidadcobre + Cantidad;
			}

			KilosPactiva = KilosPactiva + cantidadcobre;
			KilosPeso = KilosPeso + cantidadcobre;
			DespVolumetrico = DespVolumetrico + DespVolumetricoCobre;

			// ' Cable de salida de TAPS
			double Largoducto = 0;
			Largoducto = 0;

			if (i == 2 && espe.getDerivacion() != 8) {
				Largoducto = dis.getFase()
						* (3 * nucFerr.getVentanaNucleo() + 18 * nucFerr
								.getChapaMayorNucleo());
				// Detax = "derivaciones"
				if (discondbxx == RECTANGULAR) {
					Cantidad = DenSi * dis.getFase() * Largoducto
							* (TolCobreRec + PesoPapel) * bob.getCondParalelo()
							* bob.getDimensionRadial()
							* bob.getDimensionAxial() * Math.pow(10, -6);
					sAcumCosto(bob.getCodigoCondCu());
				} else {
					Cantidad = DenSi * dis.getFase() * Largoducto * TolCobreCir
							* bob.getCondParalelo() * bob.getDimensionAxial()
							* bob.getDimensionAxial() * 3.14 / 4
							* Math.pow(10, -6);
					sAcumCosto(bob.getCodigoCondCu());
				}
				KilosPactiva = KilosPactiva + Cantidad;
				KilosPeso = KilosPeso + Cantidad;
			}

			/*
			 * '==========================================================================
			 * ' PRESSPAN
			 * '==========================================================================
			 */

			Auxiliar aux = new Auxiliar();
			aux.setAux9(diseno);
			switch (i) {
			case 1:
				Parte2 = "BO1";
				FleXG = "BBT";
				aux.setAux1(1);
				pbbt = isus.getPresspan(aux);
				press = pbbt;
				break;
			case 2:
				Parte2 = "BO2";
				FleXG = "BAT";
				aux.setAux1(2);
				pbat = isus.getPresspan(aux);
				press = pbat;
				break;
			case 3:
				Parte2 = "BO3";
				FleXG = "BTE";
				aux.setAux1(3);
				pbte = isus.getPresspan(aux);
				press = pbte;
				break;
			}

			// 'Presspan Base
			sPBaseBXX(i);

			// 'Ducto Presspan (Bobina de disco No Lleva)
			double FactorLargo = 0, Propcab = 0;
			if (nucFerr.getTipo() == '1') {
				Propcab = nucFerr.getChapaMayorNucleo()
						/ (nucFerr.getChapaMayorNucleo() + nucFerr
								.getApilamientoChapas());
			} else {
				Propcab = 0;
			}
			// 'Ducto Bobina
			// Detax = "ducto bobina"
			if (discondbxx != DE_DISCO) {
				FactorLargo = (int) (Seplar[2 * (i - 1)] / (bob
						.getLargoBobinaCuellos()));
				Cantidad = dis.getFase()
						* ((bob.getLargoEspiraMedia() * (bob
								.getnDuctosCompletos() + bob
								.getnDuctosCabezal() * Propcab))) / 45;
				Cantidad = Cantidad * bob.getLargoBobinaCuellos() * 1.1 / 1000;
				if (Cantidad > 0) {
					Cantidad = ((double) Math.round(Cantidad * 100)) / 100;
				}
				if (Cantidad > 0) {
					long va = (long) codSepar[2 * (i - 1)];
					sAcumCosto(String.valueOf(va));
				}
			}
			// 'Ducto Interior
			// Detax = "ducto base"
			if (discondbxx != DE_DISCO) {
				FactorLargo = (int) (Seplar[2 * (i - 1) + 1] / (bob
						.getLargoBobinaCuellos()));
				Cantidad = dis.getFase()
						* ((bob.getLargoEspiraMedia() * (bob.getnDuctosNubTat() + bob
								.getnDuctosCabezaLatBt() * Propcab))) / 45;
				Cantidad = Cantidad * bob.getLargoBobinaCuellos() * 1.1 / 1000;
				if (Cantidad > 0) {
					Cantidad = Cantidad / FactorLargo;
					long va = (long) codSepar[2 * (i - 1) + 1];
					sAcumCosto(String.valueOf(va));
				}

			}

			// Detax = ""
			// 'Presspan AT-BT
			sPXXXX_BXX(i);

			// 'Presspan Entrecapas
			sPEC_BXX(i);

			// 'Cuello Presspan
			// ' Bobina de disco no lleva
			if (discondbxx != DE_DISCO) {
				if (bob.getCondAxiales() > 0) {
					Cantidad = 0.3
							* DenNomex
							* TolMatAisl
							* dis.getFase()
							* bob.getLargoEspiraMedia()
							* (bob.getLargoBobinaCuellos() - bob
									.getLargoFisicoBobina())
							* bob.getCondParalelo()
							/ bob.getCondAxiales()
							* (bob.getDimensionRadial() + bob
									.getIncrementoPapel())
							* bob.getnCapasDisco() * Math.pow(10, -6);
					if (discondbxx == CIRCULAR) {
						Cantidad = DenNomex
								* TolMatAisl
								* dis.getFase()
								* bob.getLargoEspiraMedia()
								* (bob.getLargoBobinaCuellos() - bob
										.getLargoFisicoBobina())
								* bob.getCondParalelo() / bob.getCondAxiales()
								* bob.getDimensionAxial()
								* bob.getnCapasDisco() * Math.pow(10, -6);
					}
				}
				KilosPeso = KilosPeso + Cantidad / TolMatAisl;
				KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
				// Detax = "Cuello"
				sAcumCosto("9009002020");
			}

			/*
			 * '==========================================================================
			 * ' CINTAS
			 * '==========================================================================
			 */

			switch (i) {
			case 1:
				Parte2 = "BO1";
				FleXG = "BBT";
				break;
			case 2:
				Parte2 = "BO2";
				FleXG = "BAT";
				break;
			case 3:
				Parte2 = "BO3";
				FleXG = "BTE";
				break;
			}

			// ' Cinta Fibra C/Adhesivo 3/4"
			Cantidad = dis.getFase()
					* ((bob.getLargoEspiraMedia() * (bob.getnDuctosNubTat()
							+ bob.getnDuctosCompletos()
							+ bob.getnDuctosCabezal() * Propcab + bob
							.getnDuctosCabezaLatBt() * Propcab))) / 50;
			Cantidad = Cantidad * 3 * 250;
			Cantidad = Cantidad + bob.getnCapasDisco()
					* (int) (bob.getLargoBobinaCuellos() / 100) * 100;
			Cantidad = Cantidad / 1000 / 33;
			sAcumCosto("9007003001");

			// ' Cinta Fibra S/Adhesivo 3/4"
			Cantidad = dis.getFase() * bob.getnCapasDisco() * 2 / 50;
			sAcumCosto("9007003005");

			/*
			 * '==========================================================================
			 * ' Aislaci�n de Conductor y Soldadura
			 * '==========================================================================
			 */

			// 'Ductos de Papel Crep�

			// ' Soldadura de esta�o
			Cantidad = 0.02;
			// Detax = ""
			sAcumCosto("9006001010");

		}// /gbgfdfgdfg
		return val;
	}

	private void sAcumCosto(String Item) throws Exception {
		itemCubi = isus.getItemCubicacion(Item);
		Auxiliar auxs1 = scha.detalleMaterialesSchaffner(Item);
		if (Cantidad == 0) {

		} else {
			Costo costo = new Costo();
			costo.setProceso(FleXG);
			costo.setCodigoMaterial(Item);
			costo.setUm(auxs1.getAux11());
			costo.setDescripcion(auxs1.getAux10());
			costo.setCantidad(Math.abs(Cantidad));
			costo.setCostoUnitario(itemCubi.getCostoUnitario());
			costo.setCostoTotal(Math.abs(Cantidad)
					* itemCubi.getCostoUnitario());
			costo.setDiseno(diseno);
			costos.add(costo);
		}

	}

	private void sPBaseBXX(int i) throws Exception {

		// Presspan 1
		if (press.getCanEspesoresBase1() > 0) {
			Cantidad = DenNomex
					* TolMatAisl
					* dis.getFase()
					* bob.getLargoBobinaCuellos()
					* (bob.getLargoEspiraMedia() - 3.14
							* bob.getEspesorFisicoBobina() + 50)
					* press.getCanEspesoresBase1() * press.getEspesorBase1()
					* Math.pow(10, -6);

			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenNomex * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "aislaci�n base"
			sAcumCosto(press.getCodBase1());
		}

		// Presspan2
		if (press.getCanEspesoresBase2() > 0) {
			Cantidad = DenNomex
					* TolMatAisl
					* dis.getFase()
					* bob.getLargoBobinaCuellos()
					* (bob.getLargoEspiraMedia() - 3.14
							* bob.getEspesorFisicoBobina() + 50)
					* pbbt.getCanEspesoresBase2() * pbbt.getEspesorBase2()
					* Math.pow(10, -6);

			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenNomex * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "aislaci�n base"
			sAcumCosto(press.getCodBase2());
		}

		// Presspan3
		if (press.getCanEspesoresBase3() > 0) {
			Cantidad = DenNomex
					* TolMatAisl
					* dis.getFase()
					* bob.getLargoBobinaCuellos()
					* (bob.getLargoEspiraMedia() - 3.14
							* bob.getEspesorFisicoBobina() + 50)
					* press.getCanEspesoresBase3() * press.getEspesorBase3()
					* Math.pow(10, -6);

			DespVolumetrico = DespVolumetrico
					+ (Cantidad / (DenNomex * TolMatAisl) * FImpreg);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			// Detax = "aislaci�n base"
			sAcumCosto(press.getCodBase3());
		}
	}

	private void sPXXXX_BXX(int i) throws Exception {
		// Presspan 1
		if (press.getCanEspesoresAtbt1() > 0
				&& press.getCodAtbt1().indexOf("9009001") > -1) {
			Cantidad = DenNomex
					* TolMatAisl
					* dis.getFase()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina())
							* press.getCanEspesoresAtbt1() + 50)
					* bob.getLargoBobinaCuellos() * press.getEspesorAtbt1()
					* Math.pow(10, -6);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			sAcumCosto(String.valueOf(press.getCodAtbt1()));
		}

		if (press.getCanEspesoresAtbt1() > 0
				&& press.getCodAtbt1().indexOf("9007003") > -1) {
			Cantidad = TolMatAisl
					* dis.getFase()
					* press.getCanEspesoresAtbt1()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina()) + 50)
					* bob.getLargoBobinaCuellos() / 15 / 50000;
			Cantidad = ((double) Math.round(Cantidad * 100)) / 100;
		}
		// Presspan 2
		if (press.getCanEspesoresAtbt2() > 0
				&& press.getCodAtbt2().indexOf("9009001") > -1) {
			Cantidad = DenNomex
					* TolMatAisl
					* dis.getFase()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina())
							* press.getCanEspesoresAtbt2() + 50)
					* bob.getLargoBobinaCuellos() * press.getEspesorAtbt2()
					* Math.pow(10, -6);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			sAcumCosto(press.getCodAtbt2());
		}

		if (press.getCodAtbt2().indexOf("9007003") > -1
				&& press.getCanEspesoresAtbt2() > 0) {
			Cantidad = TolMatAisl
					* dis.getFase()
					* press.getCanEspesoresAtbt2()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina()) + 50)
					* bob.getLargoBobinaCuellos() / 15 / 50000;
			Cantidad = ((double) Math.round(Cantidad * 100)) / 100;
		}

		// Presspan 3
		if (press.getCanEspesoresAtbt3() > 0
				&& press.getCodAtbt3().indexOf("9009001") > -1) {
			Cantidad = DenNomex
					* TolMatAisl
					* dis.getFase()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina())
							* press.getCanEspesoresAtbt3() + 50)
					* bob.getLargoBobinaCuellos() * press.getEspesorAtbt3()
					* Math.pow(10, -6);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			sAcumCosto(press.getCodAtbt3());
		}

		if (press.getCodAtbt3().indexOf("9007003") > -1
				&& press.getCanEspesoresAtbt3() > 0) {
			Cantidad = TolMatAisl
					* dis.getFase()
					* press.getCanEspesoresAtbt3()
					* ((bob.getLargoEspiraMedia() + 3.14 * bob
							.getEspesorFisicoBobina()) + 50)
					* bob.getLargoBobinaCuellos() / 15 / 50000;
			Cantidad = ((double) Math.round(Cantidad * 100)) / 100;
		}
	}

	private void sPEC_BXX(int i) throws Exception {
		// Presspan1
		if (press.getCanEspesoresCapa1() > 0) {
			Cantidad = DenNomex * TolMatAisl * dis.getFase()
					* (bob.getLargoEspiraMedia() + 50)
					* press.getCanEspesoresCapa1() * press.getEspesorCapa1()
					* bob.getLargoBobinaCuellos()
					* (bob.getnCapasDisco() - 1 - bob.getnDuctosCompletos())
					* Math.pow(10, -6);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			AcumCosto(press.getCodCapa1());
		}

		// Presspan 2
		if (press.getCanEspesoresCapa2() > 0) {
			Cantidad = DenNomex * TolMatAisl * dis.getFase()
					* (bob.getLargoEspiraMedia() + 50)
					* press.getCanEspesoresCapa2() * press.getEspesorCapa2()
					* bob.getLargoBobinaCuellos()
					* (bob.getnCapasDisco() - 1 - bob.getnDuctosCompletos())
					* Math.pow(10, -6);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			AcumCosto(press.getCodCapa2());
		}

		// Presspan 3
		if (press.getCanEspesoresCapa3() > 0) {
			Cantidad = DenNomex * TolMatAisl * dis.getFase()
					* (bob.getLargoEspiraMedia() + 50)
					* press.getCanEspesoresCapa3() * press.getEspesorCapa3()
					* bob.getLargoBobinaCuellos()
					* (bob.getnCapasDisco() - 1 - bob.getnDuctosCompletos())
					* Math.pow(10, -6);
			KilosPeso = KilosPeso + Cantidad / TolMatAisl;
			KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;
			AcumCosto(press.getCodCapa3());
		}
	}

	private void sCubicaNucleo() throws Exception {
		// '==============================================================================
		// ' Peso N�cleo
		// '==============================================================================
		double PesoNucleo, pesonucleo2 = 0;
		// ' N�cleo enrollado
		estTapa = isus.getEstanqueTapa(diseno);
		if (nucFerr.getCodigoSilicoso1().substring(0, 7).equals("9005001")) {
			Parte2 = "PAC";
			FleXG = "APA";
			Cantidad = nucFerr.getCantidad1();
			PesoNucleo = nucFerr.getPesoNucleo1();
			sAcumCosto(nucFerr.getCodigoSilicoso1());
			KilosPeso = KilosPeso + PesoNucleo;
			KilosPactiva = KilosPactiva + PesoNucleo;
			if (!nucFerr.getCodigoSilicoso2().trim().equals("")
					&& !nucFerr.getCodigoSilicoso2().trim().equals("0")) {
				Cantidad = nucFerr.getCantidad2();
				PesoNucleo = nucFerr.getPesoNucleo2();
				sAcumCosto(nucFerr.getCodigoSilicoso2());
				KilosPeso = KilosPeso + pesonucleo2;
				KilosPactiva = KilosPactiva + pesonucleo2;
			}
		} else {

			// ' Normal
			double pesox, k, a, b;
			pesox = 0;
			a = nucFerr.getChapaMayorNucleo();
			k = nucFerr.getEntreCentroNucleo();
			b = nucFerr.getVentanaNucleo();
			if (nucFerr.getTipo() == '1') {
				if (nucFerr.getCorte() == 0) {
					pesox = a * (4 * k + 2 * (b + a) + b);
					pesox = 1 + a * a / pesox;
					Parte2 = "NTE";
					FleXG = "NUC";
				} else {
					pesox = 1.025;
					Parte2 = "NTE";
					FleXG = "NUC";
				}
			} else {
				a = nucFerr.getDiametroNucleo();
				pesox = 3.1416 * Math.pow((a / 2), 2)
						* (4 * k + 2 * (b + a) + b);
				pesox = 1 + 3.1416 * Math.pow((a / 2), 3) / pesox / 3;
			}
			Cantidad = pesox * nucFerr.getPesoNucleo1()
					* nucFerr.getCantidad1() * TolAceroSil;
			PesoNucleo = Cantidad;
			KilosPeso = KilosPeso + (PesoNucleo / TolAceroSil / pesox);
			KilosPactiva = KilosPactiva + (PesoNucleo / TolAceroSil / pesox);
			sAcumCosto(nucFerr.getCodigoSilicoso1());
			if (!nucFerr.getCodigoSilicoso2().trim().equals("")
					&& !nucFerr.getCodigoSilicoso2().trim().equals("0")) {
				Cantidad = pesox * nucFerr.getPesoNucleo2()
						* nucFerr.getCantidad2() * TolAceroSil;
				pesonucleo2 = Cantidad;
				sAcumCosto(nucFerr.getCodigoSilicoso2());
				KilosPeso = KilosPeso + (pesonucleo2 / TolAceroSil / pesox);
				KilosPactiva = KilosPactiva
						+ (pesonucleo2 / TolAceroSil / pesox);
			}
		}

		// '=============================================================================
		// ' Presspan Nucleo - Ferreteria (Presspan de 1.5 mm.)
		// '=============================================================================
		ParteX = diseno;
		Parte2 = "NTE";
		FleXG = "NUC";
		int CTuer = 0;
		String CodTuer = "";
		Cantidad = 0;
		if (dis.getFase() > 1) {
			Cantidad = DenPresspan
					* TolMatAisl
					* 3
					* 4
					* nucFerr.getChapaMayorNucleo()
					* (dis.getFase() * nucFerr.getEntreCentroNucleo() + nucFerr
							.getChapaMayorNucleo()) * Math.pow(10, -6);
			sAcumCosto("9001001010");
		}

		// ' Desplazamiento volumetrico

		KilosPeso = KilosPeso + Cantidad / TolMatAisl;
		KilosPactiva = KilosPactiva + Cantidad / TolMatAisl;

		// '==========================================================================
		// ' Hilos y Tirantes
		// '==========================================================================

		// ' Conjunto hilo metro yugo
		if (nucFerr.getSoporte() == 0 || nucFerr.getSoporte() == 1) {
			Cantidad = 4 * (nucFerr.getApilamientoChapas() + 75) * 0.001;
			CTuer = 8;
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				CodTuer = "10";
			}
			if (nucFerr.getChapaMayorNucleo() >= 81
					&& nucFerr.getChapaMayorNucleo() <= 100) {
				CodTuer = "20";
			}
			if (nucFerr.getChapaMayorNucleo() >= 100
					&& nucFerr.getChapaMayorNucleo() <= 5000) {
				CodTuer = "30";
			}
			sAcumCosto("90100010" + CodTuer);
		}

		// ' Esparragos tirante
		double cabex = 0, espex = 0, ala = 0, altura = 0;
		if (nucFerr.getChapaMayorNucleo() > 80) {
			espex = 6;
		} else {
			espex = 5;
		}
		if (nucFerr.getChapaMayorNucleo() >= 0
				&& nucFerr.getChapaMayorNucleo() <= 60) {
			ala = 40;
			altura = 45;
		}
		if (nucFerr.getChapaMayorNucleo() >= 61
				&& nucFerr.getChapaMayorNucleo() <= 80) {
			ala = 45;
			altura = 60;
		}
		if (nucFerr.getChapaMayorNucleo() >= 81
				&& nucFerr.getChapaMayorNucleo() <= 100) {
			ala = 50;
			altura = 75;
		}
		if (nucFerr.getChapaMayorNucleo() >= 101
				&& nucFerr.getChapaMayorNucleo() <= 120) {
			ala = 55;
			altura = 95;
		}
		if (nucFerr.getChapaMayorNucleo() >= 121
				&& nucFerr.getChapaMayorNucleo() <= 140) {
			ala = 60;
			altura = 115;
		}
		if (nucFerr.getChapaMayorNucleo() >= 141
				&& nucFerr.getChapaMayorNucleo() <= 500) {
			ala = 60;
			altura = 130;
		}

		cabex = 4 * (estTapa.getLargoEstanque() - 15)
				* (ala * 2 + altura - 4 * espex) * espex * DenEstanque
				* TolEstanque * Math.pow(10, -6);
		switch (nucFerr.getSoporte()) {
		// 'Hilo Tirante
		case 0:
			CTuer = CTuer + 8;
			Cantidad = CTuer;
			sAcumCosto("90100040" + CodTuer);
			sAcumCosto("90100020" + CodTuer);
			Cantidad = CTuer * 2;
			sAcumCosto("90100030" + CodTuer);
			Cantidad = 4 * (nucFerr.getVentanaNucleo() + 2
					* nucFerr.getChapaMayorNucleo() + 80) * 0.001;

			sAcumCosto("90100010" + CodTuer);
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				Cantidad = cabex;
				Detax = "cabezal";
				Parte2 = "CTE";
				FleXG = "CAB";
				sAcumCosto("9008001050");
				KilosPeso = KilosPeso + Cantidad;
				KilosPactiva = KilosPactiva + Cantidad;
			}
			if (nucFerr.getChapaMayorNucleo() >= 81
					&& nucFerr.getChapaMayorNucleo() <= 5000) {
				Cantidad = cabex;
				Detax = "cabezal";
				Parte2 = "CTE";
				FleXG = "CAB";
				sAcumCosto("9008001060");
				KilosPeso = KilosPeso + Cantidad;
				KilosPactiva = KilosPactiva + Cantidad;
			}

			break;

		// 'Tirante Plano
		case 1:
			Cantidad = CTuer;
			sAcumCosto("90100040" + CodTuer);
			sAcumCosto("90100020" + CodTuer);
			Cantidad = CTuer * 2;
			sAcumCosto("90100030" + CodTuer);
			Cantidad = dis.getFase()
					* 2
					* nucFerr.getChapaMayorNucleo()
					* (2 * nucFerr.getChapaMayorNucleo() + nucFerr
							.getVentanaNucleo()) * DenEstanque * TolEstanque
					* Math.pow(10, -6);
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				Cantidad = Cantidad * 5;
				Cantidad = cabex + Cantidad;
				Parte2 = "CTE";
				FleXG = "APA";
				Detax = "cabezal y tirantes";
				sAcumCosto("9008001050");
			}
			if (nucFerr.getChapaMayorNucleo() >= 81
					&& nucFerr.getChapaMayorNucleo() <= 10000) {
				Cantidad = Cantidad * 6;
				Cantidad = cabex + Cantidad;
				Parte2 = "CTE";
				FleXG = "APA";
				Detax = "cabezal y tirantes";
				sAcumCosto("9008001060");
			}
			KilosPeso = KilosPeso + Cantidad;
			KilosPactiva = KilosPactiva + Cantidad;
			break;
		// 'Cajet�n 3f
		case 2:
			Parte2 = "NTE";
			FleXG = "APA";
			Cantidad = 8;
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				sAcumCosto("9010005010");
				sAcumCosto("9010002010");
				sAcumCosto("9010004010");
				Cantidad = Cantidad * 2;
				sAcumCosto("9010003010");
			}
			if (nucFerr.getChapaMayorNucleo() >= 0
					&& nucFerr.getChapaMayorNucleo() <= 80) {
				sAcumCosto("9010005020");
				sAcumCosto("9010002020");
				sAcumCosto("9010004020");
				Cantidad = Cantidad * 2;
				sAcumCosto("9010003020");
			}
			Cantidad = dis.getFase()
					* 2
					* (estTapa.getAnchoEstanque() - 100
							+ estTapa.getAltoEstanque() - 40)
					* (nucFerr.getChapaMayorNucleo() + nucFerr
							.getApilamientoChapas()) * 2 * DenEstanque
					* TolEstanque * Math.pow(10, -6);

			KilosPeso = KilosPeso + Cantidad;
			KilosPactiva = KilosPactiva + Cantidad;
			// 'Planchas 1f
		case 3:
			Cantidad = dis.getFase()
					* 2
					* (nucFerr.getEntreCentroNucleo())
					* (nucFerr.getChapaMayorNucleo() + nucFerr
							.getApilamientoChapas()) * 2 * DenEstanque
					* TolEstanque * Math.pow(10, -6);
			Detax = "cajet�n";
			Parte2 = "PAC";
			FleXG = "APA";
			sAcumCosto("9008001010");
			KilosPeso = KilosPeso + Cantidad;
			KilosPactiva = KilosPactiva + Cantidad;
		}

		// ' Cinta Poliglas
		if (dis.getFase() != 1 && dis.getPotencia() > 700) {
			if (nucFerr.getDiametroNucleo() == 0) {
				Cantidad = 3
						* dis.getFase()
						* 2
						* (nucFerr.getChapaMayorNucleo() + nucFerr
								.getApilamientoChapas())
						* nucFerr.getVentanaNucleo() / 150 * 0.001;
			} else {
				Cantidad = 3 * dis.getFase() * 3.14
						* nucFerr.getDiametroNucleo()
						* nucFerr.getVentanaNucleo() / 200 * 0.001;
			}
			Parte2 = "NTE";
			FleXG = "NUC";
			sAcumCosto("9007002001");
		}

		// ' Madera Trupan aprete nucleo
		Cantidad = (dis.getFase() - 1) * 0.08
				* Math.sqrt(dis.getPotencia() / 1000);
		Parte2 = "NTE";
		FleXG = "NUC";
		sAcumCosto("9001006010");
		KilosPeso = KilosPeso + Cantidad;
		KilosPactiva = KilosPactiva + Cantidad;
	}

	private void sEstanqueyTapa() throws Exception {
		short poteciax = 0;
		// '============================================================================
		// ' Calculo Estanque
		// '============================================================================
		if (dis.getIdDistribucion() != -1) {
			KilosEstanque = 0;

			// 'tapa
			Cantidad = 1.05 * estTapa.getLargoEstanque()
					* estTapa.getAnchoEstanque() * estTapa.getEspTapa()
					* TolEstanque * DenEstanque * Math.pow(10, -6);
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			Parte2 = "KTC";
			FleXG = "TAP";
			Detax = "tapa";
			sAcumCosto(estTapa.getCodTapa());
			// 'manto
			Cantidad = 1.05 * 2
					* (estTapa.getLargoEstanque() + estTapa.getAnchoEstanque())
					* (estTapa.getAltoEstanque() + estTapa.getAltoBaseFalsa())
					* estTapa.getEspManto() * TolEstanque * DenEstanque
					* Math.pow(10, -6);
			KilosEstanque = Cantidad + KilosEstanque;
			KilosPeso = Cantidad + KilosPeso;
			Parte2 = "KEP";
			FleXG = "EST";
			// Detax = "manto"
			sAcumCosto(estTapa.getCodManto());
			// 'fondo
			// ' Cantidad = 1.05 * .LargoEstanque * .AnchoEstanque * .espfondo _
			// ' * TolEstanque * DenEstanque * 10 ^ -6
			// ' KilosEstanque = Cantidad + KilosEstanque
			// ' KilosPeso = Cantidad + KilosPeso

			double seccionPerfilbase = 0;

			if (tipoXX == -2) {
				if (PotenciaX >= 0 && PotenciaX <= 100) {
					seccionPerfilbase = 1250 / 5 * estTapa.getEspFondo();
				}
				if (PotenciaX >= 101 && PotenciaX <= 499) {
					seccionPerfilbase = 1250 / 5 * estTapa.getEspFondo();
				}
				if (PotenciaX >= 500 && PotenciaX <= 100000) {
					seccionPerfilbase = 1000 / 5 * estTapa.getEspFondo();
				}

				Cantidad = 2.1
						* (estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque()) * seccionPerfilbase
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				KilosEstanque = Cantidad + KilosEstanque;
				KilosPeso = Cantidad + KilosPeso;
				Parte2 = "KEP";
				FleXG = "EST";
				// Detax = "fondo"
				sAcumCosto(estTapa.getCodFondo());
			} else {
				Cantidad = 1.05 * estTapa.getLargoEstanque()
						* estTapa.getAnchoEstanque() * estTapa.getEspFondo()
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				KilosEstanque = Cantidad + KilosEstanque;
				KilosPeso = Cantidad + KilosPeso;
				Parte2 = "KEP";
				FleXG = "EST";
				// Detax = "fondo"
				sAcumCosto(estTapa.getCodFondo());
			}
			// ' Malla Base
			if (tipoXX == -2) {
				Cantidad = 1.05
						* (estTapa.getLargoEstanque() * estTapa
								.getAnchoEstanque()) / 1000 / 1000 / 6.2;
				Parte2 = "KEP";
				FleXG = "EST";
				// Detax = "Malla Base"
				sAcumCosto("9008004010");
			}
			// ' Malla Lateral
			if (poteciax >= 500 && tipoXX == -2) {
				Cantidad = 1.05
						* 2
						* 0.3
						* (estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque())
						* (estTapa.getAltoEstanque()) / 1000 / 1000 / 6.2;
				Parte2 = "KEP";
				FleXG = "EST";
				// Detax = "Malla Lateral"
				sAcumCosto("9008004010");
			}
			// ' Perfil Soporte
			if (poteciax >= 500) {
				seccionPerfilbase = 1600;
				Cantidad = 2.1 * estTapa.getAnchoEstanque() * seccionPerfilbase
						* TolEstanque * DenEstanque * Math.pow(10, -6);
				// 'KilosEstanque = Cantidad + KilosEstanque
				// 'KilosPeso = Cantidad + KilosPeso
				Parte2 = "KEP";
				FleXG = "EST";
				// Detax = "Perfil Soporte"
				sAcumCosto("9008001070");
				// ' Ferreteria de Anclaje
				Cantidad = 4;
				// 'Caluga
				Parte2 = "PAC";
				FleXG = "APA";
				// Detax = "Ferreteria Soporte PA"
				sAcumCosto("9008004030");
				sAcumCosto("9011003020");
				sAcumCosto("9011001020");
			} else {
				Cantidad = 1.05 * 2 * estTapa.getLargoEstanque() / 1000;
				// 'KilosEstanque = Cantidad + KilosEstanque
				// 'KilosPeso = Cantidad + KilosPeso
				Parte2 = "KEP";
				FleXG = "EST";
				sAcumCosto("9008004020");
				// ' Ferreteria de Anclaje
				Cantidad = 4;
				Parte2 = "PAC";
				FleXG = "APA";
				// Detax = "Ferreteria Soporte PA"
				sAcumCosto("9011003020");
				sAcumCosto("9011001020");
				sAcumCosto("9008004030");
			}

			// ' sAcumCosto 400

			/*
			 * '==========================================================================
			 * ' Calculo superficie pintura
			 * '==========================================================================
			 */
			Parte2 = "EST";
			FleXG = "EST";

			double SuperficieRadiador = 0;
			String Area = "";

			// ' Superficie de estanque (para pintura)
			double Supext, Supint;
			if (estTapa.getDiametroCircular() > 0) {
				Supext = 3.14
						* estTapa.getDiametroCircular()
						* (estTapa.getAltoEstanque()
								+ estTapa.getAltoBaseFalsa() * 2 + estTapa
								.getDiametroCircular() / 2) * Math.pow(10, -6);
				Supint = 3.14
						* estTapa.getDiametroCircular()
						* (estTapa.getAltoEstanque() + estTapa
								.getDiametroCircular() / 4) * Math.pow(10, -6);
			} else {
				Supext = 2
						* ((estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque())
								* (estTapa.getAltoEstanque() + 2 * estTapa
										.getAltoBaseFalsa()) + estTapa
								.getLargoEstanque()
								* estTapa.getAnchoEstanque())
						* Math.pow(10, -6);
				Supint = 2
						* ((estTapa.getLargoEstanque() + estTapa
								.getAnchoEstanque())
								* (estTapa.getAltoEstanque()) + estTapa
								.getLargoEstanque()
								* estTapa.getAnchoEstanque())
						* Math.pow(10, -6);
			}

			// ' Pintura estanque/radiador
			// ItemCubicacion it1 = isus.getItemCubicacion(estTapa.getPin1());
			// if (it1.getUnidad().equals("KG")) {
			Auxiliar auxPin1 = scha.detalleMaterialesSchaffner(estTapa
					.getPin1());
			if (auxPin1.getAux11() != null
					&& auxPin1.getAux11().trim().equals("KG")) {
				Cantidad = (Supint) / 7;
			} else {
				Cantidad = 2 * (Supint) / 18 / 1.25;
			}
			if (!estTapa.getPin1().trim().equals("")
					&& !estTapa.getPin1().trim().equals("0")) {
				Area = "1";
				// Detax = "interior"
				sAcumCosto(estTapa.getPin1());
			}
			// '==========================
			// ItemCubicacion it2 = isus.getItemCubicacion(estTapa.getPin2());
			// if (it2.getUnidad().trim().equals("KG")) {
			Auxiliar auxPin2 = scha.detalleMaterialesSchaffner(estTapa
					.getPin2());
			if (auxPin2.getAux11() != null
					&& auxPin2.getAux11().trim().equals("KG")) {
				Cantidad = (Supint) / 7;
			} else {
				Cantidad = 2 * (Supint) / 18 / 1.25;
			}
			if (!estTapa.getPin2().trim().equals("")
					&& !estTapa.getPin2().trim().equals("0")) {
				Area = "1";
				// Detax = "interior"
				sAcumCosto(estTapa.getPin2());
			}
			// ItemCubicacion it3 = isus.getItemCubicacion(estTapa.getPin3());
			// if (it3.getUnidad().trim().equals("KG")) {
			Auxiliar auxPin3 = scha.detalleMaterialesSchaffner(estTapa
					.getPin3());
			if (auxPin3.getAux11() != null
					&& auxPin3.getAux11().trim().equals("KG")) {
				Cantidad = (Supext + SuperficieRadiador) / 7;
			} else {
				Cantidad = 2 * (Supext + SuperficieRadiador) / 18 / 1.25;
			}
			if (!estTapa.getPin3().trim().equals("")
					&& !estTapa.getPin3().trim().equals("0")) {
				Area = "1";
				// Detax = "exterior"
				sAcumCosto(estTapa.getPin3());
				Parte2 = "TTE";
				FleXG = "LTX";
				sAcumCosto(estTapa.getPin3());
				Parte2 = "EST";
				FleXG = "EST";
			}
			// ItemCubicacion it4 = isus.getItemCubicacion(estTapa.getPin4());
			// if (it4.getUnidad().trim().equals("KG")) {
			Auxiliar auxPin4 = scha.detalleMaterialesSchaffner(estTapa
					.getPin4());
			if (auxPin4.getAux11() != null
					&& auxPin4.getAux11().trim().equals("KG")) {
				Cantidad = (Supext + SuperficieRadiador) / 7;
			} else {
				Cantidad = 2 * (Supext + SuperficieRadiador) / 18 / 1.25;
			}
			if (!estTapa.getPin4().trim().equals("")
					&& !estTapa.getPin4().trim().equals("0")) {
				Area = "1";
				// Detax = "exterior"
				sAcumCosto(estTapa.getPin4());
			}

			// ' Pernos
			if (dis.getIdDistribucion() == -2) {
				Cantidad = ((int) (estTapa.getAltoEstanque() / 315) + 1) * 4;
				Cantidad = Cantidad
						+ ((int) (estTapa.getLargoEstanque() / 315) - 1) * 6;
				Cantidad = Cantidad
						+ ((int) (estTapa.getAnchoEstanque() / 315) + 1) * 2;
			} else {
				Cantidad = ((int) ((estTapa.getAltoEstanque() - 170) / 130) + 1) * 4;
				Cantidad = Cantidad
						+ ((int) (estTapa.getLargoEstanque() / 135) - 1) * 2;
				Cantidad = Cantidad
						+ ((int) (estTapa.getAnchoEstanque() / 101) + 1) * 4;
			}
			Cantidad = (int) Cantidad;
			if (dis.getPotencia() <= 500) {
				Parte2 = "PAC";
				FleXG = "APA";
				sAcumCosto("9011001010");
				sAcumCosto("9011004010");
				sAcumCosto("9011002010");
				Cantidad = Cantidad * 2;
				sAcumCosto("9011003010");
			} else {
				Parte2 = "PAC";
				FleXG = "APA";
				sAcumCosto("9011001020");
				sAcumCosto("9011004020");
				sAcumCosto("9011002020");
				Cantidad = Cantidad * 2;
				sAcumCosto("9011003020");
			}
			// ' Soldadura
			Cantidad = KilosEstanque * 0.05 * 0.7;
			Parte2 = "KEP";
			FleXG = "EST";
			// Detax = "MIG";
			sAcumCosto("9006001030");

			// 'Cantidad = KilosEstanque * 0.05 * 0.3
			// 'Detax = "TIG"
			// 'sAcumCosto "9006001040"
		}
	}

	private void sAislaciones() throws Exception {
		double largocable = dis.getFase()
				* (estTapa.getAltoEstanque() - nucFerr.getVentanaNucleo() - 2 * nucFerr
						.getChapaMayorNucleo());

		double cobrrre = 0;
		if (dis.getCodCondBat() == CIRCULAR) {
			largocable = Math.pow(bat.getDimensionAxial(), 2) * 3.1416 / 4
					* largocable;
		} else {
			largocable = bat.getDimensionAxial() * bat.getDimensionRadial()
					* largocable;
		}
		cubicaCupp(bat.getCodigoCondCu());
		Cantidad = largocable * DenSi * Math.pow(10, -6);
		cobrrre = Cantidad;
		// Detax = "salida aislador AT"
		sAcumCosto(bat.getCodigoCondCu());
		KilosPeso = KilosPeso + Cantidad;
		KilosPactiva = KilosPactiva + Cantidad;

		// ' CU Terminal BT
		double secc = 0;
		if (dis.getCodCondBbt() == CIRCULAR) {
			secc = Math.pow(bbt.getDimensionAxial(), 2) * 3.1416 / 4;
		} else {
			secc = bbt.getDimensionAxial() * bbt.getDimensionRadial();
		}
		cubicaCupp(bbt.getCodigoCondCu());
		Cantidad = secc
				* 2
				* DenSi
				* bbt.getCondParalelo()
				* dis.getFase()
				* (estTapa.getAltoEstanque() - 2 * nucFerr
						.getChapaMayorNucleo()) * Math.pow(10, -6);
		cobrrre = Cantidad + cobrrre;
		// Detax = "salida aislador BT"
		sAcumCosto(bbt.getCodigoCondCu());
		KilosPeso = KilosPeso + Cantidad;
		KilosPactiva = KilosPactiva + Cantidad;

		/*
		 * '============================================================================
		 * ' Soldaduras
		 * '============================================================================
		 */

		// ' Soldadura de plata
		Cantidad = cobrrre * 0.01;
		sAcumCosto("9006001020");

		// ' Soldadura esta�o
		Cantidad = cobrrre * 0.1;
		sAcumCosto("9006001010");

		// ' Spaguetti papel crepe AT
		if (bat.getCondAxiales() == 2) {
			Cantidad = largocable * 0.001;
			// ' sAcumCosto 650
		} else {
			if (bat.getCondAxiales() == 1) {
				if ((bat.getDimensionAxial() + bat.getDimensionRadial()) < 15) {
					Cantidad = largocable * 0.001;
					// ' sAcumCosto 650
				} else {
					Cantidad = largocable * 0.001;
					// ' sAcumCosto 650
				}
			}
		}
	}

	/* DOCUMENTOS APLICABLES */

	public int eliminarPermiso(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.eliminarPermiso(aux);
	}
	
	public int actualizarDocAplicableEnc(DocAplicableEnc enc) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarDocAplicableEnc(enc);
	}
	
	public int actualizarDocAplicableDet(DocAplicableDet det) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarDocAplicableDet(det);
	}
	
	public int actualizarEstadoDocApli(int num) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarEstadoDocApli(num);
	}
	
	public int actualizarRevisionPlanoRev(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarRevisionPlanoRev(aux);
	}
	
	public int actualizarEstadoDocApliDetRev(DocAplicableDet idDet) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarEstadoDocApliDetRev(idDet);
	}
	
	public int actualizarRevisionPlanoDet(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.actualizarRevisionPlanoDet(aux);
	}
	
	public int insertPermiso(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertPermiso(aux);
	}
	
	public int insertPlanoDescripcion(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertPlanoDescripcion(aux);
	}

	public int insertArchivoDocApli(CargarArchivo car) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertArchivoDocApli(car);
	}

	public int insertDocAplicableEnc(DocAplicableEnc enc) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertDocAplicableEnc(enc);
	}

	public int insertDocAplicableDet(DocAplicableDet det) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertDocAplicableDet(det);
	}
	
	public int insertDocApliDetRev(DocAplicableDet det) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.insertDocApliDetRev(det);
	}
	
	public List<Auxiliar> getPlanoDescripciones() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getPlanoDescripciones();
	}

	public int maxNumDocAplicable() throws Exception {
		if (((IIngenieriaDAO) context.getBean("ingenieriaDao"))
				.maxNumDocAplicable() != null) {
			return ((IIngenieriaDAO) context.getBean("ingenieriaDao"))
					.maxNumDocAplicable() + 1;
		} else
			return new Integer(1);
	}

	public List<Auxiliar> getIdPlanos(String id) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getIdPlanos(id);
	}

	public List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDocAplicableDet(aux);
	}

	public DocAplicableEnc getDocAplicableEnc(int num) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDocAplicableEnc(num);
	}

	public int docAplicableXnum(int num) throws Exception {
		if (((IIngenieriaDAO) context.getBean("ingenieriaDao"))
				.docAplicableXnum(num) != null) {
			return ((IIngenieriaDAO) context.getBean("ingenieriaDao"))
					.docAplicableXnum(num);
		} else {
			return 0;
		}
	}
	
	public List<DocAplicableEnc> getDocsAplicablesEnc(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDocsAplicablesEnc(aux);
	}
	
	public DocAplicableDet getDocApliDet(int num) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDocApliDet(num);
	}
	
	public int getDocApliDetRevCount(int num) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDocApliDetRevCount(num);
	}
	
	public CargarArchivo docApliDetArchivo(Auxiliar aux) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.docApliDetArchivo(aux);
	}
	
	public List<Usuario> geUsuariosXarea(int area) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.geUsuariosXarea(area);
	}
	
	public List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.gePermisosXareaXusuario(groupPrincipal);
	}
	
	public List<Auxiliar> geSubGroupPrincipal(int groupPrincipal) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.geSubGroupPrincipal(groupPrincipal);
	}
	
	public int getPlanoRevision(String idPlano) throws Exception {
		Integer a = ((IIngenieriaDAO) context.getBean("ingenieriaDao")).getPlanoRevision(idPlano);
		if(a == null){
			return 0;
		}else{
			return a+1;
		}
	}
	
	public Integer getLastPlanoRevision(String idPlano) throws Exception {
		return ((IIngenieriaDAO) context.getBean("ingenieriaDao")).getLastPlanoRevision(idPlano);
	}
	
	public List<Auxiliar> getEquiposVigentes() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEquiposVigentes();
	}
	
	public List<Auxiliar> getEquiposModificarRev(String idPlano) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEquiposModificarRev(idPlano);
	}
	
	public List<Auxiliar> getEquiposVigentesTpmp() throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEquiposVigentesTpmp();
	}
	
	public int getLastDocApliDet() throws Exception {
		Integer a = ((IIngenieriaDAO) context.getBean("ingenieriaDao")).getLastDocApliDet();
		if(a == null){
			return 1;
		}else{
			return a+1;
		}
	}
	
	public int getMaxPlanoRevision(String idPlano) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getMaxPlanoRevision(idPlano);
	}
	
	public List<Auxiliar> getDetPlanoActualizar(String idPlano) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getDetPlanoActualizar(idPlano);
	}
	
	public List<Auxiliar> getEnviarMailCambioPlano(int idDet) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEnviarMailCambioPlano(idDet);
	}
	
	public List<Auxiliar> getEnviarMailRevision(int idDetRev) throws Exception {
		IIngenieriaDAO ing = (IIngenieriaDAO) context.getBean("ingenieriaDao");
		return ing.getEnviarMailRevision(idDetRev);
	}
}
