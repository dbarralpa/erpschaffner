package com.schaffner.core.servicio;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.schaffner.infraestructure.data.repositories.contracts.IComercialDAO;
import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.ProyectoComercialTransformador;
import com.schaffner.modelo.ProyectoComercialVentaDet;
import com.schaffner.modelo.ProyectoComercialVentaEnc;


public class ComercialServicio implements IComercialServicio {
	private ApplicationContext context;

	public ComercialServicio() {
		context = new ClassPathXmlApplicationContext(
				"classpath:com/schaffner/persistencia/beans/comercial.xml");
	}

	public int updateProyectoVenta(ProyectoComercialVentaEnc pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.updateProyectoVenta(pro);
	}
	
	public int insertProyectoVenta(ProyectoComercialVentaEnc pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProyectoVenta(pro);
	}
	
	public int insertProyectoVentaDet(ProyectoComercialVentaDet pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProyectoVentaDet(pro);
	}
	
	
	public int insertProyectoVentaArchivos(CargarArchivo arch)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProyectoVentaArchivos(arch);
	}
	
	public int insertProyectoVentaTrafos(ProyectoComercialTransformador pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProyectoVentaTrafos(pro);
	}
	
	public int insertProgresoProyecto(Auxiliar pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProgresoProyecto(pro);
	}

	public int insertProyectoEncDetTrafo(ProyectoComercialVentaEnc enc, ProyectoComercialVentaDet det, ProyectoComercialTransformador trafo)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProyectoEncDetTrafo(enc, det, trafo);
	}
	
	public int insertProyectoDetTrafo(ProyectoComercialVentaDet det, ProyectoComercialTransformador trafo)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.insertProyectoDetTrafo(det, trafo);
	}
	
	public int updateProyectoVentaDet(ProyectoComercialVentaDet pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.updateProyectoVentaDet(pro);
	}
	
	public int updateProyectoVentaEncDatosGenerales(ProyectoComercialVentaEnc pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.updateProyectoVentaEncDatosGenerales(pro);
	}
	
	public int updateProyectoVentaDetTrafo(ProyectoComercialTransformador tra,ProyectoComercialVentaDet pro)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.updateProyectoVentaDetTrafo(tra,pro);
	}
	
	
	public int deleteProyectoVentaDet(int idProyecto)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.deleteProyectoVentaDet(idProyecto);
	}
	
	public int deleteProyectoVentaTrafo(int idProyectoDet)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.deleteProyectoVentaTrafo(idProyectoDet);
	}
	
	public int deleteProyectoDetTrafo(int idProDet)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.deleteProyectoDetTrafo(idProDet);
	}

	public int ultimoNumeroProyectoVenta() throws Exception {
		if (((IComercialDAO) context.getBean("comercialDao"))
				.ultimoNumeroProyectoVenta() != null) {
			return (Integer) ((IComercialDAO) context.getBean("comercialDao"))
					.ultimoNumeroProyectoVenta();
		} else {
			return new Integer(0);
		}
	}
	
	public int ultimoNumeroProyectoVentaDet() throws Exception {
		if (((IComercialDAO) context.getBean("comercialDao"))
				.ultimoNumeroProyectoVentaDet() != null) {
			return (Integer) ((IComercialDAO) context.getBean("comercialDao"))
					.ultimoNumeroProyectoVentaDet();
		} else {
			return new Integer(0);
		}
	}
	
	public List<ProyectoComercialVentaEnc> proyectosVentas() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.proyectosVentas();
	}
	
	public List<Auxiliar> tipoCotizacion() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.tipoCotizacion();
	}
	
	public List<Auxiliar> tipoMoneda() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.tipoMoneda();
	}
	
	public List<Auxiliar> formaPago() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.formaPago();
	}
	
	public List<Auxiliar> estadoCotizacion() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.estadoCotizacion();
	}
	
	public List<Auxiliar> parametros() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.parametros();
	}
	
	public List<Auxiliar> margenes() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.margenes();
	}
	
	public List<ProyectoComercialVentaDet> proyectosVentasDet(int id) throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.proyectosVentasDet(id);
	}
	
	public List<ProyectoComercialTransformador> proyectosVentasTrafos(int id) throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.proyectosVentasTrafos(id);
	}
	
	public List<CargarArchivo> proyectosVentasArchivos(int id) throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.proyectosVentasArchivos(id);
	}
	
	public List<Auxiliar> normas() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.normas();
	}
	
	public List<Auxiliar> temPerdidas() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.temPerdidas();
	}
	
	public List<Auxiliar> ventilacion() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.ventilacion();
	}
	
	public List<Auxiliar> temperaturas() throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.temperaturas();
	}
	
	public List<Auxiliar> progresoProyecto(int id) throws Exception{
		IComercialDAO pro = (IComercialDAO) context
				.getBean("comercialDao");
		return pro.progresoProyecto(id);
	}
	
	public int idProyectoEncExiste(int id)
			throws Exception {
		return ((IComercialDAO) context.getBean("comercialDao"))
				.idProyectoEncExiste(id);
	}

}
