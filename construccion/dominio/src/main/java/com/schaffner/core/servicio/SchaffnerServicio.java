package com.schaffner.core.servicio;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.schaffner.infraestructure.data.repositories.contracts.ISchaffnerDAO;
import com.schaffner.modelo.Auxiliar;


public class SchaffnerServicio implements ISchaffnerServicio {
	private ApplicationContext context;

	public SchaffnerServicio(){
		context = new ClassPathXmlApplicationContext(
				"classpath:com/schaffner/persistencia/beans/schaffner.xml");
	}
	
	
	public Auxiliar detalleMaterialesSchaffner(String codigo) throws Exception {
		ISchaffnerDAO scha = (ISchaffnerDAO) context.getBean("schaffnerDAO");
		return scha.detalleMaterialesSchaffner(codigo);
	}
}
