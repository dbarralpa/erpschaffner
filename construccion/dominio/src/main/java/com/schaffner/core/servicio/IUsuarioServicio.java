package com.schaffner.core.servicio;

import com.schaffner.modelo.Usuario;

public interface IUsuarioServicio {
	Usuario getUsuario(String userName) throws Exception;
}