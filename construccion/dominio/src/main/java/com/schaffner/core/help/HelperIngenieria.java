package com.schaffner.core.help;

import java.util.ArrayList;

import com.schaffner.modelo.Auxiliar;

public class HelperIngenieria {

	public static String descripcion(ArrayList<Auxiliar> aux, String codigo){
		String descri = "";
		for(int i=0;i<aux.size();i++){
			if(codigo.equals(aux.get(i).getAux9())){
				descri = aux.get(i).getAux10();
				break;
			}
		}
		return descri;
	}
	
	public static String um(ArrayList<Auxiliar> aux, String codigo){
		String um = "";
		for(int i=0;i<aux.size();i++){
			if(codigo.equals(aux.get(i).getAux9())){
				um = aux.get(i).getAux11();
				break;
			}
		}
		return um;
	}
}
