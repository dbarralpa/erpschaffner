package com.schaffner.core.servicio;

import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.Usuario;
import com.schaffner.modelo.ingenieria.Accesorio;
import com.schaffner.modelo.ingenieria.Bobina;
import com.schaffner.modelo.ingenieria.Costo;
import com.schaffner.modelo.ingenieria.Derivacion;
import com.schaffner.modelo.ingenieria.Diseno;
import com.schaffner.modelo.ingenieria.Distribucion;
import com.schaffner.modelo.ingenieria.DocAplicableDet;
import com.schaffner.modelo.ingenieria.DocAplicableEnc;
import com.schaffner.modelo.ingenieria.Especificacion;
import com.schaffner.modelo.ingenieria.EstanqueTapa;
import com.schaffner.modelo.ingenieria.ItemCubicacion;
import com.schaffner.modelo.ingenieria.NucleoFerreteria;
import com.schaffner.modelo.ingenieria.Presspan;
import com.schaffner.modelo.ingenieria.Radiador;
import com.schaffner.modelo.ingenieria.Refrigerante;
import com.schaffner.modelo.ingenieria.TipoAislador;
import com.schaffner.modelo.ingenieria.Tolerancia;
/*CUBICA*/
public interface IIngenieriaServicio {
	int insertSeco(List<Accesorio> accesorio,Diseno diseno,Especificacion especificacion,NucleoFerreteria nucFerr,EstanqueTapa estTapa,
		       Bobina bobinabbt,Bobina bobinabat,Bobina bobinabte,Presspan presspanBbt,Presspan presspanBat,Presspan presspanBte
		       ) throws Exception;
	int insertAceite(List<Accesorio> accesorios,Diseno diseno,Especificacion especificacion,NucleoFerreteria nucFerr,EstanqueTapa estTapa,
		       Bobina bobinabbt,Bobina bobinabat,Bobina bobinabte,Presspan presspanBbt,Presspan presspanBat,Presspan presspanBte,Radiador rad
		       ) throws Exception;
	int actualizarAccesorios(Accesorio acce) throws Exception;
	int updateSeco(Diseno diseno,Especificacion especificacion,NucleoFerreteria nucFerr,EstanqueTapa estTapa,
		       Bobina bobinabbt,Bobina bobinabat,Bobina bobinabte,Presspan presspanBbt,Presspan presspanBat,Presspan presspanBte) throws Exception;
	int updateAceite(Diseno diseno,Especificacion especificacion,NucleoFerreteria nucFerr,EstanqueTapa estTapa,
		       Bobina bobinabbt,Bobina bobinabat,Bobina bobinabte,Presspan presspanBbt,Presspan presspanBat,Presspan presspanBte, Radiador rad) throws Exception;
	int actualizarPlano(CargarArchivo arch) throws Exception;
	int deleteAccesorio(Accesorio accesorio) throws Exception;
	Diseno getDiseno(String diseno) throws Exception;
	Bobina getBobina(Auxiliar aux) throws Exception;
	NucleoFerreteria getNucleoFerreteria(String diseno) throws Exception;
	Especificacion getEspecificacion(String diseno) throws Exception;
	Presspan getPresspan(Auxiliar aux) throws Exception;
	Tolerancia  getTolerancia(int id) throws Exception;
	ItemCubicacion getItemCubicacion(String item) throws Exception;
	EstanqueTapa getEstanqueTapa(String diseno) throws Exception;
	Radiador getRadiador(String diseno) throws Exception;
	List<Accesorio> getAccesorios(String diseno) throws Exception;
	List<ItemCubicacion> getItemAdicionales(double potencia) throws Exception;
	List<Distribucion> getDistribuciones(String tipo) throws Exception;
	List<TipoAislador> getTipoAisladores() throws Exception;
	List<Refrigerante> getRefrigerantes() throws Exception;
	List<Derivacion> getDerivaciones() throws Exception;
	List<Auxiliar> getMaterialesXclasificacionAceite() throws Exception;
	List<Auxiliar> getMaterialesXclasificacionSeco() throws Exception;
	List<Auxiliar> getMaterialesPintura() throws Exception;
	List<Auxiliar> getMaterialesPlanchaFierro() throws Exception;
	List<Auxiliar> getMaterialesPresspan() throws Exception;
	List<Auxiliar> getTiposRadiadores() throws Exception;
	List<Auxiliar> getMaterialesRadiador(int tipo) throws Exception;
	List<Auxiliar> getAceroSilicoso() throws Exception;
	List<Auxiliar> getTipoSoporte() throws Exception;
	List<Auxiliar> getTipoNucleo() throws Exception;
	List<Auxiliar> getAislaciones() throws Exception;
	List<Auxiliar> getDisenoBusqueda() throws Exception;
	List<Distribucion> getDistribucionesAll() throws Exception;
	List<Auxiliar> getTablaC1() throws Exception;
	List<Auxiliar> getTablaC2() throws Exception;
	List<Auxiliar> getTablaC3() throws Exception;
	List<Auxiliar> getTablaC4() throws Exception;
	int getMaxCodigoDiseno(String codigo) throws Exception;
	List<Auxiliar> getCodigosPrueba() throws Exception;
	List<Costo> cubicacion(String diseno) throws Exception;
	
	/*DOCUMENTOS APLICABLES*/
	int eliminarPermiso(Auxiliar aux) throws Exception;
	int actualizarDocAplicableEnc(DocAplicableEnc enc) throws Exception;
	int actualizarDocAplicableDet(DocAplicableDet det) throws Exception;
	int actualizarEstadoDocApli(int num) throws Exception;
	int actualizarRevisionPlanoRev(Auxiliar aux) throws Exception;
	int actualizarRevisionPlanoDet(Auxiliar aux) throws Exception;
	int actualizarEstadoDocApliDetRev(DocAplicableDet num) throws Exception;
	int insertPermiso(Auxiliar aux) throws Exception;
	int insertPlanoDescripcion(Auxiliar aux) throws Exception;
	int insertArchivoDocApli(CargarArchivo car) throws Exception;
	int insertDocAplicableEnc(DocAplicableEnc enc) throws Exception;
	int insertDocAplicableDet(DocAplicableDet det) throws Exception;
	int insertDocApliDetRev(DocAplicableDet det) throws Exception;
	List<Auxiliar> getPlanoDescripciones() throws Exception;
	int maxNumDocAplicable() throws Exception;
	List<Auxiliar> getIdPlanos(String id) throws Exception;
	List<DocAplicableDet> getDocAplicableDet(int aux) throws Exception;
	DocAplicableEnc getDocAplicableEnc(int numero) throws Exception;
	int docAplicableXnum(int num) throws Exception;
	List<DocAplicableEnc> getDocsAplicablesEnc(Auxiliar aux) throws Exception;
	DocAplicableDet getDocApliDet(int numero) throws Exception;
	int getDocApliDetRevCount(int id_doc) throws Exception;
	CargarArchivo docApliDetArchivo(Auxiliar aux) throws Exception;
	List<Usuario> geUsuariosXarea(int area) throws Exception;
	List<Auxiliar> gePermisosXareaXusuario(int groupPrincipal) throws Exception;
	List<Auxiliar> geSubGroupPrincipal(int subGroupPrincipal) throws Exception;
	int getPlanoRevision(String idPlano) throws Exception;
	Integer getLastPlanoRevision(String idPlano) throws Exception;
	List<Auxiliar> getEquiposVigentes() throws Exception;
	List<Auxiliar> getEquiposModificarRev(String idPlano) throws Exception;
	List<Auxiliar> getEquiposVigentesTpmp() throws Exception;
	int getLastDocApliDet() throws Exception;
	int getMaxPlanoRevision(String idPlano) throws Exception;
	List<Auxiliar> getDetPlanoActualizar(String idPlano) throws Exception;
	List<Auxiliar> getEnviarMailCambioPlano(int idDet) throws Exception;
	List<Auxiliar> getEnviarMailRevision(int idDetRev) throws Exception;
}
