package com.schaffner.core.servicio;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.schaffner.infraestructure.data.repositories.contracts.IUsuarioDAO;
import com.schaffner.modelo.Usuario;



public class UsuarioServicio implements IUsuarioServicio {
	private ApplicationContext context;

	public UsuarioServicio() {
		context = new ClassPathXmlApplicationContext(
				"classpath:com/schaffner/persistencia/beans/usuario.xml");
	}

	public Usuario getUsuario(String userName) throws Exception{
		IUsuarioDAO usuario = (IUsuarioDAO) context
				.getBean("usuarioDao");
		return usuario.getUsuario(userName);
/*	public Usuario getUsuarioByUsuario(String userName) {
		IUsuarioDAO usuario = (IUsuarioDAO) context
				.getBean("usuarioDao");
		return usuario.getUsuarioByUsuario(userName);
		*/

	}

}
