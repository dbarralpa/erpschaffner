package com.schaffner.core.servicio;

import java.util.List;

import com.schaffner.modelo.Auxiliar;
import com.schaffner.modelo.CargarArchivo;
import com.schaffner.modelo.ProyectoComercialTransformador;
import com.schaffner.modelo.ProyectoComercialVentaDet;
import com.schaffner.modelo.ProyectoComercialVentaEnc;

public interface IComercialServicio {
	int insertProyectoVenta(ProyectoComercialVentaEnc pro) throws Exception;
	int insertProyectoVentaDet(ProyectoComercialVentaDet pro) throws Exception;
	int insertProyectoVentaArchivos(CargarArchivo arch) throws Exception;
	int insertProyectoVentaTrafos(ProyectoComercialTransformador trafos) throws Exception;
	int insertProgresoProyecto(Auxiliar pro) throws Exception;
	int insertProyectoEncDetTrafo(ProyectoComercialVentaEnc pro, ProyectoComercialVentaDet det, ProyectoComercialTransformador trafo) throws Exception;
	int insertProyectoDetTrafo(ProyectoComercialVentaDet det, ProyectoComercialTransformador trafo) throws Exception;
	int updateProyectoVentaDet(ProyectoComercialVentaDet pro) throws Exception;
	int updateProyectoVentaEncDatosGenerales(ProyectoComercialVentaEnc pro) throws Exception;
	int updateProyectoVentaDetTrafo(ProyectoComercialTransformador tra,ProyectoComercialVentaDet det) throws Exception;
	int deleteProyectoVentaDet(int idProyecto) throws Exception;
	int deleteProyectoVentaTrafo(int idProyectoDet) throws Exception;
	int deleteProyectoDetTrafo(int idTrafo) throws Exception;
	int ultimoNumeroProyectoVenta() throws Exception;
	int ultimoNumeroProyectoVentaDet() throws Exception;
	List<ProyectoComercialVentaEnc> proyectosVentas() throws Exception;
	List<Auxiliar> tipoCotizacion() throws Exception;
	List<Auxiliar> tipoMoneda() throws Exception;
	List<Auxiliar> formaPago() throws Exception;
	List<Auxiliar> estadoCotizacion() throws Exception;
	int updateProyectoVenta(ProyectoComercialVentaEnc pro) throws Exception;
	List<Auxiliar> parametros() throws Exception;
	List<Auxiliar> margenes() throws Exception;
	List<ProyectoComercialVentaDet> proyectosVentasDet(int id) throws Exception;
	List<ProyectoComercialTransformador> proyectosVentasTrafos(int id) throws Exception;
	List<CargarArchivo> proyectosVentasArchivos(int id) throws Exception;
	List<Auxiliar> normas() throws Exception;
	List<Auxiliar> temPerdidas() throws Exception;
	List<Auxiliar> ventilacion() throws Exception;
	List<Auxiliar> temperaturas() throws Exception;
	List<Auxiliar> progresoProyecto(int id) throws Exception;
	int idProyectoEncExiste(int id) throws Exception;
}